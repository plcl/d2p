#include<mpi.h>
#include<sys/time.h>
#include "HelperFunctions.h"

int procRank;
int totalProcs;
//int argcCopy;
//char** argvCopy;
long gPreprocessTime;
long gTaskExecutionTime;
//void MakeCLACopy(int argc, char** argv);


#ifdef IDLE_TIME
#include<limits>
#include<math.h>
#define MAX_NUM_NODES 8
#define CORES_PER_NODE 16
extern double idleTime;
extern int numMsgsRecvd;
double GetGeoMean(const double* data, int numElems);
double GetVariance(const double* data, int numElems);
double GetStddev(const double* data, int numElems);
#endif

int lNodewiseMsgLog[64], gNodewiseMsgLog[64];
#ifdef PAPI
#include"papi.h"
int papiStat, eventSet=PAPI_NULL;	
long long values[2] = {(long long)0, (long long)0};
inline void handle_error(int retval)
{
	printf("PAPI error %d: %s\n",retval, PAPI_strerror(retval));
	exit(1);
}
#endif


int main(int argc, char* argv[])
{
#ifdef PAPI
	int retval, EventSet=PAPI_NULL;
	long long values[4] = {(long long)0, (long long)0, (long long)0, (long long)0};

	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if(retval != PAPI_VER_CURRENT)
		handle_error(retval);
	
	retval = PAPI_multiplex_init();
	if (retval != PAPI_OK) 
		handle_error(retval);
	retval = PAPI_create_eventset(&EventSet);
	if (retval != PAPI_OK) 
		handle_error(retval);
 
	// Add Total L2Cache Misses 
	retval = PAPI_add_event(EventSet, PAPI_L2_TCM);
	if (retval != PAPI_OK) 
		handle_error(retval);
	
	// Total L1 cache accesses = total memory accesses. Needed for computing L2 miss rate. On Qstruct, there are 2 layers of cache. 
	retval = PAPI_add_event(EventSet, PAPI_L2_TCA);
	if (retval != PAPI_OK) 
		handle_error(retval);

	retval = PAPI_set_multiplex(EventSet);
	if (retval != PAPI_OK) 
		handle_error(retval);

	// TOTAL cycles 
	retval = PAPI_add_event(EventSet, PAPI_TOT_CYC);
	if (retval != PAPI_OK) 
		handle_error(retval);
	
	// TOTAL stalls 
	retval = PAPI_add_event(EventSet, PAPI_RES_STL);
	if (retval != PAPI_OK) 
		handle_error(retval);
#endif
	if((argc==1) || strcmp(argv[1],"-h")==0)
	{
		printf("./<exe> <app-specific-arguments-parsed-by-ReadInput> <system-parameters>\n");
		printf("*****system-parameters****\n");
		printf("-t <>\t -- number of Cilk workers to use in parallel execution.-DPARALLEL must be specified during compilation.\n");
		printf("-recursion_depth <>\t -- unfolds the recursion a specified number of levels to generate tasks.\n");
		printf("-partition <>\t -- sets the data partitioning scheme: ");
		printf("PARTITIONING_BLOCKED_VERTICAL - 0, PARTITIONING_BLOCKCYCLIC_HORIZONTAL - 1, PARTITIONING_BLOCKED_VERTICAL - 2, PARTITIONING_BLOCKCYCLIC_VERTICAL - 3, PARTITIONING_TILED - 4\n");
		printf("-cycle_length <>\t -- sets the cycle length in case of BLOCKCYCLIC schemes. ignored in all other cases.\n");
		printf("*****App-specific parameters***\n");
	}
	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&totalProcs);
	MPI_Comm_rank(MPI_COMM_WORLD,&procRank);
	//Get parameters.
	ParseSystemParams(argc, argv);
	//Create tasks
	GenerateTasks(argc, argv);

	/*for(int i=0;i<64;i++)
		lNodewiseMsgLog[i]=0;*/
#ifdef PAPI
	retval = PAPI_start(EventSet);
	if (retval != PAPI_OK) handle_error(retval);
#endif	
	//execute tasks
	ExecuteTasks();

#ifdef PAPI
		retval = PAPI_stop(EventSet, values);
		if (retval != PAPI_OK) handle_error(retval);
#endif

	if(procRank == 0)
	{
		printf("Preprocess time: %f seconds\n",gPreprocessTime/(float)1000000);
		printf("Task completion time: %f seconds\n",gTaskExecutionTime/(float)1000000);
	}
#ifdef IDLE_TIME
	double avgTimeForDataReception = idleTime/numMsgsRecvd;
	if(numMsgsRecvd == 0)
		avgTimeForDataReception = 0;
	printf("%d: NumMsgsRecvd:%d avg time elapsed before receiving:%f\n",procRank,numMsgsRecvd,avgTimeForDataReception);
	
	double geomeanAvg, gAvgTimeForDataReception;
	/*double* arrAvgTimeForDataReception=nullptr;
	if(procRank == 0)
		arrAvgTimeForDataReception= new double[totalProcs];
	MPI_Gather(&avgTimeForDataReception,1, MPI_DOUBLE,arrAvgTimeForDataReception,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	if(procRank == 0)
	{
		//printf("%f %f %f %f\n",arrAvgTimeForDataReception[0],arrAvgTimeForDataReception[1],arrAvgTimeForDataReception[2],arrAvgTimeForDataReception[3]);
		geomeanAvg = GetGeoMean(arrAvgTimeForDataReception,totalProcs);
	}
	MPI_Reduce(&avgTimeForDataReception,&gAvgTimeForDataReception, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
	if(procRank == 0)
		printf("Avg time elapsed between two messages (all processes):%f geomean: %f\n",gAvgTimeForDataReception/totalProcs, geomeanAvg); */

	MPI_Reduce(&avgTimeForDataReception,&gAvgTimeForDataReception, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
	if(procRank == 0)
		printf("Avg time elapsed between two messages (all processes):%f\n",gAvgTimeForDataReception/totalProcs); 
	double overhead = idleTime/(gTaskExecutionTime/(float)1000000);
	//printf("%d: idleTime:%f overhead:%f\n",procRank,idleTime,overhead);
	double perNodeOverhead[MAX_NUM_NODES], gPerNodeOverhead[MAX_NUM_NODES];
	int numProcsMappedToNodes[MAX_NUM_NODES], gNumProcsMappedToNodes[MAX_NUM_NODES];
	for(int i=0;i<MAX_NUM_NODES;i++)
	{
		perNodeOverhead[i]=0;
		numProcsMappedToNodes[i]=0;
	}
	
	numProcsMappedToNodes[(procRank/CORES_PER_NODE) % MAX_NUM_NODES]=1;
	MPI_Reduce(numProcsMappedToNodes,gNumProcsMappedToNodes, MAX_NUM_NODES, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	perNodeOverhead[(procRank/CORES_PER_NODE) % MAX_NUM_NODES] = overhead;
	MPI_Reduce(perNodeOverhead,gPerNodeOverhead, MAX_NUM_NODES, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
	int numNodesUsed = (totalProcs/CORES_PER_NODE >= MAX_NUM_NODES) ? MAX_NUM_NODES: ceil(totalProcs/(float)(CORES_PER_NODE));
	if(procRank == 0)
	{
		for(int i=0;i<MAX_NUM_NODES;i++)
		{
			if(gNumProcsMappedToNodes[i] != 0)
				gPerNodeOverhead[i] /= gNumProcsMappedToNodes[i];
			else
				gPerNodeOverhead[i] = 0;
		}

		for(int i=0;i<MAX_NUM_NODES;i++)
		{
			if(gPerNodeOverhead[i] != 0)
				printf("Wabash%d: NumProcsMapped: %d perNodeOverhead (avg): %f\n",i,gNumProcsMappedToNodes[i], gPerNodeOverhead[i]);
		}

		double mean=0;
		for(int i=0;i<numNodesUsed;i++)
			mean += gPerNodeOverhead[i];
		mean /= numNodesUsed;
		double var = GetVariance(gPerNodeOverhead, numNodesUsed);
		double stddev = sqrt(var);
		double cov = stddev / mean;
		printf("Number of nodes utilized:%d idle_time: Mean=%f stddev=%f CV(stddev/Mean)=%f\n",numNodesUsed,mean,stddev,cov);
	}
#endif

#ifdef PAPI
	/*float avgCPI, CPI = values[0]/(float)(values[1]);
	MPI_Reduce(&CPI, &avgCPI, 1, MPI_FLOAT,MPI_SUM,0, MPI_COMM_WORLD);
	if(procRank==0)
		printf("total ins:%lld cycles stalled:%lld\n", values[1],values[0]);*/
	//printf("%d %lld %lld %lld %lld\n",procRank, values[0],values[1],values[2],values[3]);	
	assert(values[2] > values[3]);
	float ratio = values[3]/(float)(values[2]);
	//printf("%d %lld %lld %f\n",procRank, values[3],values[2],values[3]/(float)(values[2]));	
	float lAvgRatio[8],gAvgRatio[8];
	for(int i=0;i<8;i++)
		lAvgRatio[i]=0;
	#ifndef REMOTENODEFIRST
		lAvgRatio[procRank/16]=ratio;
	#else
		lAvgRatio[procRank%16]=ratio;
	#endif
		
	MPI_Reduce(lAvgRatio,gAvgRatio,8,MPI_FLOAT,MPI_SUM,0,MPI_COMM_WORLD);
	if(procRank == 0)
	{
		for(int i=0;i<8;i++)
		{
			printf("node %d sumRatio %f avgRatio %f\n",i, gAvgRatio[i],gAvgRatio[i]/16);
		}
	}

#endif
	/*MPI_Reduce(lNodewiseMsgLog,gNodewiseMsgLog,64,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
	if(procRank == 0)
	{
		int totalIntra=0, totalInter=0;
		float intraToInterRatio=0;
		for(int i=0;i<8;i++)
		{
			for(int j=0;j<8;j++)
			{
				printf("%d",gNodewiseMsgLog[i*8+j]);
				if(j!=8)
					printf(" ");
				if(i==j)
					totalIntra += gNodewiseMsgLog[i*8+j];
				else
					totalInter += gNodewiseMsgLog[i*8+j];
			}
			printf("\n");
		}
		printf("Total Msgs %d (Intra %d Inter %d) ratio (intra/total) %f\n",totalIntra+totalInter, totalIntra, totalInter, totalIntra/(float)(totalIntra+totalInter));
	}*/
	MPI_Finalize();
	
	//PrintCellCost(procRank);
	return 0;
}

/*void MakeCLACopy(int argc, char** argv)
{
	argcCopy = argc;
	argvCopy = new char*[argcCopy];
	for(unsigned int i=0;i<argcCopy;i++)
	{
		int argLen = strlen(argv[i]);
		argvCopy[i] = new char[argLen+1];
		strcpy(argvCopy[i],argv[i]);
	}
}*/

#ifdef IDLE_TIME
double GetGeoMean(const double* data, int numElems)
{
    double partialSignificandProd = 1.0;
    long long exp = 0;
    double invN = 1.0 / numElems;

    for (int i=0;i<numElems; i++)
    {
        int curExp;
        double curSignificand = frexp(data[i],&curExp);
        partialSignificandProd *= curSignificand;
        exp += curExp;
    }

    return pow( std::numeric_limits<double>::radix,exp * invN) * pow(partialSignificandProd,invN);

}

double GetVariance(const double* data, int numElems)
{
    // Compute mean (average of elements)
    double sum = 0;
    for (int i = 0; i < numElems; i++)
        sum += data[i];

    double mean = sum / numElems;
 
    // Compute sum squared 
    // differences with mean.
    double sqDiff = 0;
    for (int i = 0; i < numElems; i++) 
        sqDiff += (data[i] - mean) * 
                  (data[i] - mean);
    return sqDiff / numElems;
}

double GetStddev(const double* data, int numElems)
{
	double var = GetVariance(data, numElems);
	return sqrt(var);
}

#endif
