/*#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<vector>
#include<fstream>*/
#include "HelperFunctions.h"

vector<pair<int, int> > vertices;
long int inputSizeX[DIMENSION];

void _ReadInput(char* fileName, int maxVertices, vector<pair<int, int> >& points)
{
	string line;
	int xCoord, yCoord;
	ifstream inFile(fileName, ifstream::in);
	if(!fileName || !inFile.is_open())
	{
		printf("ERROR: Unable to open input file\n");
		exit(0);
	}
	while(!inFile.eof())
	{
		getline(inFile, line);
		if(inFile.eof())
			break;
		size_t pos = line.find(',');
		if(pos==string::npos)
		{
			printf("ERROR.Invalid input format.\n");
			exit(0);
		}
		xCoord = atoi(line.substr(0,pos).c_str());
		yCoord = atoi(line.substr(pos+1).c_str());
		points.push_back(make_pair(xCoord,yCoord));
		if(points.size()>=maxVertices)
			break;
	}
	return;
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || strcmp(argv[1],"-h")==0)
	{
		printf("Usage: ./exe <input> <numvertices>\n");
		exit(0);
	}
	_ReadInput(argv[1],atoi(argv[2]),vertices);
	inputSizeX[0] = vertices.size()-1;inputSizeX[1] = vertices.size()-1;
	return inputSizeX[0]+1;
}

CELL_TYPE Distance(int i, int j)
{
	int x1=vertices[i].first;
	int y1=vertices[i].second;
	int x2=vertices[j].first;
	int y2=vertices[j].second;
	return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

CELL_TYPE Weight(int i, int j, int k)
{
	return Distance(i,j)+Distance(j,k)+Distance(k,i);
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	unsigned int relIndx = 0;
	for(unsigned int i=b->coords[1];i<=b->coords[3];i++)
		for(unsigned int j=b->coords[0];j<=b->coords[2];j++)
			data[relIndx++] = ((i==j)||(i==j-1))?0:FLT_MAX;
}

void PrintResults()
{
	vector<int> coords;
	coords.push_back(inputSizeX[0]);
	coords.push_back(0);
	CELL_TYPE* data = GetTileOfDPTable(inputSizeX[0],0);
	if(data != nullptr)
	{
		CELL_TYPE* result = GetDPTableCell(0,inputSizeX[0],data);
		if(result!=nullptr)
			std::cout<<"Minimum cost of triangulation:"<<*result<<std::endl;
	}
}

/*
 * Terminating case for the MWT problem. Copy paste the code between == into the terminating case of a recursive method in RecursiveFunctions.cpp
 *
==================Terminating case for method A (no dependencies. cell is computed from initial conditions.)=============================
	int i=X->coords[1];
	int j=X->coords[0];
	int k=X->coords[0];
	if((i == j)||(i==j-1)||(i==k))
		return;
	CELL_TYPE w_ikj=Weight(i,j,k);
	CELL_TYPE* cost_ij = GetDPTableCell(i,j,XData);
	CELL_TYPE* cost_ik = GetDPTableCell(i,k,XData);
	CELL_TYPE* cost_kj = GetDPTableCell(k,j,XData);
	CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
	if(newCost < *cost_ij)
		*cost_ij =  newCost;
==================Terminating case for methods B and C (reads two cells that UData and VData point to.)=========================================
	int i=X->coords[1];
	int j=X->coords[0];
	int k=U->coords[0];
	if((i == j)||(i==j-1)||(i==k))
		return;
	CELL_TYPE w_ikj=Weight(i,j,k);
	CELL_TYPE* cost_ij = GetDPTableCell(i,j,XData);
	CELL_TYPE* cost_ik = GetDPTableCell(i,k,UData);
	CELL_TYPE* cost_kj = GetDPTableCell(k,j,VData);
	CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
	if(newCost < *cost_ij)
		*cost_ij =  newCost;
========================================================================================================================================
 */
