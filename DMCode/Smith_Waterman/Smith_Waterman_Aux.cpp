#include"HelperFunctions.h"
long int inputSizex[DIMENSION];
#define DEF_STR_LEN 45000
//Remember to add extern declarations to the following in GlobalTypes.h
int gap=-4, match=5, nomatch=-3;
char* input_i, *input_j;

int ReadFASTAFile(FILE *fp, int maxLen, char** seq)
{
	char line[1024];
	int seqLength = 0, seqLenToProcess;
	char* localSeq=NULL;
	fgets(line, sizeof(line), fp);
	if(line == NULL)
		return -1;
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(strlen(line)+seqLength > maxLen)
			seqLenToProcess = maxLen-seqLength;
		else
			seqLenToProcess = strlen(line);
		localSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);
		if(!localSeq)
		{
			printf("ERROR in realloc.\n");
			exit(0);
		}
		for(unsigned int i = 0; i < seqLenToProcess; ++i)
		{
			if (line[i] == '*' || line[i] == '-')
				localSeq[seqLength++] = line[i];
			else if(isalpha(line[i]))
				localSeq[seqLength++] = toupper(line[i]);

		}
		if(seqLength>=maxLen) break;
	}
	localSeq[seqLength] = '\0';
	*seq = localSeq;
	return seqLength;
}

int ReadInput(int argc, char** argv)
{
	if(strcmp(argv[1],"-h")==0) 
	{
		printf("Usage: ./<exec> <input_string_i> <input_string_j> <input_length_i> <input_length_j>\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r"), *fpj= fopen(argv[2],"r");
	if(!fpi || !fpj)
	{
		printf("ERROR: Unable to open input files.\n");
		exit(0);
	}
	int inputLength_I=DEF_STR_LEN,inputLength_J=DEF_STR_LEN;
	inputLength_I=atoi(argv[3]); inputLength_J=atoi(argv[4]);
	int ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);
	int ret2=ReadFASTAFile(fpj, inputLength_J, &input_j);
	fclose(fpi); fclose(fpj);
	if((ret1<0) || (ret2<0))
	{
		printf("ERROR: Unable to read input files correctly. file1 len:%d file2 len:%d\n",ret1,ret2);
		exit(0);
	}
	else
	{
		inputSizex[0]=ret1; inputSizex[1]=ret2;
	}
	if(inputSizex[1] > inputSizex[0])
	{
		std::swap(inputSizex[0],inputSizex[1]);
		std::swap(input_i,input_j);
	}
	//printf("string length (x):%d (y):%d\n",inputSizex[0],inputSizex[1]);
	return inputSizex[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	memset(data,0,sizeof(CELL_TYPE)*(numCells));
	return;
}

void PrintResults()
{
	PrintGlobalMax();
}

/*
 * Terminating case for the Smith_Waterman problem. Copy paste the code between == into the terminating case of a recursive method in RecursiveFunctions.cpp
 *
==================Terminating case for method A (no dependencies. cell is computed from initial conditions.)=============================
 		int i=x->coords[1], j=x->coords[0], k=x->coords[1], l=x->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,xData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
==================Terminating case for methods B, C, and D (reads another cell that yData points to.)=========================================
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;

========================================================================================================================================


 
 */
