#include "HelperFunctions.h"
int fnCallHierarchySummary[4][4]={{2,2,2,2},{0,4,0,4},{0,0,6,2},{0,0,0,8}};
map<int, vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_FULL;
extern long int inputSizex[2];

#define _paramListFunctionA Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionD Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int

void A(Box* x, CELL_TYPE* xData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void A_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, int callStackDepth);
void B_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, Box* z, CELL_TYPE* zData, int parentTileIDz, int callStackDepth);
void C_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, Box* z, CELL_TYPE* zData, int parentTileIDz, int callStackDepth);
void D_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, Box* z, CELL_TYPE* zData, int parentTileIDz, int callStackDepth);
void A(Box* x, CELL_TYPE* xData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=x->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,xData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		return;
	}
	
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);*/

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	if(callStackDepth > recursionDepth+4)
	{

	A(&x00, xData, callStackDepth+1);

	B(&x01, xData, &x00, xData, &x01, xData, callStackDepth+1);	C(&x10, xData, &x10, xData, &x00, xData, callStackDepth+1);

	D(&x11, xData, &x10, xData, &x01, xData, callStackDepth+1);

	A(&x11, xData, callStackDepth+1);
	B(&x10, xData, &x11, xData, &x10, xData, callStackDepth+1);	C(&x01, xData, &x01, xData, &x11, xData, callStackDepth+1);

	D(&x00, xData, &x01, xData, &x11, xData, callStackDepth+1);

	return;

	}

	A(&x00, xData, callStackDepth+1);

#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x01, xData, &x00, xData, &x01, xData, callStackDepth+1);	C(&x10, xData, &x10, xData, &x00, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	D(&x11, xData, &x10, xData, &x01, xData, callStackDepth+1);

#ifdef PARALLEL
	cilk_spawn
#endif
	A(&x11, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x10, xData, &x11, xData, &x10, xData, callStackDepth+1);	C(&x01, xData, &x01, xData, &x11, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	D(&x00, xData, &x01, xData, &x11, xData, callStackDepth+1);

	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		return;
	}



	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);
	
	int yside_x=(y->coords[2]-y->coords[0]+1), yside_y=(y->coords[3]-y->coords[1]+1);
	int yoffset_x=yside_x/2-1, yoffset_y=yside_y/2-1;
	if(yoffset_x < 0)
		yoffset_x=0;
	if(yoffset_y < 0)
		yoffset_y=0;
	Box y00(y->coords[0],y->coords[1],y->coords[0]+yoffset_x,y->coords[1]+yoffset_y);
	Box y01(y->coords[0]+yside_x/2,y->coords[1],y->coords[2],y->coords[1]+yoffset_y);
	Box y10(y->coords[0],y->coords[1]+yside_y/2, y->coords[0]+yoffset_x,y->coords[3]);
	Box y11(y->coords[0]+yside_x/2,y->coords[1]+yside_y/2,y->coords[2],y->coords[3]);

	int zside_x=(z->coords[2]-z->coords[0]+1), zside_y=(z->coords[3]-z->coords[1]+1);
	int zoffset_x=zside_x/2-1, zoffset_y=zside_y/2-1;
	if(zoffset_x < 0)
		zoffset_x=0;
	if(zoffset_y < 0)
		zoffset_y=0;
	Box z00(z->coords[0],z->coords[1],z->coords[0]+zoffset_x,z->coords[1]+zoffset_y);
	Box z01(z->coords[0]+zside_x/2,z->coords[1],z->coords[2],z->coords[1]+zoffset_y);
	Box z10(z->coords[0],z->coords[1]+zside_y/2, z->coords[0]+zoffset_x,z->coords[3]);
	Box z11(z->coords[0]+zside_x/2,z->coords[1]+zside_y/2,z->coords[2],z->coords[3]);*/

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	if(callStackDepth > recursionDepth+4)
	{


	B(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);	B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);


	D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);


	B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);	B(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);


	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);	D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);


	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);	B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);	B(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);	D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);
	
	int yside_x=(y->coords[2]-y->coords[0]+1), yside_y=(y->coords[3]-y->coords[1]+1);
	int yoffset_x=yside_x/2-1, yoffset_y=yside_y/2-1;
	if(yoffset_x < 0)
		yoffset_x=0;
	if(yoffset_y < 0)
		yoffset_y=0;
	Box y00(y->coords[0],y->coords[1],y->coords[0]+yoffset_x,y->coords[1]+yoffset_y);
	Box y01(y->coords[0]+yside_x/2,y->coords[1],y->coords[2],y->coords[1]+yoffset_y);
	Box y10(y->coords[0],y->coords[1]+yside_y/2, y->coords[0]+yoffset_x,y->coords[3]);
	Box y11(y->coords[0]+yside_x/2,y->coords[1]+yside_y/2,y->coords[2],y->coords[3]);

	int zside_x=(z->coords[2]-z->coords[0]+1), zside_y=(z->coords[3]-z->coords[1]+1);
	int zoffset_x=zside_x/2-1, zoffset_y=zside_y/2-1;
	if(zoffset_x < 0)
		zoffset_x=0;
	if(zoffset_y < 0)
		zoffset_y=0;
	Box z00(z->coords[0],z->coords[1],z->coords[0]+zoffset_x,z->coords[1]+zoffset_y);
	Box z01(z->coords[0]+zside_x/2,z->coords[1],z->coords[2],z->coords[1]+zoffset_y);
	Box z10(z->coords[0],z->coords[1]+zside_y/2, z->coords[0]+zoffset_x,z->coords[3]);
	Box z11(z->coords[0]+zside_x/2,z->coords[1]+zside_y/2,z->coords[2],z->coords[3]);*/


	if(callStackDepth > recursionDepth+4)
	{


	C(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);	C(&x10, xData, &y10, yData, &z10, zData, callStackDepth+1);


	D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);	C(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);


	C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);	C(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);


	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);	C(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);


	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);	C(&x10, xData, &y10, yData, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);	C(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);	C(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);	C(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


	return;
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);
	
	int yside_x=(y->coords[2]-y->coords[0]+1), yside_y=(y->coords[3]-y->coords[1]+1);
	int yoffset_x=yside_x/2-1, yoffset_y=yside_y/2-1;
	if(yoffset_x < 0)
		yoffset_x=0;
	if(yoffset_y < 0)
		yoffset_y=0;
	Box y00(y->coords[0],y->coords[1],y->coords[0]+yoffset_x,y->coords[1]+yoffset_y);
	Box y01(y->coords[0]+yside_x/2,y->coords[1],y->coords[2],y->coords[1]+yoffset_y);
	Box y10(y->coords[0],y->coords[1]+yside_y/2, y->coords[0]+yoffset_x,y->coords[3]);
	Box y11(y->coords[0]+yside_x/2,y->coords[1]+yside_y/2,y->coords[2],y->coords[3]);

	int zside_x=(z->coords[2]-z->coords[0]+1), zside_y=(z->coords[3]-z->coords[1]+1);
	int zoffset_x=zside_x/2-1, zoffset_y=zside_y/2-1;
	if(zoffset_x < 0)
		zoffset_x=0;
	if(zoffset_y < 0)
		zoffset_y=0;
	Box z00(z->coords[0],z->coords[1],z->coords[0]+zoffset_x,z->coords[1]+zoffset_y);
	Box z01(z->coords[0]+zside_x/2,z->coords[1],z->coords[2],z->coords[1]+zoffset_y);
	Box z10(z->coords[0],z->coords[1]+zside_y/2, z->coords[0]+zoffset_x,z->coords[3]);
	Box z11(z->coords[0]+zside_x/2,z->coords[1]+zside_y/2,z->coords[2],z->coords[3]);*/

	if(callStackDepth > recursionDepth+4)
	{


	D(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
	D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
	D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);


	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
	D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
	D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);	D(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);



	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);	D(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif



	return;
}

void A_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, xData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);*/


	A_unroll(&x00, xData, parentTileIDx*4+0, callStackDepth+1);
	B_unroll(&x01, xData, parentTileIDx*4+1, &x00, xData, parentTileIDx*4+0, &x01, xData, parentTileIDx*4+1, callStackDepth+1);	C_unroll(&x10, xData, parentTileIDx*4+2, &x10, xData, parentTileIDx*4+2, &x00, xData, parentTileIDx*4+0, callStackDepth+1);
	D_unroll(&x11, xData, parentTileIDx*4+3, &x10, xData, parentTileIDx*4+2, &x01, xData, parentTileIDx*4+1, callStackDepth+1);
	A_unroll(&x11, xData, parentTileIDx*4+3, callStackDepth+1);	B_unroll(&x10, xData, parentTileIDx*4+2, &x11, xData, parentTileIDx*4+3, &x10, xData, parentTileIDx*4+2, callStackDepth+1);	C_unroll(&x01, xData, parentTileIDx*4+1, &x01, xData, parentTileIDx*4+1, &x11, xData, parentTileIDx*4+3, callStackDepth+1);
	D_unroll(&x00, xData, parentTileIDx*4+0, &x01, xData, parentTileIDx*4+1, &x11, xData, parentTileIDx*4+3, callStackDepth+1);

	return;
}

void B_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, Box* z, CELL_TYPE* zData, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*y);
			p.data = b3;
			p.tile = yData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*z);
			p.data = b6;
			p.tile = zData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, xData, b3, yData, b6, zData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID2D(parentTileIDy);
		readTileIDs[1]=GetTileID2D(parentTileIDz);
		tiles[0]=yData;
		tiles[1]=zData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);
	
	int yside_x=(y->coords[2]-y->coords[0]+1), yside_y=(y->coords[3]-y->coords[1]+1);
	int yoffset_x=yside_x/2-1, yoffset_y=yside_y/2-1;
	if(yoffset_x < 0)
		yoffset_x=0;
	if(yoffset_y < 0)
		yoffset_y=0;
	Box y00(y->coords[0],y->coords[1],y->coords[0]+yoffset_x,y->coords[1]+yoffset_y);
	Box y01(y->coords[0]+yside_x/2,y->coords[1],y->coords[2],y->coords[1]+yoffset_y);
	Box y10(y->coords[0],y->coords[1]+yside_y/2, y->coords[0]+yoffset_x,y->coords[3]);
	Box y11(y->coords[0]+yside_x/2,y->coords[1]+yside_y/2,y->coords[2],y->coords[3]);

	int zside_x=(z->coords[2]-z->coords[0]+1), zside_y=(z->coords[3]-z->coords[1]+1);
	int zoffset_x=zside_x/2-1, zoffset_y=zside_y/2-1;
	if(zoffset_x < 0)
		zoffset_x=0;
	if(zoffset_y < 0)
		zoffset_y=0;
	Box z00(z->coords[0],z->coords[1],z->coords[0]+zoffset_x,z->coords[1]+zoffset_y);
	Box z01(z->coords[0]+zside_x/2,z->coords[1],z->coords[2],z->coords[1]+zoffset_y);
	Box z10(z->coords[0],z->coords[1]+zside_y/2, z->coords[0]+zoffset_x,z->coords[3]);
	Box z11(z->coords[0]+zside_x/2,z->coords[1]+zside_y/2,z->coords[2],z->coords[3]);*/


	B_unroll(&x00, xData, parentTileIDx*4+0, &y00, yData, parentTileIDy*4+0, &z00, zData, parentTileIDz*4+0, callStackDepth+1);	B_unroll(&x01, xData, parentTileIDx*4+1, &y00, yData, parentTileIDy*4+0, &z01, zData, parentTileIDz*4+1, callStackDepth+1);
	D_unroll(&x10, xData, parentTileIDx*4+2, &y10, yData, parentTileIDy*4+2, &z00, zData, parentTileIDz*4+0, callStackDepth+1);	D_unroll(&x11, xData, parentTileIDx*4+3, &y10, yData, parentTileIDy*4+2, &z01, zData, parentTileIDz*4+1, callStackDepth+1);
	B_unroll(&x10, xData, parentTileIDx*4+2, &y11, yData, parentTileIDy*4+3, &z10, zData, parentTileIDz*4+2, callStackDepth+1);	B_unroll(&x11, xData, parentTileIDx*4+3, &y11, yData, parentTileIDy*4+3, &z11, zData, parentTileIDz*4+3, callStackDepth+1);
	D_unroll(&x00, xData, parentTileIDx*4+0, &y01, yData, parentTileIDy*4+1, &z10, zData, parentTileIDz*4+2, callStackDepth+1);	D_unroll(&x01, xData, parentTileIDx*4+1, &y01, yData, parentTileIDy*4+1, &z11, zData, parentTileIDz*4+3, callStackDepth+1);

	return;
}

void C_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, Box* z, CELL_TYPE* zData, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*y);
			p.data = b3;
			p.tile = yData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*z);
			p.data = b6;
			p.tile = zData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, xData, b3, yData, b6, zData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID2D(parentTileIDy);
		readTileIDs[1]=GetTileID2D(parentTileIDz);
		tiles[0]=yData;
		tiles[1]=zData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);
	
	int yside_x=(y->coords[2]-y->coords[0]+1), yside_y=(y->coords[3]-y->coords[1]+1);
	int yoffset_x=yside_x/2-1, yoffset_y=yside_y/2-1;
	if(yoffset_x < 0)
		yoffset_x=0;
	if(yoffset_y < 0)
		yoffset_y=0;
	Box y00(y->coords[0],y->coords[1],y->coords[0]+yoffset_x,y->coords[1]+yoffset_y);
	Box y01(y->coords[0]+yside_x/2,y->coords[1],y->coords[2],y->coords[1]+yoffset_y);
	Box y10(y->coords[0],y->coords[1]+yside_y/2, y->coords[0]+yoffset_x,y->coords[3]);
	Box y11(y->coords[0]+yside_x/2,y->coords[1]+yside_y/2,y->coords[2],y->coords[3]);

	int zside_x=(z->coords[2]-z->coords[0]+1), zside_y=(z->coords[3]-z->coords[1]+1);
	int zoffset_x=zside_x/2-1, zoffset_y=zside_y/2-1;
	if(zoffset_x < 0)
		zoffset_x=0;
	if(zoffset_y < 0)
		zoffset_y=0;
	Box z00(z->coords[0],z->coords[1],z->coords[0]+zoffset_x,z->coords[1]+zoffset_y);
	Box z01(z->coords[0]+zside_x/2,z->coords[1],z->coords[2],z->coords[1]+zoffset_y);
	Box z10(z->coords[0],z->coords[1]+zside_y/2, z->coords[0]+zoffset_x,z->coords[3]);
	Box z11(z->coords[0]+zside_x/2,z->coords[1]+zside_y/2,z->coords[2],z->coords[3]);*/


	C_unroll(&x00, xData, parentTileIDx*4+0, &y00, yData, parentTileIDy*4+0, &z00, zData, parentTileIDz*4+0, callStackDepth+1);	C_unroll(&x10, xData, parentTileIDx*4+2, &y10, yData, parentTileIDy*4+2, &z10, zData, parentTileIDz*4+2, callStackDepth+1);
	D_unroll(&x01, xData, parentTileIDx*4+1, &y00, yData, parentTileIDy*4+0, &z01, zData, parentTileIDz*4+1, callStackDepth+1);	C_unroll(&x11, xData, parentTileIDx*4+3, &y10, yData, parentTileIDy*4+2, &z01, zData, parentTileIDz*4+1, callStackDepth+1);
	C_unroll(&x01, xData, parentTileIDx*4+1, &y01, yData, parentTileIDy*4+1, &z11, zData, parentTileIDz*4+3, callStackDepth+1);	C_unroll(&x11, xData, parentTileIDx*4+3, &y11, yData, parentTileIDy*4+3, &z11, zData, parentTileIDz*4+3, callStackDepth+1);
	D_unroll(&x00, xData, parentTileIDx*4+0, &y01, yData, parentTileIDy*4+1, &z10, zData, parentTileIDz*4+2, callStackDepth+1);	C_unroll(&x10, xData, parentTileIDx*4+2, &y11, yData, parentTileIDy*4+3, &z10, zData, parentTileIDz*4+2, callStackDepth+1);

	return;
}

void D_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, Box* z, CELL_TYPE* zData, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'D';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*y);
			p.data = b3;
			p.tile = yData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*z);
			p.data = b6;
			p.tile = zData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionD> t = std::make_tuple(b0, xData, b3, yData, b6, zData, callStackDepth+1);
			DeferredCall<_paramListFunctionD>* defdCall = new DeferredCall<_paramListFunctionD>();
			defdCall->params=t;
			defdCall->fptr=D;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID2D(parentTileIDy);
		readTileIDs[1]=GetTileID2D(parentTileIDz);
		tiles[0]=yData;
		tiles[1]=zData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	/*int xside_x=(x->coords[2]-x->coords[0]+1), xside_y=(x->coords[3]-x->coords[1]+1);
	int xoffset_x=xside_x/2-1, xoffset_y=xside_y/2-1;
	if(xoffset_x < 0)
		xoffset_x=0;
	if(xoffset_y < 0)
		xoffset_y=0;
	Box x00(x->coords[0],x->coords[1],x->coords[0]+xoffset_x,x->coords[1]+xoffset_y);
	Box x01(x->coords[0]+xside_x/2,x->coords[1],x->coords[2],x->coords[1]+xoffset_y);
	Box x10(x->coords[0],x->coords[1]+xside_y/2, x->coords[0]+xoffset_x,x->coords[3]);
	Box x11(x->coords[0]+xside_x/2,x->coords[1]+xside_y/2,x->coords[2],x->coords[3]);
	
	int yside_x=(y->coords[2]-y->coords[0]+1), yside_y=(y->coords[3]-y->coords[1]+1);
	int yoffset_x=yside_x/2-1, yoffset_y=yside_y/2-1;
	if(yoffset_x < 0)
		yoffset_x=0;
	if(yoffset_y < 0)
		yoffset_y=0;
	Box y00(y->coords[0],y->coords[1],y->coords[0]+yoffset_x,y->coords[1]+yoffset_y);
	Box y01(y->coords[0]+yside_x/2,y->coords[1],y->coords[2],y->coords[1]+yoffset_y);
	Box y10(y->coords[0],y->coords[1]+yside_y/2, y->coords[0]+yoffset_x,y->coords[3]);
	Box y11(y->coords[0]+yside_x/2,y->coords[1]+yside_y/2,y->coords[2],y->coords[3]);

	int zside_x=(z->coords[2]-z->coords[0]+1), zside_y=(z->coords[3]-z->coords[1]+1);
	int zoffset_x=zside_x/2-1, zoffset_y=zside_y/2-1;
	if(zoffset_x < 0)
		zoffset_x=0;
	if(zoffset_y < 0)
		zoffset_y=0;
	Box z00(z->coords[0],z->coords[1],z->coords[0]+zoffset_x,z->coords[1]+zoffset_y);
	Box z01(z->coords[0]+zside_x/2,z->coords[1],z->coords[2],z->coords[1]+zoffset_y);
	Box z10(z->coords[0],z->coords[1]+zside_y/2, z->coords[0]+zoffset_x,z->coords[3]);
	Box z11(z->coords[0]+zside_x/2,z->coords[1]+zside_y/2,z->coords[2],z->coords[3]);*/


	D_unroll(&x00, xData, parentTileIDx*4+0, &y00, yData, parentTileIDy*4+0, &z00, zData, parentTileIDz*4+0, callStackDepth+1);	D_unroll(&x01, xData, parentTileIDx*4+1, &y00, yData, parentTileIDy*4+0, &z01, zData, parentTileIDz*4+1, callStackDepth+1);	D_unroll(&x10, xData, parentTileIDx*4+2, &y10, yData, parentTileIDy*4+2, &z00, zData, parentTileIDz*4+0, callStackDepth+1);	D_unroll(&x11, xData, parentTileIDx*4+3, &y10, yData, parentTileIDy*4+2, &z01, zData, parentTileIDz*4+1, callStackDepth+1);
	D_unroll(&x00, xData, parentTileIDx*4+0, &y01, yData, parentTileIDy*4+1, &z10, zData, parentTileIDz*4+2, callStackDepth+1);	D_unroll(&x01, xData, parentTileIDx*4+1, &y01, yData, parentTileIDy*4+1, &z11, zData, parentTileIDz*4+3, callStackDepth+1);	D_unroll(&x10, xData, parentTileIDx*4+2, &y11, yData, parentTileIDy*4+3, &z10, zData, parentTileIDz*4+2, callStackDepth+1);	D_unroll(&x11, xData, parentTileIDx*4+3, &y11, yData, parentTileIDy*4+3, &z11, zData, parentTileIDz*4+3, callStackDepth+1);


	return;
}

void Unroll()
{
	int parentTileIDx=0;
	Box *x = new Box(0,0,inputSizex[0],inputSizex[1]);
	if(0 == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = nullData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	A_unroll(x, nullptr , 0, 0);
	delete x;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 1);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
			}
			break;
		case 'D':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionD>* df = (reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall))->Run();
			}
			break;
		default: break;
	}
}

