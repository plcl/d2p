#pragma once
#define DIMENSION 4
#define METADATASPACE (2*DIMENSION+1)
typedef int CELL_TYPE;
extern int fnCallHierarchySummary[4][4];
inline CELL_TYPE* GetDPTableCell(int i, int j, int k, int l, CELL_TYPE* data)
{
	CELL_TYPE* cell = data+METADATASPACE;
	int side = data[DIMENSION];
	int iOffset=i - data[3];
	cell += (iOffset*3* side);
	int jOffset=j - data[2];
	cell += (jOffset*2* side);
	int kOffset=k - data[1];
	cell += (kOffset*1* side);
	int lOffset=l - data[0];
	cell += lOffset;
	return cell;
}


typedef struct Box
{	int coords[8];
	Box(){}
	Box(int a, int b, int c, int d, int e, int f, int g, int h)
	{
		coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;coords[4]=e;coords[5]=f;coords[6]=g;coords[7]=h;
	}
	Box(const Box&b)
	{
		coords[0]=b.coords[0];coords[1]=b.coords[1];coords[2]=b.coords[2];coords[3]=b.coords[3];coords[4]=b.coords[4];coords[5]=b.coords[5];coords[6]=b.coords[6];coords[7]=b.coords[7];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) || (this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]) || (this->coords[4]!=rhs.coords[4]) || (this->coords[5]!=rhs.coords[5]) || (this->coords[6]!=rhs.coords[6]) || (this->coords[7]!=rhs.coords[7]))
			flag=false;
		return flag;
	}
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<DIMENSION;i++)
		{
			len *= (coords[i+DIMENSION]-coords[i]+1);
		}
		return len;
	}
	char* PrintStr(char *str)const
	{
		sprintf(str,"%d%d%d%d%d%d%d%d\n",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5],coords[6],coords[7]);
	}
}Box;
