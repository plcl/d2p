#include "HelperFunctions.h"
int fnCallHierarchySummary[3][3]={{2,1,0},{0,4,4},{0,0,8}};
map<int, vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_UTM;
extern long int inputSizeX[2];

#define _paramListFunctionA Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int

void A(Box* X, CELL_TYPE* XData, int callStackDepth);
void B(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth);
void C(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth);
void A_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, int callStackDepth);
void B_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth);
void C_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth);
void A(Box* X, CELL_TYPE* XData, int callStackDepth)
{
	if((X->coords[0]==X->coords[2]) && (X->coords[1]==X->coords[3]))
	{
		//Write the code for terminating case here.
	int i=X->coords[1];
	int j=X->coords[0];
	int k=X->coords[0];
	if((i == j)||(i==j-1)||(i==k))
		return;
	CELL_TYPE w_ikj=Weight(i,j,k);
	CELL_TYPE* cost_ij = GetDPTableCell(i,j,XData);
	CELL_TYPE* cost_ik = GetDPTableCell(i,k,XData);
	CELL_TYPE* cost_kj = GetDPTableCell(k,j,XData);
	CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
	if(newCost < *cost_ij)
		*cost_ij =  newCost;


		return;
	}
	
	int Xside_X=(X->coords[2]-X->coords[0]+1), Xside_Y=(X->coords[3]-X->coords[1]+1);
	int Xoffset_X=Xside_X/2-1, Xoffset_Y=Xside_Y/2-1;
	if(Xoffset_X < 0)
		Xoffset_X=0;
	if(Xoffset_Y < 0)
		Xoffset_Y=0;
	Box X00(X->coords[0],X->coords[1],X->coords[0]+Xoffset_X,X->coords[1]+Xoffset_Y);
	Box X01(X->coords[0]+Xside_X/2,X->coords[1],X->coords[2],X->coords[1]+Xoffset_Y);
	Box X11(X->coords[0]+Xside_X/2,X->coords[1]+Xside_Y/2,X->coords[2],X->coords[3]);

	/*Box X00(X->coords[0],X->coords[1],X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X01(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X11(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2],X->coords[3]);*/

	if(callStackDepth > recursionDepth+4)
	{


	A(&X00, XData, callStackDepth+1);	A(&X11, XData, callStackDepth+1);

	B(&X01, XData, &X00, XData, &X11, XData, callStackDepth+1);

	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	A(&X00, XData, callStackDepth+1);	A(&X11, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	B(&X01, XData, &X00, XData, &X11, XData, callStackDepth+1);

	return;
}

void B(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth)
{
	if((X->coords[0]==X->coords[2]) && (X->coords[1]==X->coords[3]))
	{
		//Write the code for terminating case here.
	int i=X->coords[1];
	int j=X->coords[0];
	int k=U->coords[0];
	if((i == j)||(i==j-1)||(i==k))
		return;
	CELL_TYPE w_ikj=Weight(i,j,k);
	CELL_TYPE* cost_ij = GetDPTableCell(i,j,XData);
	CELL_TYPE* cost_ik = GetDPTableCell(i,k,UData);
	CELL_TYPE* cost_kj = GetDPTableCell(k,j,VData);
	CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
	if(newCost < *cost_ij)
		*cost_ij =  newCost;

		return;
	}

	int Xside_X=(X->coords[2]-X->coords[0]+1), Xside_Y=(X->coords[3]-X->coords[1]+1);
	int Xoffset_X=Xside_X/2-1, Xoffset_Y=Xside_Y/2-1;
	if(Xoffset_X < 0)
		Xoffset_X=0;
	if(Xoffset_Y < 0)
		Xoffset_Y=0;
	Box X00(X->coords[0],X->coords[1],X->coords[0]+Xoffset_X,X->coords[1]+Xoffset_Y);
	Box X01(X->coords[0]+Xside_X/2,X->coords[1],X->coords[2],X->coords[1]+Xoffset_Y);
	Box X10(X->coords[0],X->coords[1]+Xside_Y/2, X->coords[0]+Xoffset_X,X->coords[3]);
	Box X11(X->coords[0]+Xside_X/2,X->coords[1]+Xside_Y/2,X->coords[2],X->coords[3]);

	int Uside_X=(U->coords[2]-U->coords[0]+1), Uside_Y=(U->coords[3]-U->coords[1]+1);
	int Uoffset_X=Uside_X/2-1, Uoffset_U=Uside_Y/2-1;
	if(Uoffset_X < 0)
		Uoffset_X=0;
	if(Uoffset_U < 0)
		Uoffset_U=0;
	Box U00(U->coords[0],U->coords[1],U->coords[0]+Uoffset_X,U->coords[1]+Uoffset_U);
	Box U01(U->coords[0]+Uside_X/2,U->coords[1],U->coords[2],U->coords[1]+Uoffset_U);
	Box U10(U->coords[0],U->coords[1]+Uside_Y/2, U->coords[0]+Uoffset_X,U->coords[3]);
	Box U11(U->coords[0]+Uside_X/2,U->coords[1]+Uside_Y/2,U->coords[2],U->coords[3]);

	int Vside_X=(V->coords[2]-V->coords[0]+1), Vside_Y=(V->coords[3]-V->coords[1]+1);
	int Voffset_X=Vside_X/2-1, Voffset_Y=Vside_Y/2-1;
	if(Voffset_X < 0)
		Voffset_X=0;
	if(Voffset_Y < 0)
		Voffset_Y=0;
	Box V00(V->coords[0],V->coords[1],V->coords[0]+Voffset_X,V->coords[1]+Voffset_Y);
	Box V01(V->coords[0]+Vside_X/2,V->coords[1],V->coords[2],V->coords[1]+Voffset_Y);
	Box V10(V->coords[0],V->coords[1]+Vside_Y/2, V->coords[0]+Voffset_X,V->coords[3]);
	Box V11(V->coords[0]+Vside_X/2,V->coords[1]+Vside_Y/2,V->coords[2],V->coords[3]);

	/*Box U00(U->coords[0],U->coords[1],U->coords[2]-(U->coords[2]-U->coords[0]+1)/2,U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U01(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U11(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1]+(U->coords[3]-U->coords[1]+1)/2,U->coords[2],U->coords[3]);
	Box V00(V->coords[0],V->coords[1],V->coords[2]-(V->coords[2]-V->coords[0]+1)/2,V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V01(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V11(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1]+(V->coords[3]-V->coords[1]+1)/2,V->coords[2],V->coords[3]);
	Box X00(X->coords[0],X->coords[1],X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X01(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X10(X->coords[0],X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]);
	Box X11(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2],X->coords[3]);*/

	if(callStackDepth > recursionDepth+4)
	{

	B(&X10, XData, &U11, UData, &V00, VData, callStackDepth+1);

	C(&X00, XData, &U01, UData, &X10, XData, callStackDepth+1);	C(&X11, XData, &X10, XData, &V01, VData, callStackDepth+1);


	B(&X00, XData, &U00, UData, &V00, VData, callStackDepth+1);	B(&X11, XData, &U11, UData, &V11, VData, callStackDepth+1);

	C(&X01, XData, &U01, UData, &X11, XData, callStackDepth+1);
	C(&X01, XData, &X00, XData, &V01, VData, callStackDepth+1);
	B(&X01, XData, &U00, UData, &V11, VData, callStackDepth+1);

	return;

	}

	B(&X10, XData, &U11, UData, &V00, VData, callStackDepth+1);

#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X00, XData, &U01, UData, &X10, XData, callStackDepth+1);	C(&X11, XData, &X10, XData, &V01, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&X00, XData, &U00, UData, &V00, VData, callStackDepth+1);	B(&X11, XData, &U11, UData, &V11, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&X01, XData, &U01, UData, &X11, XData, callStackDepth+1);
	C(&X01, XData, &X00, XData, &V01, VData, callStackDepth+1);
	B(&X01, XData, &U00, UData, &V11, VData, callStackDepth+1);

	return;
}

void C(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth)
{
	if((X->coords[0]==X->coords[2]) && (X->coords[1]==X->coords[3]))
	{
		//Write the code for terminating case here.
	int i=X->coords[1];
	int j=X->coords[0];
	int k=U->coords[0];
	if((i == j)||(i==j-1)||(i==k))
		return;
	CELL_TYPE w_ikj=Weight(i,j,k);
	CELL_TYPE* cost_ij = GetDPTableCell(i,j,XData);
	CELL_TYPE* cost_ik = GetDPTableCell(i,k,UData);
	CELL_TYPE* cost_kj = GetDPTableCell(k,j,VData);
	CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
	if(newCost < *cost_ij)
		*cost_ij =  newCost;

		return;
	}

	int Xside_X=(X->coords[2]-X->coords[0]+1), Xside_Y=(X->coords[3]-X->coords[1]+1);
	int Xoffset_X=Xside_X/2-1, Xoffset_Y=Xside_Y/2-1;
	if(Xoffset_X < 0)
		Xoffset_X=0;
	if(Xoffset_Y < 0)
		Xoffset_Y=0;
	Box X00(X->coords[0],X->coords[1],X->coords[0]+Xoffset_X,X->coords[1]+Xoffset_Y);
	Box X01(X->coords[0]+Xside_X/2,X->coords[1],X->coords[2],X->coords[1]+Xoffset_Y);
	Box X10(X->coords[0],X->coords[1]+Xside_Y/2, X->coords[0]+Xoffset_X,X->coords[3]);
	Box X11(X->coords[0]+Xside_X/2,X->coords[1]+Xside_Y/2,X->coords[2],X->coords[3]);

	int Uside_X=(U->coords[2]-U->coords[0]+1), Uside_Y=(U->coords[3]-U->coords[1]+1);
	int Uoffset_X=Uside_X/2-1, Uoffset_U=Uside_Y/2-1;
	if(Uoffset_X < 0)
		Uoffset_X=0;
	if(Uoffset_U < 0)
		Uoffset_U=0;
	Box U00(U->coords[0],U->coords[1],U->coords[0]+Uoffset_X,U->coords[1]+Uoffset_U);
	Box U01(U->coords[0]+Uside_X/2,U->coords[1],U->coords[2],U->coords[1]+Uoffset_U);
	Box U10(U->coords[0],U->coords[1]+Uside_Y/2, U->coords[0]+Uoffset_X,U->coords[3]);
	Box U11(U->coords[0]+Uside_X/2,U->coords[1]+Uside_Y/2,U->coords[2],U->coords[3]);

	int Vside_X=(V->coords[2]-V->coords[0]+1), Vside_Y=(V->coords[3]-V->coords[1]+1);
	int Voffset_X=Vside_X/2-1, Voffset_Y=Vside_Y/2-1;
	if(Voffset_X < 0)
		Voffset_X=0;
	if(Voffset_Y < 0)
		Voffset_Y=0;
	Box V00(V->coords[0],V->coords[1],V->coords[0]+Voffset_X,V->coords[1]+Voffset_Y);
	Box V01(V->coords[0]+Vside_X/2,V->coords[1],V->coords[2],V->coords[1]+Voffset_Y);
	Box V10(V->coords[0],V->coords[1]+Vside_Y/2, V->coords[0]+Voffset_X,V->coords[3]);
	Box V11(V->coords[0]+Vside_X/2,V->coords[1]+Vside_Y/2,V->coords[2],V->coords[3]);

	/*Box U00(U->coords[0],U->coords[1],U->coords[2]-(U->coords[2]-U->coords[0]+1)/2,U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U01(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U10(U->coords[0],U->coords[1]+(U->coords[3]-U->coords[1]+1)/2,U->coords[2]-(U->coords[2]-U->coords[0]+1)/2,U->coords[3]);
	Box U11(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1]+(U->coords[3]-U->coords[1]+1)/2,U->coords[2],U->coords[3]);
	Box V00(V->coords[0],V->coords[1],V->coords[2]-(V->coords[2]-V->coords[0]+1)/2,V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V01(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V10(V->coords[0],V->coords[1]+(V->coords[3]-V->coords[1]+1)/2,V->coords[2]-(V->coords[2]-V->coords[0]+1)/2,V->coords[3]);
	Box V11(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1]+(V->coords[3]-V->coords[1]+1)/2,V->coords[2],V->coords[3]);
	Box X00(X->coords[0],X->coords[1],X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X01(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X10(X->coords[0],X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]);
	Box X11(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2],X->coords[3]);*/

	if(callStackDepth > recursionDepth+4)
	{


	C(&X00, XData, &U00, UData, &V00, VData, callStackDepth+1);
	C(&X01, XData, &U00, UData, &V01, VData, callStackDepth+1);
	C(&X10, XData, &U10, UData, &V00, VData, callStackDepth+1);	C(&X11, XData, &U10, UData, &V01, VData, callStackDepth+1);


	C(&X00, XData, &U01, UData, &V10, VData, callStackDepth+1);
	C(&X01, XData, &U01, UData, &V11, VData, callStackDepth+1);
	C(&X10, XData, &U11, UData, &V10, VData, callStackDepth+1);	C(&X11, XData, &U11, UData, &V11, VData, callStackDepth+1);



	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X00, XData, &U00, UData, &V00, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X01, XData, &U00, UData, &V01, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X10, XData, &U10, UData, &V00, VData, callStackDepth+1);	C(&X11, XData, &U10, UData, &V01, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X00, XData, &U01, UData, &V10, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X01, XData, &U01, UData, &V11, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X10, XData, &U11, UData, &V10, VData, callStackDepth+1);	C(&X11, XData, &U11, UData, &V11, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif



	return;
}

void A_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDX);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, XData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}

	int Xside_X=(X->coords[2]-X->coords[0]+1), Xside_Y=(X->coords[3]-X->coords[1]+1);
	int Xoffset_X=Xside_X/2-1, Xoffset_Y=Xside_Y/2-1;
	if(Xoffset_X < 0)
		Xoffset_X=0;
	if(Xoffset_Y < 0)
		Xoffset_Y=0;
	Box X00(X->coords[0],X->coords[1],X->coords[0]+Xoffset_X,X->coords[1]+Xoffset_Y);
	Box X01(X->coords[0]+Xside_X/2,X->coords[1],X->coords[2],X->coords[1]+Xoffset_Y);
	Box X11(X->coords[0]+Xside_X/2,X->coords[1]+Xside_Y/2,X->coords[2],X->coords[3]);

	/*Box X00(X->coords[0],X->coords[1],X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X01(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X11(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2],X->coords[3]);*/

	A_unroll(&X00, XData, parentTileIDX*4+0, callStackDepth+1);	A_unroll(&X11, XData, parentTileIDX*4+3, callStackDepth+1);
	B_unroll(&X01, XData, parentTileIDX*4+1, &X00, XData, parentTileIDX*4+0, &X11, XData, parentTileIDX*4+3, callStackDepth+1);

	return;
}

void B_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDX);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*U);
			p.data = b3;
			p.tile = UData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*V);
			p.data = b6;
			p.tile = VData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, XData, b3, UData, b6, VData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID2D(parentTileIDU);
		readTileIDs[1]=GetTileID2D(parentTileIDV);
		tiles[0]=UData;
		tiles[1]=VData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}
	
	int Xside_X=(X->coords[2]-X->coords[0]+1), Xside_Y=(X->coords[3]-X->coords[1]+1);
	int Xoffset_X=Xside_X/2-1, Xoffset_Y=Xside_Y/2-1;
	if(Xoffset_X < 0)
		Xoffset_X=0;
	if(Xoffset_Y < 0)
		Xoffset_Y=0;
	Box X00(X->coords[0],X->coords[1],X->coords[0]+Xoffset_X,X->coords[1]+Xoffset_Y);
	Box X01(X->coords[0]+Xside_X/2,X->coords[1],X->coords[2],X->coords[1]+Xoffset_Y);
	Box X10(X->coords[0],X->coords[1]+Xside_Y/2, X->coords[0]+Xoffset_X,X->coords[3]);
	Box X11(X->coords[0]+Xside_X/2,X->coords[1]+Xside_Y/2,X->coords[2],X->coords[3]);

	int Uside_X=(U->coords[2]-U->coords[0]+1), Uside_Y=(U->coords[3]-U->coords[1]+1);
	int Uoffset_X=Uside_X/2-1, Uoffset_U=Uside_Y/2-1;
	if(Uoffset_X < 0)
		Uoffset_X=0;
	if(Uoffset_U < 0)
		Uoffset_U=0;
	Box U00(U->coords[0],U->coords[1],U->coords[0]+Uoffset_X,U->coords[1]+Uoffset_U);
	Box U01(U->coords[0]+Uside_X/2,U->coords[1],U->coords[2],U->coords[1]+Uoffset_U);
	Box U10(U->coords[0],U->coords[1]+Uside_Y/2, U->coords[0]+Uoffset_X,U->coords[3]);
	Box U11(U->coords[0]+Uside_X/2,U->coords[1]+Uside_Y/2,U->coords[2],U->coords[3]);

	int Vside_X=(V->coords[2]-V->coords[0]+1), Vside_Y=(V->coords[3]-V->coords[1]+1);
	int Voffset_X=Vside_X/2-1, Voffset_Y=Vside_Y/2-1;
	if(Voffset_X < 0)
		Voffset_X=0;
	if(Voffset_Y < 0)
		Voffset_Y=0;
	Box V00(V->coords[0],V->coords[1],V->coords[0]+Voffset_X,V->coords[1]+Voffset_Y);
	Box V01(V->coords[0]+Vside_X/2,V->coords[1],V->coords[2],V->coords[1]+Voffset_Y);
	Box V10(V->coords[0],V->coords[1]+Vside_Y/2, V->coords[0]+Voffset_X,V->coords[3]);
	Box V11(V->coords[0]+Vside_X/2,V->coords[1]+Vside_Y/2,V->coords[2],V->coords[3]);

	/*Box U00(U->coords[0],U->coords[1],U->coords[2]-(U->coords[2]-U->coords[0]+1)/2,U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U01(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U11(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1]+(U->coords[3]-U->coords[1]+1)/2,U->coords[2],U->coords[3]);
	Box V00(V->coords[0],V->coords[1],V->coords[2]-(V->coords[2]-V->coords[0]+1)/2,V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V01(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V11(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1]+(V->coords[3]-V->coords[1]+1)/2,V->coords[2],V->coords[3]);
	Box X00(X->coords[0],X->coords[1],X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X01(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X10(X->coords[0],X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]);
	Box X11(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2],X->coords[3]);*/

	B_unroll(&X10, XData, parentTileIDX*4+2, &U11, UData, parentTileIDU*4+3, &V00, VData, parentTileIDV*4+0, callStackDepth+1);
	C_unroll(&X00, XData, parentTileIDX*4+0, &U01, UData, parentTileIDU*4+1, &X10, XData, parentTileIDX*4+2, callStackDepth+1);	C_unroll(&X11, XData, parentTileIDX*4+3, &X10, XData, parentTileIDX*4+2, &V01, VData, parentTileIDV*4+1, callStackDepth+1);
	B_unroll(&X00, XData, parentTileIDX*4+0, &U00, UData, parentTileIDU*4+0, &V00, VData, parentTileIDV*4+0, callStackDepth+1);	B_unroll(&X11, XData, parentTileIDX*4+3, &U11, UData, parentTileIDU*4+3, &V11, VData, parentTileIDV*4+3, callStackDepth+1);
	C_unroll(&X01, XData, parentTileIDX*4+1, &U01, UData, parentTileIDU*4+1, &X11, XData, parentTileIDX*4+3, callStackDepth+1);
	C_unroll(&X01, XData, parentTileIDX*4+1, &X00, XData, parentTileIDX*4+0, &V01, VData, parentTileIDV*4+1, callStackDepth+1);
	B_unroll(&X01, XData, parentTileIDX*4+1, &U00, UData, parentTileIDU*4+0, &V11, VData, parentTileIDV*4+3, callStackDepth+1);

	return;
}

void C_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDX);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*U);
			p.data = b3;
			p.tile = UData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*V);
			p.data = b6;
			p.tile = VData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, XData, b3, UData, b6, VData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID2D(parentTileIDU);
		readTileIDs[1]=GetTileID2D(parentTileIDV);
		tiles[0]=UData;
		tiles[1]=VData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	int Xside_X=(X->coords[2]-X->coords[0]+1), Xside_Y=(X->coords[3]-X->coords[1]+1);
	int Xoffset_X=Xside_X/2-1, Xoffset_Y=Xside_Y/2-1;
	if(Xoffset_X < 0)
		Xoffset_X=0;
	if(Xoffset_Y < 0)
		Xoffset_Y=0;
	Box X00(X->coords[0],X->coords[1],X->coords[0]+Xoffset_X,X->coords[1]+Xoffset_Y);
	Box X01(X->coords[0]+Xside_X/2,X->coords[1],X->coords[2],X->coords[1]+Xoffset_Y);
	Box X10(X->coords[0],X->coords[1]+Xside_Y/2, X->coords[0]+Xoffset_X,X->coords[3]);
	Box X11(X->coords[0]+Xside_X/2,X->coords[1]+Xside_Y/2,X->coords[2],X->coords[3]);

	int Uside_X=(U->coords[2]-U->coords[0]+1), Uside_Y=(U->coords[3]-U->coords[1]+1);
	int Uoffset_X=Uside_X/2-1, Uoffset_U=Uside_Y/2-1;
	if(Uoffset_X < 0)
		Uoffset_X=0;
	if(Uoffset_U < 0)
		Uoffset_U=0;
	Box U00(U->coords[0],U->coords[1],U->coords[0]+Uoffset_X,U->coords[1]+Uoffset_U);
	Box U01(U->coords[0]+Uside_X/2,U->coords[1],U->coords[2],U->coords[1]+Uoffset_U);
	Box U10(U->coords[0],U->coords[1]+Uside_Y/2, U->coords[0]+Uoffset_X,U->coords[3]);
	Box U11(U->coords[0]+Uside_X/2,U->coords[1]+Uside_Y/2,U->coords[2],U->coords[3]);

	int Vside_X=(V->coords[2]-V->coords[0]+1), Vside_Y=(V->coords[3]-V->coords[1]+1);
	int Voffset_X=Vside_X/2-1, Voffset_Y=Vside_Y/2-1;
	if(Voffset_X < 0)
		Voffset_X=0;
	if(Voffset_Y < 0)
		Voffset_Y=0;
	Box V00(V->coords[0],V->coords[1],V->coords[0]+Voffset_X,V->coords[1]+Voffset_Y);
	Box V01(V->coords[0]+Vside_X/2,V->coords[1],V->coords[2],V->coords[1]+Voffset_Y);
	Box V10(V->coords[0],V->coords[1]+Vside_Y/2, V->coords[0]+Voffset_X,V->coords[3]);
	Box V11(V->coords[0]+Vside_X/2,V->coords[1]+Vside_Y/2,V->coords[2],V->coords[3]);

	/*Box U00(U->coords[0],U->coords[1],U->coords[2]-(U->coords[2]-U->coords[0]+1)/2,U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U01(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3]-(U->coords[3]-U->coords[1]+1)/2);
	Box U10(U->coords[0],U->coords[1]+(U->coords[3]-U->coords[1]+1)/2,U->coords[2]-(U->coords[2]-U->coords[0]+1)/2,U->coords[3]);
	Box U11(U->coords[0]+(U->coords[2]-U->coords[0]+1)/2,U->coords[1]+(U->coords[3]-U->coords[1]+1)/2,U->coords[2],U->coords[3]);
	Box V00(V->coords[0],V->coords[1],V->coords[2]-(V->coords[2]-V->coords[0]+1)/2,V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V01(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3]-(V->coords[3]-V->coords[1]+1)/2);
	Box V10(V->coords[0],V->coords[1]+(V->coords[3]-V->coords[1]+1)/2,V->coords[2]-(V->coords[2]-V->coords[0]+1)/2,V->coords[3]);
	Box V11(V->coords[0]+(V->coords[2]-V->coords[0]+1)/2,V->coords[1]+(V->coords[3]-V->coords[1]+1)/2,V->coords[2],V->coords[3]);
	Box X00(X->coords[0],X->coords[1],X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X01(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3]-(X->coords[3]-X->coords[1]+1)/2);
	Box X10(X->coords[0],X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2]-(X->coords[2]-X->coords[0]+1)/2,X->coords[3]);
	Box X11(X->coords[0]+(X->coords[2]-X->coords[0]+1)/2,X->coords[1]+(X->coords[3]-X->coords[1]+1)/2,X->coords[2],X->coords[3]);*/

	C_unroll(&X00, XData, parentTileIDX*4+0, &U00, UData, parentTileIDU*4+0, &V00, VData, parentTileIDV*4+0, callStackDepth+1);	C_unroll(&X01, XData, parentTileIDX*4+1, &U00, UData, parentTileIDU*4+0, &V01, VData, parentTileIDV*4+1, callStackDepth+1);	C_unroll(&X10, XData, parentTileIDX*4+2, &U10, UData, parentTileIDU*4+2, &V00, VData, parentTileIDV*4+0, callStackDepth+1);	C_unroll(&X11, XData, parentTileIDX*4+3, &U10, UData, parentTileIDU*4+2, &V01, VData, parentTileIDV*4+1, callStackDepth+1);
	C_unroll(&X00, XData, parentTileIDX*4+0, &U01, UData, parentTileIDU*4+1, &V10, VData, parentTileIDV*4+2, callStackDepth+1);	C_unroll(&X01, XData, parentTileIDX*4+1, &U01, UData, parentTileIDU*4+1, &V11, VData, parentTileIDV*4+3, callStackDepth+1);	C_unroll(&X10, XData, parentTileIDX*4+2, &U11, UData, parentTileIDU*4+3, &V10, VData, parentTileIDV*4+2, callStackDepth+1);	C_unroll(&X11, XData, parentTileIDX*4+3, &U11, UData, parentTileIDU*4+3, &V11, VData, parentTileIDV*4+3, callStackDepth+1);


	return;
}

void Unroll()
{
	int parentTileIDX=0;
	Box *X = new Box(0,0,inputSizeX[0],inputSizeX[1]);
	if(0 == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDX);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = nullData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	A_unroll(X, nullptr , 0, 0);
	delete X;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 1);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
			}
			break;
		default: break;
	}
}

