#include "HelperFunctions.h"
int fnCallHierarchySummary[4][4]={{4,2,2,1},{0,2,0,1},{0,0,2,1},{0,0,0,1}};
map<int, vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_FULL;
extern long int inputSizex[2];

#define _paramListFunctionA Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionD Box*, CELL_TYPE*, Box*, CELL_TYPE*, int

void A(Box* x, CELL_TYPE* xData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void A_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, int callStackDepth);
void B_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, int callStackDepth);
void C_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, int callStackDepth);
void D_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, int callStackDepth);
void A(Box* x, CELL_TYPE* xData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=x->coords[1], l=x->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,xData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;

		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	if(callStackDepth > recursionDepth+4)
	{

	A(&x00, xData, callStackDepth+1);

	B(&x01, xData, &x00, xData, callStackDepth+1);
	C(&x10, xData, &x00, xData, callStackDepth+1);	D(&x11, xData, &x00, xData, callStackDepth+1);


	A(&x01, xData, callStackDepth+1);	A(&x10, xData, callStackDepth+1);

	C(&x11, xData, &x01, xData, callStackDepth+1);
	B(&x11, xData, &x10, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);


	return;

	}

	A(&x00, xData, callStackDepth+1);

#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x01, xData, &x00, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x10, xData, &x00, xData, callStackDepth+1);	D(&x11, xData, &x00, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	A(&x01, xData, callStackDepth+1);	A(&x10, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&x11, xData, &x01, xData, callStackDepth+1);
	B(&x11, xData, &x10, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);


	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;

		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	if(callStackDepth > recursionDepth+4)
	{


	B(&x00, xData, &y01, yData, callStackDepth+1);	D(&x10, xData, &y01, yData, callStackDepth+1);

	B(&x10, xData, &y11, yData, callStackDepth+1);


	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x00, xData, &y01, yData, callStackDepth+1);	D(&x10, xData, &y01, yData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	B(&x10, xData, &y11, yData, callStackDepth+1);


	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;

		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	if(callStackDepth > recursionDepth+4)
	{


	C(&x00, xData, &y10, yData, callStackDepth+1);	D(&x01, xData, &y10, yData, callStackDepth+1);

	C(&x01, xData, &y11, yData, callStackDepth+1);


	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x00, xData, &y10, yData, callStackDepth+1);	D(&x01, xData, &y10, yData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&x01, xData, &y11, yData, callStackDepth+1);


	return;
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		//Write the code for terminating case here.
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;

		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	D(&x00, xData, &y11, yData, callStackDepth+1);


	return;
}

void A_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, xData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A_unroll(&x00, xData, parentTileIDx*4+0, callStackDepth+1);
	B_unroll(&x01, xData, parentTileIDx*4+1, &x00, xData, parentTileIDx*4+0, callStackDepth+1);	C_unroll(&x10, xData, parentTileIDx*4+2, &x00, xData, parentTileIDx*4+0, callStackDepth+1);	D_unroll(&x11, xData, parentTileIDx*4+3, &x00, xData, parentTileIDx*4+0, callStackDepth+1);
	A_unroll(&x01, xData, parentTileIDx*4+1, callStackDepth+1);	A_unroll(&x10, xData, parentTileIDx*4+2, callStackDepth+1);
	C_unroll(&x11, xData, parentTileIDx*4+3, &x01, xData, parentTileIDx*4+1, callStackDepth+1);
	B_unroll(&x11, xData, parentTileIDx*4+3, &x10, xData, parentTileIDx*4+2, callStackDepth+1);
	A_unroll(&x11, xData, parentTileIDx*4+3, callStackDepth+1);


	return;
}

void B_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*y);
			p.data = b3;
			p.tile = yData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, xData, b3, yData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[1];
		CELL_TYPE* tiles[1];
		readTileIDs[0]=GetTileID2D(parentTileIDy);
		tiles[0]=yData;
		for(int i=0;i<1;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	B_unroll(&x00, xData, parentTileIDx*4+0, &y01, yData, parentTileIDy*4+1, callStackDepth+1);	D_unroll(&x10, xData, parentTileIDx*4+2, &y01, yData, parentTileIDy*4+1, callStackDepth+1);
	B_unroll(&x10, xData, parentTileIDx*4+2, &y11, yData, parentTileIDy*4+3, callStackDepth+1);


	return;
}

void C_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*y);
			p.data = b3;
			p.tile = yData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, xData, b3, yData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[1];
		CELL_TYPE* tiles[1];
		readTileIDs[0]=GetTileID2D(parentTileIDy);
		tiles[0]=yData;
		for(int i=0;i<1;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	C_unroll(&x00, xData, parentTileIDx*4+0, &y10, yData, parentTileIDy*4+2, callStackDepth+1);	D_unroll(&x01, xData, parentTileIDx*4+1, &y10, yData, parentTileIDy*4+2, callStackDepth+1);
	C_unroll(&x01, xData, parentTileIDx*4+1, &y11, yData, parentTileIDy*4+3, callStackDepth+1);


	return;
}

void D_unroll(Box* x, CELL_TYPE* xData, int parentTileIDx, Box* y, CELL_TYPE* yData, int parentTileIDy, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'D';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = xData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*y);
			p.data = b3;
			p.tile = yData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionD> t = std::make_tuple(b0, xData, b3, yData, callStackDepth+1);
			DeferredCall<_paramListFunctionD>* defdCall = new DeferredCall<_paramListFunctionD>();
			defdCall->params=t;
			defdCall->fptr=D;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[1];
		CELL_TYPE* tiles[1];
		readTileIDs[0]=GetTileID2D(parentTileIDy);
		tiles[0]=yData;
		for(int i=0;i<1;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	D_unroll(&x00, xData, parentTileIDx*4+0, &y11, yData, parentTileIDy*4+3, callStackDepth+1);


	return;
}

void Unroll()
{
	int parentTileIDx=0;
	Box *x = new Box(0,0,inputSizex[0],inputSizex[1]);
	if(0 == recursionDepth)
	{
		int writeTileID = GetTileID2D(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			p.tile = nullData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	A_unroll(x, nullptr , 0, 0);
	delete x;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 1);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 2);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 2);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
			}
			break;
		case 'D':
			{
				assert(dataRegions.size() == 2);
				DeferredCall<_paramListFunctionD>* df = (reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				(reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall))->Run();
			}
			break;
		default: break;
	}
}

