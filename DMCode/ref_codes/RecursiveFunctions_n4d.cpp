#include "HelperFunctions.h"
int fnCallHierarchySummary[4][4]={{4,2,2,3},{0,6,14,3},{0,0,16,0},{0,0,14,9}};
map<int, vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_SPARSE;
extern long int inputSizeX[4];

#define _paramListFunctionA Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionD Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int

void A(Box* X, CELL_TYPE* XData, int callStackDepth);
void B(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth);
void C(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth);
void D(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth);
void A_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, int callStackDepth);
void B_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth);
void C_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth);
void D_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth);
void A(Box* X, CELL_TYPE* XData, int callStackDepth)
{
	if((X->coords[0]==X->coords[4]) && (X->coords[1]==X->coords[5]) && (X->coords[2]==X->coords[6]) && (X->coords[3]==X->coords[7]))
	{
		//Write the code for terminating case here.
		return;
	}

	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	if(callStackDepth > recursionDepth+4)
	{


	A(&X0000, XData, callStackDepth+1);
	A(&X0011, XData, callStackDepth+1);
	A(&X1100, XData, callStackDepth+1);	A(&X1111, XData, callStackDepth+1);


	B(&X0001, XData, &X0000, XData, &X0011, XData, callStackDepth+1);
	D(&X0100, XData, &X0000, XData, &X1100, XData, callStackDepth+1);
	D(&X0111, XData, &X0011, XData, &X1111, XData, callStackDepth+1);	B(&X1101, XData, &X1100, XData, &X1111, XData, callStackDepth+1);

	C(&X0101, XData, &X0001, XData, &X0111, XData, callStackDepth+1);
	C(&X0101, XData, &X0100, XData, &X1101, XData, callStackDepth+1);
	D(&X0101, XData, &X0000, XData, &X1111, XData, callStackDepth+1);

	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	A(&X0000, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	A(&X0011, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	A(&X1100, XData, callStackDepth+1);	A(&X1111, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&X0001, XData, &X0000, XData, &X0011, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0100, XData, &X0000, XData, &X1100, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0111, XData, &X0011, XData, &X1111, XData, callStackDepth+1);	B(&X1101, XData, &X1100, XData, &X1111, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&X0101, XData, &X0001, XData, &X0111, XData, callStackDepth+1);
	C(&X0101, XData, &X0100, XData, &X1101, XData, callStackDepth+1);
	D(&X0101, XData, &X0000, XData, &X1111, XData, callStackDepth+1);

	return;
}

void B(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth)
{
	if((X->coords[0]==X->coords[4]) && (X->coords[1]==X->coords[5]) && (X->coords[2]==X->coords[6]) && (X->coords[3]==X->coords[7]))
	{
		//Write the code for terminating case here.
		return;
	}

	Box U0000(U->coords[0],U->coords[1],U->coords[2],U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0001(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0011(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2],U->coords[3],U->coords[4],U->coords[5],U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5],U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U1100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5],U->coords[6],U->coords[7]);
	Box V0000(V->coords[0],V->coords[1],V->coords[2],V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0001(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0011(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2],V->coords[3],V->coords[4],V->coords[5],V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5],V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V1100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5],V->coords[6],V->coords[7]);
	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	if(callStackDepth > recursionDepth+4)
	{


	B(&X0000, XData, &U0000, UData, &V0000, VData, callStackDepth+1);
	B(&X0011, XData, &U0011, UData, &V0011, VData, callStackDepth+1);
	B(&X1100, XData, &U1100, UData, &V1100, VData, callStackDepth+1);	B(&X1111, XData, &U1111, UData, &V1111, VData, callStackDepth+1);


	C(&X0001, XData, &U0001, UData, &X0011, XData, callStackDepth+1);
	C(&X0100, XData, &X0000, XData, &V0100, VData, callStackDepth+1);
	C(&X0111, XData, &X0011, XData, &V0111, VData, callStackDepth+1);	C(&X1101, XData, &U1101, UData, &X1111, XData, callStackDepth+1);


	C(&X0001, XData, &X0000, XData, &V0001, VData, callStackDepth+1);
	C(&X0100, XData, &U0100, UData, &X1100, XData, callStackDepth+1);
	C(&X0111, XData, &U0111, UData, &X1111, XData, callStackDepth+1);	C(&X1101, XData, &X1100, XData, &V1101, VData, callStackDepth+1);


	B(&X0001, XData, &U0000, UData, &V0011, VData, callStackDepth+1);
	D(&X0100, XData, &U0000, UData, &V1100, VData, callStackDepth+1);
	D(&X0111, XData, &U0011, UData, &V1111, VData, callStackDepth+1);	B(&X1101, XData, &U1100, UData, &V1111, VData, callStackDepth+1);

	C(&X0101, XData, &U0001, UData, &X0111, XData, callStackDepth+1);
	C(&X0101, XData, &X0000, XData, &V0101, VData, callStackDepth+1);
	C(&X0101, XData, &X0001, XData, &V0111, VData, callStackDepth+1);
	C(&X0101, XData, &U0100, UData, &X1101, XData, callStackDepth+1);
	C(&X0101, XData, &U0101, UData, &X1111, XData, callStackDepth+1);
	C(&X0101, XData, &X0100, XData, &V1101, VData, callStackDepth+1);
	D(&X0101, XData, &U0000, UData, &V1111, VData, callStackDepth+1);

	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&X0000, XData, &U0000, UData, &V0000, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	B(&X0011, XData, &U0011, UData, &V0011, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	B(&X1100, XData, &U1100, UData, &V1100, VData, callStackDepth+1);	B(&X1111, XData, &U1111, UData, &V1111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0001, XData, &U0001, UData, &X0011, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0100, XData, &X0000, XData, &V0100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0111, XData, &X0011, XData, &V0111, VData, callStackDepth+1);	C(&X1101, XData, &U1101, UData, &X1111, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0001, XData, &X0000, XData, &V0001, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0100, XData, &U0100, UData, &X1100, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0111, XData, &U0111, UData, &X1111, XData, callStackDepth+1);	C(&X1101, XData, &X1100, XData, &V1101, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	B(&X0001, XData, &U0000, UData, &V0011, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0100, XData, &U0000, UData, &V1100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0111, XData, &U0011, UData, &V1111, VData, callStackDepth+1);	B(&X1101, XData, &U1100, UData, &V1111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&X0101, XData, &U0001, UData, &X0111, XData, callStackDepth+1);
	C(&X0101, XData, &X0000, XData, &V0101, VData, callStackDepth+1);
	C(&X0101, XData, &X0001, XData, &V0111, VData, callStackDepth+1);
	C(&X0101, XData, &U0100, UData, &X1101, XData, callStackDepth+1);
	C(&X0101, XData, &U0101, UData, &X1111, XData, callStackDepth+1);
	C(&X0101, XData, &X0100, XData, &V1101, VData, callStackDepth+1);
	D(&X0101, XData, &U0000, UData, &V1111, VData, callStackDepth+1);

	return;
}

void C(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth)
{
	if((X->coords[0]==X->coords[4]) && (X->coords[1]==X->coords[5]) && (X->coords[2]==X->coords[6]) && (X->coords[3]==X->coords[7]))
	{
		//Write the code for terminating case here.
		return;
	}

	Box U0000(U->coords[0],U->coords[1],U->coords[2],U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0001(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0011(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2],U->coords[3],U->coords[4],U->coords[5],U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5],U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U1100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5],U->coords[6],U->coords[7]);
	Box V0000(V->coords[0],V->coords[1],V->coords[2],V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0001(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0011(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2],V->coords[3],V->coords[4],V->coords[5],V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5],V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V1100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5],V->coords[6],V->coords[7]);
	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	if(callStackDepth > recursionDepth+4)
	{


	C(&X0000, XData, &U0000, UData, &V0000, VData, callStackDepth+1);
	C(&X0001, XData, &U0000, UData, &V0001, VData, callStackDepth+1);
	C(&X0100, XData, &U0000, UData, &V0100, VData, callStackDepth+1);
	C(&X0101, XData, &U0000, UData, &V0101, VData, callStackDepth+1);
	C(&X0011, XData, &U0011, UData, &V0011, VData, callStackDepth+1);
	C(&X0111, XData, &U0011, UData, &V0111, VData, callStackDepth+1);
	C(&X1100, XData, &U1100, UData, &V1100, VData, callStackDepth+1);
	C(&X1101, XData, &U1100, UData, &V1101, VData, callStackDepth+1);	C(&X1111, XData, &U1111, UData, &V1111, VData, callStackDepth+1);


	C(&X0001, XData, &U0001, UData, &V0011, VData, callStackDepth+1);
	C(&X0100, XData, &U0100, UData, &V1100, VData, callStackDepth+1);
	C(&X0101, XData, &U0001, UData, &V0111, VData, callStackDepth+1);
	C(&X0111, XData, &U0111, UData, &V1111, VData, callStackDepth+1);	C(&X1101, XData, &U1101, UData, &V1111, VData, callStackDepth+1);

	C(&X0101, XData, &U0100, UData, &V1101, VData, callStackDepth+1);
	C(&X0101, XData, &U0101, UData, &V1111, VData, callStackDepth+1);

	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0000, XData, &U0000, UData, &V0000, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0001, XData, &U0000, UData, &V0001, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0100, XData, &U0000, UData, &V0100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0101, XData, &U0000, UData, &V0101, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0011, XData, &U0011, UData, &V0011, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0111, XData, &U0011, UData, &V0111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X1100, XData, &U1100, UData, &V1100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X1101, XData, &U1100, UData, &V1101, VData, callStackDepth+1);	C(&X1111, XData, &U1111, UData, &V1111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0001, XData, &U0001, UData, &V0011, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0100, XData, &U0100, UData, &V1100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0101, XData, &U0001, UData, &V0111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0111, XData, &U0111, UData, &V1111, VData, callStackDepth+1);	C(&X1101, XData, &U1101, UData, &V1111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&X0101, XData, &U0100, UData, &V1101, VData, callStackDepth+1);
	C(&X0101, XData, &U0101, UData, &V1111, VData, callStackDepth+1);

	return;
}

void D(Box* X, CELL_TYPE* XData, Box* U, CELL_TYPE* UData, Box* V, CELL_TYPE* VData, int callStackDepth)
{
	if((X->coords[0]==X->coords[4]) && (X->coords[1]==X->coords[5]) && (X->coords[2]==X->coords[6]) && (X->coords[3]==X->coords[7]))
	{
		//Write the code for terminating case here.
		return;
	}

	Box U0000(U->coords[0],U->coords[1],U->coords[2],U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0001(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0011(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2],U->coords[3],U->coords[4],U->coords[5],U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5],U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U1100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5],U->coords[6],U->coords[7]);
	Box V0000(V->coords[0],V->coords[1],V->coords[2],V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0001(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0011(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2],V->coords[3],V->coords[4],V->coords[5],V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5],V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V1100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5],V->coords[6],V->coords[7]);
	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	if(callStackDepth > recursionDepth+4)
	{


	D(&X0000, XData, &U0000, UData, &V0000, VData, callStackDepth+1);
	D(&X0011, XData, &U0011, UData, &V0011, VData, callStackDepth+1);
	D(&X1100, XData, &U1100, UData, &V1100, VData, callStackDepth+1);	D(&X1111, XData, &U1111, UData, &V1111, VData, callStackDepth+1);


	C(&X0001, XData, &U0001, UData, &X0011, XData, callStackDepth+1);
	C(&X0100, XData, &U0100, UData, &X1100, XData, callStackDepth+1);
	C(&X0111, XData, &U0111, UData, &X1111, XData, callStackDepth+1);	C(&X1101, XData, &U1101, UData, &X1111, XData, callStackDepth+1);


	C(&X0001, XData, &X0000, XData, &V0001, VData, callStackDepth+1);
	C(&X0100, XData, &X0000, XData, &V0100, VData, callStackDepth+1);
	C(&X0111, XData, &X0011, XData, &V0111, VData, callStackDepth+1);	C(&X1101, XData, &X1100, XData, &V1101, VData, callStackDepth+1);


	D(&X0001, XData, &U0000, UData, &V0011, VData, callStackDepth+1);
	D(&X0100, XData, &U0000, UData, &V1100, VData, callStackDepth+1);
	D(&X0111, XData, &U0011, UData, &V1111, VData, callStackDepth+1);	D(&X1101, XData, &U1100, UData, &V1111, VData, callStackDepth+1);

	C(&X0101, XData, &U0001, UData, &X0111, XData, callStackDepth+1);
	C(&X0101, XData, &U0100, UData, &X1101, XData, callStackDepth+1);
	C(&X0101, XData, &U0101, UData, &X1111, XData, callStackDepth+1);
	C(&X0101, XData, &X0000, XData, &V0101, VData, callStackDepth+1);
	C(&X0101, XData, &X0001, XData, &V0111, VData, callStackDepth+1);
	C(&X0101, XData, &X0100, XData, &V1101, VData, callStackDepth+1);
	D(&X0101, XData, &U0000, UData, &V1111, VData, callStackDepth+1);


	return;

	}


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0000, XData, &U0000, UData, &V0000, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0011, XData, &U0011, UData, &V0011, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X1100, XData, &U1100, UData, &V1100, VData, callStackDepth+1);	D(&X1111, XData, &U1111, UData, &V1111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0001, XData, &U0001, UData, &X0011, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0100, XData, &U0100, UData, &X1100, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0111, XData, &U0111, UData, &X1111, XData, callStackDepth+1);	C(&X1101, XData, &U1101, UData, &X1111, XData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0001, XData, &X0000, XData, &V0001, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0100, XData, &X0000, XData, &V0100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&X0111, XData, &X0011, XData, &V0111, VData, callStackDepth+1);	C(&X1101, XData, &X1100, XData, &V1101, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif


#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0001, XData, &U0000, UData, &V0011, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0100, XData, &U0000, UData, &V1100, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	D(&X0111, XData, &U0011, UData, &V1111, VData, callStackDepth+1);	D(&X1101, XData, &U1100, UData, &V1111, VData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&X0101, XData, &U0001, UData, &X0111, XData, callStackDepth+1);
	C(&X0101, XData, &U0100, UData, &X1101, XData, callStackDepth+1);
	C(&X0101, XData, &U0101, UData, &X1111, XData, callStackDepth+1);
	C(&X0101, XData, &X0000, XData, &V0101, VData, callStackDepth+1);
	C(&X0101, XData, &X0001, XData, &V0111, VData, callStackDepth+1);
	C(&X0101, XData, &X0100, XData, &V1101, VData, callStackDepth+1);
	D(&X0101, XData, &U0000, UData, &V1111, VData, callStackDepth+1);


	return;
}

void A_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(X);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(true)
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, XData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}

	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	A_unroll(&X0000, XData, parentTileIDX*16+0, callStackDepth+1);	A_unroll(&X0011, XData, parentTileIDX*16+3, callStackDepth+1);	A_unroll(&X1100, XData, parentTileIDX*16+12, callStackDepth+1);	A_unroll(&X1111, XData, parentTileIDX*16+15, callStackDepth+1);
	B_unroll(&X0001, XData, parentTileIDX*16+1, &X0000, XData, parentTileIDX*16+0, &X0011, XData, parentTileIDX*16+3, callStackDepth+1);	D_unroll(&X0100, XData, parentTileIDX*16+4, &X0000, XData, parentTileIDX*16+0, &X1100, XData, parentTileIDX*16+12, callStackDepth+1);	D_unroll(&X0111, XData, parentTileIDX*16+7, &X0011, XData, parentTileIDX*16+3, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);	B_unroll(&X1101, XData, parentTileIDX*16+13, &X1100, XData, parentTileIDX*16+12, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0001, XData, parentTileIDX*16+1, &X0111, XData, parentTileIDX*16+7, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0100, XData, parentTileIDX*16+4, &X1101, XData, parentTileIDX*16+13, callStackDepth+1);
	D_unroll(&X0101, XData, parentTileIDX*16+5, &X0000, XData, parentTileIDX*16+0, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);

	return;
}

void B_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(X);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(true)
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*U);
			p.data = b3;
			p.tile = UData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*V);
			p.data = b6;
			p.tile = VData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, XData, b3, UData, b6, VData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID(U);
		readTileIDs[1]=GetTileID(V);
		tiles[0]=UData;
		tiles[1]=VData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box U0000(U->coords[0],U->coords[1],U->coords[2],U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0001(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0011(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2],U->coords[3],U->coords[4],U->coords[5],U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5],U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U1100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5],U->coords[6],U->coords[7]);
	Box V0000(V->coords[0],V->coords[1],V->coords[2],V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0001(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0011(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2],V->coords[3],V->coords[4],V->coords[5],V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5],V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V1100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5],V->coords[6],V->coords[7]);
	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	B_unroll(&X0000, XData, parentTileIDX*16+0, &U0000, UData, parentTileIDU*16+0, &V0000, VData, parentTileIDV*16+0, callStackDepth+1);	B_unroll(&X0011, XData, parentTileIDX*16+3, &U0011, UData, parentTileIDU*16+3, &V0011, VData, parentTileIDV*16+3, callStackDepth+1);	B_unroll(&X1100, XData, parentTileIDX*16+12, &U1100, UData, parentTileIDU*16+12, &V1100, VData, parentTileIDV*16+12, callStackDepth+1);	B_unroll(&X1111, XData, parentTileIDX*16+15, &U1111, UData, parentTileIDU*16+15, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);
	C_unroll(&X0001, XData, parentTileIDX*16+1, &U0001, UData, parentTileIDU*16+1, &X0011, XData, parentTileIDX*16+3, callStackDepth+1);	C_unroll(&X0100, XData, parentTileIDX*16+4, &X0000, XData, parentTileIDX*16+0, &V0100, VData, parentTileIDV*16+4, callStackDepth+1);	C_unroll(&X0111, XData, parentTileIDX*16+7, &X0011, XData, parentTileIDX*16+3, &V0111, VData, parentTileIDV*16+7, callStackDepth+1);	C_unroll(&X1101, XData, parentTileIDX*16+13, &U1101, UData, parentTileIDU*16+13, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);
	C_unroll(&X0001, XData, parentTileIDX*16+1, &X0000, XData, parentTileIDX*16+0, &V0001, VData, parentTileIDV*16+1, callStackDepth+1);	C_unroll(&X0100, XData, parentTileIDX*16+4, &U0100, UData, parentTileIDU*16+4, &X1100, XData, parentTileIDX*16+12, callStackDepth+1);	C_unroll(&X0111, XData, parentTileIDX*16+7, &U0111, UData, parentTileIDU*16+7, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);	C_unroll(&X1101, XData, parentTileIDX*16+13, &X1100, XData, parentTileIDX*16+12, &V1101, VData, parentTileIDV*16+13, callStackDepth+1);
	B_unroll(&X0001, XData, parentTileIDX*16+1, &U0000, UData, parentTileIDU*16+0, &V0011, VData, parentTileIDV*16+3, callStackDepth+1);	D_unroll(&X0100, XData, parentTileIDX*16+4, &U0000, UData, parentTileIDU*16+0, &V1100, VData, parentTileIDV*16+12, callStackDepth+1);	D_unroll(&X0111, XData, parentTileIDX*16+7, &U0011, UData, parentTileIDU*16+3, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);	B_unroll(&X1101, XData, parentTileIDX*16+13, &U1100, UData, parentTileIDU*16+12, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0001, UData, parentTileIDU*16+1, &X0111, XData, parentTileIDX*16+7, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0000, XData, parentTileIDX*16+0, &V0101, VData, parentTileIDV*16+5, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0001, XData, parentTileIDX*16+1, &V0111, VData, parentTileIDV*16+7, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0100, UData, parentTileIDU*16+4, &X1101, XData, parentTileIDX*16+13, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0101, UData, parentTileIDU*16+5, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0100, XData, parentTileIDX*16+4, &V1101, VData, parentTileIDV*16+13, callStackDepth+1);
	D_unroll(&X0101, XData, parentTileIDX*16+5, &U0000, UData, parentTileIDU*16+0, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);

	return;
}

void C_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(X);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(true)
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*U);
			p.data = b3;
			p.tile = UData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*V);
			p.data = b6;
			p.tile = VData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, XData, b3, UData, b6, VData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID(U);
		readTileIDs[1]=GetTileID(V);
		tiles[0]=UData;
		tiles[1]=VData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box U0000(U->coords[0],U->coords[1],U->coords[2],U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0001(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0011(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2],U->coords[3],U->coords[4],U->coords[5],U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5],U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U1100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5],U->coords[6],U->coords[7]);
	Box V0000(V->coords[0],V->coords[1],V->coords[2],V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0001(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0011(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2],V->coords[3],V->coords[4],V->coords[5],V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5],V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V1100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5],V->coords[6],V->coords[7]);
	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	C_unroll(&X0000, XData, parentTileIDX*16+0, &U0000, UData, parentTileIDU*16+0, &V0000, VData, parentTileIDV*16+0, callStackDepth+1);	C_unroll(&X0001, XData, parentTileIDX*16+1, &U0000, UData, parentTileIDU*16+0, &V0001, VData, parentTileIDV*16+1, callStackDepth+1);	C_unroll(&X0100, XData, parentTileIDX*16+4, &U0000, UData, parentTileIDU*16+0, &V0100, VData, parentTileIDV*16+4, callStackDepth+1);	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0000, UData, parentTileIDU*16+0, &V0101, VData, parentTileIDV*16+5, callStackDepth+1);	C_unroll(&X0011, XData, parentTileIDX*16+3, &U0011, UData, parentTileIDU*16+3, &V0011, VData, parentTileIDV*16+3, callStackDepth+1);	C_unroll(&X0111, XData, parentTileIDX*16+7, &U0011, UData, parentTileIDU*16+3, &V0111, VData, parentTileIDV*16+7, callStackDepth+1);	C_unroll(&X1100, XData, parentTileIDX*16+12, &U1100, UData, parentTileIDU*16+12, &V1100, VData, parentTileIDV*16+12, callStackDepth+1);	C_unroll(&X1101, XData, parentTileIDX*16+13, &U1100, UData, parentTileIDU*16+12, &V1101, VData, parentTileIDV*16+13, callStackDepth+1);	C_unroll(&X1111, XData, parentTileIDX*16+15, &U1111, UData, parentTileIDU*16+15, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);
	C_unroll(&X0001, XData, parentTileIDX*16+1, &U0001, UData, parentTileIDU*16+1, &V0011, VData, parentTileIDV*16+3, callStackDepth+1);	C_unroll(&X0100, XData, parentTileIDX*16+4, &U0100, UData, parentTileIDU*16+4, &V1100, VData, parentTileIDV*16+12, callStackDepth+1);	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0001, UData, parentTileIDU*16+1, &V0111, VData, parentTileIDV*16+7, callStackDepth+1);	C_unroll(&X0111, XData, parentTileIDX*16+7, &U0111, UData, parentTileIDU*16+7, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);	C_unroll(&X1101, XData, parentTileIDX*16+13, &U1101, UData, parentTileIDU*16+13, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0100, UData, parentTileIDU*16+4, &V1101, VData, parentTileIDV*16+13, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0101, UData, parentTileIDU*16+5, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);

	return;
}

void D_unroll(Box* X, CELL_TYPE* XData, int parentTileIDX, Box* U, CELL_TYPE* UData, int parentTileIDU, Box* V, CELL_TYPE* VData, int parentTileIDV, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(X);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(true)
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'D';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = XData;
			fnCall->params.push_back(p);
			Box* b3=new Box(*U);
			p.data = b3;
			p.tile = UData;
			fnCall->params.push_back(p);
			Box* b6=new Box(*V);
			p.data = b6;
			p.tile = VData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionD> t = std::make_tuple(b0, XData, b3, UData, b6, VData, callStackDepth+1);
			DeferredCall<_paramListFunctionD>* defdCall = new DeferredCall<_paramListFunctionD>();
			defdCall->params=t;
			defdCall->fptr=D;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		CELL_TYPE* tiles[2];
		readTileIDs[0]=GetTileID(U);
		readTileIDs[1]=GetTileID(V);
		tiles[0]=UData;
		tiles[1]=VData;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			CELL_TYPE* curTile=tiles[i];
			if((curTile==nullptr) && (readTileID != writeTileID))
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
			}
			else if((curTile!=nullptr) && localUpdate)
				fnCall->params[i+1].portID = readTileID;
		}
		return;
	}

	Box U0000(U->coords[0],U->coords[1],U->coords[2],U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0001(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2],U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0011(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2],U->coords[3],U->coords[4],U->coords[5],U->coords[6]-(U->coords[6]-U->coords[2]+1)/2,U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U0111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3],U->coords[4],U->coords[5],U->coords[6],U->coords[7]-(U->coords[7]-U->coords[3]+1)/2);
	Box U1100(U->coords[0],U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4]-(U->coords[4]-U->coords[0]+1)/2,U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1101(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1],U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5]-(U->coords[5]-U->coords[1]+1)/2,U->coords[6],U->coords[7]);
	Box U1111(U->coords[0]+(U->coords[4]-U->coords[0]+1)/2,U->coords[1]+(U->coords[5]-U->coords[1]+1)/2,U->coords[2]+(U->coords[6]-U->coords[2]+1)/2,U->coords[3]+(U->coords[7]-U->coords[3]+1)/2,U->coords[4],U->coords[5],U->coords[6],U->coords[7]);
	Box V0000(V->coords[0],V->coords[1],V->coords[2],V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0001(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2],V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0011(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2],V->coords[3],V->coords[4],V->coords[5],V->coords[6]-(V->coords[6]-V->coords[2]+1)/2,V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V0111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3],V->coords[4],V->coords[5],V->coords[6],V->coords[7]-(V->coords[7]-V->coords[3]+1)/2);
	Box V1100(V->coords[0],V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4]-(V->coords[4]-V->coords[0]+1)/2,V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1101(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1],V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5]-(V->coords[5]-V->coords[1]+1)/2,V->coords[6],V->coords[7]);
	Box V1111(V->coords[0]+(V->coords[4]-V->coords[0]+1)/2,V->coords[1]+(V->coords[5]-V->coords[1]+1)/2,V->coords[2]+(V->coords[6]-V->coords[2]+1)/2,V->coords[3]+(V->coords[7]-V->coords[3]+1)/2,V->coords[4],V->coords[5],V->coords[6],V->coords[7]);
	Box X0000(X->coords[0],X->coords[1],X->coords[2],X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0001(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2],X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0011(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2],X->coords[3],X->coords[4],X->coords[5],X->coords[6]-(X->coords[6]-X->coords[2]+1)/2,X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X0111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3],X->coords[4],X->coords[5],X->coords[6],X->coords[7]-(X->coords[7]-X->coords[3]+1)/2);
	Box X1100(X->coords[0],X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4]-(X->coords[4]-X->coords[0]+1)/2,X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1101(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1],X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5]-(X->coords[5]-X->coords[1]+1)/2,X->coords[6],X->coords[7]);
	Box X1111(X->coords[0]+(X->coords[4]-X->coords[0]+1)/2,X->coords[1]+(X->coords[5]-X->coords[1]+1)/2,X->coords[2]+(X->coords[6]-X->coords[2]+1)/2,X->coords[3]+(X->coords[7]-X->coords[3]+1)/2,X->coords[4],X->coords[5],X->coords[6],X->coords[7]);

	D_unroll(&X0000, XData, parentTileIDX*16+0, &U0000, UData, parentTileIDU*16+0, &V0000, VData, parentTileIDV*16+0, callStackDepth+1);	D_unroll(&X0011, XData, parentTileIDX*16+3, &U0011, UData, parentTileIDU*16+3, &V0011, VData, parentTileIDV*16+3, callStackDepth+1);	D_unroll(&X1100, XData, parentTileIDX*16+12, &U1100, UData, parentTileIDU*16+12, &V1100, VData, parentTileIDV*16+12, callStackDepth+1);	D_unroll(&X1111, XData, parentTileIDX*16+15, &U1111, UData, parentTileIDU*16+15, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);
	C_unroll(&X0001, XData, parentTileIDX*16+1, &U0001, UData, parentTileIDU*16+1, &X0011, XData, parentTileIDX*16+3, callStackDepth+1);	C_unroll(&X0100, XData, parentTileIDX*16+4, &U0100, UData, parentTileIDU*16+4, &X1100, XData, parentTileIDX*16+12, callStackDepth+1);	C_unroll(&X0111, XData, parentTileIDX*16+7, &U0111, UData, parentTileIDU*16+7, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);	C_unroll(&X1101, XData, parentTileIDX*16+13, &U1101, UData, parentTileIDU*16+13, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);
	C_unroll(&X0001, XData, parentTileIDX*16+1, &X0000, XData, parentTileIDX*16+0, &V0001, VData, parentTileIDV*16+1, callStackDepth+1);	C_unroll(&X0100, XData, parentTileIDX*16+4, &X0000, XData, parentTileIDX*16+0, &V0100, VData, parentTileIDV*16+4, callStackDepth+1);	C_unroll(&X0111, XData, parentTileIDX*16+7, &X0011, XData, parentTileIDX*16+3, &V0111, VData, parentTileIDV*16+7, callStackDepth+1);	C_unroll(&X1101, XData, parentTileIDX*16+13, &X1100, XData, parentTileIDX*16+12, &V1101, VData, parentTileIDV*16+13, callStackDepth+1);
	D_unroll(&X0001, XData, parentTileIDX*16+1, &U0000, UData, parentTileIDU*16+0, &V0011, VData, parentTileIDV*16+3, callStackDepth+1);	D_unroll(&X0100, XData, parentTileIDX*16+4, &U0000, UData, parentTileIDU*16+0, &V1100, VData, parentTileIDV*16+12, callStackDepth+1);	D_unroll(&X0111, XData, parentTileIDX*16+7, &U0011, UData, parentTileIDU*16+3, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);	D_unroll(&X1101, XData, parentTileIDX*16+13, &U1100, UData, parentTileIDU*16+12, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0001, UData, parentTileIDU*16+1, &X0111, XData, parentTileIDX*16+7, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0100, UData, parentTileIDU*16+4, &X1101, XData, parentTileIDX*16+13, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &U0101, UData, parentTileIDU*16+5, &X1111, XData, parentTileIDX*16+15, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0000, XData, parentTileIDX*16+0, &V0101, VData, parentTileIDV*16+5, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0001, XData, parentTileIDX*16+1, &V0111, VData, parentTileIDV*16+7, callStackDepth+1);
	C_unroll(&X0101, XData, parentTileIDX*16+5, &X0100, XData, parentTileIDX*16+4, &V1101, VData, parentTileIDV*16+13, callStackDepth+1);
	D_unroll(&X0101, XData, parentTileIDX*16+5, &U0000, UData, parentTileIDU*16+0, &V1111, VData, parentTileIDV*16+15, callStackDepth+1);


	return;
}

void Unroll()
{
	int parentTileIDX=0;
	Box *X = new Box(0,0,0,0,inputSizeX[0],inputSizeX[1],inputSizeX[2],inputSizeX[3]);
	if(0 == recursionDepth)
	{
		int writeTileID = GetTileID(X);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(true)
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*X);
			p.data = b0;
			p.tile = nullData;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	A_unroll(X, nullptr , 0, 0);
	delete X;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 1);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
			}
			break;
		case 'D':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionD>* df = (reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				(reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall))->Run();
			}
			break;
		default: break;
	}
}

