#include "HelperFunctions.h"
#include<stack>
#include <mpi.h>
int fnCallHierarchySummary[3][3]={{2,1,0},{0,4,4},{0,0,8}};
long int inputSize[DIMENSION];
map<int, vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
extern map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_UTM;

#define _paramListFunctionA Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
void A_unroll(Box* x, int parentTileIDx, int callStackDepth);
void B_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, Box* z2, int parentTileIDz2, int callStackDepth);
void C_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z2, int parentTileIDz2, Box* z, int parentTileIDz, int callStackDepth);
void A(Box* x, CELL_TYPE* xData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, Box* z2, CELL_TYPE* z2Data, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z2, CELL_TYPE* z2Data, Box* z, CELL_TYPE* zData, int callStackDepth);
vector<int> matrixChain;
int curIndex_i, curIndex_j;
void BackTracing(CELL_TYPE* tile); 

void _ReadInput(char* fileName, vector<int>& matrixChain, int maxChainLength)
{
	char matrixDimStr[8];
	int matrixDim;
	ifstream inFile(fileName, ifstream::in);
	if(!fileName || !inFile.is_open())
	{
		printf("ERROR: Unable to open input file\n");
		exit(0);
	}
	while(!inFile.eof())
	{
		inFile.getline(matrixDimStr,8);
		if(inFile.eof())
			break;
		matrixDim = atoi(matrixDimStr);
		memset(matrixDimStr,0,8);
		matrixChain.push_back(matrixDim);
		if(matrixChain.size() == maxChainLength)
			break;
	}
}

int ReadInput(int argc, char* argv[])
{
	if((strcmp(argv[1],"-h")==0)||(argc==1))
	{
		printf("Usage: ./<exe> <input> <chainLength>\n");
		exit(0);
	}
	_ReadInput(argv[1],matrixChain, atoi(argv[2]));
	inputSize[0] = matrixChain.size()-1;inputSize[1] = matrixChain.size()-1;
	return inputSize[0];
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	unsigned int relIndx = 0;
	for(unsigned int i=b->coords[1];i<=b->coords[3];i++)
		for(unsigned int j=b->coords[0];j<=b->coords[2];j++)
			data[relIndx++] = (i==j)?0:INT_MAX;
	return;
}

void PrintResults()
{
	curIndex_i = inputSize[0];
	curIndex_j = 1;
	CELL_TYPE* tile = GetTileOfDPTable(inputSize[0],1);
	if(tile != nullptr)
	{
		CELL_TYPE* result = GetDPTableCell(1,inputSize[0],tile);
		if(result!=nullptr) printf("Optimum cost:%d\n",*result);
	}
	BackTracing(tile);
	//DebugPrintAllCells();
}

void BackTracing(CELL_TYPE* tile)
{
	CELL_TYPE* curDPTableTile = tile; 
	int *msgBktrac=new int[4];
	int backTracingProcID=-1;
	stack<pair<int,int> > s;
	vector<pair<int, int> > localResult;
	int containingSeq_i=-1, containingSeq_j=-1;
	if(tile != nullptr)
	{
		backTracingProcID = procRank;
		s.push(make_pair(-1,-1));
		s.push(make_pair(curIndex_j, curIndex_i));
		localResult.push_back(make_pair(curIndex_j, curIndex_i));
	}
	
	while(true)
	{
		bool forwardMsg = true;
		if(procRank != backTracingProcID)
		{
			MPI_Recv(msgBktrac, 4, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			curIndex_i = msgBktrac[0]; curIndex_j = msgBktrac[1];
			containingSeq_i = msgBktrac[2];containingSeq_j=msgBktrac[3];
			if((curIndex_i < 0) || (curIndex_j < 0))
				break;
			curDPTableTile = GetTileOfDPTable(curIndex_i, curIndex_j);
			assert(curDPTableTile != NULL);
			if(containingSeq_j == -1)
			{
				forwardMsg = false;
				if((s.size() == 0) && (curIndex_j == 1))
				{
					assert(s.size() == 0);
					s.push(make_pair(-1,-1));
					s.push(make_pair(curIndex_j, curIndex_i));
					localResult.push_back(make_pair(curIndex_j, curIndex_i));
				}
				curIndex_i = s.top().second;
				curIndex_j = s.top().first;
			}
		}
		
		if(containingSeq_j !=-1)
		{
			s.push(make_pair(-1,-1));
			s.push(make_pair(containingSeq_j, containingSeq_i));
			s.push(make_pair(curIndex_j, curIndex_i));
		}
		
		//printf("tile backtracing started.curIndex_i=%d curIndex_j=%d\n",curIndex_i,curIndex_j);
		assert(curDPTableTile != NULL);
		if(forwardMsg)
		{
			containingSeq_j = curIndex_j; containingSeq_i = curIndex_i;
		}
		else
		{
			containingSeq_i = msgBktrac[0];containingSeq_j=msgBktrac[1];
		}
		while(!s.empty())
		{
			pair<int,int> curSequence = s.top();
			s.pop();
			curIndex_j = curSequence.first;
			curIndex_i = curSequence.second;
			
			if((curIndex_i-curIndex_j) > 1)
			{
				if((curIndex_i < curDPTableTile[0]) || (curIndex_j < curDPTableTile[1]) || (curIndex_i >= (curDPTableTile[0]+curDPTableTile[2])) || (curIndex_j >= (curDPTableTile[1]+curDPTableTile[3])))
				{
					if((curIndex_i > 0) && (curIndex_j > 0))
					{
						int curTileID = GetZMortonTileIDFromCellCoords(curIndex_i-1,curIndex_j-1);
						backTracingProcID = GetOwner(GetTileID(curTileID));
						assert(s.size() > 0);
						if(s.top().first < 0)
						{
							s.pop();
							containingSeq_j = -1; containingSeq_i = -1;
						}
						if(backTracingProcID == procRank)
						{
							curDPTableTile = GetTileOfDPTable(curIndex_i,curIndex_j);
							s.push(make_pair(curIndex_j,curIndex_i));
							continue;
						}
						else
							break;
					}
					break;
				}
				else
				{
					CELL_TYPE* result = GetLTMCell(curIndex_j,curIndex_i,curDPTableTile);
					int newCurIndex_j = *result+1;
					s.push(make_pair(curIndex_j,*result));
					localResult.push_back(make_pair(curIndex_j, *result));
					s.push(make_pair(newCurIndex_j,curIndex_i));
					localResult.push_back(make_pair(newCurIndex_j, curIndex_i));
				}
			}
		}
		msgBktrac[0]=curIndex_i;
		msgBktrac[1]=curIndex_j;
		msgBktrac[2]=containingSeq_i;
		msgBktrac[3]=containingSeq_j;

		if((curIndex_i < 0) || (curIndex_j < 0))
		{
			for(int i=0;i<totalProcs;i++)
				if(i!= procRank)
					MPI_Send(msgBktrac, 4, MPI_INT, i, 0, MPI_COMM_WORLD);
			break;
		}
		else
		{
			if(backTracingProcID != procRank)
				MPI_Send(msgBktrac, 4, MPI_INT, backTracingProcID, 0, MPI_COMM_WORLD);
		}
		//printf("tile backtracing finished.curIndex_i=%d curIndex_j=%d\n",curIndex_i,curIndex_j);
	}
#ifdef PRINT_RESULTS
	//printf("Fully parenthesized sequence:\n");	
	for(int i=0;i<localResult.size();i++)
	{
		printf("(%d %d) ",(localResult[i].first),(localResult[i].second));
	}
	printf("\n");
#endif
	delete [] msgBktrac;
}

void A_unroll(Box* x, int parentTileIDx, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A_unroll(&x00, parentTileIDx*4+0, callStackDepth+1);
	A_unroll(&x11, parentTileIDx*4+3, callStackDepth+1);

	B_unroll(&x01, parentTileIDx*4+1, &x00, parentTileIDx*4+0, &x11, parentTileIDx*4+3, &x11, parentTileIDx*4+3, callStackDepth+1);

	return;
}

void B_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, Box* z2, int parentTileIDz2, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*z);
			p.data = b4;
			fnCall->params.push_back(p);
			Box* b6=new Box(*z2);
			p.data = b6;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, b6, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[3], readZOrderIDs[3];
		readTileIDs[0]=GetTileID(parentTileIDy);
		readTileIDs[1]=GetTileID(parentTileIDz);
		readTileIDs[2]=GetTileID(parentTileIDz2);
		readZOrderIDs[0]=parentTileIDy;
		readZOrderIDs[1]=parentTileIDz;
		readZOrderIDs[2]=parentTileIDz2;
		for(int i=0;i<3;i++)
		{
			int readTileID=readTileIDs[i];
			int readZOrderID=readZOrderIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readZOrderID].size() > 0)
					(fnCalls[readZOrderID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readZOrderID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readZOrderID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z00(z->coords[0],z->coords[1],z->coords[0]+(z->coords[2]-z->coords[0]+1)/2-1,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2-1);
	Box z11(z00.coords[2]+1,z00.coords[3]+1,z->coords[2],z->coords[3]);

	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);
	Box z221(z2->coords[0],z222.coords[1],z211.coords[2],z2->coords[3]);
	if(z221.coords[1] > z221.coords[0])
	{
		z221 = z211;
	}

	B_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, &z00, parentTileIDz*4+0, &z211, parentTileIDz2*4+0, callStackDepth+1);
	C_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, &z211, parentTileIDz2*4+0,&x10, parentTileIDx*4+2, callStackDepth+1);
	C_unroll(&x11, parentTileIDx*4+3, &x10, parentTileIDx*4+2, &z11, parentTileIDz*4+3, &z01, parentTileIDz*4+1, callStackDepth+1);

	B_unroll(&x00, parentTileIDx*4+0, &y00, parentTileIDy*4+0, &z00, parentTileIDz*4+0, &x10, parentTileIDx*4+2, callStackDepth+1);
	B_unroll(&x11, parentTileIDx*4+3, &y11, parentTileIDy*4+3, &z11, parentTileIDz*4+3, &z212, parentTileIDz2*4+1, callStackDepth+1);

	C_unroll(&x01, parentTileIDx*4+1, &y01, parentTileIDy*4+1, &z212, parentTileIDz2*4+1, &x11, parentTileIDx*4+3, callStackDepth+1);
	C_unroll(&x01, parentTileIDx*4+1, &x00, parentTileIDx*4+0, &z11, parentTileIDz*4+3, &z01, parentTileIDz*4+1, callStackDepth+1);
	B_unroll(&x01, parentTileIDx*4+1, &y00, parentTileIDy*4+0, &z11, parentTileIDz*4+3, &x11, parentTileIDx*4+3, callStackDepth+1);

	return;
}

void C_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z2, int parentTileIDz2, Box* z, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*z2);
			p.data = b4;
			fnCall->params.push_back(p);
			Box* b6=new Box(*z);
			p.data = b6;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, b6, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[3], readZOrderIDs[3];
		readTileIDs[0]=GetTileID(parentTileIDy);
		readTileIDs[1]=GetTileID(parentTileIDz2);
		readTileIDs[2]=GetTileID(parentTileIDz);
		readZOrderIDs[0]=parentTileIDy;
		readZOrderIDs[1]=parentTileIDz2;
		readZOrderIDs[2]=parentTileIDz;
		for(int i=0;i<3;i++)
		{
			int readTileID=readTileIDs[i];
			int readZOrderID=readZOrderIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readZOrderID].size() > 0)
					(fnCalls[readZOrderID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readZOrderID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readZOrderID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);
	Box z221(z2->coords[0],z222.coords[1],z211.coords[2],z2->coords[3]);

	C_unroll(&x00, parentTileIDx*4+0, &y00, parentTileIDy*4+0, &z10, parentTileIDz*4+2, &z00, parentTileIDz*4+0, callStackDepth+1);
	C_unroll(&x01, parentTileIDx*4+1, &y00, parentTileIDy*4+0, &z11, parentTileIDz*4+3, &z01, parentTileIDz*4+1, callStackDepth+1);
	C_unroll(&x10, parentTileIDx*4+2, &y10, parentTileIDy*4+2, &z10, parentTileIDz*4+2, &z00, parentTileIDz*4+0, callStackDepth+1);
	C_unroll(&x11, parentTileIDx*4+3, &y10, parentTileIDy*4+2, &z11, parentTileIDz*4+3, &z01, parentTileIDz*4+1, callStackDepth+1);

	C_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, &z211, parentTileIDz2*4+0, &z10, parentTileIDz*4+2, callStackDepth+1);
	C_unroll(&x01, parentTileIDx*4+1, &y01, parentTileIDy*4+1, &z212, parentTileIDz2*4+1, &z11, parentTileIDz*4+3, callStackDepth+1);
	C_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, &z211, parentTileIDz2*4+0, &z10, parentTileIDz*4+2, callStackDepth+1);
	C_unroll(&x11, parentTileIDx*4+3, &y11, parentTileIDy*4+3, &z212, parentTileIDz2*4+1, &z11, parentTileIDz*4+3, callStackDepth+1);

	return;
}

void A(Box* x, CELL_TYPE* xData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=x->coords[0];
		if(i == j)
			return;
		CELL_TYPE w_ikj=matrixChain[i-1]*matrixChain[k]*matrixChain[j];
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,xData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
		if(newCost < *cost_ij)
		{
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetLTMCell(i,j,xData);
			*cell = k;
		}
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A(&x00, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);

	B(&x01, xData, &x00, xData, &x11, xData, &x11, xData, callStackDepth+1);

	return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
	A(&x00, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	B(&x01, xData, &x00, xData, &x11, xData, &x11, xData, callStackDepth+1);
	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, Box* z2, CELL_TYPE* z2Data, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=y->coords[0];
		if(i == j)
			return;
		int* kjData = NULL;
		if(((k+1)>z->coords[3]) || ((k+1)<z->coords[1]) || (j>z->coords[2]) || (j<z->coords[0])) 
		{
			if(!(((k+1)>z2->coords[3]) || ((k+1)<z2->coords[1]) || (j>z2->coords[2]) || (j<z2->coords[0]))) 
				kjData = z2Data;
			else if(!(((k+1)>x->coords[3]) || ((k+1)<x->coords[1]) || (j>x->coords[2]) || (j<x->coords[0]))) 
				kjData = xData;
		}
		else
			kjData = zData;
		assert(kjData);

		CELL_TYPE w_ikj=matrixChain[i-1]*matrixChain[k]*matrixChain[j];
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,kjData);
		CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
		if(newCost < *cost_ij)
		{
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetLTMCell(i,j,xData);
			*cell = k;
		}
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z00(z->coords[0],z->coords[1],z->coords[0]+(z->coords[2]-z->coords[0]+1)/2-1,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2-1);
	Box z11(z00.coords[2]+1,z00.coords[3]+1,z->coords[2],z->coords[3]);

	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);
	Box z221(z2->coords[0],z222.coords[1],z211.coords[2],z2->coords[3]);
	if(z221.coords[1] > z221.coords[0])
	{
		z221 = z211;
	}

	B(&x10, xData, &y11, yData, &z00, zData, &z211, z2Data, callStackDepth+1);
	C(&x00, xData, &y01, yData, &z211, z2Data,&x10, xData, callStackDepth+1);
	C(&x11, xData, &x10, xData, &z11, zData, &z01, zData, callStackDepth+1);

	B(&x00, xData, &y00, yData, &z00, zData, &x10, xData, callStackDepth+1);
	B(&x11, xData, &y11, yData, &z11, zData, &z212, z2Data, callStackDepth+1);

	C(&x01, xData, &y01, yData, &z212, z2Data, &x11, xData, callStackDepth+1);
	C(&x01, xData, &x00, xData, &z11, zData, &z01, zData, callStackDepth+1);
	B(&x01, xData, &y00, yData, &z11, zData, &x11, xData, callStackDepth+1);

	return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[0]+(z->coords[2]-z->coords[0]+1)/2-1,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2-1);
	Box z11(z00.coords[2]+1,z00.coords[3]+1,z->coords[2],z->coords[3]);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);
	Box z221(z2->coords[0],z222.coords[1],z211.coords[2],z2->coords[3]);

	B(&x10, xData, &y11, yData, &z00, zData, &z211, z2Data, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x00, xData, &y01, yData, &z211, z2Data,&x10, xData, callStackDepth+1);
	C(&x11, xData, &x10, xData, &z11, zData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x00, xData, &y00, yData, &z00, zData, &x10, xData, callStackDepth+1);
	B(&x11, xData, &y11, yData, &z11, zData, &z212, z2Data, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&x01, xData, &y01, yData, &z212, z2Data, &x11, xData, callStackDepth+1);
	C(&x01, xData, &x00, xData, &z11, zData, &z01, zData, callStackDepth+1);
	B(&x01, xData, &y00, yData, &z11, zData, &x11, xData, callStackDepth+1);
	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z2, CELL_TYPE* z2Data, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=y->coords[0];
		if(i == j)
			return;
		int* kjData = NULL;
		if(((k+1)>z->coords[3]) || ((k+1)<z->coords[1]) || (j>z->coords[2]) || (j<z->coords[0])) 
		{
			if(!(((k+1)>z2->coords[3]) || ((k+1)<z2->coords[1]) || (j>z2->coords[2]) || (j<z2->coords[0]))) 
				kjData = z2Data;
		}
		else
			kjData = zData;
		assert(kjData);

		CELL_TYPE w_ikj=matrixChain[i-1]*matrixChain[k]*matrixChain[j];
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,kjData);
		CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
		if(newCost < *cost_ij)
		{
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetLTMCell(i,j,xData);
			*cell = k;
		}
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);
	Box z221(z2->coords[0],z222.coords[1],z211.coords[2],z2->coords[3]);

	C(&x00, xData, &y00, yData, &z10, zData, &z00, zData, callStackDepth+1);
	C(&x01, xData, &y00, yData, &z11, zData, &z01, zData, callStackDepth+1);
	C(&x10, xData, &y10, yData, &z10, zData, &z00, zData, callStackDepth+1);
	C(&x11, xData, &y10, yData, &z11, zData, &z01, zData, callStackDepth+1);

	C(&x00, xData, &y01, yData, &z211, z2Data, &z10, zData, callStackDepth+1);
	C(&x01, xData, &y01, yData, &z212, z2Data, &z11, zData, callStackDepth+1);
	C(&x10, xData, &y11, yData, &z211, z2Data, &z10, zData, callStackDepth+1);
	C(&x11, xData, &y11, yData, &z212, z2Data, &z11, zData, callStackDepth+1);


	return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);
	Box z221(z2->coords[0],z222.coords[1],z211.coords[2],z2->coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x00, xData, &y00, yData, &z10, zData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x01, xData, &y00, yData, &z11, zData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x10, xData, &y10, yData, &z10, zData, &z00, zData, callStackDepth+1);
	C(&x11, xData, &y10, yData, &z11, zData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x00, xData, &y01, yData, &z211, z2Data, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x01, xData, &y01, yData, &z212, z2Data, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x10, xData, &y11, yData, &z211, z2Data, &z10, zData, callStackDepth+1);
	C(&x11, xData, &y11, yData, &z212, z2Data, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	return;
}

void Unroll()
{
	int parentTileIDx=0;
	Box *x = new Box(1,1,inputSize[0],inputSize[1]);
	if(0 == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	A_unroll(x, 0, 0);
	delete x;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 1);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 4);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				std::get<7>(df->params) = dataRegions[3];
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 4);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
				std::get<7>(df->params) = dataRegions[3];
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
			}
			break;
		default: break;
	}
}

