#pragma once
#include<stdio.h>
#include<vector>
#include<set>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<fstream>
#include<algorithm>
#include<climits>
#include<float.h>
#include<boost/any.hpp>
#include<tuple>
#include<cassert>
#include<math.h>
#include<map>
#include<cstdarg>
#include<sstream>
#ifdef PARALLEL
#include<cilk/cilk.h>
#include<cilk/cilk_api.h>
#endif
#include"GlobalTypes.h"

using namespace std;

#define TASK_AGGREGATION
#define MAX_LEVELS_TO_UNFOLD 4

typedef enum ComputeGrid{
COMPUTE_FULL,
COMPUTE_UTM,
COMPUTE_SPARSE
}ComputeGrid;

typedef enum PartitioningScheme{
PARTITIONING_BLOCKED_VERTICAL,
PARTITIONING_BLOCKCYCLIC_HORIZONTAL,
PARTITIONING_BLOCKED_HORIZONTAL,
PARTITIONING_BLOCKCYCLIC_VERTICAL,
PARTITIONING_TILED
}PartitioningScheme;
extern  int dataPartitioningScheme;
typedef map<int,set<int> > Adjacency_List;
typedef struct Parameter
{
	int portID;	
	void* data;	
	CELL_TYPE* tile;
	bool operator==(const int k) const
	{
		return ((this->portID == k)?true:false);
	}
	Parameter():portID(-1),tile(nullptr){}
}Parameter;

typedef struct FunctionCall
{
	int ID;
	char functionName;
	void* fnCall;
	vector<Parameter> params;
	set<int> outPortOwners;
	int numInPorts;
	int wawSource;
#ifdef TASK_AGGREGATION
	bool isReadBeforeNextWrite;
	FunctionCall():numInPorts(0),wawSource(-1), isReadBeforeNextWrite(false){}
#else
	FunctionCall():numInPorts(0),wawSource(-1){}
#endif
	void GetSignature(char* signature);
}FunctionCall;

class Task
{
	public:
	vector<FunctionCall*> functionCalls;
	vector<pair<bool,CELL_TYPE*> > recvBuffers;
	set<int> outPortOwners;
	int numInPorts;
	CELL_TYPE* updatedRegion;
	int updateLen;
	Task* next;
#ifdef TASK_AGGREGATION
	Task* aggrSafeTask;
	Task():numInPorts(0),next(nullptr),updatedRegion(nullptr),updateLen(0),aggrSafeTask(nullptr){}
#else
	Task():numInPorts(0),next(nullptr),updatedRegion(nullptr),updateLen(0){}
#endif
};


#if DIMENSION == 2
typedef struct Box
{
	int coords[4]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d)
	{
		coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;
	}
	Box(){}
	Box(const Box& b)
	{
		coords[0]=b.coords[0];coords[1]=b.coords[1];coords[2]=b.coords[2];coords[3]=b.coords[3];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) ||(this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]))
			flag = false;
		return flag;
	}
	/*void PrintStr(char *str)const 
	{
		ostringstream ostrstrm;
		for(int i=0;i<2*DIMENSION;i++)
		{
			ostrstrm<<coords[i];
			if(i!= (2*DIMENSION-1))
				ostrstrm<<string(" ");
		}
		//cout<<ostrstrm.str();
		strcpy(str,ostrstrm.str().c_str());
		//sprintf(str,"%d %d %d %d",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5],coords[6],coords[7]);
	}*/
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<DIMENSION;i++)
		{
			len *= (coords[i+DIMENSION]-coords[i]+1);
		}	
		return len;
	}
	char* PrintStr(char *str)const 
	{
		sprintf(str,"%d%d%d%d",coords[0],coords[1],coords[2],coords[3]);
	}
}Box;

#elif DIMENSION == 4

typedef struct Box
{
	int coords[8]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d, int e, int f, int g, int h)
	{
		coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;coords[4]=e;coords[5]=f;coords[6]=g;coords[7]=h;
	}
	Box(){}
	Box(const Box& b)
	{
		coords[0]=b.coords[0];coords[1]=b.coords[1];coords[2]=b.coords[2];coords[3]=b.coords[3];
		coords[4]=b.coords[4];coords[5]=b.coords[5];coords[6]=b.coords[6];coords[7]=b.coords[7];
	}
	/*void PrintStr(char *str)const 
	{
		ostringstream ostrstrm;
		for(int i=0;i<2*DIMENSION;i++)
		{
			ostrstrm<<coords[i];
			if(i!= (2*DIMENSION-1))
				ostrstrm<<string(" ");
		}
		//cout<<ostrstrm.str();
		strcpy(str,ostrstrm.str().c_str());
		//sprintf(str,"%d %d %d %d",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5],coords[6],coords[7]);
	}*/
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<DIMENSION;i++)
		{
			len *= (coords[i+DIMENSION]-coords[i]+1);
		}	
		return len;
	}
	char* PrintStr(char *str)const 
	{
		sprintf(str,"%d%d%d%d%d%d%d%d",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5],coords[6],coords[7]);
	}
}Box;
#endif
/*typedef struct Box
{
	int  coords[DIMENSION*2]; //topleft and bottom right (x and y coordinates)
	Box(int coord0...)
	{
		va_list argList;
		coords[0]=coord0;
		va_start(argList,coord0);
		for(int argNum=1;argNum<2*DIMENSION;argNum++)
			coords[argNum]=va_arg(argList,int); 
		va_end(argList);
	}
	Box(){}
	void PrintStr(char *str)const 
	{
		ostringstream ostrstrm;
		for(int i=0;i<2*DIMENSION;i++)
		{
			ostrstrm<<coords[i];
			if(i!= (2*DIMENSION-1))
				ostrstrm<<string(" ");
		}
		//cout<<ostrstrm.str();
		strcpy(str,ostrstrm.str().c_str());
		//sprintf(str,"%d %d %d %d",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5],coords[6],coords[7]);
	}
	Box& operator=(const Box& rhs)
	{
		for(int i = 0;i<DIMENSION*2;i++)
			this->coords[i]=rhs.coords[i];
		return *this;
	}
	Box(const Box& rhs)
	{
		for(int i = 0;i<DIMENSION*2;i++)
			this->coords[i]=rhs.coords[i];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		for(int i = 0;i<DIMENSION*2;i++)
		{
			if(this->coords[i]!=rhs.coords[i])
			{
				flag = false;
				break;
			}
		}
		return flag;
	}
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<DIMENSION;i++)
		{
			len *= (coords[i+DIMENSION]-coords[i]+1);
		}	
		return len;
	}
}Box;*/

CELL_TYPE* GetTileOfDPTable(int zMortonID);
CELL_TYPE* GetTileOfDPTable(int i, int j);
CELL_TYPE* GetDependencySourceTile(int tileID);
void GenerateTasks(int argc, char** argv);
void ExecuteTasks();
int PrintGlobalMaxSW();
void DebugPrintAllCells();
void GetAggregate(long int partialSum);
void ParseSystemParams(int argc, char* argv[]);
bool IsLocalOwner(int tileID);
int GetOwner(int tileID);
int GetTileID(int zmortTileID);
int GetTileID(const Box* tile);
int GetZMortonTileIDFromCellCoords(int i, int j);


int ReadInput(int argc, char** argv);
void InitializeDPTable(Box* b, CELL_TYPE* data);
void PrintResults();
void Unroll();
void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions);

template<int ...> struct Sequence{};
template<int N, int ...S> struct Generate_Seq : Generate_Seq<N-1, N-1, S...> {};
template<int ...S> struct Generate_Seq<0, S...>{ typedef Sequence<S...> type; }; //base case of recursive class template definition
template <typename ...Args> class DeferredCall {
public:
	std::tuple<Args...> params;
	void (*fptr)(Args...);
	void Run()
	{
		_Run(typename Generate_Seq<sizeof...(Args)>::type());
		return;
	}

	template<int ...S>
	void _Run(Sequence<S...>)
	{
		fptr(std::get<S>(params)...);
		return;
	}
};

extern int procRank;
extern int totalProcs;
extern long gPreprocessTime;
extern long gTaskExecutionTime;
extern long gBacktracingTime;
extern int computeGrid;
extern int curIndex_i, curIndex_j;
extern long int inputSize[DIMENSION];

//#define GRAPHVIZ_OUTPUT
#ifdef GRAPHVIZ_OUTPUT
void UpdateWorkSpanAnalysis(const int curFunctionID, const float curFunctionExecTime);
#endif

//#define IDLE_TIME
//#define PAPI
#ifdef PAPI
#include"papi.h"
extern int papiStat, eventSet;
extern long long values[2];
void handle_error(int retval);
#endif

