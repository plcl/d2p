#include "HelperFunctions.h"
#include<stack>
#include <mpi.h>
#ifdef GRAPHVIZ_OUTPUT
#include<sys/time.h>
#endif
int fnCallHierarchySummary[4][4]={{2,2,2,2},{0,4,0,4},{0,0,4,4},{0,0,0,8}};
long int inputSize[2];
map<int,vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
extern map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_FULL;
#define INF 65535
int curIndex_i, curIndex_j;

#define _paramListFunctionA Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionD Box*, CELL_TYPE*, Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
void A_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth);
void B_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth);
void C_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth);
void D_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth);
void A(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
#define MAX_NUM_VERTICES 45000
Adjacency_List graphData;
vector<int> vertexIDList;

/*Helper function to tokenize a string */
vector<string> Split(const char *str, char c)
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(string(begin, str));
    } while (0 != *str++);

    return result;
}

/* this function is for reading wiki data set. */
int ReadGraphDataGeneral(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int weight = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		g[fromNode].insert(make_pair(toNode,weight));
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}


/* this function is for reading autonomous systems (AS) data set. */
int ReadGraphData(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int relationship = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		if((relationship == 1) || (relationship == -1))
			g[fromNode].insert(make_pair(toNode,1));
		else if((relationship == 0) || (relationship == 2))
		{
			g[fromNode].insert(make_pair(toNode,1));
			g[toNode].insert(make_pair(fromNode,1));
		}
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || strcmp(argv[1],"-h")==0)
	{
		printf("Usage: ./<exec> <input_graph> <num_vertices> (use -h for more options)\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r");
	if(!fpi)
	{
		printf("ERROR: Unable to open input file.\n");
		exit(0);
	}
	int maxVertices=MAX_NUM_VERTICES;
	maxVertices=atoi(argv[2]);
	int ret1=ReadGraphData(fpi, maxVertices, graphData);
	fclose(fpi);
	if(ret1<0)
	{
		printf("ERROR: Unable to read input file correctly.\n");
		exit(0);
	}
	else
	{
		inputSize[0]=ret1-1; inputSize[1]=ret1-1;
	}

	return inputSize[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	unsigned int relIndx = 0;
	for(unsigned int i=b->coords[1];i<=b->coords[3];i++)
	{
		for(unsigned int j=b->coords[0];j<=b->coords[2];j++)
		{
			CELL_TYPE initVal=INF;
			if(i==j)
				initVal = 0;
			else
			{
				set<pair<int, int> >::iterator it=graphData[vertexIDList[i]].begin();
				while(it!=graphData[vertexIDList[i]].end())
				{
					if(it->first == vertexIDList[j])
					{
						initVal = it->second;
						break;
					}
					it++;
				}
			}
			if(initVal != INF)
				data[relIndx+numCells] = j;
			data[relIndx++] = initVal;
		}
	}

	return;
}

void GetPath(CELL_TYPE* tile)
{
	CELL_TYPE* curDPTableTile = tile; 
	int *msgBktrac=new int[4];
	int backTracingProcID=-1;
	stack<pair<int,int> > s;
	int containingSeq_i=-1, containingSeq_j=-1;
	int source = curIndex_i, destn=curIndex_j;
	typedef struct TPath
	{
		int from;
		int nextHop;
		int to;
		TPath(int a, int b, int c):from(a),nextHop(b),to(c){}
	}TPath;
	vector<TPath> localResult;
	if(tile != nullptr)
	{
		backTracingProcID = procRank;
		s.push(make_pair(-1,-1));
		s.push(make_pair(source, destn));
	}

	while(true)
	{
		bool forwardMsg = true;
		if(procRank != backTracingProcID)
		{
			MPI_Recv(msgBktrac, 4, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			curIndex_i = msgBktrac[0]; curIndex_j = msgBktrac[1];
			containingSeq_i = msgBktrac[2];containingSeq_j=msgBktrac[3];
			if((curIndex_i < 0) || (curIndex_j < 0))
				break;
			curDPTableTile = GetTileOfDPTable(curIndex_j, curIndex_i);
			assert(curDPTableTile != NULL);
			if(containingSeq_j == -1)
			{
				forwardMsg = false;
				if((s.size() == 0) && ((curIndex_j == destn)||(curIndex_i == source)))
				{
					assert(s.size() == 0);
					s.push(make_pair(-1,-1));
					s.push(make_pair(curIndex_i, curIndex_j));
				}
				curIndex_j = s.top().second;
				curIndex_i = s.top().first;
			}
		}

		if(containingSeq_j !=-1)
		{
			s.push(make_pair(-1,-1));
			s.push(make_pair(containingSeq_i, containingSeq_j));
			s.push(make_pair(curIndex_i, curIndex_j));
		}
		
		assert(curDPTableTile != NULL);
		if(forwardMsg)
		{
			containingSeq_j = curIndex_j; containingSeq_i = curIndex_i;
		}
		else
		{
			containingSeq_i = msgBktrac[0];containingSeq_j=msgBktrac[1];
		}
		while(!s.empty())
		{
			pair<int,int> curSequence = s.top();
			s.pop();
			curIndex_i = curSequence.first;
			curIndex_j = curSequence.second;

			if((curIndex_i < curDPTableTile[1]) || (curIndex_j < curDPTableTile[0]) || (curIndex_i >= (curDPTableTile[1]+curDPTableTile[3])) || (curIndex_j >= (curDPTableTile[0]+curDPTableTile[2])))
			{
				if((curIndex_i >= 0) && (curIndex_j >= 0))
				{
					int curTileID = GetZMortonTileIDFromCellCoords(curIndex_j,curIndex_i);
					backTracingProcID = GetOwner(GetTileID(curTileID));
					assert(s.size() > 0);
					if(backTracingProcID == procRank)
					{
						curDPTableTile = GetTileOfDPTable(curIndex_j,curIndex_i);
						s.push(make_pair(curIndex_i,curIndex_j));
						continue;
					}
					else
					{
						if(s.top().first < 0)
						{
							s.pop();
							containingSeq_j = -1; containingSeq_i = -1;
						}
						break;
					}
				}
				break;
			}
			else
			{
				CELL_TYPE* nextHop = GetNextHopCell(curIndex_i, curIndex_j, curDPTableTile);
				if((*nextHop != curIndex_j) && (*nextHop != curIndex_i))
				{
					TPath t(curIndex_i, *nextHop, curIndex_j);
					localResult.push_back(t);
					s.push(make_pair(*nextHop,curIndex_j));
					s.push(make_pair(curIndex_i,*nextHop));
				}
			}
		}
		msgBktrac[0]=curIndex_i;
		msgBktrac[1]=curIndex_j;
		msgBktrac[2]=containingSeq_i;
		msgBktrac[3]=containingSeq_j;
		if((curIndex_i < 0) || (curIndex_j < 0))
		{
			for(int i=0;i<totalProcs;i++)
				if(i!= procRank)
					MPI_Send(msgBktrac, 4, MPI_INT, i, 0, MPI_COMM_WORLD);
			break;
		}
		else
		{
			if(backTracingProcID != procRank)
				MPI_Send(msgBktrac, 4, MPI_INT, backTracingProcID, 0, MPI_COMM_WORLD);
		}
	}
	
#ifdef PRINT_RESULTS
	//each process prints a triple <start_vertex, nexthop, end_vertex>. It may contain multiple such triples.
	for(int i=localResult.size()-1;i>=0;i--)
	{
		printf("%d-.->%d-.->%d\n",localResult[i].from, localResult[i].nextHop,localResult[i].to);
	}
#endif
	delete [] msgBktrac;
}

void PrintResults()
{
	//DebugPrintAllCells();
	int initialBackTracProcID = PrintGlobalMax();
	//printf("%d: initProcID:%d i:%d j:%d \n",procRank,initialBackTracProcID,curIndex_i,curIndex_j);
	CELL_TYPE* tile = GetTileOfDPTable(curIndex_j,curIndex_i);
	GetPath(tile);
}

void A_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*z);
			p.data = b4;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		readTileIDs[0]=GetTileID(parentTileIDy);
		readTileIDs[1]=GetTileID(parentTileIDz);
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		A_unroll(&x00, parentTileIDx*4+0, &y00, parentTileIDy*4+0, &z00, parentTileIDz*4+0, callStackDepth+1);
		B_unroll(&x01, parentTileIDx*4+1, &y00, parentTileIDy*4+0, &z01, parentTileIDz*4+1, callStackDepth+1);
		C_unroll(&x10, parentTileIDx*4+2, &y10, parentTileIDy*4+2, &z00, parentTileIDz*4+0, callStackDepth+1);
		D_unroll(&x11, parentTileIDx*4+3, &y10, parentTileIDy*4+2, &z01, parentTileIDz*4+1, callStackDepth+1);
		A_unroll(&x11, parentTileIDx*4+3, &y11, parentTileIDy*4+3, &z11, parentTileIDz*4+3, callStackDepth+1);
		B_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, &z10, parentTileIDz*4+2, callStackDepth+1);
		C_unroll(&x01, parentTileIDx*4+1, &y01, parentTileIDy*4+1, &z11, parentTileIDz*4+3, callStackDepth+1);
		D_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, &z10, parentTileIDz*4+2, callStackDepth+1);
		return;
}

void B_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*z);
			p.data = b4;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		readTileIDs[0]=GetTileID(parentTileIDy);
		readTileIDs[1]=GetTileID(parentTileIDz);
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		B_unroll(&x00, parentTileIDx*4+0, &y00, parentTileIDy*4+0, &z00, parentTileIDz*4+0, callStackDepth+1);
		B_unroll(&x01, parentTileIDx*4+1, &y00, parentTileIDy*4+0, &z01, parentTileIDz*4+1, callStackDepth+1);
		D_unroll(&x10, parentTileIDx*4+2, &y10, parentTileIDy*4+2, &z00, parentTileIDz*4+0, callStackDepth+1);
		D_unroll(&x11, parentTileIDx*4+3, &y10, parentTileIDy*4+2, &z01, parentTileIDz*4+1, callStackDepth+1);
		B_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, &z10, parentTileIDz*4+2, callStackDepth+1);
		B_unroll(&x11, parentTileIDx*4+3, &y11, parentTileIDy*4+3, &z11, parentTileIDz*4+3, callStackDepth+1);
		D_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, &z10, parentTileIDz*4+2, callStackDepth+1);
		D_unroll(&x01, parentTileIDx*4+1, &y01, parentTileIDy*4+1, &z11, parentTileIDz*4+3, callStackDepth+1);
		return;
}

void C_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*z);
			p.data = b4;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		readTileIDs[0]=GetTileID(parentTileIDy);
		readTileIDs[1]=GetTileID(parentTileIDz);
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		C_unroll(&x00, parentTileIDx*4+0, &y00, parentTileIDy*4+0, &z00, parentTileIDz*4+0, callStackDepth+1);
		C_unroll(&x10, parentTileIDx*4+2, &y10, parentTileIDy*4+2, &z00, parentTileIDz*4+0, callStackDepth+1);
		D_unroll(&x01, parentTileIDx*4+1, &y00, parentTileIDy*4+0, &z01, parentTileIDz*4+1, callStackDepth+1);
		D_unroll(&x11, parentTileIDx*4+3, &y10, parentTileIDy*4+2, &z01, parentTileIDz*4+1, callStackDepth+1);

		C_unroll(&x01, parentTileIDx*4+1, &y01, parentTileIDy*4+1, &z11, parentTileIDz*4+3, callStackDepth+1);
		C_unroll(&x11, parentTileIDx*4+3, &y11, parentTileIDy*4+3, &z11, parentTileIDz*4+3, callStackDepth+1);
		D_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, &z10, parentTileIDz*4+2, callStackDepth+1);
		D_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, &z10, parentTileIDz*4+2, callStackDepth+1);

		return;
}

void D_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, Box* z, int parentTileIDz, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'D';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*z);
			p.data = b4;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionD> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionD>* defdCall = new DeferredCall<_paramListFunctionD>();
			defdCall->params=t;
			defdCall->fptr=D;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		readTileIDs[0]=GetTileID(parentTileIDy);
		readTileIDs[1]=GetTileID(parentTileIDz);
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);


		D_unroll(&x00, parentTileIDx*4+0, &y00, parentTileIDy*4+0, &z00, parentTileIDz*4+0, callStackDepth+1);
		D_unroll(&x01, parentTileIDx*4+1, &y00, parentTileIDy*4+0, &z01, parentTileIDz*4+1, callStackDepth+1);
		D_unroll(&x10, parentTileIDx*4+2, &y10, parentTileIDy*4+2, &z00, parentTileIDz*4+0, callStackDepth+1);
		D_unroll(&x11, parentTileIDx*4+3, &y10, parentTileIDy*4+2, &z01, parentTileIDz*4+1, callStackDepth+1);

		D_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, &z10, parentTileIDz*4+2, callStackDepth+1);
		D_unroll(&x01, parentTileIDx*4+1, &y01, parentTileIDy*4+1, &z11, parentTileIDz*4+3, callStackDepth+1);
		D_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, &z10, parentTileIDz*4+2, callStackDepth+1);
		D_unroll(&x11, parentTileIDx*4+3, &y11, parentTileIDy*4+3, &z11, parentTileIDz*4+3, callStackDepth+1);
		return;
}


void A(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
#ifdef DEBUG1
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("A: x: %s y:%s z:%s\n",xindx,yindx,zindx);
#endif

	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
		{
			/*if((i==34) && (j==32))
				printf("A: k:%d\n",k);*/
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetNextHopCell(i,j,xData);
			*cell = k;
		}
    		//printf("A: xi:%d xj:%d uj:%d\n", i, j, k);
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		A(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
		B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
		C(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
		A(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
		B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
		C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		A(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
		C(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
	
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
		A(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
		C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
#ifdef DEBUG1
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("B: x: %s y:%s z:%s\n",xindx,yindx,zindx);
#endif
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
		{
			/*if((i==34) && (j==32))
				printf("B: k:%d\n",k);*/
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetNextHopCell(i,j,xData);
			*cell = k;
		}

    		//printf("B: xi:%d xj:%d uj:%d\n", i, j, k);
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		B(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
		B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
		D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
		B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
		B(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
		B(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
		B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
		B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
		B(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
		return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
#ifdef DEBUG1
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("C: x: %s y:%s z:%s\n",xindx,yindx,zindx);
#endif
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
		{
			/*if((i==34) && (j==32))
				printf("C: k:%d\n",k);*/
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetNextHopCell(i,j,xData);
			*cell = k;
		}
    		//printf("C: xi:%d xj:%d uj:%d\n", i, j, k);
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

		C(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
		C(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
		D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

		C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
		C(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);

		return;
	}
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	
#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
		C(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
		C(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
		return;
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
#ifdef DEBUG1
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("D: x: %s y:%s z:%s\n",xindx,yindx,zindx);
#endif
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
		{
			/*if((i==34) && (j==32))
				printf("D: k:%d\n",k);*/
			*cost_ij =  newCost;
			CELL_TYPE* cell = GetNextHopCell(i,j,xData);
			*cell = k;
		}

    		//printf("D: xi:%d xj:%d uj:%d\n", i, j, k);
		return;
	}
	else if (callStackDepth > recursionDepth+4)
	{
		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);


		D(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
		D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
		D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
		D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
		D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
		D(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
		return;

	}

		Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
		Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
		Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
		Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
		Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
		Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
		Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
		Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
		Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);


#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
		D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

#ifdef PARALLEL
	cilk_sync;
#endif
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
		D(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif
	return;
}

void Unroll()
{
	CELL_TYPE* nullData=nullptr;
	Box *x = new Box(0,0,inputSize[0],inputSize[1]);
	if(0 == recursionDepth)
	{
		int writeTileID = 0;
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*x);
			p.data = b2;
			fnCall->params.push_back(p);
			Box* b4=new Box(*x);
			p.data = b4;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, b2, nullData, b4, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[writeTileID].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[writeTileID].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[writeTileID].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[2];
		readTileIDs[0]=0;
		readTileIDs[1]=0;
		for(int i=0;i<2;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[readTileID].size() > 0)
					(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[readTileID].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}

	A_unroll(x, 0, x, 0, x, 0, 0);
	delete x;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
#ifdef GRAPHVIZ_OUTPUT
				struct timeval startTime, endTime;
				gettimeofday(&startTime,0);
#endif
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
#ifdef GRAPHVIZ_OUTPUT
				gettimeofday(&endTime,0);
				long lFunctionExecTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
				float fFunctionExecTime = lFunctionExecTime/(float)1000000;
				UpdateWorkSpanAnalysis(fn->ID,fFunctionExecTime);
				/*Box* writeTile = std::get<0>(df->params);
				char writeTileStr[1024];
				writeTile->PrintStr(writeTileStr);
				printf("Function A (%s): %f seconds\n",writeTileStr,fFunctionExecTime);*/
#endif
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
#ifdef GRAPHVIZ_OUTPUT
				struct timeval startTime, endTime;
				gettimeofday(&startTime,0);
#endif
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
#ifdef GRAPHVIZ_OUTPUT
				gettimeofday(&endTime,0);
				long lFunctionExecTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
				float fFunctionExecTime = lFunctionExecTime/(float)1000000;
				UpdateWorkSpanAnalysis(fn->ID,fFunctionExecTime);
				/*Box* writeTile = std::get<0>(df->params);
				char writeTileStr[1024];
				writeTile->PrintStr(writeTileStr);
				printf("Function B (%s): %f seconds\n",writeTileStr,fFunctionExecTime);*/
#endif
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
#ifdef GRAPHVIZ_OUTPUT
				struct timeval startTime, endTime;
				gettimeofday(&startTime,0);
#endif
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
#ifdef GRAPHVIZ_OUTPUT
				gettimeofday(&endTime,0);
				long lFunctionExecTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
				float fFunctionExecTime = lFunctionExecTime/(float)1000000;
				UpdateWorkSpanAnalysis(fn->ID,fFunctionExecTime);
				/*Box* writeTile = std::get<0>(df->params);
				char writeTileStr[1024];
				writeTile->PrintStr(writeTileStr);
				printf("Function C (%s): %f seconds\n",writeTileStr,fFunctionExecTime);*/
#endif
			}
			break;
		case 'D':
			{
				assert(dataRegions.size() == 3);
				DeferredCall<_paramListFunctionD>* df = (reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				std::get<5>(df->params) = dataRegions[2];
#ifdef GRAPHVIZ_OUTPUT
				struct timeval startTime, endTime;
				gettimeofday(&startTime,0);
#endif
				(reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall))->Run();
#ifdef GRAPHVIZ_OUTPUT
				gettimeofday(&endTime,0);
				long lFunctionExecTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
				float fFunctionExecTime = lFunctionExecTime/(float)1000000;
				UpdateWorkSpanAnalysis(fn->ID,fFunctionExecTime);
				/*Box* writeTile = std::get<0>(df->params);
				char writeTileStr[1024];
				writeTile->PrintStr(writeTileStr);
				printf("Function D (%s): %f seconds\n",writeTileStr,fFunctionExecTime);*/
#endif
			}
			break;
		default: break;
	}
}

