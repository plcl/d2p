#include "HelperFunctions.h"
#include <mpi.h>
int fnCallHierarchySummary[4][4]={{4,2,2,1},{0,2,0,1},{0,0,2,1},{0,0,0,1}};
long int inputSize[DIMENSION];
map<int, vector<FunctionCall*> > fnCalls;
extern int recursionDepth;
int fnCounter=0;
extern map<int,int> tileUpdateLog;
int computeGrid = COMPUTE_FULL;
int numCellsTraced=0;

#define _paramListFunctionA Box*, CELL_TYPE*, int
#define _paramListFunctionB Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionC Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
#define _paramListFunctionD Box*, CELL_TYPE*, Box*, CELL_TYPE*, int
void A_unroll(Box* x, int parentTileIDx, int callStackDepth);
void A(Box* x, CELL_TYPE* xData, int callStackDepth);
void B_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void C_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void D_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void BackTracing(int); 
int nxt_move(int, int, CELL_TYPE*);
int nxt_move_from_tile_edge(int i, int j, CELL_TYPE* curTile, CELL_TYPE* topTile, CELL_TYPE* leftTile, CELL_TYPE* diagTile);
#define MAX_STR_LEN 45000
#define END 0
#define DIAG 1
#define UP 2
#define LEFT 3
int curIndex_i, curIndex_j, curTileID;

int gap=-4, match=5, nomatch=-3;
char* input_i, *input_j;

int ReadFASTAFile(FILE *fp, int maxLen, char** seq)
{
	char line[1024];
	int seqLength = 0, seqLenToProcess;
	char* localSeq=NULL;
	fgets(line, sizeof(line), fp);
	if(line == NULL)
		return -1;
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(strlen(line)+seqLength > maxLen)
			seqLenToProcess = maxLen-seqLength;
		else
			seqLenToProcess = strlen(line);
		localSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);
		if(!localSeq)
		{
			printf("ERROR in realloc.\n");
			exit(0);
		}
		for(unsigned int i = 0; i < seqLenToProcess; ++i)
		{
			if (line[i] == '*' || line[i] == '-')
				localSeq[seqLength++] = line[i];
			else if(isalpha(line[i]))
				localSeq[seqLength++] = toupper(line[i]);

		}
		if(seqLength>=maxLen) break;
	}
	localSeq[seqLength] = '\0';
	*seq = localSeq;
	return seqLength;
}

int ReadInput(int argc, char** argv)
{
	if(strcmp(argv[1],"-h")==0) 
	{
		printf("Usage: ./<exec> <input_string_i> <input_string_j> <input_length_i> <input_length_j>\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r"), *fpj= fopen(argv[2],"r");
	if(!fpi || !fpj)
	{
		printf("ERROR: Unable to open input files.\n");
		exit(0);
	}
	int inputLength_I=MAX_STR_LEN,inputLength_J=MAX_STR_LEN;
	inputLength_I=atoi(argv[3]); inputLength_J=atoi(argv[4]);
	int ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);
	int ret2=ReadFASTAFile(fpj, inputLength_J, &input_j);
	fclose(fpi); fclose(fpj);
	if((ret1<0) || (ret2<0))
	{
		printf("ERROR: Unable to read input files correctly. file1 len:%d file2 len:%d\n",ret1,ret2);
		exit(0);
	}
	else
	{
		inputSize[0]=ret1; inputSize[1]=ret2;
	}
	if(inputSize[1] > inputSize[0])
	{
		std::swap(inputSize[0],inputSize[1]);
		std::swap(input_i,input_j);
	}
	//printf("string length (x):%d (y):%d\n",inputSize[0],inputSize[1]);
	return inputSize[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	memset(data,0,sizeof(CELL_TYPE)*(numCells));
	return;
}

void PrintResults()
{
	int ownerRank = PrintGlobalMaxSW();
	BackTracing(ownerRank);
	int gNumCellsTraced;
	MPI_Reduce(&numCellsTraced,&gNumCellsTraced,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
	if(procRank == 0)
		std::cout<<"Total num cells traced:"<<gNumCellsTraced<<std::endl;
	//DebugPrintAllCells();

}

void BackTracing(int ownerRank)
{
	int *msgBktrac=new int[3];
	int backTracingProcID = ownerRank;

#ifdef PRINT_RESULTS
	if(procRank == backTracingProcID)
		cout << input_i[curIndex_i] << " " << input_j[curIndex_j] << endl;
#endif

	while(true)
	{
		if(procRank != backTracingProcID)
		{
			MPI_Recv(msgBktrac, 3, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			curIndex_i = msgBktrac[0]; curIndex_j = msgBktrac[1];
			curTileID = msgBktrac[2];
			if((curIndex_i == 0) || (curIndex_j==0))
				break;
		}
		//printf("tile backtracing started.curIndex_i=%d curIndex_j=%d\n",curIndex_i,curIndex_j);
		CELL_TYPE* topTile=NULL, *leftTile=NULL, *diagTile=NULL, *curDPTableTile = GetTileOfDPTable(curTileID);
		assert(curDPTableTile != NULL);
		
		string resultSeq1;
		string resultSeq2;
		char curInput_i, curInput_j;
		int move=LEFT; //any initialization value other than END would do.
		bool edgeReached = false;
		while(move!=END)
		{
			if((curIndex_i < curDPTableTile[1]) || (curIndex_j < curDPTableTile[0]))
			{
				//curTileID=GetZMortonTileIDFromCellCoords(curIndex_j,curIndex_i);
				if((curIndex_i< curDPTableTile[1]) && (curIndex_j < curDPTableTile[0]))
				{
					int tmp1    = ((curTileID & 0xAAAAAAAA) - 1 & 0xAAAAAAAA) | (curTileID & 0x55555555); //top
					int tmp2   = ((tmp1 & 0x55555555) - 1 & 0x55555555) | (tmp1 & 0xAAAAAAAA); //left
					curTileID = tmp2;
				}
				else if(curIndex_i< curDPTableTile[1])
				{
					int tmp1 = ((curTileID & 0xAAAAAAAA) - 1 & 0xAAAAAAAA) | (curTileID & 0x55555555); //top
					curTileID = tmp1;
				}
				else if(curIndex_j < curDPTableTile[0])
				{
					int tmp1 = ((curTileID & 0x55555555) - 1 & 0x55555555) | (curTileID & 0xAAAAAAAA); //left
					curTileID = tmp1;
				}
				break;
			}
			else if((curIndex_i > curDPTableTile[1]) && (curIndex_j > curDPTableTile[0]))
			{
				numCellsTraced++;
				move = nxt_move(curIndex_i,curIndex_j, curDPTableTile);
			}
			else
			{
				numCellsTraced++;
				if(!edgeReached)
				{
					int leftTileID   = ((curTileID & 0x55555555) - 1 & 0x55555555) | (curTileID & 0xAAAAAAAA); //left
					leftTile = GetDependencySourceTile(leftTileID);
					int topTileID = ((curTileID & 0xAAAAAAAA) - 1 & 0xAAAAAAAA) | (curTileID & 0x55555555); //top
					topTile = GetDependencySourceTile(topTileID);
					int diagTileID = ((curTileID & 0xAAAAAAAA) - 1 & 0xAAAAAAAA) | (curTileID & 0x55555555); //top
					diagTileID = ((diagTileID & 0x55555555) - 1 & 0x55555555) | (diagTileID & 0xAAAAAAAA); //left
					diagTile = GetDependencySourceTile(diagTileID);
					edgeReached = true;
				}
				move = nxt_move_from_tile_edge(curIndex_i, curIndex_j, curDPTableTile, topTile, leftTile, diagTile);
			}
			switch(move)
			{
				case DIAG:
				{
					//printf("%d: (%d %d) \\ \n",procRank,curIndex_i,curIndex_j);
					curIndex_i--;
					curIndex_j--;
					assert((curIndex_i >= 0) && ((curIndex_j) >= 0));
					curInput_i = input_i[curIndex_i];
					curInput_j = input_j[curIndex_j];
					break;
				}
				case UP:
				{
					//printf("%d: (%d %d) | \n",procRank,curIndex_i,curIndex_j);
					curIndex_i--;
					assert((curIndex_i >= 0) && ((curIndex_j) >= 0));
					curInput_j = '-';
					curInput_i = input_i[curIndex_i];
					break;
				}
				break;
				case LEFT:
				{
					//printf("%d: (%d %d) <- \n",procRank,curIndex_i,curIndex_j);
					curIndex_j--;
					assert((curIndex_i >= 0) && ((curIndex_j) >= 0));
					curInput_j = input_j[curIndex_j];
					curInput_i = '-';
					break;
				}
				case END: break;
			}
			resultSeq1.push_back(curInput_i);
			resultSeq2.push_back(curInput_j);
		}
		msgBktrac[0]=curIndex_i;
		msgBktrac[1]=curIndex_j;
		msgBktrac[2]=curTileID;
		//cout << procRank << " printing" << endl;
#ifdef PRINT_RESULTS
		for(int ct1 = resultSeq1.length()-1; ct1 >= 0; ct1--)
			cout << resultSeq1[ct1] << " " << resultSeq2[ct1] << endl;
#endif

		if(move == END)
		{
			for(int i=0;i<totalProcs;i++)
				if(i!= procRank)
					MPI_Send(msgBktrac, 3, MPI_INT, i, 0, MPI_COMM_WORLD);
			break;
		}
		else
		{
			backTracingProcID = GetOwner(GetTileID(curTileID));
			if(backTracingProcID != procRank)
				MPI_Send(msgBktrac, 3, MPI_INT, backTracingProcID, 0, MPI_COMM_WORLD);
		}
		//printf("tile backtracing finished.curIndex_i=%d curIndex_j=%d\n",curIndex_i,curIndex_j);
	}	
}

int nxt_move(int i, int j, CELL_TYPE* DPTable)
{
	
	if((i == 0)||(j == 0))
		return END;
	CELL_TYPE* diag = GetDPTableCell(i-1, j-1, DPTable);
	CELL_TYPE* up = GetDPTableCell(i-1, j, DPTable);
	CELL_TYPE* left = GetDPTableCell(i, j-1, DPTable);
	if(*diag == std::max(std::max(*diag, *up), *left))
		return DIAG;
	else if(*up == std::max(std::max(*diag, *up), *left))
		return UP;
	else if(*left == std::max(std::max(*diag, *up), *left))
		return LEFT;
}

/* From a tile edge, the next move may backtrack to the above, or to the left, or to the top-left diagonal tile. Further, 
 * if the destination tile is owned by a remote process, the current process will have a copy of only the last row or column
 * of the destination tile. In this scenario, the indexing (translating (i,j) into an index value of an array) into the data 
 * (tile) differs. Hence GetDPTableCellAdjacent and GetDPTableCell are defined to compute appropriate index values depending
 * upon whether the destination tile is remote or self-owned resp. */
int nxt_move_from_tile_edge(int i, int j, CELL_TYPE* curTile, CELL_TYPE* topTile, CELL_TYPE* leftTile, CELL_TYPE* diagTile)
{
	if((i == 0)||(j == 0))
		return END;
	CELL_TYPE* left=NULL, *up=NULL, *diag=NULL;

	if((i==curTile[1])&&(j!=curTile[0])) //fetch data from top tile 
	{
		if(topTile[METADATASPACE-1]!=-1)
		{
			up = GetDPTableCell(i-1, j, topTile);
			diag = GetDPTableCell(i-1, j-1, topTile);
		}
		else
		{
			up = GetDPTableCellAdjacent(i-1,j,topTile);
			diag = GetDPTableCellAdjacent(i-1,j-1,topTile);
		}
		left = GetDPTableCell(i, j-1, curTile);
	}
	else if((i!=curTile[1])&&(j==curTile[0])) //fetch data from left tile 
	{
		if(leftTile[METADATASPACE-1]!=-1)
		{
			left = GetDPTableCell(i, j-1, leftTile);
			diag = GetDPTableCell(i-1, j-1, leftTile);
		}
		else
		{
			left = GetDPTableCellAdjacent(i,j-1,leftTile);
			diag = GetDPTableCellAdjacent(i-1,j-1,leftTile);
		}
		up = GetDPTableCell(i,j-1,curTile);
	}
	else if ((i==curTile[1])&&(j==curTile[0])) //fetch data from top, left, and diag tiles
	{
		if(diagTile[METADATASPACE-1]!=-1)
			diag = GetDPTableCell(i-1, j-1, diagTile);
		else
			diag = GetDPTableCellAdjacent(i-1,j-1,diagTile);
		
		if(leftTile[METADATASPACE-1]!=-1)
			left = GetDPTableCell(i, j-1, leftTile);
		else
			left = GetDPTableCellAdjacent(i,j-1,leftTile);
		
		if(topTile[METADATASPACE-1]!=-1)
			up = GetDPTableCell(i-1, j, topTile);
		else
			up = GetDPTableCellAdjacent(i-1,j,topTile);
	}
	if(*diag == std::max(std::max(*diag, *up), *left))
		return DIAG;
	else if(*up == std::max(std::max(*diag, *up), *left))
		return UP;
	else if(*left == std::max(std::max(*diag, *up), *left))
		return LEFT;
}

void A_unroll(Box* x, int parentTileIDx, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A_unroll(&x00, parentTileIDx*4+0, callStackDepth+1);
	B_unroll(&x01, parentTileIDx*4+1, &x00, parentTileIDx*4+0, callStackDepth+1);
	C_unroll(&x10, parentTileIDx*4+2, &x00, parentTileIDx*4+0, callStackDepth+1);
	D_unroll(&x11, parentTileIDx*4+3, &x00, parentTileIDx*4+0, callStackDepth+1);
	A_unroll(&x01, parentTileIDx*4+1, callStackDepth+1);
	A_unroll(&x10, parentTileIDx*4+2, callStackDepth+1);
	C_unroll(&x11, parentTileIDx*4+3, &x01, parentTileIDx*4+1, callStackDepth+1);
	B_unroll(&x11, parentTileIDx*4+3, &x10, parentTileIDx*4+2, callStackDepth+1);
	A_unroll(&x11, parentTileIDx*4+3, callStackDepth+1);
}

void A(Box* x, CELL_TYPE* xData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=x->coords[1], l=x->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,xData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A(&x00, xData, callStackDepth+1);
	B(&x01, xData, &x00, xData, callStackDepth+1);
	C(&x10, xData, &x00, xData, callStackDepth+1);
	D(&x11, xData, &x00, xData, callStackDepth+1);

	A(&x01, xData, callStackDepth+1);
	A(&x10, xData, callStackDepth+1);

	C(&x11, xData, &x01, xData, callStackDepth+1);
	B(&x11, xData, &x10, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);

	return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A(&x00, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x01, xData, &x00, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x10, xData, &x00, xData, callStackDepth+1);
	D(&x11, xData, &x00, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
	A(&x01, xData, callStackDepth+1);
	A(&x10, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&x11, xData, &x01, xData, callStackDepth+1);
	B(&x11, xData, &x10, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);
	return;
}

void B_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'B';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionB> t = std::make_tuple(b0, nullData, b2, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionB>* defdCall = new DeferredCall<_paramListFunctionB>();
			defdCall->params=t;
			defdCall->fptr=B;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[1];
		readTileIDs[0]=GetTileID(parentTileIDy);
		for(int i=0;i<1;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[parentTileIDy].size() > 0)
					(fnCalls[parentTileIDy].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[parentTileIDy].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDy].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	B_unroll(&x00, parentTileIDx*4+0, &y01, parentTileIDy*4+1, callStackDepth+1);
	D_unroll(&x10, parentTileIDx*4+2, &y01, parentTileIDy*4+1, callStackDepth+1);
	B_unroll(&x10, parentTileIDx*4+2, &y11, parentTileIDy*4+3, callStackDepth+1);
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	B(&x00, xData, &y01, yData, callStackDepth+1);
	D(&x10, xData, &y01, yData, callStackDepth+1);

	B(&x10, xData, &y11, yData, callStackDepth+1);

	return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
	B(&x00, xData, &y01, yData, callStackDepth+1);
	D(&x10, xData, &y01, yData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	B(&x10, xData, &y11, yData, callStackDepth+1);
	return;
}

void C_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'C';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionC> t = std::make_tuple(b0, nullData, b2, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionC>* defdCall = new DeferredCall<_paramListFunctionC>();
			defdCall->params=t;
			defdCall->fptr=C;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[1];
		readTileIDs[0]=GetTileID(parentTileIDy);
		for(int i=0;i<1;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[parentTileIDy].size() > 0)
					(fnCalls[parentTileIDy].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[parentTileIDy].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDy].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	C_unroll(&x00, parentTileIDx*4+0, &y10, parentTileIDy*4+2, callStackDepth+1);
	D_unroll(&x01, parentTileIDx*4+1, &y10, parentTileIDy*4+2, callStackDepth+1);
	C_unroll(&x01, parentTileIDx*4+1, &y11, parentTileIDy*4+3, callStackDepth+1);
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}
	else if(callStackDepth > recursionDepth+4)
	{
	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	C(&x00, xData, &y10, yData, callStackDepth+1);
	D(&x01, xData, &y10, yData, callStackDepth+1);

	C(&x01, xData, &y11, yData, callStackDepth+1);

	return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
	C(&x00, xData, &y10, yData, callStackDepth+1);
	D(&x01, xData, &y10, yData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&x01, xData, &y11, yData, callStackDepth+1);
	return;
}

void D_unroll(Box* x, int parentTileIDx, Box* y, int parentTileIDy, int callStackDepth)
{
	if(callStackDepth == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'D';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			Box* b2=new Box(*y);
			p.data = b2;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionD> t = std::make_tuple(b0, nullData, b2, nullData, callStackDepth+1);
			DeferredCall<_paramListFunctionD>* defdCall = new DeferredCall<_paramListFunctionD>();
			defdCall->params=t;
			defdCall->fptr=D;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		int readTileIDs[1];
		readTileIDs[0]=GetTileID(parentTileIDy);
		for(int i=0;i<1;i++)
		{
			int readTileID=readTileIDs[i];
			if(readTileID != writeTileID)
			{
#ifdef TASK_AGGREGATION
				if(fnCalls[parentTileIDy].size() > 0)
					(fnCalls[parentTileIDy].back())->isReadBeforeNextWrite = true;
#endif
				if(localUpdate)
				{
					fnCall->numInPorts +=1;
					fnCall->params[i+1].portID = tileUpdateLog[readTileID];
				}
				if(fnCalls[parentTileIDy].size() > 0)
				{
					FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDy].back();
					lastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));
				}
			}
		}
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	D_unroll(&x00, parentTileIDx*4+0, &y11, parentTileIDy*4+3, callStackDepth+1);
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,xData), costkl = 0;
			cost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,yData);
		CELL_TYPE reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	D(&x00, xData, &y11, yData, callStackDepth+1);
	return;
}

void Unroll()
{
	int parentTileIDx=0;
	Box *x = new Box(0,0,inputSize[0],inputSize[1]);
	if(0 == recursionDepth)
	{
		int writeTileID = GetTileID(parentTileIDx);
		bool localUpdate = false;
		FunctionCall* fnCall= nullptr;
		tileUpdateLog[writeTileID]=fnCounter;
		if(IsLocalOwner(writeTileID))
		{
			CELL_TYPE* nullData=nullptr;
			fnCall=new FunctionCall();
			fnCall->functionName = 'A';
			Parameter p;
			Box* b0=new Box(*x);
			p.data = b0;
			fnCall->params.push_back(p);
			std::tuple<_paramListFunctionA> t = std::make_tuple(b0, nullData, 0+1);
			DeferredCall<_paramListFunctionA>* defdCall = new DeferredCall<_paramListFunctionA>();
			defdCall->params=t;
			defdCall->fptr=A;
			fnCall->fnCall = defdCall;
			fnCall->ID = fnCounter;
			if(fnCalls[parentTileIDx].size() > 0)
			{
				fnCall->numInPorts +=1;
				fnCall->wawSource = (fnCalls[parentTileIDx].back())->ID;
				FunctionCall* lastFunctionToUpdate = fnCalls[parentTileIDx].back();
				lastFunctionToUpdate->outPortOwners.insert(procRank);
			}
			fnCalls[parentTileIDx].push_back(fnCall);
			localUpdate = true;
		}
		fnCounter++;
		return;
	}
	A_unroll(x, 0, 0);
	delete x;
}

void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)
{
	switch(fn->functionName)
	{
		case 'A':
			{
				assert(dataRegions.size() == 1);
				DeferredCall<_paramListFunctionA>* df = (reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				(reinterpret_cast<DeferredCall<_paramListFunctionA>* >(fn->fnCall))->Run();
			}
			break;
		case 'B':
			{
				assert(dataRegions.size() == 2);
				DeferredCall<_paramListFunctionB>* df = (reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				(reinterpret_cast<DeferredCall<_paramListFunctionB>* >(fn->fnCall))->Run();
			}
			break;
		case 'C':
			{
				assert(dataRegions.size() == 2);
				DeferredCall<_paramListFunctionC>* df = (reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				(reinterpret_cast<DeferredCall<_paramListFunctionC>* >(fn->fnCall))->Run();
			}
			break;
		case 'D':
			{
				assert(dataRegions.size() == 2);
				DeferredCall<_paramListFunctionD>* df = (reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall));
				std::get<1>(df->params) = dataRegions[0];
				std::get<3>(df->params) = dataRegions[1];
				(reinterpret_cast<DeferredCall<_paramListFunctionD>* >(fn->fnCall))->Run();
			}
			break;
		default: break;
	}
}

