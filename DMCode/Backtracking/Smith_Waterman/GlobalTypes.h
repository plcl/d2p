#pragma once
#define DIMENSION 2
#define METADATASPACE (2*DIMENSION+2)
typedef int CELL_TYPE;
extern int fnCallHierarchySummary[4][4];
inline CELL_TYPE* GetDPTableCell(int i, int j, CELL_TYPE* data)
{
	CELL_TYPE* cell = data+METADATASPACE;
	int side = data[DIMENSION];
	int iOffset=i - data[1];
	cell += (iOffset*1* side);
	int jOffset=j - data[0];
	cell += jOffset;
	return cell;
}

inline CELL_TYPE* GetDPTableCellAdjacent(int i, int j, CELL_TYPE* data)
{
	CELL_TYPE* cell = data+METADATASPACE;
	int side = data[DIMENSION];
	int iOffset=i - data[1];
	int jOffset=j - data[0];
	cell += ((data[3]-1)-iOffset+jOffset);
	return cell;
}
