#include<fstream>
#include<string>
#include<iostream>

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		printf("specify input file (s) to convert to binary\n");
		return 0;
	}
	
	int i=1;
	while(i<argc)
	{
		std::string binFileName(argv[i]);
		int dotPosition = binFileName.find('.');
		if(dotPosition != std::string::npos)
		{
			binFileName.replace(dotPosition,std::string::npos,".bin");	
		}
		else
			binFileName+=".bin";
		std::ofstream  outFile(binFileName.c_str(),std::ofstream::binary);
		if(!outFile.is_open())
		{
			printf("ERROR: Unable to open output file %s\n",binFileName.c_str());
			continue;
		}
		std::ifstream inFile(argv[i],std::ifstream::in);
		if(!inFile.is_open())
		{
			printf("ERROR: Unable to open input file %s\n",argv[i]);
			i++;
			continue;
		}
		inFile.seekg(0,inFile.end);
		unsigned long fileLen = inFile.tellg();
		inFile.seekg(0,inFile.beg);
		printf("file length:%ld\n",fileLen);
		char chunk[1024];
		unsigned long j=0;
		while(j<fileLen)
		{
			inFile.read(chunk,1024);
			if(!inFile)
			{
				int numRead = inFile.gcount();
				//printf("num chunk items read: %d\n",numRead);
				chunk[numRead-1]=0;
				outFile.write(chunk,numRead);
				j+=numRead;
				printf("string read: %s\n",chunk);
				break;
			}
			outFile.write(chunk,1024);
			//printf("string read: %s\n",chunk);
			j+=1024;
		}
		printf("total chars read from file:%ld\n",j);
		inFile.close();
		outFile.close();
		i++;
	}
	return 0;
}
