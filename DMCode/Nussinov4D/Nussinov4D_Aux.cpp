#include"HelperFunctions.h"
long int inputSizeX[DIMENSION];
#define MAX_STR_LEN 45000
char* input_i=NULL;

int ReadFASTAFile(FILE *fp, int maxLen, char** seq)
{
	char line[1024];
	int seqLength = 0, seqLenToProcess;
	char* localSeq=NULL;
	fgets(line, sizeof(line), fp);
	if(line == NULL)
		return -1;
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(strlen(line)+seqLength > maxLen)
			seqLenToProcess = maxLen-seqLength;
		else
			seqLenToProcess = strlen(line);
		localSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);
		if(!localSeq)
		{
			printf("ERROR in realloc.\n");
			exit(0);
		}
		for(unsigned int i = 0; i < seqLenToProcess; ++i)
		{
			if (line[i] == '*' || line[i] == '-')
				localSeq[seqLength++] = line[i];
			else if(isalpha(line[i]))
				localSeq[seqLength++] = toupper(line[i]);

		}
		if(seqLength>=maxLen) break;
	}
	localSeq[seqLength] = '\0';
	*seq = localSeq;
	return seqLength;
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || strcmp(argv[1],"-h")==0) 
	{
		printf("Usage: ./<exec> <input_string_i> <input_length_i> \n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r");
	if(!fpi)
	{
		printf("ERROR: Unable to open input file.\n");
		exit(0);
	}
	int inputLength_I=atoi(argv[2]);
	int ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);
	fclose(fpi);
	if((ret1<0)||(input_i==NULL))
	{
		printf("ERROR: Unable to read input files correctly. file1 len:%d\n",ret1);
		exit(0);
	}
	else
	{
		for(int i=0;i<DIMENSION;i++) inputSizeX[i]=ret1-1;
	}
	return inputSizeX[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	memset(data,0,sizeof(CELL_TYPE)*(numCells));
	return;
}

void PrintResults()
{
	//PrintGlobalMax();
}
