#include"HelperFunctions.h"
#define MAX_NUM_VERTICES 45000
#define INF 65535
typedef map<int,set<pair<int,int> > > Adjacency_List;
Adjacency_List graphData;
vector<int> vertexIDList;
long int inputSizex[DIMENSION];

/*Helper function to tokenize a string */
vector<string> Split(const char *str, char c)
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(string(begin, str));
    } while (0 != *str++);

    return result;
}

/* this function is for reading wiki data set. */
int ReadGraphDataGeneral(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int weight = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		g[fromNode].insert(make_pair(toNode,weight));
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}


/* this function is for reading autonomous systems (AS) data set. */
int ReadGraphData(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int relationship = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		if((relationship == 1) || (relationship == -1))
			g[fromNode].insert(make_pair(toNode,1));
		else if((relationship == 0) || (relationship == 2))
		{
			g[fromNode].insert(make_pair(toNode,1));
			g[toNode].insert(make_pair(fromNode,1));
		}
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || strcmp(argv[1],"-h")==0)
	{
		printf("Usage: ./<exec> <input_graph> <num_vertices> (use -h for more options)\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r");
	if(!fpi)
	{
		printf("ERROR: Unable to open input file.\n");
		exit(0);
	}
	int maxVertices=MAX_NUM_VERTICES;
	maxVertices=atoi(argv[2]);
	int ret1=ReadGraphData(fpi, maxVertices, graphData);
	fclose(fpi);
	if(ret1<0)
	{
		printf("ERROR: Unable to read input file correctly.\n");
		exit(0);
	}
	else
	{
		inputSizex[0]=ret1-1; inputSizex[1]=ret1-1;
	}

	return inputSizex[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	unsigned int relIndx = 0;
	for(unsigned int i=b->coords[1];i<=b->coords[3];i++)
	{
		for(unsigned int j=b->coords[0];j<=b->coords[2];j++)
		{
			CELL_TYPE initVal=INF;
			if(i==j)
				initVal = 0;
			else
			{
				set<pair<int, int> >::iterator it=graphData[vertexIDList[i]].begin();
				while(it!=graphData[vertexIDList[i]].end())
				{
					if(it->first == vertexIDList[j])
					{
						initVal = it->second;
						break;
					}
					it++;
				}
			}
			data[relIndx++] = initVal;
		}
	}
	return;
}

void PrintResults()
{
	//DebugPrintAllCells();
}

/*
 * Terminating case for the APSP problem. Copy paste the code between == into the terminating case of a recursive method in RecursiveFunctions.cpp
 * 
==================Terminating case for method A (no dependencies. cell is computed from initial conditions.)=============================
		int i=x->coords[1], j=x->coords[0], k=x->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,xData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
==================Terminating case for methods B, C, and D (reads other cells that yData and zData point to.)=========================================
 		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;

========================================================================================================================================
 */
