#include<iostream>
#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<vector>
#include<string.h>
#include<cassert>
#include<fstream>
#include<algorithm>
#include<stdlib.h>
#include<sys/time.h>
#include<math.h>
#ifdef PARALLEL
#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif

#define TILE_WIDTH 8
using namespace std;
typedef double CELL_TYPE;
CELL_TYPE *cost;
vector<pair<int,int> > vertices;
int inputSize[2];

typedef struct Box
{
	int  coords[4]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d){coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;}
	Box(){}
	void PrintStr(char *str)const {sprintf(str,"%d %d %d %d",coords[0],coords[1],coords[2],coords[3]);}
	Box& operator=(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
		return *this;
	}
	Box(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) ||(this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]))
			flag = false;
		return flag;
	}
}Box;

void A(Box* x, CELL_TYPE* xData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, Box* z2, CELL_TYPE* z2Data, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z2, CELL_TYPE* z2Data, Box* z, CELL_TYPE* zData, int callStackDepth);

void _ReadInput(char* fileName, int maxVertices, vector<pair<int, int> >& points)
{
	string line;
	int xCoord, yCoord;
	ifstream inFile(fileName, ifstream::in);
	if(!fileName || !inFile.is_open())
	{
		printf("ERROR: Unable to open input file\n");
		exit(0);
	}
	while(!inFile.eof())
	{
		getline(inFile, line);
		if(inFile.eof())
			break;
		size_t pos = line.find(',');
		if(pos==string::npos)
		{
			printf("ERROR.Invalid input format.\n");
			exit(0);
		}
		xCoord = atoi(line.substr(0,pos).c_str());
		yCoord = atoi(line.substr(pos+1).c_str());
		points.push_back(make_pair(xCoord,yCoord));
		if(points.size()>=maxVertices)
			break;
	}
	return;
}

void ReadInput(int argc, char** argv)
{
	if(argc != 3)
	{
		printf("Usage: ./exe <input> <numvertices>\n");
		exit(0);
	}
	_ReadInput(argv[1],atoi(argv[2]),vertices);
	inputSize[0] = vertices.size()-1;inputSize[1] = vertices.size()-1;
	printf("%d %d\n",inputSize[0],inputSize[1]);
}

CELL_TYPE Distance(int i, int j)
{
	int x1=vertices[i].first;
	int y1=vertices[i].second;
	int x2=vertices[j].first;
	int y2=vertices[j].second;
	return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

CELL_TYPE Weight(int i, int j, int k)
{
	return Distance(i,j)+Distance(j,k)+Distance(k,i);
}

CELL_TYPE* InitializeDPTable(Box* b)
{
	int numCells = (b->coords[2]-b->coords[0]+1) * (b->coords[3]-b->coords[1]+1);
	CELL_TYPE* newTile = new CELL_TYPE[numCells+4];
	newTile[0] = b->coords[0];
	newTile[1] = b->coords[1];
	newTile[2] = (b->coords[2]-b->coords[0]+1);
	newTile[3] = (b->coords[3]-b->coords[1]+1);
	unsigned int relIndx = 4;
	for(unsigned int i=b->coords[1];i<=b->coords[3];i++)
		for(unsigned int j=b->coords[0];j<=b->coords[2];j++)
			newTile[relIndx++] = ((i==j)||(i==j-1))?0:FLT_MAX;
	return newTile;
}


inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data+4;
	int xOffset = (i - data[0]);
	int yOffset = (j - data[1]); 
	int side  = data[2];
	ret = ret + (xOffset * side)+yOffset;
	return ret;
}

void PrintResults()
{
	CELL_TYPE* result = GetDPTableCell(0,inputSize[0],cost);
	if(result!=NULL)
		std::cout<<"Minimum cost of triangulation:"<<*result<<std::endl;
}

int main(int argc, char* argv[])
{
	int numVertices = 0;
	
	if(argc != 3)
	{
		printf("Usage:./exe inputFile length\n");
		return 0;
	}
	//matrix chain is specified in a file input to the program. Read the file and populate matrixChain
	ReadInput(argc,argv);

	numVertices = vertices.size();
	//The program currently works for matrix chain lengths that are power of two. 
	/*if(!PowerOfTwo(numVertices))
	{
		printf("Matrix chain length must be power of 2. Exiting.\n");
		return 0; 
	}*/
	Box matrixDim(0,0,numVertices-1,numVertices-1);
	cost = InitializeDPTable(&matrixDim);
	printf("Num vertices in polygon :%d\n",numVertices);	
#ifdef PARALLEL
	int nworkers=0;
	nworkers= __cilkrts_get_nworkers();
	printf("number of total Cilk worker threads available:%d. numworkers to set:%d\n", nworkers,1);
	if (0!= __cilkrts_set_param("nworkers","1")) {
            printf("Failed to set worker count %s\n","1");
            return 0;
        }
#endif

	//Set the bounds of DP table.
	struct timeval startTime, endTime;
	
	gettimeofday(&startTime,0);
	//Specify the bounds of DP table, whose cells are to be computed.
	A(&matrixDim, cost, 0);
	gettimeofday(&endTime,0);

	//Print results
	printf("Optimal cost : %f\n",*GetDPTableCell(0,numVertices-1,cost));
	PrintResults();
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;
}


void A(Box* x, CELL_TYPE* xData, int callStackDepth)
{
#ifdef DEBUG
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=x->coords[0];
		if((i==j)||(i==j-1)||(i==k))
			return;
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		x->PrintStr(yindx);
		x->PrintStr(zindx);
		printf("A: x: %s y:%s z:%s\n",xindx,yindx,zindx);
	}
#endif

	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=x->coords[0];
		if((i == j)||(i==j-1)||(i==k))
			return;
		CELL_TYPE w_ikj=Weight(i,j,k);
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,xData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
#ifdef DEBUG
		printf("A: (%d %d)(%d %d)=(%d %d)\n",i,k,k,j,i,j);
		assert((i==x->coords[1]) && (k==x->coords[0]));
		assert((k==x->coords[1]) && (j==x->coords[0]));
#endif
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
		A(&x00, xData, callStackDepth+1);
	A(&x11, xData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	B(&x01, xData, &x00, xData, &x11, xData, &x11, xData, callStackDepth+1);
	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, Box* z2, CELL_TYPE* z2Data, int callStackDepth)
{
#ifdef DEBUG
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=y->coords[0];
		if((i==j)||(i==j-1)||(i==k))
			return;
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("B: x: %s y:%s z:%s\n",xindx,yindx,zindx);
	}
#endif

	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=y->coords[0];
		if((i == j)||(i==j-1)||(i==k))
			return;
		CELL_TYPE w_ikj=Weight(i,j,k);
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
#ifdef DEBUG
		printf("B: (%d %d)(%d %d)=(%d %d)\n",i,k,k,j,i,j);
		assert((i==y->coords[1]) && (k==y->coords[0]));
		assert(((k==z->coords[1]) && (j==z->coords[0]))||((k==z2->coords[1]) && (j==z2->coords[0])));
#endif

		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);

	B(&x10, xData, &y11, yData, &z00, zData, &z211, z2Data, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x00, xData, &y01, yData, &z211, z2Data, &x10, xData, callStackDepth+1);
	C(&x11, xData, &x10, xData, &z11, zData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
		B(&x00, xData, &y00, yData, &z00, zData, &x10, xData, callStackDepth+1);
	B(&x11, xData, &y11, yData, &z11, zData, &z212, z2Data, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	C(&x01, xData, &y01, yData, &z212, z2Data, &x11, xData, callStackDepth+1);
	C(&x01, xData, &x00, xData, &z11, zData, &z01, zData, callStackDepth+1);
	B(&x01, xData, &y00, yData, &z11, zData, &x11, xData, callStackDepth+1);
	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z2, CELL_TYPE* z2Data, Box* z, CELL_TYPE* zData, int callStackDepth)
{
#ifdef DEBUG
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=y->coords[0];
		if((i==j)||(i==j-1)||(i==k))
			return;
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("C: x: %s y:%s z:%s\n",xindx,yindx,zindx);
	}
#endif

	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1];
		int j=x->coords[0];
		int k=y->coords[0];
		if((i == j)||(i==j-1)||(i==k))
			return;
		CELL_TYPE w_ikj=Weight(i,j,k);
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
#ifdef DEBUG
		printf("C: (%d %d)(%d %d)=(%d %d)\n",i,k,k,j,i,j);
		assert((i==y->coords[1]) && (k==y->coords[0]));
		assert(((k==z->coords[1]) && (j==z->coords[0]))||((k==z2->coords[1]) && (j==z2->coords[0])));
#endif

		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);
	Box z211(z2->coords[0],z2->coords[1],z2->coords[0]+(z2->coords[2]-z2->coords[0]+1)/2-1,z2->coords[1]+(z2->coords[3]-z2->coords[1]+1)/2-1);
	Box z222(z211.coords[2]+1,z211.coords[3]+1,z2->coords[2],z2->coords[3]);
	Box z212(z222.coords[0],z211.coords[1],z2->coords[2],z211.coords[3]);

#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x00, xData, &y00, yData, &z10, zData, &z00, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x01, xData, &y00, yData, &z11, zData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x10, xData, &y10, yData, &z10, zData, &z00, zData, callStackDepth+1);
	C(&x11, xData, &y10, yData, &z11, zData, &z01, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x00, xData, &y01, yData, &z211, z2Data, &z10, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x01, xData, &y01, yData, &z212, z2Data, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_spawn
#endif
		C(&x10, xData, &y11, yData, &z211, z2Data, &z10, zData, callStackDepth+1);
	C(&x11, xData, &y11, yData, &z212, z2Data, &z11, zData, callStackDepth+1);
#ifdef PARALLEL
	cilk_sync;
#endif

	return;
}
