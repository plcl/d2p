#include<stdio.h>
#include<limits.h>
#include<string.h>
#include<stdlib.h>
#include<vector>
#include<fstream>
#include<sys/time.h>
#ifdef PARALLEL
#include<omp.h>
#endif

typedef int CELL_TYPE;

using namespace std;
CELL_TYPE *cost;
vector<int> matrixChain;
int dpTableRowLen;

void ReadInput(char* fileName, vector<int>& matrixChain, int maxVertices);

inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data;
	ret = ret + (j * dpTableRowLen)+i;
	return ret;
}

int main(int argc, char* argv[])
{
	//input holder
	//to specify matrix dimensions
	pair<short, short> dim;
	int numMatrices = 0;

#ifdef DEBUG
	if(argc !=4)
	{
		//creating input
		matrixChain.push_back(3);
		matrixChain.push_back(5);
		matrixChain.push_back(2);
		matrixChain.push_back(5);
		matrixChain.push_back(3);
	}
	else
#else

	//creating input
	if(argc != 4)
	{
		printf("Usage:./matchain_xx numthreads inputFile maxvertices\n");
		return 0;
	}
#endif
	//matrix chain is specified in a file input to the program. Read the file and populate matrixChain
	ReadInput(argv[2],matrixChain, atoi(argv[3]));
	
	numMatrices = matrixChain.size()-1;

	//allocating memory for DP cost table
	dpTableRowLen = numMatrices+1;
	int dpTableSize = dpTableRowLen * dpTableRowLen;
	cost = new CELL_TYPE[dpTableSize];
	//initializing DP cost table with base values;
	for(int i=0;i<dpTableSize;i++)
		cost[i]= INT_MAX;
	for(int i=0;i<dpTableSize;i++)
	{
		if(i<dpTableRowLen)
			cost[i*dpTableRowLen+i]= 0;
	}
	printf("Matrix chain size :%d\n",numMatrices);	

#ifdef PARALLEL
	int numProcs = omp_get_num_procs();
	int numThreads = omp_get_num_threads();
	printf("Number of processors available %d threads:%d. num threads to set:%d\n",numProcs,numThreads,atoi(argv[1]));		
	omp_set_dynamic(0);
	omp_set_num_threads(atoi(argv[1]));
#endif
	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	//computing optimal cost
	for(int l=1;l<numMatrices;l++)
	{
		int i;
#ifdef PARALLEL
	#pragma omp parallel for shared(numMatrices,l,cost,matrixChain)
#endif
		for(i=1;i<numMatrices-l+1;i++)
		{

/*#ifdef PARALLEL
			int curThread=omp_get_thread_num();
			if(curThread==0)
			{
				printf("number of threads set:%d\n",omp_get_num_threads());
			}
#endif*/

			int j=i+l;	
			for(int k=i;k<j;k++)
			{
				CELL_TYPE* cost_ij = GetDPTableCell(i,j,cost);
				CELL_TYPE* cost_ik = GetDPTableCell(i,k,cost);
				CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,cost);
				int newCost = *cost_ik + *cost_kj + matrixChain[i-1] * matrixChain[k] * matrixChain[j];	
				if(newCost < *cost_ij)
					*cost_ij=newCost;
			}
			//printf("(%d %d) = %d\n",i,j,cost[i][j]);
		}
	}
	gettimeofday(&endTime,0);
	printf("Optimal cost : %ld\n",*GetDPTableCell(1,numMatrices,cost));
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;
}


/* Funtion to read an input file and populate the matrixChain list*/
void ReadInput(char* fileName, vector<int>& matrixChain, int maxVertices)
{
	char matrixDimStr[8];
	int matrixDim;
	ifstream inFile(fileName, ifstream::in); 
	while(!inFile.eof())
	{
		inFile.getline(matrixDimStr,8);
		if(inFile.eof())
			break;
		matrixDim = atoi(matrixDimStr);
		memset(matrixDimStr,0,8);
		matrixChain.push_back(matrixDim);
		if(matrixChain.size() == maxVertices)
			break;
	}
#ifdef DEBUG
	/*printf("Matrix Dimensions: ");
	for(int i=0;i<matrixChain.size();i++)
	{
		printf("%d ",matrixChain[i]);
	}
	printf("\n");*/
#endif
	return;
}

