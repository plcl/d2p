#include<stdio.h>
#include<math.h>
#include<limits.h>
#include<float.h>
#include<string.h>
#include<stdlib.h>
#include<vector>
#include<fstream>
#include<sys/time.h>
#include<cassert>
#ifdef PARALLEL
#include<omp.h>
#endif

typedef double CELL_TYPE;

using namespace std;
CELL_TYPE *cost;
vector<pair<int,int> > vertices;
int dpTableRowLen;

CELL_TYPE Distance(int i, int j)
{
	int x1=vertices[i].first;
	int y1=vertices[i].second;
	int x2=vertices[j].first;
	int y2=vertices[j].second;
	//truncation is intentional. TODO: generic DP cell types.
	return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); 
}

CELL_TYPE Weight(int i, int j, int k)
{
	return Distance(i,j)+Distance(j,k)+Distance(k,i);
}

void ReadInput(char* fileName, int maxVertices, vector<pair<int,int> >& points);

inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data;
	ret = ret + (i * dpTableRowLen)+j;
	return ret;
}

int main(int argc, char* argv[])
{
	int numVertices = 0;
	
	//creating input
	if(argc == 3)
	{
		//matrix chain is specified in a file input to the program. Read the file and populate matrixChain
		ReadInput(argv[1],atoi(argv[2]),vertices);
	}
	else
	{
		printf("Usage:./exe <input> <numVertices>\n");
		exit(0);
	}
	
	numVertices = vertices.size();

	//allocating memory for DP cost table
	dpTableRowLen = numVertices;
	long long dpTableSize = dpTableRowLen * dpTableRowLen;
	printf("dp table size:%llu\n",dpTableSize);
	cost = new CELL_TYPE[dpTableSize];
	//initializing DP cost table with base values;
	for(int i=0;i<dpTableSize;i++)
		cost[i]= FLT_MAX;
	for(int i=0;i<dpTableRowLen;i++)
	{
		cost[i*dpTableRowLen+i]= 0;
		if(i!=dpTableRowLen-1)
			cost[i*dpTableRowLen+i+1]= 0;
	}
	printf("Num vertices in polygon :%d\n",numVertices);	

#ifdef PARALLEL
	int numProcs = omp_get_num_procs();
	printf("Number of processors available %d\n",numProcs);		
#endif
	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	//computing optimal cost
	for(int l=1;l<numVertices;l++)
	{
		int i;
#ifdef PARALLEL
	#pragma omp parallel for shared(numVertices,l,cost,vertices)
#endif
		for(i=0;i<numVertices-l;i++)
		{
			int j=i+l;	
			for(int k=i+1;k<j;k++)
			{
				//printf("(%d %d) + (%d %d) = (%d %d)\n",i,k,k,j,i,j);
				CELL_TYPE* cost_ij = GetDPTableCell(i,j,cost);
				CELL_TYPE* cost_ik = GetDPTableCell(i,k,cost);
				CELL_TYPE* cost_kj = GetDPTableCell(k,j,cost);
				CELL_TYPE newCost = *cost_ik + *cost_kj + Weight(i,k,j);	
				if(newCost < *cost_ij)
					*cost_ij=newCost;
			}
#ifdef DEBUG
			//printf("(%d %d) = %f\n",i,j,*GetDPTableCell(i,j,cost));
#endif
		}
	}
	gettimeofday(&endTime,0);
	printf("Optimal cost : %f\n",*GetDPTableCell(0,numVertices-1,cost));
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;
}


/* Funtion to read an input file and populate the matrixChain list*/
void ReadInput(char* fileName, int maxVertices, vector<pair<int,int> >& points)
{
	string line;//[128];
	int xCoord,yCoord;
	ifstream inFile(fileName, ifstream::in);
	if(!fileName || !inFile.is_open())
	{
		printf("ERROR: Unable to open input file\n");
		exit(0);
	}
	
	while(!inFile.eof())
	{
		getline(inFile,line);
		if(inFile.eof())
			break;
		size_t pos = line.find(',');
		if(pos == string::npos)
		{
			printf("ERROR. Invalid input format\n");
			exit(0);
		}
		xCoord = atoi(line.substr(0,pos).c_str());
		yCoord = atoi(line.substr(pos+1).c_str());
		points.push_back(make_pair(xCoord,yCoord));
		if(points.size() >= maxVertices)
			break;
	}
#ifdef DEBUG
	vector<pair<int,int> >::iterator iter = vertices.begin();
	for(;iter!=vertices.end();iter++)	
	{
		printf("(%d %d)\n",iter->first,iter->second);
	}
#endif
	return;
}

