#include<stdio.h>
#include<limits.h>
#include<vector>
#include<string.h>
#include<cassert>
#include<fstream>
#include<algorithm>
#include<stdlib.h>
#include<sys/time.h>
#ifdef PARALLEL
#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif

#define TILE_WIDTH 8
using namespace std;
typedef int CELL_TYPE;
CELL_TYPE *cost;
vector<int> matrixChain;
int dpTableRowLen;

typedef struct Box
{
	int tlx, tly, brx, bry; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d):tlx(a),tly(b),brx(c),bry(d){}
	Box(){}
	Box(const Box& b):tlx(b.tlx),tly(b.tly),brx(b.brx),bry(b.bry){}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->tlx!=rhs.tlx) ||(this->tly!=rhs.tly) || (this->brx!=rhs.brx) || (this->bry!=rhs.bry))
			flag = false;
		return flag;
	}

	char* PrintStr(char *str)const {sprintf(str,"%d,%d,%d,%d",tly,tlx,bry, brx);}
}Box;

void ReadInput(char* fileName, vector<int>& matrixChain, int maxVertices);
bool PowerOfTwo(int num);
void A(const Box& x, const Box& y, const Box& z, int callStackDepth=0);
void B(const Box& x, const Box& u, const Box& v, const Box& v2, int callStackDepth);
void C(const Box& x, const Box& u1, const Box& u2, const Box& v, int callStackDepth);
#ifdef PARALLEL
//functions specific to parallel version
void D(const Box& x, const Box& u1, const Box& v12, const Box& v1, const Box& u2, const Box& v2, const Box& v22, int callStackDepth);
void E(const Box& x, const Box* UV, const Box& V2, int callStackDepth);
#endif

inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data;
	ret = ret + (i * dpTableRowLen)+j;
	return ret;
}

int main(int argc, char* argv[])
{
	int numMatrices = 0;
	
#ifdef DEBUG
	if(argc !=4)
	{
		//creating input
		matrixChain.push_back(3);
		matrixChain.push_back(5);
		matrixChain.push_back(2);
		matrixChain.push_back(5);
		matrixChain.push_back(3);
	}
	else
#else
	if(argc !=4)
	{
		printf("Usage:./matchain_xx numthreads inputFile maxvertices\n");
		return 0;
	}
#endif
	//matrix chain is specified in a file input to the program. Read the file and populate matrixChain
	ReadInput(argv[2],matrixChain,atoi(argv[3]));

	numMatrices = matrixChain.size()-1;
	//The program currently works for matrix chain lengths that are power of two. 
	/*if(!PowerOfTwo(numMatrices))
	{
		printf("Matrix chain length (%d) must be power of 2. Exiting.\n", numMatrices);
		return 0; 
	}*/

	//allocating memory for DP cost table
	dpTableRowLen = numMatrices+1;
	int dpTableSize = dpTableRowLen * dpTableRowLen;
	cost = new CELL_TYPE[dpTableSize];

	//initializing DP cost table with base values;
	for(int i=0;i<dpTableSize;i++)
		cost[i]= INT_MAX;
	for(int i=0;i<dpTableRowLen;i++)
		cost[i*dpTableRowLen+i]= 0;

	printf("Matrix chain size :%d\n",numMatrices);	

	//Set the bounds of DP table.
	Box matrixDim(1,1,numMatrices,numMatrices);
#ifdef PARALLEL
	int nworkers=0;
	nworkers= __cilkrts_get_nworkers();
	printf("number of total Cilk worker threads available:%d. numworkers to set:%d\n", nworkers,atoi(argv[1]));
	if (0!= __cilkrts_set_param("nworkers",argv[1])) {
            printf("Failed to set worker count %s\n",atoi(argv[1]));
            return 0;
        }
#endif

	struct timeval startTime, endTime;
	
	gettimeofday(&startTime,0);
	//Specify the bounds of DP table, whose cells are to be computed.
	A(matrixDim, matrixDim, matrixDim);
	gettimeofday(&endTime,0);

	//Print results
	printf("Optimal cost : %ld\n",*GetDPTableCell(1,numMatrices,cost));//cost[2*dpTableRowLen-1]); //==[1][numMatrices]
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;
}


/* Funtion to read an input file and populate the matrixChain list*/
void ReadInput(char* fileName, vector<int>& matrixChain, int maxVertices)
{
	char matrixDimStr[8];
	int matrixDim;
	ifstream inFile(fileName, ifstream::in); 
	while(!inFile.eof())
	{
		inFile.getline(matrixDimStr,8);
		if(inFile.eof())
			break;
		matrixDim = atoi(matrixDimStr);
		memset(matrixDimStr,0,8);
		matrixChain.push_back(matrixDim);
		if(matrixChain.size() == maxVertices)
			break;
	}
	return;
}

/* Funtion to check if an input number is a power of two or not */
bool PowerOfTwo(int num)
{
	if((num & (num-1)) == 0)
		return true;
	else 
		return false;
}

/* Recursive function to decompose a triangle into two subtriangles and a square */
void A(const Box& x, const Box& y, const Box& z, int callStackDepth)
{
#ifdef DEBUG
		if((x.tlx==x.brx) && (x.tly==x.bry))
		{
			if(x.tly == x.tlx)
				return;
		}
		char xindx[32], yindx[32], zindx[32];
		x.PrintStr(xindx);
		y.PrintStr(yindx);
		z.PrintStr(zindx);
		printf("A: x: %s y:%s z:%s\n",xindx,yindx,zindx);
#endif

	if(x.bry-x.tly<=TILE_WIDTH-1)
	{
		assert((y.bry-y.tly<=TILE_WIDTH-1) && (z.bry-z.tly<=TILE_WIDTH-1));
		int seqLen = x.bry-x.tly+1;
		for(int l=seqLen;l>=2;l--)
		{
	/*#ifdef PARALLEL
		#pragma omp parallel for shared(x,y,l,seqLen,cost,matrixChain)
	#endif*/
			for(int i=1;i<=seqLen-l+1;i++)
			{
				int j=i+l-1;
				int transi = x.tly+j-1;
				int transj = x.tlx+i-1;
				if(transi >= transj)
					continue;
				CELL_TYPE* cost_ij = GetDPTableCell(transi,transj,cost);
				int startK=(transi>y.tlx)?transi:y.tlx;
				int k=startK;
				while(k<transj)
				{
					CELL_TYPE* cost_ik = GetDPTableCell(transi,k,cost);
					CELL_TYPE* cost_kj = GetDPTableCell(k+1,transj,cost);
					CELL_TYPE w_ikj = matrixChain[transi-1] * matrixChain[k] * matrixChain[transj];	
					CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
#ifdef DEBUG
					CELL_TYPE tmpNewCost = (newCost < *cost_ij)?newCost:*cost_ij;
					printf("A: (%d %d)(%d %d)=(%d %d) cost_ik:%d  + cost_kj:%d + w_ikj:%d = %d cost_ij(old):%d cost_ij(new):%d\n",transi,k,k+1,transj,transi,transj,*cost_ik, *cost_kj, w_ikj,newCost, *cost_ij, tmpNewCost);
#endif
					if(newCost < *cost_ij)
						*cost_ij=newCost;
					k++;
					if(k==(y.brx+1))
						k=x.tlx;
				}
			}
		}
		for(int l=1;l<=seqLen;l++)
		{
	/*#ifdef PARALLEL
		#pragma omp parallel for shared(x,y,l,seqLen,cost,matrixChain)
	#endif*/
			for(int i=1;i<=seqLen-l+1;i++)
			{
				int j=i+l-1;
				int transi = x.tly+i-1;
				int transj = x.tlx+j-1;
				if(transi >= transj)
					continue;
				CELL_TYPE* cost_ij = GetDPTableCell(transi,transj,cost);
				int startK=(transi>y.tlx)?transi:y.tlx;
				int k=startK;
				while(k<transj)
				{
					CELL_TYPE* cost_ik = GetDPTableCell(transi,k,cost);
					CELL_TYPE* cost_kj = GetDPTableCell(k+1,transj,cost);
					int w_ikj = matrixChain[transi-1] * matrixChain[k] * matrixChain[transj];	
					CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
#ifdef DEBUG
					CELL_TYPE tmpNewCost = (newCost < *cost_ij)?newCost:*cost_ij;
					printf("A: (%d %d)(%d %d)=(%d %d) cost_ik:%d  + cost_kj:%d + w_ikj:%d = %d cost_ij(old):%d cost_ij(new):%d\n",transi,k,k+1,transj,transi,transj,*cost_ik, *cost_kj, w_ikj,newCost, *cost_ij, tmpNewCost);
#endif
					if(newCost < *cost_ij)
						*cost_ij=newCost;
					k++;
					if(k==(y.brx+1))
						k=x.tlx;
				}
			}
		}
		return;
	}
	
	Box x11(x.tlx,x.tly,x.tlx+(x.brx-x.tlx+1)/2-1,x.tly+(x.bry-x.tly+1)/2-1);
	Box x22(x11.brx+1,x11.bry+1,x.brx,x.bry);
	Box x12(x22.tlx,x11.tly,x.brx,x11.bry);
	Box x21(x.tlx,x22.tly,x11.brx,x.bry);
 
#ifdef PARALLEL
	cilk_spawn A(x11,x11,x11);	
#else
	A(x11,x11,x11,callStackDepth+1);	
#endif
	A(x22,x22,x22, callStackDepth+1);	
#ifdef PARALLEL
	cilk_sync;
#endif
	B(x12,x11,x22,x22,callStackDepth+1);	
}

/* Recursive function to decompose a square into 4 quadrants */
void B(const Box& x, const Box& u, const Box& v, const Box& v2, int callStackDepth)
{
#ifdef DEBUG
		if((x.tlx==x.brx) && (x.tly==x.bry))
		{
			if(x.tly == x.tlx)
				return;
		}
		char xindx[32], uindx[32], vindx[32], v2indx[32];
		x.PrintStr(xindx);
		u.PrintStr(uindx);
		v.PrintStr(vindx);
		v2.PrintStr(v2indx);
		printf("B: x: %s u:%s v:%s v2:%s\n",xindx,uindx,vindx, v2indx);
#endif
	if(x.bry-x.tly<=TILE_WIDTH-1)
	{
		assert((u.bry-u.tly<=TILE_WIDTH-1) && (v.bry-v.tly<=TILE_WIDTH-1));
		int seqLen = x.bry-x.tly+1;
		for(int l=seqLen;l>=2;l--)
		{
	/*#ifdef PARALLEL
		#pragma omp parallel for shared(x,u,l,seqLen,cost,matrixChain)
	#endif*/
			for(int i=1;i<=seqLen-l+1;i++)
			{
				int j=i+l-1;
				int transi = x.tly+j-1;
				int transj = x.tlx+i-1;
				if(transi >= transj)
					continue;
				CELL_TYPE* cost_ij = GetDPTableCell(transi,transj,cost);
				int startK=(transi>u.tlx)?transi:u.tlx;
				int k=startK;
				while(k<transj)
				{
					CELL_TYPE* cost_ik = GetDPTableCell(transi,k,cost);
					CELL_TYPE* cost_kj = GetDPTableCell(k+1,transj,cost);
					int w_ikj = matrixChain[transi-1] * matrixChain[k] * matrixChain[transj];	
					CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
#ifdef DEBUG
					CELL_TYPE tmpNewCost = (newCost < *cost_ij)?newCost:*cost_ij;
					printf("B: (%d %d)(%d %d)=(%d %d) cost_ik:%d  + cost_kj:%d + w_ikj:%d = %d cost_ij(old):%d cost_ij(new):%d\n",transi,k,k+1,transj,transi,transj,*cost_ik, *cost_kj, w_ikj,newCost, *cost_ij, tmpNewCost);
#endif
					if(newCost < *cost_ij)
						*cost_ij=newCost;
					k++;
					if(k==(u.brx+1))
						k=x.tlx;
				}
			}
		}
		for(int l=1;l<=seqLen;l++)
		{
	/*#ifdef PARALLEL
		#pragma omp parallel for shared(x,u,l,seqLen,cost,matrixChain)
	#endif*/
			for(int i=1;i<=seqLen-l+1;i++)
			{
				int j=i+l-1;
				int transi = x.tly+i-1;
				int transj = x.tlx+j-1;
				if(transi >= transj)
					continue;
				CELL_TYPE* cost_ij = GetDPTableCell(transi,transj,cost);
				int startK=(transi>u.tlx)?transi:u.tlx;
				int k=startK;
				while(k<transj)
				{
					CELL_TYPE* cost_ik = GetDPTableCell(transi,k,cost);
					CELL_TYPE* cost_kj = GetDPTableCell(k+1,transj,cost);
					int w_ikj = matrixChain[transi-1] * matrixChain[k] * matrixChain[transj];	
					CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
#ifdef DEBUG
					int tmpNewCost = (newCost < *cost_ij)?newCost:*cost_ij;
					printf("B: (%d %d)(%d %d)=(%d %d) cost_ik:%d  + cost_kj:%d + w_ikj:%d = %d cost_ij(old):%d cost_ij(new):%d\n",transi,k,k+1,transj,transi,transj,*cost_ik, *cost_kj, w_ikj,newCost, *cost_ij, tmpNewCost);
#endif
					if(newCost < *cost_ij)
						*cost_ij=newCost;
					k++;
					if(k==(u.brx+1))
						k=x.tlx;
				}
			}
		}
		return;
	}
	
	Box x11(x.tlx,x.tly,x.tlx+(x.brx-x.tlx+1)/2-1,x.tly+(x.bry-x.tly+1)/2-1);
	Box x22(x11.brx+1,x11.bry+1,x.brx,x.bry);
	Box x12(x22.tlx,x11.tly,x.brx,x11.bry);
	Box x21(x.tlx,x22.tly,x11.brx,x.bry);
 
	Box u11(u.tlx,u.tly,u.tlx+(u.brx-u.tlx+1)/2-1,u.tly+(u.bry-u.tly+1)/2-1);
	Box u22(u11.brx+1,u11.bry+1,u.brx,u.bry);
	Box u12(u22.tlx,u11.tly,u.brx,u11.bry);

	Box v11(v.tlx,v.tly,v.tlx+(v.brx-v.tlx+1)/2-1,v.tly+(v.bry-v.tly+1)/2-1);
	Box v22(v11.brx+1,v11.bry+1,v.brx,v.bry);
	Box v12(v22.tlx,v11.tly,v.brx,v11.bry);

	Box v211(v2.tlx,v2.tly,v2.tlx+(v2.brx-v2.tlx+1)/2-1,v2.tly+(v2.bry-v2.tly+1)/2-1);
	Box v222(v211.brx+1,v211.bry+1,v2.brx,v2.bry);
	Box v212(v222.tlx,v211.tly,v2.brx,v211.bry);

	B(x21,u22,v11,v211,callStackDepth+1);	
	
	
#ifdef PARALLEL
	cilk_spawn D(x11,u12,v211,x21,u11,v11,x21, callStackDepth);
	D(x22,x21,v22,v12,u22,v22,v212, callStackDepth); 
#else

	C(x11,u12,v211,x21,callStackDepth+1);

	C(x22,x21,v22,v12,callStackDepth+1);

	B(x11,u11,v11,x21,callStackDepth+1);

	B(x22,u22,v22,v212,callStackDepth+1);
#endif
	
		
#ifdef PARALLEL
	cilk_sync;
#endif

	C(x12,u12,v212,x22,callStackDepth+1);

	C(x12,x11,v22,v12,callStackDepth+1);

	B(x12,u11,v22,x22,callStackDepth+1);
	return;
}


/* Recursive function to compute all quadrants of a square (no inter-quadrant dependencies) quickly */
void C(const Box& x, const Box& u, const Box& v2, const Box& v, int callStackDepth)
{
#ifdef DEBUG
	if((x.tlx==x.brx) && (x.tly==x.bry))
	{
		if(x.tly == x.tlx)
			return;
	}
	char xindx[32], uindx[32], v2indx[32], vindx[32];
	x.PrintStr(xindx);
	u.PrintStr(uindx);
	v2.PrintStr(v2indx);
	v.PrintStr(vindx);
	printf("C: x: %s u:%s v2:%s v:%s\n",xindx,uindx,v2indx,vindx);
#endif
	if(x.bry-x.tly<=TILE_WIDTH-1)
	{
		assert((u.bry-u.tly<=TILE_WIDTH-1) && (v.bry-v.tly<=TILE_WIDTH-1));
		int seqLen = x.bry-x.tly+1;
		for(int l=seqLen;l>=2;l--)
		{
	/*#ifdef PARALLEL
		#pragma omp parallel for shared(x,u,l,seqLen,cost,matrixChain)
	#endif*/
			for(int i=1;i<=seqLen-l+1;i++)
			{
				int j=i+l-1;
				int transi = x.tly+j-1;
				int transj = x.tlx+i-1;
				if(transi >= transj)
					continue;
				CELL_TYPE* cost_ij = GetDPTableCell(transi,transj,cost);
				int startK=(transi>u.tlx)?transi:u.tlx;
				int k=startK;
				while(k<transj)
				{
					CELL_TYPE* cost_ik = GetDPTableCell(transi,k,cost);
					CELL_TYPE* cost_kj = GetDPTableCell(k+1,transj,cost);
					int w_ikj = matrixChain[transi-1] * matrixChain[k] * matrixChain[transj];	
					CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
#ifdef DEBUG
					CELL_TYPE tmpNewCost = (newCost < *cost_ij)?newCost:*cost_ij;
					printf("C: (%d %d)(%d %d)=(%d %d) cost_ik:%d  + cost_kj:%d + w_ikj:%d = %d cost_ij(old):%d cost_ij(new):%d\n",transi,k,k+1,transj,transi,transj,*cost_ik, *cost_kj, w_ikj,newCost, *cost_ij, tmpNewCost);
#endif
					if(newCost < *cost_ij)
						*cost_ij=newCost;
					k++;
					if(k==(u.brx+1))
						k=x.tlx;
				}
			}
		}
		for(int l=1;l<=seqLen;l++)
		{
	/*#ifdef PARALLEL
		#pragma omp parallel for shared(x,u,l,seqLen,cost,matrixChain)
	#endif*/
			for(int i=1;i<=seqLen-l+1;i++)
			{
				int j=i+l-1;
				int transi = x.tly+i-1;
				int transj = x.tlx+j-1;
				if(transi >= transj)
					continue;
				CELL_TYPE* cost_ij = GetDPTableCell(transi,transj,cost);
				int startK=(transi>u.tlx)?transi:u.tlx;
				int k=startK;
				while(k<transj)
				{
					CELL_TYPE* cost_ik = GetDPTableCell(transi,k,cost);
					CELL_TYPE* cost_kj = GetDPTableCell(k+1,transj,cost);
					int w_ikj = matrixChain[transi-1] * matrixChain[k] * matrixChain[transj];	
					CELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;
#ifdef DEBUG
					int tmpNewCost = (newCost < *cost_ij)?newCost:*cost_ij;
					printf("C: (%d %d)(%d %d)=(%d %d) cost_ik:%d  + cost_kj:%d + w_ikj:%d = %d cost_ij(old):%d cost_ij(new):%d\n",transi,k,k+1,transj,transi,transj,*cost_ik, *cost_kj, w_ikj,newCost, *cost_ij, tmpNewCost);
#endif
					if(newCost < *cost_ij)
						*cost_ij=newCost;
					k++;
					if(k==(u.brx+1))
						k=x.tlx;
				}
			}
		}
		return;
	}
	

	Box x11(x.tlx,x.tly,x.tlx+(x.brx-x.tlx+1)/2-1,x.tly+(x.bry-x.tly+1)/2-1);
	Box x22(x11.brx+1,x11.bry+1,x.brx,x.bry);
	Box x12(x22.tlx,x11.tly,x.brx,x11.bry);
	Box x21(x.tlx,x22.tly,x11.brx,x.bry);
 
	Box v11(v.tlx,v.tly,v.tlx+(v.brx-v.tlx+1)/2-1,v.tly+(v.bry-v.tly+1)/2-1);
	Box v22(v11.brx+1,v11.bry+1,v.brx,v.bry);
	Box v12(v22.tlx,v11.tly,v.brx,v11.bry);
	Box v21(v.tlx,v22.tly,v11.brx,v.bry);


	Box u11(u.tlx,u.tly,u.tlx+(u.brx-u.tlx+1)/2-1,u.tly+(u.bry-u.tly+1)/2-1);
	Box u22(u11.brx+1,u11.bry+1,u.brx,u.bry);
	Box u12(u22.tlx,u11.tly,u.brx,u11.bry);
	Box u21(u.tlx,u22.tly,u11.brx,u.bry);


	Box v211(v2.tlx,v2.tly,v2.tlx+(v2.brx-v2.tlx+1)/2-1,v2.tly+(v2.bry-v2.tly+1)/2-1);
	Box v222(v211.brx+1,v211.bry+1,v2.brx,v2.bry);
	Box v212(v222.tlx,v211.tly,v2.brx,v211.bry);
	

#ifdef PARALLEL
	Box tmpB[4][4];
	//composing arguments for updates to all quadrants.
	tmpB[0][0] = u11;tmpB[0][1] = v11;tmpB[0][2] = u12;tmpB[0][3] = v21;
	tmpB[1][0] = u11;tmpB[1][1] = v12;tmpB[1][2] = u12;tmpB[1][3] = v22;
	tmpB[2][0] = u21;tmpB[2][1] = v11;tmpB[2][2] = u22;tmpB[2][3] = v21;
	tmpB[3][0] = u21;tmpB[3][1] = v12;tmpB[3][2] = u22;tmpB[3][3] = v22;

	Box tmpV2[4];
	tmpV2[0]=v211;	tmpV2[1]=v212;	tmpV2[2]=v211;	tmpV2[3]=v212;

	Box tmpX[4];
	tmpX[0]=x11;	tmpX[1]=x12;	tmpX[2]=x21;	tmpX[3]=x22;
	

	cilk_for (int i=0;i<4;i++)
	{
		E(tmpX[i],tmpB[i],tmpV2[i], callStackDepth);
	}
#else

	C(x11,u11,v21,v11,callStackDepth+1);
	C(x12,u11,v22,v12,callStackDepth+1);
	C(x21,u21,v21,v11,callStackDepth+1);
	C(x22,u21,v22,v12,callStackDepth+1);

	C(x11,u12,v211,v21,callStackDepth+1);
	C(x12,u12,v212,v22,callStackDepth+1);
	C(x21,u22,v211,v21,callStackDepth+1);
	C(x22,u22,v212,v22,callStackDepth+1);
#endif
	
	return;
}


#ifdef PARALLEL
void D(const Box& x, const Box& u1, const Box& v12, const Box& v1, const Box& u2, const Box& v2, const Box& v22, int callStackDepth)
{
	C(x,u1,v12,v1,callStackDepth);
	B(x,u2,v2,v22,callStackDepth);
}

void E(const Box& x, const Box* UV, const Box& V2, int callStackDepth)
{
	C(x,UV[0],UV[3],UV[1],callStackDepth);		
	C(x,UV[2],V2,UV[3],callStackDepth);		
}
#endif
