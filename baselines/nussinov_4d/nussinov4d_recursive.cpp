#include<iostream>
#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<vector>
#include<string.h>
#include<cassert>
#include<fstream>
#include<sstream>
#include<algorithm>
#include<stdlib.h>
#include<sys/time.h>
#include<math.h>
#include<map>
#include<set>
#include<cstdarg>
#ifdef PARALLEL
#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif

#define MAX_STR_LEN 1024
#define DIMENSION 4
#define METADATASPACE (2*DIMENSION)+1
using namespace std;
typedef int CELL_TYPE;

CELL_TYPE *cost;
long int inputSize[DIMENSION];
char* input_i=NULL;
map<string,int> tilestr;

typedef struct Box
{
	int coords[8]; //topleft and bottom right coordinates
	Box(int a, int b, int c, int d, int e, int f, int g, int h)
	{
		coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;coords[4]=e;coords[5]=f;coords[6]=g;coords[7]=h;
	}
	Box(){}
	Box(const Box& b)
	{
		coords[0]=b.coords[0];coords[1]=b.coords[1];coords[2]=b.coords[2];coords[3]=b.coords[3];
		coords[4]=b.coords[4];coords[5]=b.coords[5];coords[6]=b.coords[6];coords[7]=b.coords[7];
	}
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<DIMENSION;i++)
		{
			len *= (coords[i+DIMENSION]-coords[i]+1);
		}	
		return len;
	}
	char* PrintStr(char *str)const 
	{
		sprintf(str,"%d%d%d%d%d%d%d%d",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5],coords[6],coords[7]);
	}
}Box;

void A(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);

int ReadFASTAFile(FILE *fp, int maxLen, char** seq)
{
	char line[1024];
	int seqLength = 0, seqLenToProcess;
	char* localSeq=NULL;
	fgets(line, sizeof(line), fp);
	if(line == NULL)
		return -1;
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(strlen(line)+seqLength > maxLen)
			seqLenToProcess = maxLen-seqLength;
		else
			seqLenToProcess = strlen(line);
		localSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);
		if(!localSeq)
		{
			printf("ERROR in realloc.\n");
			exit(0);
		}
		for(unsigned int i = 0; i < seqLenToProcess; ++i)
		{
			if (line[i] == '*' || line[i] == '-')
				localSeq[seqLength++] = line[i];
			else if(isalpha(line[i]))
				localSeq[seqLength++] = toupper(line[i]);

		}
		if(seqLength>=maxLen) break;
	}
	localSeq[seqLength] = '\0';
	*seq = localSeq;
	return seqLength;
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || strcmp(argv[1],"-h")==0) 
	{
		printf("Usage: ./<exec> <input_string_i> <input_length_i> \n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r");
	if(!fpi)
	{
		printf("ERROR: Unable to open input file.\n");
		exit(0);
	}
	int inputLength_I=atoi(argv[2]);
	int ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);
	fclose(fpi);
	if((ret1<0)||(input_i==NULL))
	{
		printf("ERROR: Unable to read input files correctly. file1 len:%d\n",ret1);
		exit(0);
	}
	else
	{
		for(int i=0;i<DIMENSION;i++) inputSize[i]=ret1-1;
	}
	return inputSize[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	memset(data,0,sizeof(CELL_TYPE)*(numCells));
	return;
}

inline CELL_TYPE* GetDPTableCell(int i, int j, int k, int l, CELL_TYPE* data)
{
	CELL_TYPE* cell = data+METADATASPACE;
	int side = data[DIMENSION];
	int iOffset=i - data[3];
	cell += (iOffset*3* side);
	int jOffset=j - data[2];
	cell += (jOffset*2* side);
	int kOffset=k - data[1];
	cell += (kOffset*1* side);
	int lOffset=l - data[0];
	cell += lOffset;
	return cell;
}

void PrintResults()
{
	//printf("num tiles updated:%d\n",tilestr.size());
	//DebugPrintAllCells();
}

int main(int argc, char* argv[])
{
	ReadInput(argc,argv);

	Box matrixDim(0,0,0,0,inputSize[0],inputSize[1], inputSize[2],inputSize[3]);
	cost = new CELL_TYPE[matrixDim.GetBoxSize()+METADATASPACE];
	for(int i=0;i<DIMENSION;i++)
	{
		cost[i]=matrixDim.coords[i];
		cost[i+DIMENSION]=matrixDim.coords[i+DIMENSION]-matrixDim.coords[i]+1;
	}
	cost[2*DIMENSION]=0;

	//Set the bounds of DP table.
	InitializeDPTable(&matrixDim, cost+METADATASPACE);

	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	//Specify the bounds of DP table, whose cells are to be computed.
	A(&matrixDim, cost, &matrixDim, cost, &matrixDim, cost, 0);
	gettimeofday(&endTime,0);

	//Print results
	PrintResults();
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;

}

void A(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if(x->coords[0]==x->coords[DIMENSION])
	{
		int i=x->coords[DIMENSION-1], j=x->coords[DIMENSION-2], ii=x->coords[DIMENSION-3], jj=x->coords[DIMENSION-4];
		int k=y->coords[DIMENSION-2], kk=y->coords[DIMENSION-4];
		/*CELL_TYPE* cost_ij = GetDPTableCell(i,j,ii,jj,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,ii,kk,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,kkp1,jj,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;*/
    		//printf("A: i j ii jj=:%d %d %d %d -- i k ii kk=%d %d %d %d -- k+1 j kk+1 jj:%d %d %d %d\n", i, j, ii, jj, i,k,ii,kk,k+1,j,kk+1,jj);
    		char xstr[1024], ystr[1024], zstr[1024];
		memset(xstr,0,1024);
		memset(ystr,0,1024);
		memset(zstr,0,1024);
		x->PrintStr(xstr);
		y->PrintStr(ystr);
		z->PrintStr(zstr);
		string xs(xstr), ys(ystr), zs(zstr);
		string combined = xs+ys+zs;
		/*tilestr.insert(make_pair(combined,1));
		printf("%s\n",combined.c_str());*/
		return;
	}

	Box x0000(x->coords[0],x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0001(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0011(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5],x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5],x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x1100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5],x->coords[6],x->coords[7]);
	
	Box y0000(y->coords[0],y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0001(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0011(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5],y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y1100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5],y->coords[6],y->coords[7]);
	
	Box z0000(z->coords[0],z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0011(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2],z->coords[3],z->coords[4],z->coords[5],z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5],z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z1100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5],z->coords[6],z->coords[7]);

	A(&x0000, xData, &y0000, yData, &z0000, zData, callStackDepth+1);
	A(&x0011, xData, &y0011, yData, &z0011, zData, callStackDepth+1);
	A(&x1100, xData, &y1100, yData, &z1100, zData, callStackDepth+1);
	A(&x1111, xData, &y1111, yData, &z1111, zData, callStackDepth+1);

	B(&x0001, xData, &y0000, yData, &z0011, zData, callStackDepth+1);
	D(&x0100, xData, &y0000, yData, &z1100, zData, callStackDepth+1);
	D(&x0111, xData, &y0011, yData, &z1111, zData, callStackDepth+1);
	B(&x1101, xData, &y1100, yData, &z1111, zData, callStackDepth+1);

	C(&x0101, xData, &y0001, yData, &z0111, zData, callStackDepth+1);
	C(&x0101, xData, &y0100, yData, &z1101, zData, callStackDepth+1);
	D(&x0101, xData, &y0000, yData, &z1111, zData, callStackDepth+1);
	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if(x->coords[0]==x->coords[DIMENSION])
	{
		int i=x->coords[DIMENSION-1], j=x->coords[DIMENSION-2], ii=x->coords[DIMENSION-3], jj=x->coords[DIMENSION-4];
		int k=y->coords[DIMENSION-2], kk=y->coords[DIMENSION-4];
		/*CELL_TYPE* cost_ij = GetDPTableCell(i,j,ii,jj,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,ii,kk,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,kkp1,jj,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;*/
    		//printf("B: i j ii jj=:%d %d %d %d -- i k ii kk=%d %d %d %d -- k+1 j kk+1 jj:%d %d %d %d\n", i, j, ii, jj, i,k,ii,kk,k+1,j,kk+1,jj);
		char xstr[1024], ystr[1024], zstr[1024];
		memset(xstr,0,1024);
		memset(ystr,0,1024);
		memset(zstr,0,1024);
		x->PrintStr(xstr);
		y->PrintStr(ystr);
		z->PrintStr(zstr);
		string xs(xstr), ys(ystr), zs(zstr);
		string combined = xs+ys+zs;
		/*tilestr.insert(make_pair(combined,1));
		printf("%s\n",combined.c_str());*/

		return;
	}
	
	Box x0000(x->coords[0],x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0001(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0011(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5],x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5],x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x1100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5],x->coords[6],x->coords[7]);
	
	Box y0000(y->coords[0],y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0001(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0011(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5],y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0101(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5],y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y1100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1101(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5],y->coords[6],y->coords[7]);
	
	Box z0000(z->coords[0],z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0001(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0011(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2],z->coords[3],z->coords[4],z->coords[5],z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5],z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z1100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5],z->coords[6],z->coords[7]);


	B(&x0000, xData, &y0000, yData, &z0000, zData, callStackDepth+1);
	B(&x0011, xData, &y0011, yData, &z0011, zData, callStackDepth+1);
	B(&x1100, xData, &y1100, yData, &z1100, zData, callStackDepth+1);
	B(&x1111, xData, &y1111, yData, &z1111, zData, callStackDepth+1);

	C(&x0001, xData, &y0001, yData, &x0011, xData, callStackDepth+1);
	C(&x0100, xData, &x0000, xData, &z0100, zData, callStackDepth+1);
	C(&x0111, xData, &x0011, xData, &z0111, zData, callStackDepth+1);
	C(&x1101, xData, &y1101, yData, &x1111, xData, callStackDepth+1);

	C(&x0001, xData, &x0000, xData, &z0001, zData, callStackDepth+1);
	C(&x0100, xData, &y0100, yData, &x1100, xData, callStackDepth+1);
	C(&x0111, xData, &y0111, yData, &x1111, xData, callStackDepth+1);
	C(&x1101, xData, &x1100, xData, &z1101, zData, callStackDepth+1);

	B(&x0001, xData, &y0000, yData, &z0011, zData, callStackDepth+1);
	D(&x0100, xData, &y0000, yData, &z1100, zData, callStackDepth+1);
	D(&x0111, xData, &y0011, yData, &z1111, zData, callStackDepth+1);
	B(&x1101, xData, &y1100, yData, &z1111, zData, callStackDepth+1);
	
	C(&x0101, xData, &y0001, yData, &x0111, xData, callStackDepth+1);
	C(&x0101, xData, &x0000, xData, &z0101, zData, callStackDepth+1);
	C(&x0101, xData, &x0001, xData, &z0111, zData, callStackDepth+1);
	C(&x0101, xData, &y0100, yData, &x1101, xData, callStackDepth+1);
	C(&x0101, xData, &y0101, yData, &x1111, xData, callStackDepth+1);
	C(&x0101, xData, &x0100, xData, &z1101, zData, callStackDepth+1);
	D(&x0101, xData, &y0000, yData, &z1111, zData, callStackDepth+1);
	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if(x->coords[0]==x->coords[DIMENSION])
	{
		int i=x->coords[DIMENSION-1], j=x->coords[DIMENSION-2], ii=x->coords[DIMENSION-3], jj=x->coords[DIMENSION-4];
		int k=y->coords[DIMENSION-2], kk=y->coords[DIMENSION-4];
		/*CELL_TYPE* cost_ij = GetDPTableCell(i,j,ii,jj,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,ii,kk,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,kkp1,jj,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;*/
    		//printf("C: i j ii jj=:%d %d %d %d -- i k ii kk=%d %d %d %d -- k+1 j kk+1 jj:%d %d %d %d\n", i, j, ii, jj, i,k,ii,kk,k+1,j,kk+1,jj);
		char xstr[1024], ystr[1024], zstr[1024];
		memset(xstr,0,1024);
		memset(ystr,0,1024);
		memset(zstr,0,1024);
		x->PrintStr(xstr);
		y->PrintStr(ystr);
		z->PrintStr(zstr);
		string xs(xstr), ys(ystr), zs(zstr);
		string combined = xs+ys+zs;
		/*tilestr.insert(make_pair(combined,1));
		printf("%s\n",combined.c_str());*/

		return;
	}
	
	Box x0000(x->coords[0],x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0001(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0011(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5],x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5],x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x1100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5],x->coords[6],x->coords[7]);
	
	Box y0000(y->coords[0],y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0001(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0011(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5],y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0101(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5],y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y1100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1101(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5],y->coords[6],y->coords[7]);
	
	Box z0000(z->coords[0],z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0001(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0011(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2],z->coords[3],z->coords[4],z->coords[5],z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5],z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z1100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5],z->coords[6],z->coords[7]);


	C(&x0000, xData, &y0000, yData, &z0000, zData, callStackDepth+1);
	C(&x0001, xData, &y0000, yData, &z0001, zData, callStackDepth+1);
	C(&x0100, xData, &y0000, yData, &z0100, zData, callStackDepth+1);
	C(&x0101, xData, &y0000, yData, &z0101, zData, callStackDepth+1);
	C(&x0011, xData, &y0011, yData, &z0011, zData, callStackDepth+1);
	C(&x0111, xData, &y0011, yData, &z0111, zData, callStackDepth+1);
	C(&x1100, xData, &y1100, yData, &z1100, zData, callStackDepth+1);
	C(&x1101, xData, &y1100, yData, &z1101, zData, callStackDepth+1);
	C(&x1111, xData, &y1111, yData, &z1111, zData, callStackDepth+1);

	C(&x0001, xData, &y0001, yData, &z0011, zData, callStackDepth+1);
	C(&x0100, xData, &y0100, yData, &z1100, zData, callStackDepth+1);
	C(&x0101, xData, &y0001, yData, &z0111, zData, callStackDepth+1);
	C(&x0111, xData, &y0111, yData, &z1111, zData, callStackDepth+1);
	C(&x1101, xData, &y1101, yData, &z1111, zData, callStackDepth+1);

	C(&x0101, xData, &y0100, yData, &z1101, zData, callStackDepth+1);
	C(&x0101, xData, &y0101, yData, &z1111, zData, callStackDepth+1);

	return;
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	
	if(x->coords[0]==x->coords[DIMENSION])
	{
		int i=x->coords[DIMENSION-1], j=x->coords[DIMENSION-2], ii=x->coords[DIMENSION-3], jj=x->coords[DIMENSION-4];
		int k=y->coords[DIMENSION-2], kk=y->coords[DIMENSION-4];
		/*CELL_TYPE* cost_ij = GetDPTableCell(i,j,ii,jj,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,ii,kk,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k+1,j,kkp1,jj,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;*/
    		//printf("D: i j ii jj=:%d %d %d %d -- i k ii kk=%d %d %d %d -- k+1 j kk+1 jj:%d %d %d %d\n", i, j, ii, jj, i,k,ii,kk,k+1,j,kk+1,jj);
		char xstr[1024], ystr[1024], zstr[1024];
		memset(xstr,0,1024);
		memset(ystr,0,1024);
		memset(zstr,0,1024);
		x->PrintStr(xstr);
		y->PrintStr(ystr);
		z->PrintStr(zstr);
		string xs(xstr), ys(ystr), zs(zstr);
		string combined = xs+ys+zs;
		/*tilestr.insert(make_pair(combined,1));
		printf("%s\n",combined.c_str());*/

		return;
	}
	
	Box x0000(x->coords[0],x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0001(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0011(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5],x->coords[6]-(x->coords[6]-x->coords[2]+1)/2,x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x0111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5],x->coords[6],x->coords[7]-(x->coords[7]-x->coords[3]+1)/2);
	Box x1100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4]-(x->coords[4]-x->coords[0]+1)/2,x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1101(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[1]+1)/2,x->coords[6],x->coords[7]);
	Box x1111(x->coords[0]+(x->coords[4]-x->coords[0]+1)/2,x->coords[1]+(x->coords[5]-x->coords[1]+1)/2,x->coords[2]+(x->coords[6]-x->coords[2]+1)/2,x->coords[3]+(x->coords[7]-x->coords[3]+1)/2,x->coords[4],x->coords[5],x->coords[6],x->coords[7]);
	
	Box y0000(y->coords[0],y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0001(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0011(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5],y->coords[6]-(y->coords[6]-y->coords[2]+1)/2,y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0101(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y0111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5],y->coords[6],y->coords[7]-(y->coords[7]-y->coords[3]+1)/2);
	Box y1100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4]-(y->coords[4]-y->coords[0]+1)/2,y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1101(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5]-(y->coords[5]-y->coords[1]+1)/2,y->coords[6],y->coords[7]);
	Box y1111(y->coords[0]+(y->coords[4]-y->coords[0]+1)/2,y->coords[1]+(y->coords[5]-y->coords[1]+1)/2,y->coords[2]+(y->coords[6]-y->coords[2]+1)/2,y->coords[3]+(y->coords[7]-y->coords[3]+1)/2,y->coords[4],y->coords[5],y->coords[6],y->coords[7]);
	
	Box z0000(z->coords[0],z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0001(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0011(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2],z->coords[3],z->coords[4],z->coords[5],z->coords[6]-(z->coords[6]-z->coords[2]+1)/2,z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z0111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5],z->coords[6],z->coords[7]-(z->coords[7]-z->coords[3]+1)/2);
	Box z1100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4]-(z->coords[4]-z->coords[0]+1)/2,z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1101(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5]-(z->coords[5]-z->coords[1]+1)/2,z->coords[6],z->coords[7]);
	Box z1111(z->coords[0]+(z->coords[4]-z->coords[0]+1)/2,z->coords[1]+(z->coords[5]-z->coords[1]+1)/2,z->coords[2]+(z->coords[6]-z->coords[2]+1)/2,z->coords[3]+(z->coords[7]-z->coords[3]+1)/2,z->coords[4],z->coords[5],z->coords[6],z->coords[7]);


	D(&x0000, xData, &y0000, yData, &z0000, zData, callStackDepth+1);
	D(&x0011, xData, &y0011, yData, &z0011, zData, callStackDepth+1);
	D(&x1100, xData, &y1100, yData, &z1100, zData, callStackDepth+1);
	D(&x1111, xData, &y1111, yData, &z1111, zData, callStackDepth+1);

	C(&x0001, xData, &y0001, yData, &x0011, xData, callStackDepth+1);
	C(&x0100, xData, &y0100, yData, &x1100, xData, callStackDepth+1);
	C(&x0111, xData, &y0111, yData, &x1111, xData, callStackDepth+1);
	C(&x1101, xData, &y1101, yData, &x1111, xData, callStackDepth+1);

	C(&x0001, xData, &x0000, xData, &z0001, zData, callStackDepth+1);
	C(&x0100, xData, &x0000, xData, &z0100, zData, callStackDepth+1);
	C(&x0111, xData, &x0011, xData, &z0111, zData, callStackDepth+1);
	C(&x1101, xData, &x1100, xData, &z1101, zData, callStackDepth+1);

	D(&x0001, xData, &y0000, yData, &z0011, zData, callStackDepth+1);
	D(&x0100, xData, &y0000, yData, &z1100, zData, callStackDepth+1);
	D(&x0111, xData, &y0011, yData, &z1111, zData, callStackDepth+1);
	D(&x1101, xData, &y1100, yData, &z1111, zData, callStackDepth+1);

	C(&x0101, xData, &y0001, yData, &x0111, xData, callStackDepth+1);
	C(&x0101, xData, &y0100, yData, &x1101, xData, callStackDepth+1);
	C(&x0101, xData, &y0101, yData, &x1111, xData, callStackDepth+1);
	C(&x0101, xData, &x0000, xData, &z0101, zData, callStackDepth+1);
	C(&x0101, xData, &x0001, xData, &z0111, zData, callStackDepth+1);
	C(&x0101, xData, &x0100, xData, &z1101, zData, callStackDepth+1);
	D(&x0101, xData, &y0000, yData, &z1111, zData, callStackDepth+1);
	return;
}
