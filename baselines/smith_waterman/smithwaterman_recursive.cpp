#include<fstream>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/time.h>
#ifdef PARALLEL
#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif


using namespace std;

#define TILE_WIDTH 8
#define MAX_STR_LEN 45000
typedef int CELL_TYPE;
typedef struct Box
{
	int coords[4]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d)
	{
		coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;
	}
	Box(){}
	Box(const Box& b)
	{
		coords[0]=b.coords[0];coords[1]=b.coords[1];coords[2]=b.coords[2];coords[3]=b.coords[3];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) ||(this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]))
			flag = false;
		return flag;
	}

	char* PrintStr(char *str)const {sprintf(str,"%d,%d,%d,%d",coords[0],coords[1],coords[2],coords[3]);}
}Box;

CELL_TYPE* cost;
long int inputSize[2];
int gap=-4, match=5, nomatch=-3;
int runningMax=0;
char* input_i=NULL, *input_j=NULL;

int ReadFASTAFile(FILE *fp, int maxlen, char** seq);
void ReadInput(int argc, char** argv);
int* InitializeDPTable(Box* b);
void PrintResults();
void A(Box* x);
void B(Box* x, Box* y);
void C(Box* x, Box* y);
void D(Box* x, Box* y);

inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data;
	ret = ret + (i * inputSize[0])+j;
	return ret;
}

void ReadInput(int argc, char** argv)
{
	if((argc < 4) || (argc > 6))
	{
		printf("Usage: ./<exec> <numcores> <input_string_i> <input_string_j> <input_length_i>(optional) <input_length_j>(optional)\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[2],"r");
	FILE* fpj=fopen(argv[3],"r");
	if(!fpi || !fpj)
	{
		printf("ERROR: Unable to open input files.\n");
		exit(0);
	}
	
	int inputLength_I=MAX_STR_LEN,inputLength_J=MAX_STR_LEN;
	if(argc==5)
	{
		inputLength_I=atoi(argv[4]);
		inputLength_J=MAX_STR_LEN;
	}
	else if(argc==6)
	{
		inputLength_I=atoi(argv[4]);
		inputLength_J=atoi(argv[5]);
	}
	
	int ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);
	int ret2=ReadFASTAFile(fpj, inputLength_J, &input_j);
	fclose(fpi);
	fclose(fpj);
	if((ret1<0) || (ret2<0) || (input_i==NULL) || (input_j==NULL))
	{
		printf("ERROR: Unable to read input files correctly. file1 len:%d file2 len:%d\n",ret1,ret2);
		exit(0);
	}
	else
	{
		inputSize[0]=ret1;
		inputSize[1]=ret2;
	}

	//horizontal dimension should always be greater than vertical dimension. Done to satisfy the wavefront computation algorithm.
	if(inputSize[1] > inputSize[0])
	{
#ifdef DEBUG
		printf("making horizontal dimension greater than vertical dimension.\n");
#endif
		std::swap(inputSize[0],inputSize[1]);
		std::swap(input_i,input_j);
	}
#ifdef DEBUG
	printf("string length (x):%d (y):%d\n",inputSize[0],inputSize[1]);
#endif
}

CELL_TYPE* InitializeDPTable(Box* b)
{
	int numCells = (b->coords[2]-b->coords[0]+1) * (b->coords[3]-b->coords[1]+1);
	CELL_TYPE* newTile = new CELL_TYPE[numCells];
	memset(newTile,0,sizeof(CELL_TYPE)*(numCells));
	return newTile;
}

void PrintResults()
{
#ifdef PARALLEL
	runningMax = 0;
	for(unsigned int i=1;i<=inputSize[1];i++)
	{
		for(unsigned int j=1;j<=inputSize[0];j++)
		{
			int tmpMax = *(GetDPTableCell(i,j,cost));
			if(tmpMax > runningMax)
				runningMax = tmpMax;
		}
	}
#endif
	printf("maximum cost: %d\n",runningMax);
}

void A(Box* x)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=x->coords[1], l=x->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,cost);
		cost_kl = cost_ij;
		int reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		CELL_TYPE newCost = *cost_kl + reward_penalty;
#ifndef PARALLEL
		if(newCost > runningMax)
			runningMax = newCost;
#endif
#ifdef DEBUG
		CELL_TYPE tmpNewCost = (newCost > *cost_ij)?newCost:*cost_ij;
		char xstr[128],ystr[128];x->PrintStr(xstr);printf("A: i:%d j:%d k%d l:%d x:(%s) newCost:%d cost_ij(old):%d cost_ij(new):%d runningMax:%d\n",i,j,k,l, xstr,newCost,*cost_ij,tmpNewCost,runningMax);
#endif
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);

	A(&x00);
#ifdef PARALLEL
	cilk_spawn B(&x01, &x00);	
	cilk_spawn C(&x10, &x00);	
	D(&x11, &x00);
	cilk_sync;
	cilk_spawn A(&x01);	
	A(&x10);
	cilk_sync;
#else
	B(&x01, &x00);	
	C(&x10, &x00);	
	D(&x11, &x00);
	A(&x01);	A(&x10);
#endif
	C(&x11, &x01);
	B(&x11, &x10);
	A(&x11);
	return;
}

void B(Box* x, Box* y)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		CELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,cost);
		cost_kl = GetDPTableCell(k,l,cost);
		int reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		int newCost = *cost_kl + reward_penalty;
#ifndef PARALLEL
		if(newCost > runningMax)
			runningMax = newCost;
#endif
#ifdef DEBUG
		CELL_TYPE tmpNewCost = (newCost > *cost_ij)?newCost:*cost_ij;
		char xstr[128],ystr[128];x->PrintStr(xstr);y->PrintStr(ystr);printf("B: i:%d j:%d k%d l:%d x:(%s) newCost:%d cost_ij(old):%d cost_ij(new):%d runningMax:%d\n",i,j,k,l, xstr,newCost,*cost_ij,tmpNewCost, runningMax);
#endif
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

#ifdef PARALLEL
	cilk_spawn B(&x00, &y01);	
	D(&x10, &y01);
	cilk_sync;
#else
	B(&x00, &y01);	D(&x10, &y01);
#endif
	B(&x10, &y11);
	return;
}

void C(Box* x, Box* y)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		int* cost_kl, *cost_ij=GetDPTableCell(i,j,cost);
		cost_kl = GetDPTableCell(k,l,cost);
		int reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		int newCost = *cost_kl + reward_penalty;
#ifndef PARALLEL
		if(newCost > runningMax)
			runningMax = newCost;
#endif
#ifdef DEBUG
		CELL_TYPE tmpNewCost = (newCost > *cost_ij)?newCost:*cost_ij;
		char xstr[128],ystr[128];x->PrintStr(xstr);y->PrintStr(ystr);printf("C: i:%d j:%d k%d l:%d x:(%s) newCost:%d cost_ij(old):%d cost_ij(new):%d runningMax:%d\n",i,j,k,l, xstr,newCost,*cost_ij,tmpNewCost, runningMax);
#endif
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

#ifdef PARALLEL
	cilk_spawn C(&x00, &y10);	
	D(&x01, &y10);
	cilk_sync;
#else
	C(&x00, &y10);	D(&x01, &y10);
#endif
	C(&x01, &y11);
	return;
}

void D(Box* x, Box* y)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[1], l=y->coords[0];
		if((i==0)||(j==0))
			return;
		int* cost_kl, *cost_ij=GetDPTableCell(i,j,cost);
		cost_kl = GetDPTableCell(k,l,cost);
		int reward_penalty=gap;
		if((k==i-1) && (l==j-1))
			reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
		int newCost = *cost_kl + reward_penalty;
#ifndef PARALLEL
		if(newCost > runningMax)
			runningMax = newCost;
#endif
#ifdef DEBUG
		CELL_TYPE tmpNewCost = (newCost > *cost_ij)?newCost:*cost_ij;
		char xstr[128],ystr[128];x->PrintStr(xstr);y->PrintStr(ystr);printf("D: i:%d j:%d k%d l:%d x:(%s) newCost:%d cost_ij(old):%d cost_ij(new):%d runningMax:%d\n",i,j,k,l, xstr,newCost,*cost_ij,tmpNewCost, runningMax);
#endif
		if(newCost > *cost_ij)
			*cost_ij =  newCost;
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);

	D(&x00, &y11);
	return;
}

int main(int argc, char* argv[])
{
	ReadInput(argc, argv);
	Box x(0,0,inputSize[0],inputSize[1]);
	cost = InitializeDPTable(&x);
#ifdef PARALLEL
	int nworkers=0;
	nworkers= __cilkrts_get_nworkers();
	printf("number of total Cilk worker threads available:%d. numworkers to set:%d\n", nworkers,atoi(argv[1]));
	if (0!= __cilkrts_set_param("nworkers",argv[1])) {
            printf("Failed to set worker count %s\n",atoi(argv[1]));
            return 0;
        }
#endif
	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	A(&x);
	gettimeofday(&endTime,0);
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	PrintResults();
	delete [] cost;
	free(input_i);
	free(input_j);
	return 0;
}

int ReadFASTAFile(FILE *fp, int maxLen, char** seq) 
{
	char line[1024];

	fgets(line, sizeof(line), fp);
	if (line == NULL) 
		return -1; 

	int seqLength = 0;
	char* localSeq=NULL;

	int seqLenToProcess;
	while (fgets(line, sizeof(line), fp) != NULL) 
	{
		if(strlen(line)+seqLength > maxLen)
			seqLenToProcess = maxLen-seqLength;
		else
			seqLenToProcess = strlen(line);

		localSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength + 1);
		if(!localSeq)
		{
			printf("ERROR in realloc.\n");
			exit(0);
		}
		for (unsigned int i = 0; i < seqLenToProcess; ++i) {
			if (line[i] == '*' || line[i] == '-') {
				localSeq[seqLength++] = line[i];
			} else if (isalpha(line[i])) {
				localSeq[seqLength++] = toupper(line[i]);
			}
		}
		
		if(seqLength>=maxLen)
			break;
	}
	localSeq[seqLength] = '\0';
	*seq = localSeq;

	return seqLength;
}
