#include<fstream>
#include<string.h>
#include<stdlib.h>
#include<sys/time.h>
//#include<algorithm>
#ifdef PARALLEL
#include<omp.h>
#endif

#define MAX_STR_LEN 45000

using namespace std;

typedef int CELL_TYPE;
CELL_TYPE* cost;
long int inputSize[2];
int gap=-4, match=5, nomatch=-3;
int runningMax=0;
char* input_i=NULL, *input_j=NULL;

int ReadFASTAFile(FILE *fp, int maxlen, char** seq);

inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data;
	ret = ret + (i * inputSize[0])+j;
	return ret;
}

void ReadInput(int argc, char** argv)
{
	if((argc < 4) || (argc > 6))
	{
		printf("Usage: ./<exec> <cores> <input_string_i> <input_string_j> <input_length_i>(optional) <input_length_j>(optional)\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[2],"r");
	FILE* fpj=fopen(argv[3],"r");
	if(!fpi || !fpj)
	{
		printf("ERROR: Unable to open input files. %s %s\n",argv[2],argv[3]);
		exit(0);
	}
	
	int inputLength_I=MAX_STR_LEN,inputLength_J=MAX_STR_LEN;
	if(argc==5)
	{
		inputLength_I=atoi(argv[4]);
		inputLength_J=MAX_STR_LEN;
	}
	else if(argc==6)
	{
		inputLength_I=atoi(argv[4]);
		inputLength_J=atoi(argv[5]);
	}
	
	int ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);
	int ret2=ReadFASTAFile(fpj, inputLength_J, &input_j);
	fclose(fpi);
	fclose(fpj);
	if((ret1<0) || (ret2<0) || (input_i==NULL) || (input_j==NULL))
	{
		printf("ERROR: Unable to read input files correctly. file1 len:%d file2 len:%d\n",ret1,ret2);
		exit(0);
	}
	else
	{
		inputSize[0]=ret1;
		inputSize[1]=ret2;
	}

	//horizontal dimension should always be greater than vertical dimension. Done to satisfy the wavefront computation algorithm.
	if(inputSize[1] > inputSize[0])
	{
#ifdef DEBUG
		printf("making horizontal dimension greater than vertical dimension.\n");
#endif
		std::swap(inputSize[0],inputSize[1]);
		std::swap(input_i,input_j);
	}
#ifdef DEBUG
	printf("string length (x):%d (y):%d\n",inputSize[0],inputSize[1]);
#endif
}

CELL_TYPE* InitializeDPTable(int tlx, int tly, int brx, int bry)
{
	int numCells = (brx-tlx+1) * (bry-tly+1);
	CELL_TYPE* newTile = new CELL_TYPE[numCells];
	memset(newTile,0,sizeof(CELL_TYPE)*(numCells));
	return newTile;
}

void PrintResults()
{
/*#ifdef PARALLEL
	for(unsigned int i=1;i<=inputSize[0];i++)
	{
		for(unsigned int j=1;j<=inputSize[1];j++)
		{
			int tmpMax = *(GetDPTableCell(i,j,cost));
			if(tmpMax > runningMax)
				runningMax = tmpMax;
		}
	}
#endif*/

	printf("maximum cost: %d\n",runningMax);
}


int main(int argc, char* argv[])
{
	ReadInput(argc, argv);
	cost = InitializeDPTable(0,0,inputSize[0],inputSize[1]);
	int rowLen = inputSize[0];
	int colLen = inputSize[1];
	int shorterSeqLen = std::min(inputSize[0],inputSize[1]);
	int longerSeqLen = std::max(inputSize[0],inputSize[1]);

#ifdef PARALLEL
	int numProcs = omp_get_num_procs();
	int numThreads = omp_get_num_threads();
	printf("Number of processors available %d threads:%d. num threads to set:%d\n",numProcs,numThreads,atoi(argv[1]));		
	omp_set_dynamic(0);
	omp_set_num_threads(atoi(argv[1]));
#endif

	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	for(unsigned int l=1;l<=shorterSeqLen;l++)
	{
#ifdef PARALLEL
	#pragma omp parallel for shared(cost,match,nomatch,gap,l,input_i,input_j) reduction(max:runningMax)
#endif
		for(unsigned int i=l;i>=1;i--)
		{
			unsigned int j=l-i+1;
			CELL_TYPE* cost_ij = GetDPTableCell(i,j,cost);
			CELL_TYPE* costij_diag = GetDPTableCell(i-1,j-1,cost);
			CELL_TYPE* costij_left = GetDPTableCell(i-1,j,cost);
			CELL_TYPE* costij_top = GetDPTableCell(i,j-1,cost);
			
			int reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
			CELL_TYPE newCost = std::max(*costij_diag+reward_penalty,*costij_left+gap);
			newCost = std::max(newCost,*costij_top+gap);
			runningMax = std::max(newCost,runningMax);
#ifdef DEBUG
			CELL_TYPE tmpNewCost = (newCost > *cost_ij)?newCost:*cost_ij;
			printf("i:%d j:%d newCost:%d cost_ij(old):%d cost_ij(new):%d runningMax:%d\n",i,j,newCost,*cost_ij,tmpNewCost,runningMax);
#endif
			if(newCost > *cost_ij)
				*cost_ij = newCost;
			//printf("(%d %d) ", i ,j);
		}
	//	printf("\n");
	}
	int x=1;
	for(unsigned int l=shorterSeqLen;l>=1;l--)
	{
		int count;
		if(l==shorterSeqLen)
			count = longerSeqLen-shorterSeqLen;
		else
			count=1;
		while(count)
		{
#ifdef PARALLEL
	#pragma omp parallel for shared(cost,match,nomatch,gap,l,shorterSeqLen,x,input_i,input_j) reduction(max:runningMax)
#endif
				for(unsigned int i=shorterSeqLen;i>=shorterSeqLen-l+1;i--)
				{
					int j=shorterSeqLen-i+1+x;
					CELL_TYPE* cost_ij = GetDPTableCell(i,j,cost);
					CELL_TYPE* costij_diag = GetDPTableCell(i-1,j-1,cost);
					CELL_TYPE* costij_left = GetDPTableCell(i-1,j,cost);
					CELL_TYPE* costij_top = GetDPTableCell(i,j-1,cost);
					
					int reward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;
					int newCost = std::max(*costij_diag+reward_penalty,*costij_left+gap);
					newCost = std::max(newCost,*costij_top+gap);
					runningMax = std::max(newCost,runningMax);
		#ifdef DEBUG
					CELL_TYPE tmpNewCost = (newCost > *cost_ij)?newCost:*cost_ij;
					printf("i:%d j:%d newCost:%d cost_ij(old):%d cost_ij(new):%d runningMax:%d\n",i,j,newCost,*cost_ij,tmpNewCost,runningMax);
		#endif
					if(newCost > *cost_ij)
						*cost_ij = newCost;
					//printf("(%d %d) ",i,j);
				}
			x++;
			count--;
		//	printf("\n");
		}
	}
	gettimeofday(&endTime,0);
	PrintResults();
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	delete [] cost;
	free(input_i);
	free(input_j);
	return 0;
}

int ReadFASTAFile(FILE *fp, int maxLen, char** seq) 
{
	char line[1024];

	fgets(line, sizeof(line), fp);
	if (line == NULL) 
		return -1; 

	int seqLength = 0;
	char* localSeq=NULL;

	int seqLenToProcess;
	while (fgets(line, sizeof(line), fp) != NULL) 
	{
		if(strlen(line)+seqLength > maxLen)
			seqLenToProcess = maxLen-seqLength;
		else
			seqLenToProcess = strlen(line);

		localSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);
		if(!localSeq)
		{
			printf("ERROR in realloc.\n");
			exit(0);
		}
		for (unsigned int i = 0; i < seqLenToProcess; ++i) {
			if (line[i] == '*' || line[i] == '-') {
				localSeq[seqLength++] = line[i];
			} else if (isalpha(line[i])) {
				localSeq[seqLength++] = toupper(line[i]);
			}
		}
		
		if(seqLength>=maxLen)
			break;
	}
	localSeq[seqLength] = '\0';
	*seq = localSeq;

	return seqLength;
}

