#include<iostream>
#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<vector>
#include<string.h>
#include<cassert>
#include<fstream>
#include<algorithm>
#include<stdlib.h>
#include<sys/time.h>
#include<math.h>
#include<map>
#include<set>
#ifdef PARALLEL
#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif
#define MAX_NUM_VERTICES 45000
using namespace std;
typedef map<int,set<pair<int,int> > > Adjacency_List;
typedef int CELL_TYPE;

CELL_TYPE *cost;
vector<int> vertexIDList;
Adjacency_List graphData;
long int inputSize[3];
int cellsPerRowTable;

typedef struct Box
{
	int  coords[6]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d, int e, int f){coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;coords[4]=e;coords[5]=f;}
	Box(){}
	void PrintStr(char *str)const {sprintf(str,"%d %d %d %d %d %d",coords[0],coords[1],coords[2],coords[3],coords[4],coords[5]);}
	Box& operator=(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
		this->coords[4]=rhs.coords[4];
		this->coords[5]=rhs.coords[5];
		return *this;
	}
	Box(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
		this->coords[4]=rhs.coords[4];
		this->coords[5]=rhs.coords[5];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) ||(this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]) || (this->coords[4]!=rhs.coords[4])|| (this->coords[5]!=rhs.coords[5]))
			flag = false;
		return flag;
	}
	
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<3;i++)
		{
			len *= (coords[i+3]-coords[i]+1);
		}	
		return len;
	}
}Box;

void A(Box* x, CELL_TYPE* xData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, Box* a, CELL_TYPE* aData, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void E(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void F(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);
void G(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void H(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth);


vector<string> Split(const char *str, char c)
{
	vector<string> result;
	do
	{
		const char *begin = str;
		while(*str != c && *str)
			str++;
		result.push_back(string(begin,str));
	}while(0!=*str++);
	return result;
}

/* this function is for reading wiki data set. */
int ReadGraphDataGeneral(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int weight = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		g[fromNode].insert(make_pair(toNode,weight));
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}


/* this function is for reading autonomous systems (AS) data set. */
int ReadGraphData(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int relationship = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		if((relationship == 1) || (relationship == -1))
			g[fromNode].insert(make_pair(toNode,1));
		else if((relationship == 0) || (relationship == 2))
		{
			g[fromNode].insert(make_pair(toNode,1));
			g[toNode].insert(make_pair(fromNode,1));
		}
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || (strcmp(argv[1],"-h")==0))
	{
		printf("Usage: ./<exec> <input_graph> <num_vertices>\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r");
	if(!fpi)
	{
		printf("ERROR: Unable to open input file.\n");
		exit(0);
	}
	int maxVertices=MAX_NUM_VERTICES;
	maxVertices=atoi(argv[2]);
	int ret1=ReadGraphDataGeneral(fpi, maxVertices, graphData);
	fclose(fpi);
	if(ret1<0)
	{
		printf("ERROR: Unable to read input file correctly.\n");
		exit(0);
	}
	else
	{
		inputSize[0]=ret1-1; inputSize[1]=ret1-1; inputSize[2]=ret1-1;
	}
	return inputSize[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	unsigned int relIndx = 0;
	for(unsigned int i=b->coords[2];i<=b->coords[5];i++)
	{
		for(unsigned int j=b->coords[1];j<=b->coords[4];j++)
		{
			CELL_TYPE initVal=65535;
			if(i==j)
				initVal = 0;
			else
			{
				set<pair<int, int> >::iterator it=graphData[vertexIDList[i]].begin();
				while(it!=graphData[vertexIDList[i]].end())
				{
					if(it->first == vertexIDList[j])
					{
						initVal = it->second;
						break;
					}
					it++;
				}
			}
			for(unsigned int k=b->coords[0];k<=b->coords[3];k++)
			{
				//printf("(%d %d %d) (%d) =%d\n",i,j,k,relIndx,initVal);
				data[relIndx++] = initVal;
			}
		}
	}
	return;
}

inline CELL_TYPE* GetDPTableCell(int i, int j, int k, CELL_TYPE* data)
{
	CELL_TYPE* ret = data+7;
	int side  = data[3];
	int xOffset = (i - data[2]);
	ret += (xOffset*side*side);
	int yOffset = (j - data[1]);
	ret += yOffset*side; 
	int zOffset = (k - data[0]); 
	ret += zOffset;
	return ret;
}

void DebugPrintAllCells()
{
	for(int i=0;i<cellsPerRowTable;i++)
	{
		for(int j=0;j<cellsPerRowTable;j++)
		{
			int min = 65535;
			for(int k=0;k<cellsPerRowTable;k++)
			{
				CELL_TYPE* cell = GetDPTableCell(i, j, k, cost);
				/*int relIndx = 0;
				int side  = cost[3];
				int xOffset = (i - cost[2]);
				relIndx += (xOffset*side*side);
				int yOffset = (j - cost[1]);
				relIndx += yOffset*side; 
				int zOffset = (k - cost[0]); 
				relIndx += zOffset;
				printf("(%d %d %d) (%d) =%d\n",i,j,k,relIndx,*cell);*/
				if(min > *cell)
					min = *cell;
			}
			if(min != 65535)
				printf("%d",min);
			else printf("inf");
			if(j!=cellsPerRowTable-1)
				printf(" ");
		}
		printf("\n");
	}
}

void PrintResults()
{
	DebugPrintAllCells();
}

int main(int argc, char* argv[])
{
	int numVertices = 0;
	
	//matrix chain is specified in a file input to the program. Read the file and populate matrixChain
	cellsPerRowTable = ReadInput(argc,argv);

	numVertices = graphData.size();
	Box matrixDim(0,0,0,inputSize[0],inputSize[1],inputSize[2]);
	cost = new CELL_TYPE[matrixDim.GetBoxSize()+7];
	for(int i=0;i<3;i++)
	{
		cost[i]=matrixDim.coords[i];
		cost[i+3]=matrixDim.coords[i+3]-matrixDim.coords[i]+1;
	}
	//Set the bounds of DP table.
	InitializeDPTable(&matrixDim, cost+7);
	/*printf("Initial weights\n");
	DebugPrintAllCells();*/
	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	//Specify the bounds of DP table, whose cells are to be computed.
	A(&matrixDim, cost, 0);
	gettimeofday(&endTime,0);

	printf("Final weights\n");
	//Print results
	PrintResults();
	//printf("--------\n");
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;

}

void A(Box* x, CELL_TYPE* xData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=x->coords[1];
		if(i == j)
			return;
		/*CELL_TYPE* cost_ij=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ik=GetDPTableCell(i,k,k-1,xData);
		CELL_TYPE* cost_kj=GetDPTableCell(k,j,k-1,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij = newCost;*/
		/*char xindx[32];
		x->PrintStr(xindx);
		printf("A: x: %s\n",xindx);*/
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x001(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x011(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x101(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box x111(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]);

	A(&x000, xData, callStackDepth+1);
	D(&x010, xData, &x000, xData, callStackDepth+1);
	F(&x100, xData, &x000, xData, callStackDepth+1);

	B(&x001, xData, &x000, xData, &x010, xData, &x100, xData, callStackDepth+1);
	C(&x110, xData, &x100, xData, &x010, xData, callStackDepth+1);

	E(&x011, xData, &x010, xData, &x110, xData, callStackDepth+1);
	G(&x101, xData, &x100, xData, &x110, xData, callStackDepth+1);
	H(&x111, xData, &x110, xData, callStackDepth+1);

	A(&x111, xData, callStackDepth+1);
	F(&x011, xData, &x111, xData, callStackDepth+1);
	D(&x101, xData, &x111, xData, callStackDepth+1);

	C(&x001, xData, &x011, xData, &x101, xData, callStackDepth+1);

	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, Box* a, CELL_TYPE* aData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=x->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ijk=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ijkminus1=GetDPTableCell(i,j,k-1,yData);
		int l=z->coords[1];
		CELL_TYPE* cost_ik=GetDPTableCell(i,l,k-1,zData);
		CELL_TYPE* cost_kj=GetDPTableCell(l,j,k-1,aData);
		CELL_TYPE newCost = 65535;
		if((*cost_ik!=65535) && (*cost_kj!=65535))
			newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ijk)
			*cost_ijk = newCost;
		if(*cost_ijkminus1 < *cost_ijk)
			*cost_ijk = *cost_ijkminus1;
		
		/*if((i==3) && (j==2))
			printf("%d %d %d = %d (B: min{(%d %d %d), (%d %d %d)+(%d %d %d) (%d+%d)})\n",i,j,k,*cost_ijk,i,j,k-1,i,l,k-1,l,j,k-1,*cost_ik,*cost_kj);*/
		/*char xindx[32], yindx[32], zindx[32], aindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		a->PrintStr(aindx);
		printf("B: x: %s y:%s z:%s a:%s\n",xindx,yindx,zindx,aindx);*/
		assert((y->coords[2]==i) && (y->coords[1]==j) && (y->coords[0]==k-1));
		assert((z->coords[2]==i) && (z->coords[1]==l) && (z->coords[0]==k-1));
		assert((a->coords[2]==l) && (a->coords[1]==j) && (a->coords[0]==k-1));
		//printf("(%d %d %d): min{(%d %d %d), (%d %d %d)+(%d %d %d)}\n",i,j,k,i,j,k-1,i,l,k-1,l,j,k-1);
		return;
	}
	Box a001(a->coords[0]+(a->coords[3]-a->coords[0]+1)/2,a->coords[1],a->coords[2],a->coords[3],a->coords[4]-(a->coords[4]-a->coords[1]+1)/2,a->coords[5]-(a->coords[5]-a->coords[2]+1)/2);
	Box a011(a->coords[0]+(a->coords[3]-a->coords[0]+1)/2,a->coords[1]+(a->coords[4]-a->coords[1]+1)/2,a->coords[2],a->coords[3],a->coords[4],a->coords[5]-(a->coords[5]-a->coords[2]+1)/2);
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box y001(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y011(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y101(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);
	Box z001(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]-(z->coords[5]-z->coords[2]+1)/2);
	Box z101(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[5]-z->coords[2]+1)/2,z->coords[3],z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]);

	B(&x000, xData, &y001, yData, &z001, zData, &a001, aData, callStackDepth+1);
	B(&x010, xData, &y011, yData, &z001, zData, &a011, aData, callStackDepth+1);
	B(&x100, xData, &y101, yData, &z101, zData, &a001, aData, callStackDepth+1);
	B(&x110, xData, &y111, yData, &z101, zData, &a011, aData, callStackDepth+1);


	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=x->coords[0];
		if(i == j)
			return;
		int l=y->coords[1]; 
		CELL_TYPE* cost_ijk=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ilk=GetDPTableCell(i,l,k,yData);
		CELL_TYPE* cost_ljk=GetDPTableCell(l,j,k,zData);
		CELL_TYPE newCost = 65535;
		if((*cost_ilk!=65535) && (*cost_ljk!=65535))
		 newCost = *cost_ilk + *cost_ljk;
		if(newCost < *cost_ijk)
			*cost_ijk = newCost;
		/*if((i==3) && (j==2))
			printf("%d %d %d = %d (C: min{self, (%d %d %d)+(%d %d %d)})\n",i,j,k,*cost_ijk,i,l,k,l,j,k);*/
		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("C: x: %s y:%s z:%s\n",xindx,yindx,zindx);*/
		assert((y->coords[2]==i) && (y->coords[1]==l) && (y->coords[0]==k));
		assert((z->coords[2]==l) && (z->coords[1]==j) && (z->coords[0]==k));
		//printf("(%d %d %d): min{self, (%d %d %d)+(%d %d %d)}\n",i,j,k,i,l,k,l,j,k);
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x001(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x011(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x101(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box x111(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]);
	Box y000(y->coords[0],y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y010(y->coords[0],y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y011(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y110(y->coords[0],y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4],y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);
	Box z000(z->coords[0],z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[0]+1)/2,z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]-(z->coords[5]-z->coords[2]+1)/2);
	Box z010(z->coords[0],z->coords[1]+(z->coords[4]-z->coords[1]+1)/2,z->coords[2],z->coords[3]-(z->coords[3]-z->coords[0]+1)/2,z->coords[4],z->coords[5]-(z->coords[5]-z->coords[2]+1)/2);
	Box z100(z->coords[0],z->coords[1],z->coords[2]+(z->coords[5]-z->coords[2]+1)/2,z->coords[3]-(z->coords[3]-z->coords[0]+1)/2,z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]);
	Box z101(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[5]-z->coords[2]+1)/2,z->coords[3],z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]);
	Box z110(z->coords[0],z->coords[1]+(z->coords[4]-z->coords[1]+1)/2,z->coords[2]+(z->coords[5]-z->coords[2]+1)/2,z->coords[3]-(z->coords[3]-z->coords[0]+1)/2,z->coords[4],z->coords[5]);
	Box z111(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1]+(z->coords[4]-z->coords[1]+1)/2,z->coords[2]+(z->coords[5]-z->coords[2]+1)/2,z->coords[3],z->coords[4],z->coords[5]);

	C(&x000, xData, &y000, yData, &z000, zData, callStackDepth+1);
	C(&x010, xData, &y000, yData, &z010, zData, callStackDepth+1);
	C(&x100, xData, &y100, yData, &z000, zData, callStackDepth+1);
	C(&x110, xData, &y100, yData, &z010, zData, callStackDepth+1);

	B(&x001, xData, &x000, xData, &y010, yData, &z100, zData, callStackDepth+1);
	B(&x011, xData, &x010, xData, &y010, yData, &z110, zData, callStackDepth+1);
	B(&x101, xData, &x100, xData, &y110, yData, &z100, zData, callStackDepth+1);
	B(&x111, xData, &x110, xData, &y110, yData, &z110, zData, callStackDepth+1);

	C(&x001, xData, &y011, yData, &z101, zData, callStackDepth+1);
	C(&x011, xData, &y011, yData, &z111, zData, callStackDepth+1);
	C(&x101, xData, &y111, yData, &z101, zData, callStackDepth+1);
	C(&x111, xData, &y111, yData, &z111, zData, callStackDepth+1);


	return;
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		
		int i=x->coords[2], j=x->coords[1], k=y->coords[1];
		if(i == j)
			return;
		/*CELL_TYPE* cost_ij=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ik=GetDPTableCell(i,k,k-1,yData);
		CELL_TYPE* cost_kj=GetDPTableCell(k,j,k-1,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij = newCost;*/
		/*char xindx[32], yindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		printf("D: x: %s y:%s\n",xindx,yindx);*/
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x001(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x011(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x101(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box x111(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]);
	Box y000(y->coords[0],y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y010(y->coords[0],y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y011(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y110(y->coords[0],y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4],y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);

	D(&x000, xData, &y000, yData, callStackDepth+1);
	D(&x010, xData, &y000, yData, callStackDepth+1);

	C(&x100, xData, &y100, yData, &x000, xData, callStackDepth+1);
	C(&x110, xData, &y100, yData, &x010, xData, callStackDepth+1);

	B(&x001, xData, &x000, xData, &y010, yData, &x100, xData, callStackDepth+1);
	B(&x011, xData, &x010, xData, &y010, yData, &x110, xData, callStackDepth+1);
	G(&x101, xData, &x100, xData, &y110, yData, callStackDepth+1);
	G(&x111, xData, &x110, xData, &y110, yData, callStackDepth+1);

	D(&x101, xData, &y111, yData, callStackDepth+1);
	D(&x111, xData, &y111, yData, callStackDepth+1);

	C(&x001, xData, &y011, yData, &x101, xData, callStackDepth+1);
	C(&x011, xData, &y011, yData, &x111, xData, callStackDepth+1);


	return;
}

void E(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=x->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ijk=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ijkminus1=GetDPTableCell(i,j,k-1,yData);
		int l=y->coords[1];
		CELL_TYPE* cost_ik=GetDPTableCell(i,l,k-1,yData);
		CELL_TYPE* cost_kj=GetDPTableCell(l,j,k-1,zData);
		CELL_TYPE newCost = 65535;
		if((*cost_ik!=65535) && (*cost_kj!=65535))
			newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ijk)
			*cost_ijk = newCost;
		if(*cost_ijkminus1 < *cost_ijk)
			*cost_ijk = *cost_ijkminus1;
		
		/*if((i==3) && (j==2))
			printf("%d %d %d = %d (E: min{(%d %d %d), (%d %d %d)+(%d %d %d)})\n",i,j,k,*cost_ijk,i,j,k-1,i,l,k-1,l,j,k-1);*/
		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("E: x: %s y:%s z:%s a:%s\n",xindx,yindx,yindx,zindx);*/
		//printf("E: (%d %d %d) = min {(%d %d %d), (%d %d %d)+(%d %d %d)}\n",i,j,k,i,j,k,i,l,k,l,j,k);
		assert((y->coords[2]==i) && (y->coords[1]==j) && (y->coords[0]==k-1));
		assert((y->coords[2]==i) && (y->coords[1]==l) && (y->coords[0]==k-1));
		assert((z->coords[2]==l) && (z->coords[1]==j) && (z->coords[0]==k-1));
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box y001(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y011(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y101(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);
	Box z001(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]-(z->coords[5]-z->coords[2]+1)/2);
	Box z011(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1]+(z->coords[4]-z->coords[1]+1)/2,z->coords[2],z->coords[3],z->coords[4],z->coords[5]-(z->coords[5]-z->coords[2]+1)/2);

	E(&x000, xData, &y001, yData, &z001, zData, callStackDepth+1);
	B(&x010, xData, &y011, yData, &y001, yData, &z011, zData, callStackDepth+1);
	E(&x100, xData, &y101, yData, &z001, zData, callStackDepth+1);
	B(&x110, xData, &y111, yData, &y101, yData, &z011, zData, callStackDepth+1);


	return;
}

void F(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=y->coords[1];
		if(i == j)
			return;
		/*CELL_TYPE* cost_ij=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ik=GetDPTableCell(i,k,k-1,yData);
		CELL_TYPE* cost_kj=GetDPTableCell(k,j,k-1,xData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij = newCost;*/
		/*char xindx[32], yindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		printf("F: x: %s y:%s\n",xindx,yindx);*/
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x001(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x011(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3],x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x101(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box x111(x->coords[0]+(x->coords[3]-x->coords[0]+1)/2,x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3],x->coords[4],x->coords[5]);
	Box y000(y->coords[0],y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y010(y->coords[0],y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y100(y->coords[0],y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y101(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y110(y->coords[0],y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3]-(y->coords[3]-y->coords[0]+1)/2,y->coords[4],y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);

	F(&x000, xData, &y000, yData, callStackDepth+1);
	F(&x100, xData, &y000, yData, callStackDepth+1);

	C(&x010, xData, &x000, xData, &y010, yData, callStackDepth+1);
	C(&x110, xData, &x100, xData, &y010, yData, callStackDepth+1);

	B(&x001, xData, &x000, xData, &x010, xData, &y100, yData, callStackDepth+1);
	E(&x011, xData, &x010, xData, &y110, yData, callStackDepth+1);
	B(&x101, xData, &x100, xData, &x110, xData, &y100, yData, callStackDepth+1);
	E(&x111, xData, &x110, xData, &y110, yData, callStackDepth+1);

	F(&x011, xData, &y111, yData, callStackDepth+1);
	F(&x111, xData, &y111, yData, callStackDepth+1);

	C(&x001, xData, &x011, xData, &y101, yData, callStackDepth+1);
	C(&x101, xData, &x111, xData, &y101, yData, callStackDepth+1);


	return;
}

void G(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=x->coords[0];
		if(i == j)
			return;
		CELL_TYPE* cost_ijk=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ijkminus1=GetDPTableCell(i,j,k-1,yData);
		int l=z->coords[1];
		CELL_TYPE* cost_ik=GetDPTableCell(i,l,k-1,zData);
		CELL_TYPE* cost_kj=GetDPTableCell(l,j,k-1,yData);
		CELL_TYPE newCost = 65535;
		if((*cost_ik!=65535) && (*cost_kj!=65535))
			newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ijk)
			*cost_ijk = newCost;
		if(*cost_ijkminus1 < *cost_ijk)
			*cost_ijk = *cost_ijkminus1;
		
		/*if((i==3) && (j==2))
			printf("%d %d %d = %d (G: min{(%d %d %d), (%d %d %d)+(%d %d %d)})\n",i,j,k,*cost_ijk,i,j,k-1,i,l,k-1,l,j,k-1);*/
		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("G: x: %s y:%s z:%s a:%s\n",xindx,yindx,zindx,yindx);*/
		assert((y->coords[2]==i) && (y->coords[1]==j) && (y->coords[0]==k-1));
		assert((z->coords[2]==i) && (z->coords[1]==l) && (z->coords[0]==k-1));
		assert((y->coords[2]==l) && (y->coords[1]==j) && (y->coords[0]==k-1));
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box y001(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y011(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y101(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);
	Box z001(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3],z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]-(z->coords[5]-z->coords[2]+1)/2);
	Box z101(z->coords[0]+(z->coords[3]-z->coords[0]+1)/2,z->coords[1],z->coords[2]+(z->coords[5]-z->coords[2]+1)/2,z->coords[3],z->coords[4]-(z->coords[4]-z->coords[1]+1)/2,z->coords[5]);

	G(&x000, xData, &y001, yData, &z001, zData, callStackDepth+1);
	G(&x010, xData, &y011, yData, &z001, zData, callStackDepth+1);
	B(&x100, xData, &y101, yData, &z101, zData, &y001, yData, callStackDepth+1);
	B(&x110, xData, &y111, yData, &z101, zData, &y011, yData, callStackDepth+1);


	return;
}

void H(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, int callStackDepth)
{
	if(x->coords[0]==x->coords[3])
	{
		int i=x->coords[2], j=x->coords[1], k=y->coords[1];
		if(i == j)
			return;
		/*CELL_TYPE* cost_ij=GetDPTableCell(i,j,k,xData);
		CELL_TYPE* cost_ik=GetDPTableCell(i,k,k-1,yData);
		CELL_TYPE* cost_kj=GetDPTableCell(k,j,k-1,xData);
		CELL_TYPE newCost = 65535;
		if((*cost_ik!=65535) && (*cost_kj!=65535))
		 newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij = newCost;*/
		char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		printf("H: x: %s y:%s\n",xindx,yindx);
		return;
	}
	Box x000(x->coords[0],x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x010(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2],x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]-(x->coords[5]-x->coords[2]+1)/2);
	Box x100(x->coords[0],x->coords[1],x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4]-(x->coords[4]-x->coords[1]+1)/2,x->coords[5]);
	Box x110(x->coords[0],x->coords[1]+(x->coords[4]-x->coords[1]+1)/2,x->coords[2]+(x->coords[5]-x->coords[2]+1)/2,x->coords[3]-(x->coords[3]-x->coords[0]+1)/2,x->coords[4],x->coords[5]);
	Box y001(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y011(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2],y->coords[3],y->coords[4],y->coords[5]-(y->coords[5]-y->coords[2]+1)/2);
	Box y101(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1],y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4]-(y->coords[4]-y->coords[1]+1)/2,y->coords[5]);
	Box y111(y->coords[0]+(y->coords[3]-y->coords[0]+1)/2,y->coords[1]+(y->coords[4]-y->coords[1]+1)/2,y->coords[2]+(y->coords[5]-y->coords[2]+1)/2,y->coords[3],y->coords[4],y->coords[5]);

	H(&x000, xData, &y001, yData, callStackDepth+1);
	G(&x010, xData, &y011, yData, &y001, yData, callStackDepth+1);
	E(&x100, xData, &y101, yData, &y001, yData, callStackDepth+1);
	B(&x110, xData, &y111, yData, &y101, yData, &y011, yData, callStackDepth+1);


	return;
}


