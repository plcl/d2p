#include<iostream>
#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<vector>
#include<string.h>
#include<cassert>
#include<fstream>
#include<algorithm>
#include<stdlib.h>
#include<sys/time.h>
#include<math.h>
#include<map>
#include<set>
#ifdef PARALLEL
#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif
#define MAX_NUM_VERTICES 45000
using namespace std;
typedef map<int,set<pair<int, int> > > Adjacency_List;
typedef int CELL_TYPE;

CELL_TYPE *cost;
vector<int> vertexIDList;
Adjacency_List graphData;
long int inputSize[2];
int cellsPerRowTable;

typedef struct Box
{
	int  coords[4]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d){coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;}
	Box(){}
	void PrintStr(char *str)const {sprintf(str,"%d %d %d %d",coords[0],coords[1],coords[2],coords[3]);}
	Box& operator=(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
		return *this;
	}
	Box(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) ||(this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]))
			flag = false;
		return flag;
	}
	
	long int GetBoxSize()
	{
		long int len = 1;
		for(int i=0;i<2;i++)
		{
			len *= (coords[i+2]-coords[i]+1);
		}	
		return len;
	}
}Box;

void A(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);
void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth);

/*Helper function to tokenize a string */
vector<string> Split(const char *str, char c)
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(string(begin, str));
    } while (0 != *str++);

    return result;
}

/* this function is for reading wiki data set. */
int ReadGraphDataGeneral(FILE* fp, int maxVertices, Adjacency_List& g)
{
	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int weight = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		g[fromNode].insert(make_pair(toNode,weight));
		if(g.size()>=maxVertices) break;
	}

	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}

/* this function is for reading autonomous systems (AS) data set. */
int ReadGraphData(FILE* fp, int maxVertices, Adjacency_List& g)
{

	char line[1024];
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		if(line[0]=='#')
			continue;
		vector<string> edgeInfo  = Split(line,'\t');
		if(edgeInfo.size() != 3)
			return -1;
		int relationship = atoi((edgeInfo[2]).c_str());
		int toNode = atoi((edgeInfo[1]).c_str());
		int fromNode = atoi((edgeInfo[0]).c_str());
		if((relationship == 1) || (relationship == -1))
			g[fromNode].insert(make_pair(toNode,1));
		else if((relationship == 0) || (relationship == 2))
		{
			g[fromNode].insert(make_pair(toNode,1));
			g[toNode].insert(make_pair(fromNode,1));
		}
				
		if(g.size()>=maxVertices) break;
	}
	
	int numEdges=0;
	Adjacency_List::iterator it = g.begin();
	for(;it!=g.end();it++)
	{
		vertexIDList.push_back(it->first);
		numEdges+=(it->second).size();
	}
	//printf("Number of vertices:%d edges:%d\n",g.size(),numEdges);
	return g.size();
}

int ReadInput(int argc, char** argv)
{
	if((argc==1) || (strcmp(argv[1],"-h")==0))
	{
		printf("Usage: ./<exec> <input_graph> <num_vertices>\n");
		exit(0);
	}
	FILE* fpi=fopen(argv[1],"r");
	if(!fpi)
	{
		printf("ERROR: Unable to open input file.\n");
		exit(0);
	}
	int maxVertices=MAX_NUM_VERTICES;
	maxVertices=atoi(argv[2]);
	int ret1=ReadGraphData(fpi, maxVertices, graphData);
	fclose(fpi);
	if(ret1<0)
	{
		printf("ERROR: Unable to read input file correctly.\n");
		exit(0);
	}
	else
	{
		inputSize[0]=ret1-1; inputSize[1]=ret1-1;
	}
	return inputSize[0]+1;
}

void InitializeDPTable(Box* b, CELL_TYPE* data)
{
	int numCells = b->GetBoxSize();
	unsigned int relIndx = 0;
	for(unsigned int i=b->coords[1];i<=b->coords[3];i++)
	{
		for(unsigned int j=b->coords[0];j<=b->coords[2];j++)
		{
			CELL_TYPE initVal=65535;
			if(i==j)
				initVal = 0;
			else
			{
				set<pair<int, int> >::iterator it=graphData[vertexIDList[i]].begin();
				while(it!=graphData[vertexIDList[i]].end())
				{
					if(it->first == vertexIDList[j])
					{
						initVal = it->second;
						break;
					}
					it++;
				}
			}
			data[relIndx++] = initVal;
		}
	}
	return;
}

inline CELL_TYPE* GetDPTableCell(int i, int j,CELL_TYPE* data)
{
	CELL_TYPE* ret = data+5;
	int side  = data[2];
	int xOffset = (i - data[1]);
	ret += xOffset*side;
	int yOffset = (j - data[0]); 
	ret += yOffset;
	return ret;
}

void DebugPrintAllCells()
{
	for(int i=0;i<cellsPerRowTable;i++)
	{
		for(int j=0;j<cellsPerRowTable;j++)
		{
			CELL_TYPE* cell = GetDPTableCell(i, j, cost);
			if(*cell != 65535)
				printf("%d",*cell);
			else printf("inf");
			if(j!=cellsPerRowTable-1)
				printf(" ");
		}
		printf("\n");
	}
}



void PrintResults()
{
	DebugPrintAllCells();
}

int main(int argc, char* argv[])
{
	int numVertices = 0;
	
	//matrix chain is specified in a file input to the program. Read the file and populate matrixChain
	cellsPerRowTable = ReadInput(argc,argv);

	numVertices = graphData.size();
	//Set the bounds of DP table.
	Box matrixDim(0,0,inputSize[0],inputSize[1]);
	cost = new CELL_TYPE[matrixDim.GetBoxSize()+5];
	for(int i=0;i<2;i++)
	{
		cost[i]=matrixDim.coords[i];
		cost[i+2]=matrixDim.coords[i+2]-matrixDim.coords[i]+1;
	}
	InitializeDPTable(&matrixDim, cost+5);
	/*printf("Initial weights\n");
	DebugPrintAllCells();*/

	struct timeval startTime, endTime;
	gettimeofday(&startTime,0);
	//Specify the bounds of DP table, whose cells are to be computed.
	A(&matrixDim, cost, &matrixDim, cost, &matrixDim, cost, 0);
	gettimeofday(&endTime,0);

	//Print results
	PrintResults();
	long elapsedTime = (endTime.tv_sec-startTime.tv_sec)*1000000+(endTime.tv_usec-startTime.tv_usec);
	printf("Time taken %f seconds\n",elapsedTime/(float)1000000);
	//freeing up memory
	delete [] cost;

}

void A(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return; 
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;

		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("A: x: %s y:%s z:%s\n",xindx,yindx,zindx);*/
    		//printf("A: i:%d j:%d k:%d\n", i, j, k);
    		//printf("(%d %d %d): min{self, (%d %d %d)+(%d %d %d)}\n", i, j, k,i,k,k-1,k,j,k-1);
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	A(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
	B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
	C(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

	A(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
	B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
	C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
	return;
}

void B(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return; 
               	 
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		
		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("B: x: %s y:%s z:%s\n",xindx,yindx,zindx);*/
    		//printf("B: i:%d j:%d k:%d\n", i, j, k);
    		//printf("(%d %d %d): min{self, (%d %d %d)+(%d %d %d)}\n", i, j, k,i,k,k-1,k,j,k-1);
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	B(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
	B(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
	D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

	B(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
	B(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
	D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
	return;
}

void C(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return; 
               	 
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		
		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("C: x: %s y:%s z:%s\n",xindx,yindx,zindx);*/
    		//printf("C: i:%d j:%d k:%d\n", i, j, k);
    		//printf("(%d %d %d): min{self, (%d %d %d)+(%d %d %d)}\n", i, j, k,i,k,k-1,k,j,k-1);
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	
	C(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
	C(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
	D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

	C(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
	C(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
	D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);

	return;
}

void D(Box* x, CELL_TYPE* xData, Box* y, CELL_TYPE* yData, Box* z, CELL_TYPE* zData, int callStackDepth)
{
	if((x->coords[0]==x->coords[2]) && (x->coords[1]==x->coords[3]))
	{
		int i=x->coords[1], j=x->coords[0], k=y->coords[0];
		if(i == j)
			return; 
               	 
		CELL_TYPE* cost_ij = GetDPTableCell(i,j,xData);
		CELL_TYPE* cost_ik = GetDPTableCell(i,k,yData);
		CELL_TYPE* cost_kj = GetDPTableCell(k,j,zData);
		CELL_TYPE newCost = *cost_ik + *cost_kj;
		if(newCost < *cost_ij)
			*cost_ij =  newCost;
		
		/*char xindx[32], yindx[32], zindx[32];
		x->PrintStr(xindx);
		y->PrintStr(yindx);
		z->PrintStr(zindx);
		printf("D: x: %s y:%s z:%s\n",xindx,yindx,zindx);*/
    		//printf("D: i:%d j:%d k:%d\n", i, j, k);
    		//printf("(%d %d): min{self, (%d %d)+(%d %d)}\n", i, j, i,k,k,j);
		return;
	}

	Box x00(x->coords[0],x->coords[1],x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x01(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1],x->coords[2],x->coords[3]-(x->coords[3]-x->coords[1]+1)/2);
	Box x10(x->coords[0],x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2]-(x->coords[2]-x->coords[0]+1)/2,x->coords[3]);
	Box x11(x->coords[0]+(x->coords[2]-x->coords[0]+1)/2,x->coords[1]+(x->coords[3]-x->coords[1]+1)/2,x->coords[2],x->coords[3]);
	Box y00(y->coords[0],y->coords[1],y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y01(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1],y->coords[2],y->coords[3]-(y->coords[3]-y->coords[1]+1)/2);
	Box y10(y->coords[0],y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2]-(y->coords[2]-y->coords[0]+1)/2,y->coords[3]);
	Box y11(y->coords[0]+(y->coords[2]-y->coords[0]+1)/2,y->coords[1]+(y->coords[3]-y->coords[1]+1)/2,y->coords[2],y->coords[3]);
	Box z00(z->coords[0],z->coords[1],z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z01(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1],z->coords[2],z->coords[3]-(z->coords[3]-z->coords[1]+1)/2);
	Box z10(z->coords[0],z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2]-(z->coords[2]-z->coords[0]+1)/2,z->coords[3]);
	Box z11(z->coords[0]+(z->coords[2]-z->coords[0]+1)/2,z->coords[1]+(z->coords[3]-z->coords[1]+1)/2,z->coords[2],z->coords[3]);

	
	D(&x00, xData, &y00, yData, &z00, zData, callStackDepth+1);
	D(&x01, xData, &y00, yData, &z01, zData, callStackDepth+1);
	D(&x10, xData, &y10, yData, &z00, zData, callStackDepth+1);
	D(&x11, xData, &y10, yData, &z01, zData, callStackDepth+1);

	D(&x00, xData, &y01, yData, &z10, zData, callStackDepth+1);
	D(&x01, xData, &y01, yData, &z11, zData, callStackDepth+1);
	D(&x10, xData, &y11, yData, &z10, zData, callStackDepth+1);
	D(&x11, xData, &y11, yData, &z11, zData, callStackDepth+1);
	return;
}
