#include <stdio.h>
#include<string.h>
#include <queue>
#include<cassert>
#include<fstream>
#include<sstream>
#include<set>
#include <sys/stat.h>
#include "autogen_deductive_n4.h"
#include "Nussinov4D.h"
#include "Parenthesis.h"

//map<string, int> unCalls;
string problemName;
int computeGrid=COMPUTE_FULL;
int d;
int s;
Problems problem;
vector<Function> functions;
char nextFunction = 'A';
void (*checkAll)(vector<Coordinate> &coordList, Coordinate &initialOffsets);

RecursionBase* recBase=nullptr;

//combine any calls with repeating read/write regions
vector<Call> combineRegionTuples(vector<Coordinate> &coordList){
	vector<Call> calls;
	bool combined[coordList.size()];
	for(unsigned int i=0;i<coordList.size();i++){
		combined[i] = false;
	}
	for(unsigned int i=0;i<coordList.size();i++){
		if(combined[i]) {
			continue;
		}
		//printf("checking combine:");coordList[i].print();
		bool canBeCombined = false;
		Call shared_region;
		for(int k=1;k<(s+1);k++){
			// if there is a shared read/write region in this call
			if(coordList[i].compareCoordinates(coordList[i],0,k) == 0){
				for(unsigned int j=i+1;j<coordList.size();j++){
					if(combined[j]) {
						continue;
					}
					// and it shares a write region with another call
					if(coordList[i].compareCoordinates(coordList[j],0,0) == 0){
						for(int l=1;l<(s+1);l++){
							// and that call also has a shared read write region
							if(coordList[j].compareCoordinates(coordList[j],0,l) == 0){
								combined[j] = true;
								canBeCombined = true;
								shared_region.push_back(coordList[j]); //add them to the same call and mark them as part of a shared group
								/*printf("combined:");
								coordList[j].print();*/
								break;
							}
						}
					}
				}
				break;
			}
		}
		if(canBeCombined){
			/*printf("combined:");
			coordList[i].print();*/
			combined[i] = true;
			shared_region.push_back(coordList[i]);
			shared_region.sortCoords();
			calls.push_back(shared_region);
		}
	}
	for(unsigned int i=0;i<coordList.size();i++){
		if(!combined[i]){
			Call c;
			c.push_back(coordList[i]);
			calls.push_back(c);
		}
	}
	return calls;
}

//sort the calls by dependency and return a vector where sortedCalls[t] is a list of all the calls that run at time t
vector<vector<Call>> sortCalls(vector<Call> &calls){
	vector<TreeNode> nodes;
	for(unsigned int i=0;i<calls.size();i++){ //add a node for each call
		nodes.push_back(TreeNode(calls[i],i));
	}
	for(unsigned int i=0;i<nodes.size();i++){
		for(unsigned int j=0;j<nodes.size();j++){
			if(i==j){ 
				continue;
			}
			bool exists = false;
			for(int k=1;k<(s+1);k++){
				for(int l=0;l<nodes[j].call.size();l++){
					if(nodes[i].call[0].compareCoordinates(nodes[j].call[l],0,k) == 0){
						for(unsigned int m=0;m<nodes[i].children.size();m++){
							if(nodes[i].children[m]->equals(nodes[j])){
								exists = true;
							}
						}
						if(!exists){
							nodes[i].children.push_back(&nodes[j]);
							nodes[j].parents++;
							exists = true;
						}
						break;
					}
				}
				if(exists){
					break;
				}
			}
		}
	}
	// now that we have all the write -> read dependencies, we need to check if any have the same write region, in which case we give it to the bigger one.
	for(unsigned int i=0;i<nodes.size();i++){
		for(unsigned int j=i+1;j<nodes.size();j++){
			if(i==j){
				continue;
			}
			bool exists = false;
			for(unsigned int l=0;l<nodes[i].children.size();l++){
				if(nodes[i].children[l]->equals(nodes[j])){
					exists = true;
				}
			}
			for(unsigned int l=0;l<nodes[j].children.size();l++){
				if(nodes[j].children[l]->equals(nodes[i])){
					exists = true;
				}
			}
			if(exists) continue;	
			assert(!exists);
			if(nodes[i].call[0].compareCoordinates(nodes[j].call[0],0,0) == 0){
				if(nodes[i].call[0] < nodes[j].call[0]){ // see who gets the dependency
						nodes[i].children.push_back(&nodes[j]);
						nodes[j].parents++;
				} else {
						nodes[j].children.push_back(&nodes[i]);
						nodes[i].parents++;
				}
			}
		}
	}
	queue<TreeNode *> q;
	for(unsigned int i=0;i<nodes.size();i++){ //find all the nodes with no parents
		if(nodes[i].parents == 0){
			nodes[i].depth = 0;
			q.push(&nodes[i]);
		}
	}
	while(!q.empty()){ //bfs
		TreeNode *t = q.front();
		q.pop();
		for(unsigned int i=0;i<t->children.size();i++){
			if(t->depth + 1 > t->children[i]->depth){
				t->children[i]->depth = t->depth + 1;
				q.push(t->children[i]);
			}
		}
	}
	int maxdepth = 0;
	for(unsigned int i=0;i<nodes.size();i++){
		if(nodes[i].depth > maxdepth){
			maxdepth = nodes[i].depth;
		}
	}
	// use the data we have to create the correct structure
	vector<vector<Call>> sortedCalls;
	for(int i=0;i<=maxdepth;i++){
		vector<Call> levelCalls;
		for(unsigned int j=0;j<nodes.size();j++){
			if(nodes[j].depth == i){
				levelCalls.push_back(nodes[j].call);
			}
		}
		sort(levelCalls.begin(),levelCalls.end());
		sortedCalls.push_back(levelCalls);
	}
	nodes.clear();
	return sortedCalls;
}

void PrintCalls(vector<Call>& calls)
{
	for(unsigned int i=0;i<calls.size();i++)
	{
		printf("*call* %d\n",i);
		calls[i].print();
	}
}

char solveRecurrence(vector<Coordinate> initialOffsets){
	//static int callID=0;
	vector<Coordinate> coordList;
	
	for(unsigned int i=0;i<initialOffsets.size();i++){
		checkAll(coordList, initialOffsets[i]);
	}

	vector<Call> calls = combineRegionTuples(coordList);
	
	//PrintCalls(calls);

	vector<vector<Call>> sortedCalls = sortCalls(calls);

	/*if(callID==3){
	for(unsigned int i=0;i<sortedCalls.size();i++)
	{
		printf("---sorted call %d---\n",i);
		PrintCalls(sortedCalls[i]);
		printf("---\n");
	}}
	callID++;*/
	Function f(sortedCalls, initialOffsets);

	for(unsigned int i=0;i<functions.size();i++){
		if(f.equals(functions[i])){ //if this function has been found already, stop recursion
			//printf("Function already discovered: %c\n",functions[i].getName());
			return functions[i].getName();
		}
	}


	//printf("Function newly discovered: %c\n",nextFunction);
	f.setName(nextFunction);
	nextFunction++;
	functions.push_back(f);

	int index = functions.size()-1;

	// set the new offsets to be the top corner of where we are going to recurse into. we also double the size of the problem, so all previous offsets are doubled.
	for(unsigned int h=0;h<sortedCalls.size();h++){
		for(unsigned int i=0;i<sortedCalls[h].size();i++){
			vector<Coordinate> newOffsets(sortedCalls[h][i].coords.begin(),sortedCalls[h][i].coords.end());
			for(unsigned int j=0;j<newOffsets.size();j++){
				for(int k=0;k<newOffsets[j].size();k++){
					newOffsets[j][k] *= N;
					newOffsets[j][k] += 2 * sortedCalls[h][i][j].offsets[k];
					newOffsets[j].offsets[k] = 0;
				}
			}
			char name = solveRecurrence(newOffsets);
			functions[index].outputFingerprint[h][i].setName(name);
		}
	}
	return f.getName();
}

int main(int argc, char *argv[]){
	if(argc < 2){
		printf("No problem specified.\n");
		return 1;
	}
	switch(atoi(argv[1])){ //setup the correct parameters for the problem
		case PROBLEMPARENTHESIS:
			problem = PROBLEMPARENTHESIS;
			d = 2;
			s = 2;
			checkAll = PAR_checkAll;
			problemName = "PAR";
			break;
		case PROBLEMSMITHWATERMAN:
			problem = PROBLEMSMITHWATERMAN;
			d = 2;
			s = 3;
			checkAll = SW_checkAll;
			problemName = "SW";
			break;
		case PROBLEMGAUSSIANELIMINATION:
			problem = PROBLEMGAUSSIANELIMINATION;
			d = 2;
			s = 3;
			checkAll = GE_checkAll;
			problemName = "GE";
			break;
		case PROBLEM_4D_NUSSINOV:
			problem = PROBLEM_4D_NUSSINOV;
			d = 4;
			s = 2;
			checkAll = N4_checkAll;
			problemName = "Nussinov4D";
			break;
		default:
			printf("No problem specified.\n");
			return 1;
	}
	Coordinate initialOffsets; //initialize the offsets to 0
	for(int i=0;i<(s+1)*d;i++){
		initialOffsets[i] = 0;
		initialOffsets.offsets[i] = 0;
	}
	solveRecurrence({initialOffsets});
	//sort(functions.begin(),functions.end());
	for(unsigned int i=0;i<functions.size();i++){
		functions[i].print();
	}
	
	GenerateFunctionDefs();
	/*map<string,int>::iterator iter=unCalls.begin();
	for(;iter!=unCalls.end();iter++)
	{
		printf("%s\n",iter->first.c_str());
	}*/
	return 0;
}

void PrintCoordList(vector<Coordinate>& coordList)
{
	for(int i=0;i<coordList.size();i++)
	{
		/*for(int j=0;j<s;j++)
		{
			printf("%d %d ",coordList[i][j*d], coordList[i][j*d+1]);
		}
		printf("%d %d\n",coordList[i][s*d], coordList[i][s*d+1]);*/
		coordList[i].print2();
	}
}


void PAR_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets){
	//static int callID=0;
	int xrow = initialOffsets[0];
	int xcol = initialOffsets[1];
	int ucol = initialOffsets[3];
	//printf("%d %d %d %d %d %d\n",xrow,xcol,initialOffsets[2],ucol,initialOffsets[4],initialOffsets[5]);
	for(int i=N-1;i>=0;i--){
		for(int j=0;j<N;j++){
			for(int k=0;k<N;k++){
				int I=xrow+i;
				int J=xcol+j;
				int K=ucol+k;
				if(J > I && K > I && J > K-1){
					//printf("I:%d J:%d K:%d i:%d j:%d k:%d\n",I,J,K,i,j,k);
					/*if(callID==3){char tmpstr[1024];
					sprintf(tmpstr,"%d %d %d",i,j,k);
					unCalls.insert(make_pair(string(tmpstr),1));}*/
					Coordinate coords;
					for(int l=0;l<(s+1)*d;l++){
						coords.offsets[l] = initialOffsets[l];
					}
					coords[0] = (i>=(N/2)?1:0);
					coords[1] = (j>=(N/2)?1:0);
					coords[2] = (i>=(N/2)?1:0);
					coords[3] = (k>=(N/2)?1:0);
					coords[4] = (k>=(N/2)?1:0);
					coords[5] = (j>=(N/2)?1:0);
					if(find(coordList.begin(),coordList.end(),coords) == coordList.end()){
						coordList.push_back(coords);
					}
				}
			}
		}
	}
	//callID++;
	//PrintCoordList(coordList);
}


void GE_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets){
	int xrow = initialOffsets[0];
	int xcol = initialOffsets[1];
	int ucol = initialOffsets[3];
	for(int k=0;k<N;k++){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				int I=xrow+i;
				int J=xcol+j;
				int K=ucol+k;
				if(I > K && J >= K){
					Coordinate coords;
					for(int l=0;l<(s+1)*d;l++){
						coords.offsets[l] = initialOffsets[l];
					}
					coords[0] = (i>(N/2)?1:0);
					coords[1] = (j>(N/2)?1:0);
					coords[2] = (i>(N/2)?1:0);
					coords[3] = (k>(N/2)?1:0);
					coords[4] = (k>(N/2)?1:0);
					coords[5] = (j>(N/2)?1:0);
					coords[6] = (k>(N/2)?1:0);
					coords[7] = (k>(N/2)?1:0);
					if(find(coordList.begin(),coordList.end(),coords) == coordList.end()){
						coordList.push_back(coords);
					}
				}
			}
		}
	}
}

void N4_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets){
	for (int t = 1; t < N; t++){
		for (int tt = 1; tt < N; tt++){
			for (int i = 0; i < N-t; i++){
				int j = t + i;
				for (int ii = 0; ii < N-tt; ii++){
					int jj = tt + ii;
					for (int k =i; k < j; k++){
						for (int kk = ii; kk < jj; kk++){
							//M[i,j,k,l] = max ( M[i,s,k,t ] + M[ s+1,j,t+1,l ])
							Coordinate coords;
							for(int l=0;l<(s+1)*d;l++){
								coords.offsets[l] = initialOffsets[l];
							}
							coords[0] = (i>=(N/2)?1:0);
							coords[1] = (j>=(N/2)?1:0);
							coords[2] = (ii>=(N/2)?1:0);
							coords[3] = (jj>=(N/2)?1:0);
							coords[4] = (i>=(N/2)?1:0);
							coords[5] = (k>=(N/2)?1:0);
							coords[6] = (ii>=(N/2)?1:0);
							coords[7] = (kk>=(N/2)?1:0);
							coords[8] = ((k)>=(N/2)?1:0);
							coords[9] = (j>=(N/2)?1:0);
							coords[10] = ((kk)>=(N/2)?1:0);
							coords[11] = (jj>=(N/2)?1:0);
							if(find(coordList.begin(),coordList.end(),coords) == coordList.end()){
								coordList.push_back(coords);
							}
						}
					}
				}
			}
		}
	}
}

void SW_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets){
	int xrow = initialOffsets[0];
	int xcol = initialOffsets[1];
	int ucol = initialOffsets[3];
	//printf("%d %d %d %d %d %d\n",xrow,xcol,initialOffsets[2],ucol,initialOffsets[4],initialOffsets[5]);
	for(int i=N;i>=1;i--){
		for(int j=1;j<=N;j++){
					//printf("I:%d J:%d K:%d i:%d j:%d k:%d\n",I,J,K,i,j,k);
					Coordinate coords;
					for(int l=0;l<(s+1)*d;l++){
						coords.offsets[l] = initialOffsets[l];
					}
					coords[0] = (i>(N/2)?1:0);
					coords[1] = (j>(N/2)?1:0);
					coords[2] = ((i-1)>(N/2)?1:0);
					coords[3] = ((j-1)>(N/2)?1:0);
					coords[4] = (i>(N/2)?1:0);
					coords[5] = ((j-1)>(N/2)?1:0);
					coords[6] = ((i-1)>(N/2)?1:0);
					coords[7] = (j>(N/2)?1:0);
					if(find(coordList.begin(),coordList.end(),coords) == coordList.end()){
						coordList.push_back(coords);
					}
		}
	}

	//PrintCoordList(coordList);
}

pair<string,string> CreateFunctionCallHierarchySummary()
{
	string hdrDecl(""), cppDefn("");
	
	map<char, map<char, int> > fnCallHierarchySummary;
	for(unsigned int h=0;h<functions.size();h++)
	{
		char fnName = functions[h].getName();
		for(unsigned int i=0;i<functions[h].calls.size();i++)
		{
			for(unsigned int j=0;j<functions[h].calls[i].size();j++)
			{
				Fingerprint fingerprint = functions[h].outputFingerprint[i][j];
				map<char, int> child;
				child.insert(make_pair(fingerprint.getName(),1));
				auto callTypeStatus = fnCallHierarchySummary.insert(make_pair(fnName,child));
				if(!(callTypeStatus.second))
				{
					auto numChildrenOfTypeStatus = ((callTypeStatus.first)->second).insert(make_pair(fingerprint.getName(),1));
					if(!(numChildrenOfTypeStatus.second))
						((numChildrenOfTypeStatus.first)->second)++;
				}
			}
		}
	}

	ostringstream ostrHdr;
	ostrHdr<<"extern int fnCallHierarchySummary["<<fnCallHierarchySummary.size()<<"]["<<fnCallHierarchySummary.size()<<"];\n";
	hdrDecl += ostrHdr.str();

	ostringstream ostr;
	ostr<<"int fnCallHierarchySummary["<<fnCallHierarchySummary.size()<<"]["<<fnCallHierarchySummary.size()<<"]={";
	auto callTypeIter = fnCallHierarchySummary.begin();
	for(int i=0;callTypeIter!=fnCallHierarchySummary.end();callTypeIter++,i++)
	{
		ostr<<"{";
		auto childrenTypeIter = (callTypeIter->second).begin();
		auto callTypeIter2 = fnCallHierarchySummary.begin();
		unsigned int j=0;
		for(j=0;callTypeIter2!=fnCallHierarchySummary.end();callTypeIter2++,j++)
		{
			if(childrenTypeIter->first == callTypeIter2->first)
			{
				ostr<<childrenTypeIter->second;
				childrenTypeIter++;
			}
			else
				ostr<<"0";
			if(j!=(fnCallHierarchySummary.size()-1))
				ostr<<",";
		}
		ostr<<"}";
		if(i != (fnCallHierarchySummary.size()-1))
			ostr<<",";	
	}
	ostr<<"};\n";
	cppDefn += ostr.str();
	return make_pair(hdrDecl,cppDefn);
}

string GenerateDeferredCallTemplate()
{
	string defCall("");
	defCall +="template<int ...> struct Sequence{};\n";
	defCall +="template<int N, int ...S> struct Generate_Seq : Generate_Seq<N-1, N-1, S...> {};\n";
	defCall +="template<int ...S> struct Generate_Seq<0, S...>{ typedef Sequence<S...> type; }; //base case of recursive class template definition\n";
	defCall +="template <typename ...Args> struct DeferredCall {\n";
	defCall +="\tstd::tuple<Args...> params;\n";
  	defCall +="\tvoid (*fptr)(Args...);\n";
	defCall +="\tvoid Run()\n\t{\n";
    	defCall +="\t\t_Run(typename Generate_Seq<sizeof...(Args)>::type());\n";
	defCall +="\t\treturn;\n\t}\n\n";
	
	defCall +="\ttemplate<int ...S>\n";
	defCall +="\tvoid _Run(Sequence<S...>)\n";
  	defCall +="\t{\n\t\tfptr(std::get<S>(params)...);\n";
    	defCall +="\t\treturn;\n\t}\n};\n";

	return defCall;
}


void GenerateFunctionDefs()
{
	RecursionBase* base = GetRecursionBaseInstance(problem);
	
	/*struct stat existStatus = {0};
	string directory("../DMCode");
	if (stat(directory.c_str(), &existStatus) == -1) 
	{
		printf("ERROR: Incorrect program install path\n");
		exit(0);
	}
    	else
	{
		directory += string("/")+problemName;
		//directory += string("/Parenthesis");
		if (stat(directory.c_str(), &existStatus) == -1) 
		{
			printf("Directory %s does not exist. creating..\n",directory.c_str());
			mkdir(directory.c_str(), 0777);
		}
	}*/
	string directory(".");
	filebuf fbcpp;
	string cppFile(directory+string("/RecursiveFunctions.cpp"));
	fbcpp.open(cppFile.c_str(),ios::out);
	
	ostream oscpp(&fbcpp);

	pair<string,string> fnCallHierarchySummary = CreateFunctionCallHierarchySummary();
	oscpp<<"#include \"HelperFunctions.h\"\n";
	oscpp<<fnCallHierarchySummary.second;
	oscpp<<"long int inputSize[DIMENSION];\n";
	oscpp<<"map<int, vector<FunctionCall*> > fnCalls;\n";
	oscpp<<"extern int recursionDepth;\n";
	oscpp<<"int fnCounter=0;\n";
	oscpp<<"extern map<int,int> tileUpdateLog;\n";

	pair<string,string> inputMethods = base->ReadInput();
	filebuf globalTypesHeaderBuf;
	//string globalTypesFileName("../DMCode/common/GlobalTypes.h");
	string globalTypesFileName("GlobalTypes.h");
	globalTypesHeaderBuf.open(globalTypesFileName.c_str(),ios::out);
	ostream globalTypesHeader(&globalTypesHeaderBuf);
	globalTypesHeader<<"#pragma once\n";
	globalTypesHeader<<"#define DIMENSION "<<d<<"\n";
	globalTypesHeader<<"#define METADATASPACE (2*DIMENSION+1)\n";
	//Write the user defined DP Table Cell type into GlobalTypes.h. Default:int
	if(inputMethods.first.empty())
		globalTypesHeader<<"typedef int CELL_TYPE\n";
	else
		globalTypesHeader<<inputMethods.first;
	globalTypesHeader<<fnCallHierarchySummary.first;
	globalTypesHeader<<FetchCellFnCallCode();
	globalTypesHeaderBuf.close();

	string fnDeclarations("\n");
	
	/*string deferredCall = GenerateDeferredCallTemplate();
	oscpp<<deferredCall;*/
	pair<string,string> initDPTable = base->InitializeDPTable();
	pair<string,string> resultsCode = GenerateResultsCode(base);

	string macroDefns("\n");;
	string recFunctionDefns("");
	//code for recursive method definitions and declarations. sets computeGrid.
	for(unsigned int i=0;i<functions.size();i++)
	{
		string functionCode = functions[i].GenerateFunctionBody(base);
		fnDeclarations +=functions[i].signature+";\n";
		macroDefns += functions[i].GenerateParamListMacro();
		recFunctionDefns +=functionCode;
	}

	//code for unroll methods. uses computeGrid.
	for(unsigned int i=0;i<functions.size();i++)
	{
		string functionCodeUnroll = functions[i].GenerateFunctionBodyForUnroll(base);
		fnDeclarations +=functions[i].signature+";\n";
		recFunctionDefns +=functionCodeUnroll;
	}
	
	pair<string,string> topLevel = GenerateTopLevelCalls();
	if(computeGrid == COMPUTE_UTM)
		oscpp<<"int computeGrid = COMPUTE_UTM;\n";
	else if(computeGrid == COMPUTE_FULL)
		oscpp<<"int computeGrid = COMPUTE_FULL;\n";
	else
		oscpp<<"int computeGrid = COMPUTE_SPARSE;\n";
		
	oscpp<<macroDefns;
	oscpp<<fnDeclarations;
	oscpp<<inputMethods.second;
	oscpp<<initDPTable.second;
	oscpp<<resultsCode.second;
	oscpp<<recFunctionDefns;
	oscpp<<topLevel.second;
	fbcpp.close();
}

pair<string, string> GenerateTopLevelCalls()
{
	string hdrDecl("void Unroll();\n");
	string topLevelCallWrapper("void Unroll()\n{\n");
	string topLevelFunctionCall = functions[0].GenerateTopLevelFunctionInvocation();
	topLevelCallWrapper += topLevelFunctionCall;
	topLevelCallWrapper += "}\n\n";

	pair<string,string> defdFnExecCode = GenerateDelayedFunctionExecution();
	topLevelCallWrapper += defdFnExecCode.second;
	hdrDecl += defdFnExecCode.first;
	return make_pair(hdrDecl,topLevelCallWrapper);
}

pair<string, string> GenerateDelayedFunctionExecution()
{
	string hdr(""),cpp("");
	hdr += "void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions);\n";
	
	cpp += "void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)\n{\n";
	cpp += "\tswitch(fn->functionName)\n\t{\n";
	for(unsigned int i=0;i<functions.size();i++)
	{
		string functionName(1,functions[i].getName());
		cpp += "\t\tcase '"+functionName+"':\n";
		cpp += "\t\t\t{\n";
		ostringstream cppStr;
		cppStr <<"\t\t\t\tassert(dataRegions.size() == "<<functions[i].inputFingerprint.size()<<");\n"; 
		cppStr <<"\t\t\t\tDeferredCall<_paramListFunction"<<functionName<<">* df = (reinterpret_cast<DeferredCall<_paramListFunction"<<functionName<<">* >(fn->fnCall));\n";
		for(unsigned int j=0;j<functions[i].inputFingerprint.size();j++)
			cppStr<<"\t\t\t\tstd::get<"<<(j*2+1)<<">(df->params) = dataRegions["<<j<<"];\n";
		cppStr<<"\t\t\t\t(reinterpret_cast<DeferredCall<_paramListFunction"<<functionName<<">* >(fn->fnCall))->Run();\n";
		cpp += cppStr.str();
		cpp += "\t\t\t}\n\t\t\tbreak;\n";
	}
	cpp +="\t\tdefault: break;\n";	
	cpp += "\t}\n}\n\n";

	return make_pair(hdr,cpp);
}

int GetNumArgs(char fnName)
{
	int numArgs=-1;
	auto it = functions.begin();
	for(;it!=functions.end();it++)
	{
		if(it->getName() == fnName)
		{
			numArgs  = it->inputFingerprint.size();		
			break;
		}
	}
	assert(numArgs > 0);
	return numArgs;
}

RecursionBase* GetRecursionBaseInstance(int problem)
{
	if(recBase)
		return recBase;
	RecursionBase* ret =nullptr;
	switch(problem)
	{
		case PROBLEMPARENTHESIS:
					{
						ret  = new Parenthesis();
					}
					break;
		case PROBLEM_4D_NUSSINOV:
					{
						ret = new Nussinov4D();
					}
					break;
		default:
			break;
	}
	assert(ret != nullptr);
	recBase = ret;
	return ret;
}

vector<char> Function::GenerateFunctionArgs()
{
	char start='X';
	vector<char> ret;
	ret.push_back(start);
	auto matrixNameIter = matrixNameMap.begin();
	for(;matrixNameIter!=matrixNameMap.end();matrixNameIter++)
	{
		if(matrixNameIter->second != 'X')
			ret.push_back(matrixNameIter->second);
	}
	if(ret.size() != inputFingerprint.size())
	{
		int numPendingArgs = inputFingerprint.size()-ret.size();
		char start = 'X';
		while(numPendingArgs)
		{
			if(start == 'Z')
				start = 'A';
			else
				start++; 
			ret.push_back(start);	
			numPendingArgs--;
		}
	}
	return ret;	
}

string Function::GenerateVarDefinitions(map<char, set<string> >& varsPendingDefinition)
{
	int dim = d;
	string declarations("");
	auto itArg = varsPendingDefinition.begin();
	for(;itArg!=varsPendingDefinition.end();itArg++)
	{
		auto itOrth = itArg->second.begin();
		for(;itOrth!=itArg->second.end();itOrth++)
		{
			string var = itArg->first + *itOrth;
			ostringstream box;
			box<<"Box "<<var<<"(";
			int orthant = ConvertStringToBinary(itOrth->c_str());//stoi(itOrth->c_str(),NULL,2); 
			vector<string> offsets(d);
			//preparing offsets for topleft coordinates
			for(dim=0;dim<d;dim++)
			{
				ostringstream side;
				side<<"("<<itArg->first<<"->coords["<<dim+d<<"]-"<<itArg->first<<"->coords["<<dim<<"]+1)/2";
				offsets[dim] = (orthant & 1)?side.str():""; 
				orthant >>= 1;
			}
			//setting topleft coordinates
			ostringstream topleft;
			for(dim=0;dim<d;dim++)
			{
				topleft<<itArg->first<<"->coords["<<dim<<"]";
				if(offsets[dim].compare(""))
					topleft<<"+"<<offsets[dim];
				topleft<<",";
			}
			//preparing offsets for bottomright coordinates
			for(dim=0;dim<d;dim++)
			{
				ostringstream side;
				side<<"("<<itArg->first<<"->coords["<<dim+d<<"]-"<<itArg->first<<"->coords["<<dim<<"]+1)/2";
				if(!(offsets[dim].compare("")))
					offsets[dim]="-"+side.str();
				else
					offsets[dim]="";
			}
			//setting bottomright coordinates
			ostringstream bottomright;
			for(dim=0;dim<d;dim++)
			{
				bottomright<<itArg->first<<"->coords["<<d+dim<<"]";
				if(offsets[dim].compare(""))
					bottomright<<offsets[dim];
				if(dim != d-1)
					bottomright<<",";
			}
				
			box<<topleft.str()<<bottomright.str()<<");";

			declarations += "\t"+box.str();
			declarations +="\n";
		}
	}
	return declarations;
}

string Function::GenerateTopLevelFunctionInvocation()
{
	ostringstream topLevelCallArg;
	topLevelCallArg<<"\tint parentTileIDX=0;\n\tBox *X = new Box(";
	for(unsigned int i=0;i<d;i++)
			topLevelCallArg<<0<<",";
	for(unsigned int i=0;i<d;i++)
	{
		topLevelCallArg<<"inputSize["<<i<<"]";
		if(i!=d-1)
			topLevelCallArg<<",";
		else
			topLevelCallArg<<");\n";
	}
	string topLevelCall = topLevelCallArg.str();

	vector<char> args = GenerateFunctionArgs();
	vector<string> argList;
	for(unsigned int i=0;i<args.size();i++)	
	{
		argList.push_back("X");
		argList.push_back("nullData");
	}
	argList.push_back("0");


	topLevelCall += "\t"+GenerateStopCondWhileUnfolding(argList);
	topLevelCall += "\t"+string(1, getName())+"_unroll(";
	for(unsigned int i=0;i<argList.size()-1;i+=2)
	{
		topLevelCall += argList[i];
		topLevelCall += ", 0, ";
	}
	topLevelCall +="0);\n\tdelete X;\n";
	return topLevelCall;
}

string Function::GenerateParamListMacro() const
{
	string paramList;
	paramList +="#define _paramListFunction"+string(1,getName())+" ";
	for(unsigned int j=0;j<inputFingerprint.size();j++)	
		paramList+="Box*, CELL_TYPE*, ";
	paramList+="int\n";
	return paramList;
}

string Function::GenerateStopCondWhileUnfolding(const vector<string>& fnArgs)
{
	string functionName = string(1,getName());
	string stopCond("");
	stopCond += "if("+fnArgs[fnArgs.size()-1]+" == recursionDepth)\n";
	stopCond+="\t{\n";

	ostringstream tmpStr;
	if(computeGrid != COMPUTE_SPARSE)
		tmpStr<<"\t\tint writeTileID = GetTileID(parentTileID"<<fnArgs[0]<<");\n";
	else
		tmpStr<<"\t\tint writeTileID = GetTileID("<<fnArgs[0]<<");\n";
		
	stopCond+=tmpStr.str();
	stopCond+="\t\tbool localUpdate = false;\n";
	stopCond+="\t\tFunctionCall* fnCall= nullptr;\n";
	stopCond+="\t\ttileUpdateLog[writeTileID]=fnCounter;\n";
	if(computeGrid != COMPUTE_SPARSE)
		stopCond+="\t\tif(IsLocalOwner(writeTileID))\n\t\t{\n";
	else
		stopCond+="\t\tif(true)\n\t\t{\n";
	stopCond+="\t\t\tCELL_TYPE* nullData=nullptr;\n";
	stopCond+="\t\t\tfnCall=new FunctionCall();\n";
	stopCond+="\t\t\tfnCall->functionName = '"+functionName+"';\n";
	stopCond+="\t\t\tParameter p;\n";

	string argList("");
	for(unsigned int j=0;j<fnArgs.size()-1;j+=2)	
	{
		ostringstream stopCondStr;
		stopCondStr<<"\t\t\tBox* b"<<j<<"=new Box(*"<<fnArgs[j]<<");\n"; 
		stopCondStr<<"\t\t\tp.data = b"<<j<<";\n";
		ostringstream tmpStr;
		tmpStr<<"b"<<j;
		argList += (tmpStr.str() +", nullData, ");
		stopCond += stopCondStr.str();
		stopCond +="\t\t\tfnCall->params.push_back(p);\n";
	}
	argList += fnArgs[fnArgs.size()-1]+"+1";

	stopCond +="\t\t\tstd::tuple<_paramListFunction"+functionName+"> t = std::make_tuple("+argList+");\n";
  	stopCond +="\t\t\tDeferredCall<_paramListFunction"+functionName+">* defdCall = new DeferredCall<_paramListFunction"+functionName+">();\n";
	stopCond +="\t\t\tdefdCall->params=t;\n\t\t\tdefdCall->fptr="+functionName+";\n";
	stopCond +="\t\t\tfnCall->fnCall = defdCall;\n";
	stopCond +="\t\t\tfnCall->ID = fnCounter;\n";
	stopCond +="\t\t\tif(fnCalls[writeTileID].size() > 0)\n\t\t\t{\n";
	stopCond +="\t\t\t\tfnCall->numInPorts +=1;\n";
	stopCond +="\t\t\t\tfnCall->wawSource = (fnCalls[writeTileID].back())->ID;\n"; 
	if(computeGrid != COMPUTE_SPARSE)
	{
		stopCond +="\t\t\t\tFunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();\n";
		stopCond +="\t\t\t\tlastFunctionToUpdate->outPortOwners.insert(procRank);\n";
	}
	stopCond +="\t\t\t}\n\t\t\tfnCalls[writeTileID].push_back(fnCall);\n";
	stopCond +="\t\t\tlocalUpdate = true;\n\t\t}\n";
	stopCond +="\t\tfnCounter++;\n";

	if((fnArgs.size()-1)/2 > 1)
	{	
		ostringstream tmpStr;
		tmpStr <<"\t\tint readTileIDs["<<((fnArgs.size()-1)/2) -1<<"];\n";
		for(int i=1;i<(fnArgs.size()-1)/2;i++)
		{
			if(computeGrid != COMPUTE_SPARSE)
				tmpStr <<"\t\treadTileIDs["<<i-1<<"]=GetTileID(parentTileID"<<fnArgs[2*i]<<");\n";	
			else
				tmpStr <<"\t\treadTileIDs["<<i-1<<"]=GetTileID("<<fnArgs[2*i]<<");\n";	
		}
		tmpStr<<"\t\tfor(int i=0;i<"<<((fnArgs.size()-1)/2)-1<<";i++)\n\t\t{\n";
		tmpStr<<"\t\t\tint readTileID=readTileIDs[i];\n";
		tmpStr<<"\t\t\tif(readTileID != writeTileID)\n\t\t\t{\n";
		tmpStr<<"#ifdef TASK_AGGREGATION\n";
		tmpStr<<"\t\t\t\tif(fnCalls[readTileID].size() > 0)\n";	
		tmpStr<<"\t\t\t\t\t(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;\n";
		tmpStr<<"#endif\n";			
		tmpStr<<"\t\t\t\tif(localUpdate)\n\t\t\t\t{\n";
		tmpStr<<"\t\t\t\t\tfnCall->numInPorts +=1;\n";
		tmpStr<<"\t\t\t\t\tfnCall->params[i+1].portID = tileUpdateLog[readTileID];\n\t\t\t\t}\n";
		if(computeGrid != COMPUTE_SPARSE)
		{
			tmpStr<<"\t\t\t\tif(fnCalls[readTileID].size() > 0)\n\t\t\t\t{\n";
			tmpStr<<"\t\t\t\t\tFunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();\n";
			tmpStr<<"\t\t\t\t\tlastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));\n\t\t\t\t}\n";
		}
		tmpStr<<"\t\t\t}\n";
		tmpStr<<"\t\t}\n";
		stopCond += tmpStr.str();
	}
	
	stopCond +="\t\treturn;\n\t}\n";

	return stopCond;

}

string Function::GenerateFunctionBodyForUnroll(RecursionBase* base)
{
	int K = 1<<d; //K is the number of children/orthants created on a single-level decomposition of the DP table. e.g. K=4 for 2D table. K=8 for 3D etc.
	//function signature
	ostringstream sig;
	sig<<"void "<<getName()<<"_unroll(";
	vector<char> args = GenerateFunctionArgs();
	vector<string> argList;
	for(unsigned int j=0;j<args.size();j++)	
	{
		sig<<"Box* "<<args[j]<<", int parentTileID"<<args[j]<<", ";
		argList.push_back(string(1,args[j]));
		argList.push_back("parentTileID"+string(1,args[j]));
	}
	sig<<"int callStackDepth)";
	argList.push_back("callStackDepth");
	signature = sig.str();
	
	
	string stopCond("");
	string unfoldingStopCond = "\t"+GenerateStopCondWhileUnfolding(argList);
	stopCond += unfoldingStopCond;
	//function body
	map<char, set<string> > varsPendingDefinition;
	string ret("\n");
	for(unsigned int i=0;i<calls.size();i++)
	{
		bool parallelFlag = false;
		for(unsigned int j=0;j<calls[i].size();j++)
		{
			if(calls[i].size() > 1)
				parallelFlag=true;
			Fingerprint fingerprint = outputFingerprint[i][j];
			char tempCall[3]={0,0,0};
			sprintf(tempCall,"%c_unroll(",fingerprint.getName());
			ret += "\t"+string(tempCall);
			int numArgs = GetNumArgs(fingerprint.getName());

			set<string> uniqArgs;
			for(unsigned int m=0;m<calls[i][j].size();m++)
			{
				{
					Coordinate c = calls[i][j][m].offsets;
					vector<char> mmData = GetMatrixMapData(c,matrixNameMap);
					for(int k=0;k<(s+1);k++)
					{
						ostringstream  tempArg;
						for(int l=0;l<d;l++)
							tempArg<<calls[i][j][m][k*d+l];
						
						string matName(1,mmData[k]);
						string var="&"+matName+tempArg.str();
						ostringstream  childZMortonCode;
						childZMortonCode<<"parentTileID"<<matName<<"*"<<K<<"+"<<ConvertStringToBinary(tempArg.str().c_str());
						string varData = childZMortonCode.str();
						if((calls[i][j].size() == 1) || ((calls[i][j].size() > 1) && (uniqArgs.find(var) == uniqArgs.end())))
						{
							uniqArgs.insert(var);
							set<string> tmpArgStr = {tempArg.str()};
							auto status = varsPendingDefinition.insert(make_pair(mmData[k],tmpArgStr));
							if(!status.second)
								((status.first)->second).insert(tempArg.str());
							ret += var;
							ret += ", "+varData;
							if(calls[i][j].size() == 1)
							{
								if(k != s)
									ret += ", ";
							}
							else
							{
								if(uniqArgs.size() != numArgs)
									ret += ", ";
							}
						}
					}
				}
			}
			ret += ", callStackDepth+1);";
		}
		ret += "\n";
	}
	ret+="\treturn;\n";

	//variable declarations corresponding to the variables used in function body.
	string declarations = GenerateVarDefinitions(varsPendingDefinition);
	return signature+"\n{\n"+stopCond+"\n"+declarations+ret+"}\n\n";
}

string Function::GenerateFunctionBody(RecursionBase* base)
{
	//function signature
	ostringstream sig;
	sig<<"void "<<getName()<<"(";
	vector<char> args = GenerateFunctionArgs();
	vector<string> argList;
	for(unsigned int j=0;j<args.size();j++)	
	{
		sig<<"Box* "<<args[j]<<", CELL_TYPE* "<<args[j]<<"Data, ";
		argList.push_back(string(1,args[j]));
		argList.push_back(string(1,args[j])+"Data");
	}
	sig<<"int callStackDepth)";
	argList.push_back("callStackDepth");
	signature = sig.str();
	
	
	string stopCond = base->StopCondition(argList);
	//function body
	map<char, set<string> > varsPendingDefinition;
	string ret("\n");
	for(unsigned int i=0;i<calls.size();i++)
	{
		bool parallelFlag = false;
		for(unsigned int j=0;j<calls[i].size();j++)
		{
			if(calls[i].size() > 1)
				parallelFlag=true;
			Fingerprint fingerprint = outputFingerprint[i][j];
			char tempCall[3]={0,0,0};
			sprintf(tempCall,"%c(",fingerprint.getName());
			if(parallelFlag && (j!=calls[i].size()-1))
				ret += "\n#ifdef PARALLEL\n\tcilk_spawn\n#endif\n";
			ret += "\t"+string(tempCall);
			int numArgs = GetNumArgs(fingerprint.getName());

			set<string> uniqArgs;
			for(unsigned int m=0;m<calls[i][j].size();m++)
			{
				{
					Coordinate c = calls[i][j][m].offsets;
					vector<char> mmData = GetMatrixMapData(c,matrixNameMap);
					for(int k=0;k<(s+1);k++)
					{
						ostringstream  tempArg;
						for(int l=0;l<d;l++)
							tempArg<<calls[i][j][m][k*d+l];
						
						string matName(1,mmData[k]);
						string var="&"+matName+tempArg.str();
						string varData = matName+"Data";
						if((calls[i][j].size() == 1) || ((calls[i][j].size() > 1) && (uniqArgs.find(var) == uniqArgs.end())))
						{
							uniqArgs.insert(var);
							set<string> tmpArgStr = {tempArg.str()};
							auto status = varsPendingDefinition.insert(make_pair(mmData[k],tmpArgStr));
							if(!status.second)
								((status.first)->second).insert(tempArg.str());
							ret += var;
							ret += ", "+varData;
							if(calls[i][j].size() == 1)
							{
								if(k != s)
									ret += ", ";
							}
							else
							{
								if(uniqArgs.size() != numArgs)
									ret += ", ";
							}
						}
					}
				}
			}
			ret += ", callStackDepth+1);";
			if(parallelFlag && (j==calls[i].size()-1))
				ret +="\n#ifdef PARALLEL\n\tcilk_sync;\n#endif\n";
		}
		ret += "\n";
	}
	ret+="\treturn;\n";

	if(getName() == 'A') 
	{
		if(((varsPendingDefinition['X'].size() == (1<<d)-1)||(varsPendingDefinition['x'].size() == (1<<d)-1)))
			computeGrid = COMPUTE_UTM;
		else if((varsPendingDefinition['X'].size() == (1<<d))||(varsPendingDefinition['x'].size() == (1<<d)))
			computeGrid = COMPUTE_FULL;
		else
			computeGrid = COMPUTE_SPARSE;
		
	}
	//variable declarations corresponding to the variables used in function body.
	string declarations = GenerateVarDefinitions(varsPendingDefinition);
	string serialCode = GenerateSerialCodeInFunctionBody(ret);
	//printf("%s\n",(signature+stopCond+declarations+ret).c_str());
	return signature+"\n{\n"+stopCond+"\n"+declarations+serialCode+ret+"}\n\n";
}


Fingerprint Function::computeInputFingerprint(vector<Coordinate> coords)
{
	Fingerprint inputFingerprint;
	if(coords.size() > 1){
		// if combined (set all to be 1)
		for(int i=0;i<(s+1);i++){
			inputFingerprint[i] = 1;
		}
		return inputFingerprint;
	}
	inputFingerprint[0] = 0;
	inputFingerprint.matrixNames[0] = 'X';

	int max = 0;
	char currentMatrix = 'T';

	for(int i=1;i<(s+1);i++)
	{
		bool found = false;
		for(int j=0;j<i;j++){
			if(coords[0].compareCoordinates(coords[0],i,j) == 0){
				inputFingerprint[i] = inputFingerprint[j];
				inputFingerprint.matrixNames[i] = inputFingerprint.matrixNames[j];
				found = true;
				break;
			}
		}
		if(!found){
			max++;
			currentMatrix++;
			inputFingerprint[i] = max;
			inputFingerprint.matrixNames[i] = currentMatrix;
		}
	}
	return inputFingerprint;
}

void Function::print()
{
	printf("%c_%s  ",inputFingerprint.getName(),problemName.c_str());
	if(offsets.size() > 1){
		SetMatrixMap(offsets, matrixNameMap);
		for(unsigned int i=0;i<offsets.size();i++){
			printf("<");
			vector<char> mmData = GetMatrixMapData(offsets[i],matrixNameMap);
			for(int k=0;k<(s+1);k++){
				printf("%c",mmData[k]);
				if(k != (s+1)-1){
					printf(",");
				}
			}
			printf(">");
		}
	} else {
		SetMatrixMap(offsets, matrixNameMap);
		printf("<");
		for(int j=0;j<inputFingerprint.size();j++){
			printf("%c",inputFingerprint.matrixNames[j]);
			if(j!=(s+1)-1){
				printf(",");
			}
		}
		printf(">");
	}
	printf("\n");
	for(unsigned int i=0;i<calls.size();i++){
		if(calls[i].size() > 1){
			printf("Parallel: ");
		}
		
		for(unsigned int j=0;j<calls[i].size();j++){
			Fingerprint fingerprint = outputFingerprint[i][j];
			printf("%c(",fingerprint.getName());
			for(int m=0;m<calls[i][j].size();m++){
				Coordinate c = calls[i][j][m].offsets;
				vector<char> mmData = GetMatrixMapData(c,matrixNameMap);
				printf("<");
				for(int k=0;k<(s+1);k++){
					printf("%c",mmData[k]);
					for(int l=0;l<d;l++){
						printf("%d",calls[i][j][m][k*d+l]);//+1);
					}
					if(k != (s+1)-1){
						printf(" ");
					}
				}
				printf(">");
			}
			printf(")  ");
		}
		printf("\n");
	}
	printf("\n");
}

void Function::SetMatrixMap(vector<Coordinate>& offsets, map<vector<int>,char>& matrixMap)
{
	char curMatrixName='T';
	for(unsigned int h=0;h<offsets.size();h++)
	{
		for(int i=0;i<(s+1);i++)
		{
			vector<int> regionTuple;
			for(int k=0;k<d;k++)
			{
				regionTuple.push_back(offsets[h].arr[i*d+k]+offsets[h].offsets[i*d+k]);
			}
			if(i == 0)
				matrixMap.insert(make_pair(regionTuple,'X'));
			else
			{
				if(matrixMap.find(regionTuple) == matrixMap.end())
				{
					curMatrixName++;
					matrixMap.insert(make_pair(regionTuple,curMatrixName));
				}
			}
		}
	}
}

vector<char> Function::GetMatrixMapData(Coordinate& c, map<vector<int>,char>& matrixMap)
{
	vector<char> ret;
	for(int i=0;i<(s+1);i++)
	{
		vector<int> regionTuple;
		for(int k=0;k<d;k++)
			regionTuple.push_back(c.arr[i*d+k]+c.offsets[i*d+k]);
		assert(matrixMap.find(regionTuple) != matrixMap.end());
		ret.push_back(matrixMap[regionTuple]);
	}
	return ret;
}

string FetchCellFnCallCode()
{
	ostringstream ret("");
	vector<string> argList;
	char startVar = 'i';
	for (int i=0;i<d;i++)
	{
		argList.push_back(string("int ")+string(1,startVar)+string(", "));
		startVar++;
	}
	argList.push_back(string("CELL_TYPE* data"));
	
	string sig("inline CELL_TYPE* GetDPTableCell(");
	for(int i=0;i<argList.size();i++)
		sig+=argList[i];
	sig +=string(")");
	
	ret <<sig<<"\n";
	ret << "{\n";
	ret << "\tCELL_TYPE* cell = data+METADATASPACE;\n";
	ret << "\tint side = data[DIMENSION];\n";

	startVar='i';
	string lastVarName;
	for(int i=0;i<d;i++)
	{
		string varName;
		varName = string(1,startVar)+string("Offset");
		lastVarName = varName;
		ret << "\tint "<<varName <<"="<< startVar <<" - data["<<d-1-i<<"];\n";
		if(i!=d-1)
		{
			ret <<"\tcell += ("<<varName<<"*"<<d-1-i<<"* side);\n"; 
		}
		startVar++;
	}
	ret <<"\tcell += "<<lastVarName<<";\n";
	ret <<"\treturn cell;\n}\n\n"; 
	return ret.str();
}

int ConvertStringToBinary(const char* str)
{
	int len=strlen(str);
	int i=len-1, num=0, bit_position=0;
	do
	{
		num += ((1<<bit_position) * (str[i]-'0'));
		bit_position++;
		i--;
	}while(i>=0);
	return num;
}

string GenerateSerialCodeInFunctionBody(string fnBody)
{
	string serialCode("");
	std::string queryStr1("#ifdef PARALLEL\n\tcilk_spawn\n#endif\n");
	std::string queryStr2("#ifdef PARALLEL\n\tcilk_sync;\n#endif\n");
	int queryStr1Len = queryStr1.size();
	int queryStr2Len = queryStr2.size();
	bool parallelTasksFound=false;
	while(true)
	{
		size_t res1 = fnBody.find(queryStr1,0);
		if(res1==std::string::npos)
			break;
		else
			fnBody.erase(res1,queryStr1Len);
		parallelTasksFound = true;
	}
	
	while(true)
	{
		size_t res2 = fnBody.find(queryStr2,0);
		if(res2==std::string::npos)
			break;
		else
			fnBody.erase(res2,queryStr2Len);
	}

	if(parallelTasksFound)
	{
		serialCode = string("\n\tif(callStackDepth > recursionDepth+4)\n\t{\n");
		serialCode += fnBody;
		serialCode += string("\n\t}\n");
	}
	return serialCode;
}

pair<string, string> GenerateResultsCode(RecursionBase* base)
{
	string hdrDecl(""), cppCode("");
	string sig("void PrintResults()");
	hdrDecl +=sig+";\n";
	cppCode += sig+"\n{\n";
	cppCode += base->PrintResults();
	cppCode += "}\n\n";
	return make_pair(hdrDecl,cppCode);	
}

#if 0
string Parenthesis::StopCondition(vector<string>& args)
{
	/*ostringstream stopCond;
	stopCond<<"\tif(("<<args[0]<<"->coords[0]=="<<args[0]<<"->coords[2]) && ("<<args[0]<<"->coords[1]=="<<args[0]<<"->coords[3]))\n\t{\n";
	stopCond<<"\t\tint i="<<args[0]<<"->coords[1];\n\t\tint j="<<args[0]<<"->coords[0];\n\t\tint k="<<args[1]<<"->coords[0];\n\t\tif(i == j)\n\t\t\treturn;\n";	

	stopCond<<"\t\tint side="<<args[0]<<"Data[2];\n\t\tint w_ikj=matrixChain[i-1]*matrixChain[k]*matrixChain[j];\n";
	stopCond<<"\t\tint* cost_ij = GetDPTableTile(i,j,"<<args[0]<<"Data, side);\n\t\tint* cost_ik = GetDPTableTile(i,k,"<<args[1]<<"Data,side);\n\t\tint* cost_kj = GetDPTableTile(k,j,"<<args[2]<<"Data, side);\n\t\tint newCost = *cost_ik + *cost_kj + w_ikj;\n"; 
	stopCond<<"\t\tif(newCost < *cost_ij)\n\t\t\t*cost_ij =  newCost;\n\t\treturn;\n\t}\n";
	return stopCond.str(); */
	
	string stopCond("");
	stopCond+="\tif(("+args[0]+"->coords[0]=="+args[0]+"->coords[2]) && ("+args[0]+"->coords[1]=="+args[0]+"->coords[3]))\n\t{\n";
	stopCond+="\t\tint i="+args[0]+"->coords[1];\n\t\tint j="+args[0]+"->coords[0];\n\t\tint k="+args[2]+"->coords[0];\n\t\tif(i == j)\n\t\t\treturn;\n";	

	stopCond+="\t\tint side="+args[0]+"Data[2];\n\t\tint w_ikj=matrixChain[i-1]*matrixChain[k]*matrixChain[j];\n";
	stopCond+="\t\tint* cost_ij = GetDPTableTile(i,j,"+args[0]+"Data);\n\t\tint* cost_ik = GetDPTableTile(i,k,"+args[2]+"Data);\n\t\tint* cost_kj = GetDPTableTile(k,j,"+args[4]+"Data);\n\t\tint newCost = *cost_ik + *cost_kj + w_ikj;\n"; 
	stopCond+="\t\tif(newCost < *cost_ij)\n\t\t\t*cost_ij =  newCost;\n\t\treturn;\n\t}\n";
	return stopCond;
}

pair<string,string> Parenthesis::ReadInput()
{
	string hdrDeclarations("");
	string sig ="void ReadInput(int argc, char* argv[])"; //this is not added to header file.

	ostringstream stopCond;
	stopCond<<"vector<int> matrixChain;\n\n";
	//definition of ReadInput, which calls _ReadInput
	stopCond<<sig<<"\n";
	stopCond<<"{\n\tif(argc != 2)\n\t{\n\t\tprintf(\"Usage: ./matchain <input>\\n\");\n\t\texit(0);\n\t}\n";
	stopCond<<"\t_ReadInput(argv[1],matrixChain);\n";
	stopCond<<"\tinputSize[0] = matrixChain.size()-1;\n";
	stopCond<<"\tinputSize[1] = matrixChain.size()-1;\n}\n\n";
	
	//definition of _ReadInput
	sig = "void _ReadInput(char* fileName, vector<int>& matrixChain)";
	hdrDeclarations +=sig+";\n";
	stopCond<<sig<<"\n{\n\tchar matrixDimStr[8];\n\tint matrixDim;\n\tifstream inFile(fileName, ifstream::in);\n"; 
	stopCond<<"\tif(!fileName || !inFile.is_open())\n\t{\n\t\tprintf(\"ERROR: Unable to open input file\\n\");\n\t\texit(0);\n\t}\n";
	stopCond<<"\twhile(!inFile.eof())\n\t{\n\t\tinFile.getline(matrixDimStr,8);\n\t\tif(inFile.eof())\n\t\t\tbreak;\n";
	stopCond<<"\t\tmatrixDim = atoi(matrixDimStr);\n\t\tmemset(matrixDimStr,0,8);\n\t\tmatrixChain.push_back(matrixDim);\n\t}\n}\n\n";

	return std::make_pair(hdrDeclarations,stopCond.str());
}

pair<string,string> Parenthesis::InitializeDPTable()
{
	string hdrDecl("");
	string sig ="void InitializeDPTable(CELL_TYPE* table, Box* b)"; 
	hdrDecl += sig+";\n";

	ostringstream cppDefn;
	//definition of ReadInput, which calls _ReadInput
	cppDefn<<sig<<"\n{\n\tunsigned int relIndx = 0;\n\tfor(unsigned int i=b->coords[1];i<=b->coords[3];i++)\n";
	cppDefn<<"\t\tfor(unsigned int j=b->coords[0];j<=b->coords[2];j++)\n";
	cppDefn<<"\t\t\ttable[relIndx++] = (i==j)?0:INT_MAX;\n";
	cppDefn<<"}\n\n";
	return std::make_pair(hdrDecl,cppDefn.str());
}

string Parenthesis::PrintResults()
{
	string res("");
	res += "\tCELL_TYPE* result = GetDPTableCell(0,inputSize[0]);\n";
	res += "\tif(result!=nullptr)\n\t\tstd::cout<<\"Minimum cost of triangulation:\"<<*result<<std::endl;\n";
	return res;
}
#endif
