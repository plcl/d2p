#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include<map>
#include<set>

using namespace std;

#define N 4
#define r 2

extern string problemName;

extern int d;
extern int s;

#define MAX_D 4
#define MAX_S 3

typedef struct Box
{
	int  coords[MAX_D*2]; //topleft and bottom right (x and y coordinates)
	Box(int a, int b, int c, int d){coords[0]=a;coords[1]=b;coords[2]=c;coords[3]=d;}
	Box(){}
	void PrintStr(char *str)const {sprintf(str,"%d %d %d %d",coords[1],coords[0],coords[3],coords[2]);}
	Box& operator=(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
		return *this;
	}
	Box(const Box& rhs)
	{
		this->coords[0]=rhs.coords[0];
		this->coords[1]=rhs.coords[1];
		this->coords[2]=rhs.coords[2];
		this->coords[3]=rhs.coords[3];
	}
	bool operator==(const Box& rhs)
	{
		bool flag = true;
		if((this->coords[0]!=rhs.coords[0]) ||(this->coords[1]!=rhs.coords[1]) || (this->coords[2]!=rhs.coords[2]) || (this->coords[3]!=rhs.coords[3]))
			flag = false;
		return flag;
	}
}Box;

// this represents the (s+1)*d coordinates which represent the call
struct Coordinate{
	int length = (s+1)*d;
	int arr[MAX_D*(MAX_S+1)];
	int offsets[MAX_D*(MAX_S+1)];

	int &operator[](int i){
		if(i > length){
			// return first element.
			return arr[0];
		}
		return arr[i];
	}
	int size(){
		return length;
	}
	// compares two coordinates this[i] == c[j] where i and j represent the xth position
	int compareCoordinates(Coordinate c, int i, int j) const {
		for(int k=0;k<d;k++){
			if(arr[i*d+k]+offsets[i*d+k] != c[j*d+k]+c.offsets[j*d+k]){
				if(arr[i*d+k]+offsets[i*d+k] > c[j*d+k]+c.offsets[j*d+k]){
					return 1;
				} else {
					return -1;
				}
			}
		}
		return 0;
	}
	void print() const{
		printf("{");
		for(int i=0;i<(s+1);i++){
			printf("(");
			for(int j=0;j<d;j++){
				printf("%d",arr[i*d+j]);
				if(j!=d-1){
					printf(", ");
				}
			}
			printf(")");
		}
		printf("}\n");
	}
	void print2() const{
		printf("{");
		for(int i=0;i<(s+1);i++){
			printf("(");
			for(int j=0;j<d;j++){
				printf("%d",arr[i*d+j]+offsets[i*d+j]);
				if(j!=d-1){
					printf(", ");
				}
			}
			printf(")");
		}
		printf("}\n");
	}
	bool equals(const Coordinate& c) const{
		for(int i=0;i<(s+1);i++){
			if(compareCoordinates(c,i,i) != 0){
				return false;
			}
		}
		return true;
	}
	bool operator == (const Coordinate& c) const {
		return equals(c);
    }
	bool operator < (const Coordinate& c) const {
		for(int i=0;i<(s+1);i++){
			if(compareCoordinates(c,i,i) == -1){
				return true;
			}
		}
		return false;
    }
	Coordinate(int coords[]){
		for(int i=0;i<length;i++){
			arr[i] = coords[i];
			offsets[i] = 0;
		}
	}
	Coordinate(){};
};

// basically just a wrapper for a vector of coordinates, but I may change some things about it later, so I made it a struct
struct Call{
	vector<Coordinate> coords;

	vector<Coordinate> getCoords(){
		return coords;
	}
	void sortCoords(){
		sort(coords.begin(),coords.end());
	}
	int size(){
		return coords.size();
	}
	void push_back(Coordinate c){
		coords.push_back(c);
	}
	Coordinate &operator[](unsigned int i){
		if(i > coords.size()){
			// return first element.
			return coords[0];
		}
		return coords[i];
	}
	bool operator < (Call &c) {
		if(size() != c.size()){
			return size() < c.size();
		}
		for(int i=0;i<size();i++){
			if(!coords[i].equals(c.coords[i])){
				return coords[i] < c.coords[i];
			}
		}
		return false;
    }
	void print()
	{
		for(unsigned int i=0;i<coords.size();i++)
			coords[i].print2();	
	}
};

// a tree node used for sorting the calls
struct TreeNode{
	Call call;
	vector<TreeNode *> children;
	int parents;
	int depth;
	int id;
	bool visited;
	TreeNode(Call _call,int _id){
		call = _call;
		id = _id;
		parents = 0;
		depth = -1;
	}	
	bool operator < (const TreeNode& t) const {
		if(depth != t.depth){
			return depth < t.depth;
		}
		return id < t.id;
    }
	bool equals(const TreeNode& t) const{
		if(id == t.id){
			return true;
		}
		return false;
	}
};

struct Fingerprint{
	int length = (s+1);
	int arr[(MAX_S+1)];
	char matrixNames[(MAX_S+1)];

	char name;
	void setName(char c){
		name = c;
	}
	char getName() const{
		return name;
	}
	int &operator[](int i){
		if(i > length){
			// return first element.
			return arr[0];
		}
		return arr[i];
	}
	int size() const{
		return length;
	}
	bool equals(const Fingerprint& f) const{
		for(int i=0;i<(s+1);i++){
			if(arr[i] != f.arr[i]){
				return false;
			}
		}
		return true;
	}
	char GetLastReadMatrixName()
	{
		char max='T';
		for(int i=0;i<(s+1);i++)
		{
			if((matrixNames[i] > max) && (matrixNames[i]!='X'))
				max=matrixNames[i];
		}
		return max;
	}
};

class RecursionBase
{
	public:
	virtual pair<string,string> ReadInput()=0;
	virtual pair<string,string> InitializeDPTable()=0;
	virtual string StopCondition(vector<string>& paramNames)=0;
	virtual string PrintResults()=0;
};


// a struct which represents a function call. contains the input and output fingerprint and the list of call
struct Function {
	Fingerprint inputFingerprint;
	vector<vector<Fingerprint>> outputFingerprint;
	vector<vector<Call>> calls;
	vector<Coordinate> offsets;
	map<vector<int>, char> matrixNameMap;
	string signature;

	Fingerprint computeInputFingerprint(vector<Coordinate> coords);
	void print();
	vector<char> GetMatrixMapData(Coordinate& c, map<vector<int>,char>& matrixMap);
	void SetMatrixMap(vector<Coordinate>& offsets, map<vector<int>,char>& matrixMap);
	vector<char> GenerateFunctionArgs();
	string GenerateFunctionBody(RecursionBase* base);
	string GenerateFunctionBodyForUnroll(RecursionBase* base);
	string GenerateTopLevelFunctionInvocation();
	string GenerateVarDefinitions(map<char, set<string> >& varsPendingDefinition);
	string GenerateStopCondWhileUnfolding(const vector<string>& fnArgList);
	string GenerateParamListMacro() const;

	void setName(char c){
		inputFingerprint.setName(c);
	}
	char getName() const{
		return inputFingerprint.getName();
	}

	

			bool equals(Function& f){
		for(int i=0;i<(s+1);i++){
			if(inputFingerprint[i] != f.inputFingerprint[i]){
				return false;
			}
		}
		for(unsigned int i=0;i<calls.size();i++){
			for(unsigned int j=0;j<calls[i].size();j++){
				for(int k=0;k<calls[i][j].size();k++){
					for(int l=0;l<d*(s+1);l++){
						if(calls[i][j][k][l] != f.calls[i][j][k][l]){
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	Function(vector<vector<Call>> callList, vector<Coordinate> initialOffsets){
		calls = callList;
		offsets = initialOffsets;
		inputFingerprint = computeInputFingerprint(initialOffsets);
		for(unsigned int i=0;i<calls.size();i++){
			vector<Fingerprint> temp;
			for(unsigned int j=0;j<calls[i].size();j++){
				temp.push_back(computeInputFingerprint(calls[i][j].coords));
			}
			outputFingerprint.push_back(temp);
		}
	}
	bool operator < (const Function& f) const {
		return inputFingerprint.getName() < f.inputFingerprint.getName();
    }

};

#if 0
class Parenthesis:public RecursionBase
{
	pair<string,string> ReadInput();
	pair<string,string> InitializeDPTable();
	string StopCondition(vector<string>& paramNames);
	string PrintResults();
};
#endif	

extern RecursionBase* recBase;

enum ComputeGrid{
COMPUTE_FULL,
COMPUTE_UTM,
COMPUTE_SPARSE
};

enum Problems { PROBLEMPARENTHESIS,
				PROBLEMGAUSSIANELIMINATION,
				PROBLEM_4D_NUSSINOV,
				PROBLEMSMITHWATERMAN};



void N4_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets);

void PAR_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets);
void SW_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets);
void GE_checkAll(vector<Coordinate> &coordList, Coordinate &initialOffsets);

pair<string,string> CreateFunctionCallHierarchySummary();
void GenerateFunctionDefs();
pair<string, string> GenerateTopLevelCalls();
pair<string, string> GenerateDelayedFunctionExecution();
int GetNumArgs(char fnName);
RecursionBase* GetRecursionBaseInstance(int problem);
string FetchCellFnCallCode();
int ConvertStringToBinary(const char* str);
string GenerateSerialCodeInFunctionBody(string fnBody);
pair<string, string> GenerateResultsCode(RecursionBase* base);

