#include "Nussinov4D.h"
#define STR(s) #s

pair<string,string> Nussinov4D::ReadInput()
{
	string hdrDeclarations("");
	//defining the type of DP table cell
	hdrDeclarations += "typedef int CELL_TYPE;\n";
	
	//defining ReadFASTAFile, which is called by ReadInput
	string sigFasta(STR(int ReadFASTAFile(FILE *fp, int maxLen, char** seq)));
	string cppCode("#define MAX_STR_LEN 45000\nchar* input_i=NULL;\n\n");
	cppCode += sigFasta +"\n{\n";
	cppCode +="\tchar line[1024];\n\tint seqLength = 0, seqLenToProcess;\n\tchar* localSeq=NULL;\n";
	cppCode +="\tfgets(line, sizeof(line), fp);\n\tif(line == NULL)\n\t\treturn -1;\n"; 
	cppCode +="\twhile (fgets(line, sizeof(line), fp) != NULL)\n\t{\n\t\tif(strlen(line)+seqLength > maxLen)\n\t\t\tseqLenToProcess = maxLen-seqLength;\n\t\telse\n\t\t\tseqLenToProcess = strlen(line);\n";
	cppCode +="\t\tlocalSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);\n\t\tif(!localSeq)\n\t\t{\n\t\t\tprintf(\"ERROR in realloc.\\n\");\n\t\t\texit(0);\n\t\t}\n";
	cppCode +="\t\tfor(unsigned int i = 0; i < seqLenToProcess; ++i)\n\t\t{\n\t\t\tif (line[i] == '*' || line[i] == '-')\n\t\t\t\tlocalSeq[seqLength++] = line[i];\n\t\t\telse if(isalpha(line[i]))\n\t\t\t\tlocalSeq[seqLength++] = toupper(line[i]);\n";
	cppCode +="\n\t\t}\n\t\tif(seqLength>=maxLen) break;\n\t}\n";
	cppCode +="\tlocalSeq[seqLength] = '\\0';\n\t*seq = localSeq;\n\treturn seqLength;\n}\n\n";

	//defining ReadInput
	string sig(STR(int ReadInput(int argc, char** argv)));
	cppCode += sig +"\n{\n";
	cppCode +="\tif(strcmp(argv[1],\"-h\")==0) \n\t{\n\t\tprintf(\"Usage: ./<exec> <input_string_i> <input_length_i> \\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\tFILE* fpi=fopen(argv[1],\"r\");\n\tif(!fpi)\n\t{\n\t\tprintf(\"ERROR: Unable to open input file.\\n\");\n\t\texit(0);\n\t}\n";
	cppCode += "\tint inputLength_I=atoi(argv[2]);\n";
	cppCode +="\tint ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);\n\tfclose(fpi);\n";
	cppCode +="\tif((ret1<0)||(input_i==NULL))\n\t{\n\t\tprintf(\"ERROR: Unable to read input files correctly. file1 len:%d\\n\",ret1);\n\t\texit(0);\n\t}\n";
	cppCode +="\telse\n\t{\n\t\tfor(int i=0;i<DIMENSION;i++) inputSize[i]=ret1-1;\n\t}\n\treturn inputSize[0]+1;\n}\n\n";
	return std::make_pair(hdrDeclarations,cppCode);
}


pair<string,string> Nussinov4D::InitializeDPTable()
{
	string hdrDecl("");
	string sig ="void InitializeDPTable(Box* b, CELL_TYPE* data)"; 
	hdrDecl += sig+";\n";
	string cppCode = sig+"\n{\n";
	cppCode +="\tint numCells = b->GetBoxSize();\n";
	cppCode +="\tmemset(data,0,sizeof(CELL_TYPE)*(numCells));\n\treturn;\n";
	cppCode +="}\n\n";
	return std::make_pair(hdrDecl,cppCode);
}

string Nussinov4D::StopCondition(vector<string>& args)
{
	string stopCond="\tif("+args[0]+"->coords[0]=="+args[0]+"->coords[4])\n\t{\n";
	stopCond +="\t\tint i=X->coords[DIMENSION-1], j=X->coords[DIMENSION-2], ii=X->coords[DIMENSION-3], jj=X->coords[DIMENSION-4];\n";
	stopCond +="\t\tint k=U->coords[DIMENSION-2], kk=U->coords[DIMENSION-4];\n";
	stopCond +="\t\tchar xstr[1024], ystr[1024], zstr[1024];\n";
	stopCond +="\t\tmemset(xstr,0,1024);memset(ystr,0,1024);memset(zstr,0,1024);\n";
	stopCond +="\t\tX->PrintStr(xstr);U->PrintStr(ystr);V->PrintStr(zstr);\n";
	stopCond +="\t\tstring xs(xstr), ys(ystr), zs(zstr);string combined = xs+ys+zs;\n";
	stopCond +="\t\treturn;\n\t}\n";
	return stopCond;

}

string Nussinov4D::PrintResults()
{
	return string("\tPrintGlobalMax();\n");	
}
