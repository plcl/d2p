README.txt

-------------------------------------------------------------------
Contents:
1. Introduction
2. How to run?

-------------------------------------------------------------------
1. Introduction
Autogen is an algorithm (or program) that takes an iterative description of a dynamic programming recurrence as input and automatically discovers and outputs a divide-and-conquer dynamic programming algorithm (or pseudocode).

-------------------------------------------------------------------
2. How to run?
Compilation : g++ -std=c++11 autogen.cpp -o autogen
Execution   : ./autogen [options]
Output      : autogen.txt 

Options:
-p problem     : name of the problem

Example execution:
./autogen -p 0  for minimum weight triangulation
./autogen -p 2  for Nussinov4D 
