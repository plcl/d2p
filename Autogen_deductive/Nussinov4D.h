#pragma once
#include "autogen_deductive_n4.h"
class Nussinov4D:public RecursionBase
{
	public:
	pair<string,string> ReadInput();
	pair<string,string> InitializeDPTable();
	string StopCondition(vector<string>& paramNames);
	string PrintResults();
};

