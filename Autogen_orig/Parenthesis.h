#pragma once
#include"autogen_orig.h"
class Parenthesis:public RecursionBase
{
	public:
	pair<string,string> ReadInput();
	pair<string,string> InitializeDPTable();
	string StopCondition(vector<string>& paramNames);
	string PrintResults();
};

