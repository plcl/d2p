#include "FloydWarshall.h"
#define STR(s) #s

pair<string,string> FloydWarshall::ReadInput()
{
	string hdrDeclarations("");
	//defining the type of DP table cell
	hdrDeclarations += "typedef int CELL_TYPE;\n";

	string cppCode("#define MAX_NUM_VERTICES 45000\n#define INF 65535\nAdjacency_List graphData;\nvector<int> vertexIDList;\n");
	
	cppCode +="vector<string> Split(const char *str, char c)\n{\n";
    	cppCode +="\tvector<string> result;\n\tdo\n\t{\n\t\tconst char *begin = str;\n\t\twhile(*str != c && *str)\n\t\t\tstr++;\n\t\tresult.push_back(string(begin,str));\n\t}while(0!=*str++);\n";
	cppCode +="\treturn result;\n}\n\n";
	
	cppCode +="int ReadGraphDataGeneral(FILE* fp, int maxVertices, Adjacency_List& g)\n{\n";
	cppCode +="\tchar line[1024];\n\twhile (fgets(line, sizeof(line), fp) != NULL)\n\t{\n";
	cppCode +="\t\tif(line[0]=='#')\n\t\t\tcontinue;\n\t\tvector<string> edgeInfo  = Split(line,'\\t');\n\t\tif(edgeInfo.size() != 3)\n\t\t\treturn -1;\n";
	cppCode +="\t\tint weight = atoi((edgeInfo[2]).c_str());\n\t\tint toNode = atoi((edgeInfo[1]).c_str());\n\t\tint fromNode = atoi((edgeInfo[0]).c_str());\n";
	cppCode +="\t\t	g[fromNode].insert(make_pair(toNode,weight));\n";
	cppCode +="\t\tif(g.size()>=maxVertices) break;\n\t}\n\n";
	cppCode +="\tint numEdges=0;\n\tAdjacency_List::iterator it = g.begin();\n\tfor(;it!=g.end();it++)\n\t{\n";
	cppCode +="\t\tvertexIDList.push_back(it->first);\n\t\tnumEdges+=(it->second).size();\n\t}\n";
	cppCode +="\t//printf(\"Number of vertices:%d edges:%d\\n\",g.size(),numEdges);\n\treturn g.size();\n}\n\n";
	

	cppCode +="int ReadGraphData(FILE* fp, int maxVertices, Adjacency_List& g)\n{\n";
	cppCode +="\tchar line[1024];\n\twhile (fgets(line, sizeof(line), fp) != NULL)\n\t{\n";
	cppCode +="\t\tif(line[0]=='#')\n\t\t\tcontinue;\n\t\tvector<string> edgeInfo  = Split(line,'\\t');\n\t\tif(edgeInfo.size() != 3)\n\t\t\treturn -1;\n";
	cppCode +="\t\tint relationship = atoi((edgeInfo[2]).c_str());\n\t\tint toNode = atoi((edgeInfo[1]).c_str());\n\t\tint fromNode = atoi((edgeInfo[0]).c_str());\n";
	cppCode +="\t\tif((relationship == 1) || (relationship == -1))\n\t\t\tg[fromNode].insert(make_pair(toNode,1));\n";
	cppCode +="\t\telse if((relationship == 0) || (relationship == 2))\n\t\t{\n\t\t\tg[fromNode].insert(make_pair(toNode,1));\n\t\t\tg[toNode].insert(make_pair(fromNode,1));\n\t\t}\n";
	cppCode +="\t\tif(g.size()>=maxVertices) break;\n\t}\n\n";
	cppCode +="\tint numEdges=0;\n\tAdjacency_List::iterator it = g.begin();\n\tfor(;it!=g.end();it++)\n\t{\n";
	cppCode +="\t\tvertexIDList.push_back(it->first);\n\t\tnumEdges+=(it->second).size();\n\t}\n";
	cppCode +="\t//printf(\"Number of vertices:%d edges:%d\\n\",g.size(),numEdges);\n\treturn g.size();\n}\n\n";
	
	cppCode +="int ReadInput(int argc, char** argv)\n{\n";
	cppCode +="\tif(strcmp(argv[1],\"-h\")==0)\n\t{\n\t\tprintf(\"Usage: ./<exec> <input_graph> <num_vertices>\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\tFILE* fpi=fopen(argv[1],\"r\");\n\tif(!fpi)\n\t{\n\t\tprintf(\"ERROR: Unable to open input file.\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\tint maxVertices=MAX_NUM_VERTICES;\n\tmaxVertices=atoi(argv[2]);\n\tint ret1=ReadGraphData(fpi, maxVertices, graphData);\n\tfclose(fpi);\n\tif(ret1<0)\n\t{\n";
	cppCode +="\t\tprintf(\"ERROR: Unable to read input file correctly.\\n\");\n\t\texit(0);\n\t}\n\telse\n\t{\n\t\tinputSize[0]=ret1-1; inputSize[1]=ret1-1; inputSize[2]=ret1-1;\n\t}\n";
	cppCode +="\treturn inputSize[0]+1;\n}\n\n";

	return std::make_pair(hdrDeclarations,cppCode);
}

pair<string,string> FloydWarshall::InitializeDPTable()
{

	string hdrDecl("");
	string sig ="void InitializeDPTable(Box* b, CELL_TYPE* data)"; 
	hdrDecl += sig+";\n";
	string cppCode = sig+"\n{\n";
	cppCode +="\tint numCells = b->GetBoxSize();\n";
	cppCode +="\tunsigned int relIndx = 0;\n\tfor(unsigned int i=b->coords[2];i<=b->coords[5];i++)\n\t{\n\t\tfor(unsigned int j=b->coords[1];j<=b->coords[4];j++)\n\t\t{\n";
	cppCode +="\t\t\tCELL_TYPE initVal = INF;\n\t\t\tif(i==j)\n\t\t\t\tinitVal = 0;\n\t\t\telse\n\t\t\t{\n\t\t\t\tset<pair<int,int> >::iterator it=graphData[vertexIDList[i]].begin();\n";
	cppCode +="\t\t\t\twhile(it!=graphData[vertexIDList[i]].end())\n\t\t\t\t{\n\t\t\t\t\tif(it->first == vertexIDList[j])\n\t\t\t\t\t{\n";
	cppCode +="\t\t\t\t\t\tinitVal = it->second;\n\t\t\t\t\t\tbreak;\n\t\t\t\t\t}\n\t\t\t\t\tit++;\n\t\t\t\t}\n\t\t\t}\n";
	cppCode +="\t\t\tfor(unsigned int k=b->coords[0];k<=b->coords[3];k++)\n\t\t\t{\n\t\t\t\tdata[relIndx++] = initVal;\n\t\t\t}\n";
	cppCode +="\t\t}\n\t}\n";
	cppCode +="\treturn;\n}\n";

	return std::make_pair(hdrDecl,cppCode);
}

string FloydWarshall::StopCondition(vector<string>& args)
{
	string stopCond="\tif("+args[0]+"->coords[0]=="+args[0]+"->coords[3])\n\t{\n";
	string zArg, yArg, aArg;
	if(args.size()<5)
		yArg=args[0];
	else
		yArg=args[2];
	if(args.size() > 5)
		zArg=args[4];
	else
		zArg=args[0];
	if(args.size() > 7)
		aArg = args[6];
	else
		aArg = args[0];

	stopCond += "\t\tint i="+args[0]+"->coords[2], j="+args[0]+"->coords[1], k="+args[0]+"->coords[0], l="+zArg+"->coords[1];\n";
	stopCond +="\t\tif(i==j) return;\n";
	stopCond +="\t\tCELL_TYPE* cost_ijk=GetDPTableCell(i,j,k,"+args[0]+"Data);\n";
	stopCond +="\t\tCELL_TYPE* cost_ijkminus1=GetDPTableCell(i,j,k-1,"+yArg+"Data);\n";
	stopCond +="\t\tCELL_TYPE* cost_ik=GetDPTableCell(i,l,k-1,"+zArg+"Data);\n";
	stopCond +="\t\tCELL_TYPE* cost_kj=GetDPTableCell(l,j,k-1,"+aArg+"Data);\n";
	stopCond +="\t\tCELL_TYPE newCost = INF;\n";
	stopCond +="\t\tif((*cost_ik!=INF) && (*cost_kj !=INF)) newCost = *cost_ik + *cost_kj;\n";
	stopCond +="\t\tif(newCost < *cost_ijk)\n\t\t\t*cost_ijk = newCost;\n";
	stopCond +="\t\tif(*cost_ijkminus1 < *cost_ijk)\n\t\t\t*cost_ijk = *cost_ijkminus1;\n";
	stopCond +="\t\treturn;\n\t}\n";

	return stopCond;
}

string FloydWarshall::PrintResults()
{
	return string("\t//DebugPrintAllCells();\n");	
}
