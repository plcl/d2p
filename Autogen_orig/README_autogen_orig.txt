README.txt

-------------------------------------------------------------------
Contents:
1. Introduction
2. How to run?

-------------------------------------------------------------------
1. Introduction
Autogen is an algorithm (or program) that takes an iterative description of a dynamic programming recurrence as input and automatically discovers and outputs a divide-and-conquer dynamic programming algorithm (or pseudocode).

-------------------------------------------------------------------
2. How to run?
Compilation : g++ autogen.cpp -o autogen
Execution   : ./autogen [options]
Output      : autogen.txt 

Options:
-p problem     : name of the problem
-n problemsize : problem size
-c 0/1         : copy functions
-h             : print the help screen

Example execution:
./autogen -p 0 -n 32 -c 0
./autogen -p 1 -n 64 -c 1

The problem option can be any of the following:
-p 0    : parenthesis problem
-p 1    : longest common subsequence problem 
-p 2    : gap problem (or sequence alignment with gaps)
-p 3    : Floyd-Warshall 3D
-p 4    : function approximation problem
-p 5    : bitonic traveling salesman problem

The problem size can be any of the following:
-n 32
-n 64 
-n 128
We recommend "-n 64" option.

The number of auto-generated functions can be reduced by removing copy functions. The copy functions are those functions that have a work complexity of O(n^{d - 1}), (d is the dimensions) which is asymptotically smaller than the work complexity of the recursive divide-and-conquer algorithm. The copy functions can be easy implemented.
-c 0    : copy functions will be removed
-c 1    : copy functions will not be removed

Have a nice day!
