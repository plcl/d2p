#include "MatrixChain.h"
#define STR(s) #s

pair<string,string> Matrixchain::ReadInput()
{
	/*string hdrDeclarations("");
	string sig ="void ReadInput(int argc, char* argv[])"; //this is not added to header file.

	ostringstream stopCond;
	stopCond<<"vector<int> matrixChain;\n\n";
	//definition of ReadInput, which calls _ReadInput
	stopCond<<sig<<"\n";
	stopCond<<"{\n\tif(argc != 2)\n\t{\n\t\tprintf(\"Usage: ./<exe> <input>\\n\");\n\t\texit(0);\n\t}\n";
	stopCond<<"\t_ReadInput(argv[1],matrixChain);\n";
	stopCond<<"\tinputSize[0] = matrixChain.size()-1;inputSize[1] = matrixChain.size()-1;\n";
	stopCond<<"\tinputSize[0] -= 1;inputSize[1] -= 1;\n}\n\n";
	
	//definition of _ReadInput
	sig = "void _ReadInput(char* fileName, vector<int>& matrixChain)";
	hdrDeclarations +=sig+";\n";
	stopCond<<sig<<"\n{\n\tchar matrixDimStr[8];\n\tint matrixDim;\n\tifstream inFile(fileName, ifstream::in);\n"; 
	stopCond<<"\tif(!fileName || !inFile.is_open())\n\t{\n\t\tprintf(\"ERROR: Unable to open input file\\n\");\n\t\texit(0);\n\t}\n";
	stopCond<<"\twhile(!inFile.eof())\n\t{\n\t\tinFile.getline(matrixDimStr,8);\n\t\tif(inFile.eof())\n\t\t\tbreak;\n";
	stopCond<<"\t\tmatrixDim = atoi(matrixDimStr);\n\t\tmemset(matrixDimStr,0,8);\n\t\tmatrixChain.push_back(matrixDim);\n\t}\n}\n\n";

	return std::make_pair(hdrDeclarations,stopCond.str());*/
	


	string hdrDeclarations("");
	hdrDeclarations += "typedef int CELL_TYPE;\n";
	string cppCode("");
	string sig = "void _ReadInput(char* fileName, vector<int>& matrixChain)";
	
	cppCode +="vector<int> matrixChain;\n\n";
	cppCode +=sig;
	cppCode +="\n{\n\tchar matrixDimStr[8];\n\tint matrixDim;\n\tifstream inFile(fileName, ifstream::in);\n"; 
	cppCode +="\tif(!fileName || !inFile.is_open())\n\t{\n\t\tprintf(\"ERROR: Unable to open input file\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\twhile(!inFile.eof())\n\t{\n\t\tinFile.getline(matrixDimStr,8);\n\t\tif(inFile.eof())\n\t\t\tbreak;\n";
	cppCode +="\t\tmatrixDim = atoi(matrixDimStr);\n\t\tmemset(matrixDimStr,0,8);\n\t\tmatrixChain.push_back(matrixDim);\n\t}\n}\n\n";

	sig ="void ReadInput(int argc, char* argv[])"; //this is not added to header file.
	cppCode +=sig;
	cppCode +="\n{\n\tif(argc != 4)\n\t{\n\t\tprintf(\"Usage: ./<exe> <numCores> <input> <chainLength>\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\t_ReadInput(argv[2],matrixChain);\n";
	cppCode +="\tinputSize[0] = matrixChain.size()-1;inputSize[1] = matrixChain.size()-1;\n";
	cppCode +="\tinputSize[0] -= 1;inputSize[1] -= 1;\n}\n\n";

	return std::make_pair(hdrDeclarations,cppCode);
}

pair<string,string> Matrixchain::InitializeDPTable()
{
	/*string hdrDecl("");
	string sig ="int* InitializeDPTable(Box* b)"; 
	hdrDecl += sig+";\n";

	string cppCode = sig+"\n{\n";
	cppCode +="\tint numCells = (b->coords[2]-b->coords[0]+1) * (b->coords[3]-b->coords[1]+1);\n\tint* newTile = new int[numCells+4];\n";
	cppCode +="\tnewTile[0] = b->coords[0];\n\tnewTile[1] = b->coords[1];\n\tnewTile[2] = (b->coords[2]-b->coords[0]+1);\n\tnewTile[3] = (b->coords[3]-b->coords[1]+1);\n";
	cppCode +="\tunsigned int relIndx = 4;\n\tfor(unsigned int i=b->coords[1];i<=b->coords[3];i++)\n";
	cppCode +="\t\tfor(unsigned int j=b->coords[0];j<=b->coords[2];j++)\n";
	cppCode +="\t\t\tnewTile[relIndx++] = (i==j)?0:INT_MAX;\n";
	cppCode +="\treturn newTile;\n}\n\n";*/

	string hdrDecl("");
	string sig ="void InitializeDPTable(Box* b, CELL_TYPE* data)"; 
	hdrDecl += sig+";\n";
	string cppCode = sig+"\n{\n";
	cppCode +="\tint numCells = b->GetBoxSize();\n";
	cppCode +="\tunsigned int relIndx = 0;\n\tfor(unsigned int i=b->coords[1];i<=b->coords[3];i++)\n";
	cppCode +="\t\tfor(unsigned int j=b->coords[0];j<=b->coords[2];j++)\n";
	cppCode +="\t\t\tdata[relIndx++] = (i==j)?0:INT_MAX;\n";
	cppCode +="\treturn;\n";
	cppCode +="}\n\n";
	return std::make_pair(hdrDecl,cppCode);
}

string Matrixchain::StopCondition(vector<string>& args)
{
	string stopCond="\tif(("+args[0]+"->coords[0]=="+args[0]+"->coords[2]) && ("+args[0]+"->coords[1]=="+args[0]+"->coords[3]))\n\t{\n";
	string yArg=args[2],zArg;
	if(args.size()<5)
	{
		yArg=args[0];
		zArg=args[0];
	}
	else
		zArg=args[4]; 
	stopCond+="\t\tint i="+args[0]+"->coords[1];\n\t\tint j="+args[0]+"->coords[0];\n\t\tint k="+yArg+"->coords[0];\n\t\tif(i == j)\n\t\t\treturn;\n";	

	stopCond+="\t\tCELL_TYPE w_ikj=matrixChain[i-1]*matrixChain[k]*matrixChain[j];\n";
	stopCond+="\t\tCELL_TYPE* cost_ij = GetDPTableCell(i,j,"+args[0]+"Data);\n\t\tCELL_TYPE* cost_ik = GetDPTableCell(i,k,"+yArg+"Data);\n\t\tCELL_TYPE* cost_kj = GetDPTableCell(k,j,"+zArg+"Data);\n\t\tCELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;\n"; 
	stopCond+="\t\tif(newCost < *cost_ij)\n\t\t\t*cost_ij =  newCost;\n\t\treturn;\n\t}\n";
	return stopCond;
}

string Matrixchain::PrintResults()
{
	string res("");
	res +="\tvector<int> coords;\n";
	res +="\tcoords.push_back(inputSize[0]);\n";
	res +="\tcoords.push_back(0);\n";
	res +="\tCELL_TYPE* data = GetDPTableTile(coords);\n";
	res +="\tif(data != nullptr)\n\t{\n";
	res +="\t\tCELL_TYPE* result = GetDPTableCell(0,inputSize[0],data);\n";
	res += "\t\tif(result!=nullptr) printf(\"Optimum cost:%d\\n\",*result);\n";
	res +="\t}\n";
	return res;
}
