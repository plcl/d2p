#include "Parenthesis.h"

pair<string,string> Parenthesis::ReadInput()
{
	string hdrDeclarations("");
	hdrDeclarations += "typedef double CELL_TYPE;\n";

	string cppCode("");
	cppCode += "vector<pair<int, int> > vertices;\n\n";
	//definition of _ReadInput
	cppCode +="void _ReadInput(char* fileName, int maxVertices, vector<pair<int, int> >& points)\n{\n\tstring line;\n\tint xCoord, yCoord;\n\tifstream inFile(fileName, ifstream::in);\n"; 
	cppCode +="\tif(!fileName || !inFile.is_open())\n\t{\n\t\tprintf(\"ERROR: Unable to open input file\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\twhile(!inFile.eof())\n\t{\n\t\tgetline(inFile, line);\n\t\tif(inFile.eof())\n\t\t\tbreak;\n\t\tsize_t pos = line.find(',');\n";
	cppCode +="\t\tif(pos==string::npos)\n\t\t{\n\t\t\tprintf(\"ERROR.Invalid input format.\\n\");\n\t\t\texit(0);\n\t\t}\n";
	cppCode +="\t\txCoord = atoi(line.substr(0,pos).c_str());\n\t\tyCoord = atoi(line.substr(pos+1).c_str());\n\t\tpoints.push_back(make_pair(xCoord,yCoord));\n";
	cppCode +="\t\tif(points.size()>=maxVertices)\n\t\t\tbreak;\n\t}\n";
	cppCode +="\treturn;\n}\n\n";

	//definition of ReadInput, which calls _ReadInput
	cppCode += "void ReadInput(int argc, char** argv)\n"; 
	cppCode +="{\n\tif(argc != 3)\n\t{\n\t\tprintf(\"Usage: ./exe <input> <numvertices>\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\t_ReadInput(argv[1],atoi(argv[2]),vertices);\n";
	cppCode +="\tinputSize[0] = vertices.size()-1;inputSize[1] = vertices.size()-1;\n}\n\n";

	//definition of Distance
	string helpers("");
	helpers +="CELL_TYPE Distance(int i, int j)\n{\n\tint x1=vertices[i].first;\n\tint y1=vertices[i].second;\n\tint x2=vertices[j].first;\n\tint y2=vertices[j].second;\n";
	helpers +="\treturn sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));\n}\n\n";
 
	//definition of Weight
	helpers +="CELL_TYPE Weight(int i, int j, int k)\n{\n\treturn Distance(i,j)+Distance(j,k)+Distance(k,i);\n}\n\n";

	cppCode += helpers;
		
	return std::make_pair(hdrDeclarations,cppCode);
}

pair<string,string> Parenthesis::InitializeDPTable()
{
	string hdrDecl("");
	string sig ="CELL_TYPE* InitializeDPTable(Box* b)"; 
	hdrDecl += sig+";\n";

	string cppCode = sig+"\n{\n";
	cppCode +="\tint numCells = (b->coords[2]-b->coords[0]+1) * (b->coords[3]-b->coords[1]+1);\n\tCELL_TYPE* newTile = new CELL_TYPE[numCells+4];\n";
	cppCode +="\tnewTile[0] = b->coords[0];\n\tnewTile[1] = b->coords[1];\n\tnewTile[2] = (b->coords[2]-b->coords[0]+1);\n\tnewTile[3] = (b->coords[3]-b->coords[1]+1);\n";
	cppCode +="\tunsigned int relIndx = 4;\n\tfor(unsigned int i=b->coords[1];i<=b->coords[3];i++)\n";
	cppCode +="\t\tfor(unsigned int j=b->coords[0];j<=b->coords[2];j++)\n";
	cppCode +="\t\t\tnewTile[relIndx++] = ((i==j)||(i==j-1))?0:FLT_MAX;\n";
	cppCode +="\treturn newTile;\n}\n\n";
	return std::make_pair(hdrDecl,cppCode);
}

string Parenthesis::StopCondition(vector<string>& args)
{
	string stopCond="\tif(("+args[0]+"->coords[0]=="+args[0]+"->coords[2]) && ("+args[0]+"->coords[1]=="+args[0]+"->coords[3]))\n\t{\n";
	string yArg=args[2],zArg;
	if(args.size()<5)
	{
		yArg=args[0];
		zArg=args[0];
	}
	else
		zArg=args[4]; 
	stopCond+="\t\tint i="+args[0]+"->coords[1];\n\t\tint j="+args[0]+"->coords[0];\n\t\tint k="+yArg+"->coords[0];\n\t\tif((i == j)||(i==j-1)||(i==k))\n\t\t\treturn;\n";	

	stopCond+="\t\tCELL_TYPE w_ikj=Weight(i,j,k);\n";
	stopCond+="\t\tCELL_TYPE* cost_ij = GetDPTableCell(i,j,"+args[0]+"Data);\n\t\tCELL_TYPE* cost_ik = GetDPTableCell(i,k,"+yArg+"Data);\n\t\tCELL_TYPE* cost_kj = GetDPTableCell(k,j,"+zArg+"Data);\n\t\tCELL_TYPE newCost = *cost_ik + *cost_kj + w_ikj;\n"; 
	stopCond+="\t\tif(newCost < *cost_ij)\n\t\t\t*cost_ij =  newCost;\n\t\treturn;\n\t}\n";
	return stopCond;
}

string Parenthesis::PrintResults()
{
	string res("");
	res += "\tCELL_TYPE* result = GetDPTableCell(0,inputSize[0]);\n";
	res += "\tif(result!=nullptr)\n\t\tstd::cout<<\"Minimum cost of triangulation:\"<<*result<<std::endl;\n";
	return res;
}
