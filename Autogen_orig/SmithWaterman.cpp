#include "SmithWaterman.h"
#define STR(s) #s

pair<string,string> SmithWaterman::ReadInput()
{
	string hdrDeclarations("");
	//defining the type of DP table cell
	hdrDeclarations += "typedef int CELL_TYPE;\n";
	
	//defining ReadFASTAFile, which is called by ReadInput
	string sigFasta(STR(int ReadFASTAFile(FILE *fp, int maxLen, char** seq)));
	string cppCode("#define MAX_STR_LEN 45000\nint gap=-4, match=5, nomatch=-3;\nchar* input_i, *input_j;\n\n");
	cppCode += sigFasta +"\n{\n";
	cppCode +="\tchar line[1024];\n\tint seqLength = 0, seqLenToProcess;\n\tchar* localSeq=NULL;\n";
	cppCode +="\tfgets(line, sizeof(line), fp);\n\tif(line == NULL)\n\t\treturn -1;\n"; 
	cppCode +="\twhile (fgets(line, sizeof(line), fp) != NULL)\n\t{\n\t\tif(strlen(line)+seqLength > maxLen)\n\t\t\tseqLenToProcess = maxLen-seqLength;\n\t\telse\n\t\t\tseqLenToProcess = strlen(line);\n";
	cppCode +="\t\tlocalSeq = (char*) realloc(localSeq, seqLenToProcess + seqLength+1);\n\t\tif(!localSeq)\n\t\t{\n\t\t\tprintf(\"ERROR in realloc.\\n\");\n\t\t\texit(0);\n\t\t}\n";
	cppCode +="\t\tfor(unsigned int i = 0; i < seqLenToProcess; ++i)\n\t\t{\n\t\t\tif (line[i] == '*' || line[i] == '-')\n\t\t\t\tlocalSeq[seqLength++] = line[i];\n\t\t\telse if(isalpha(line[i]))\n\t\t\t\tlocalSeq[seqLength++] = toupper(line[i]);\n";
	cppCode +="\n\t\t}\n\t\tif(seqLength>=maxLen) break;\n\t}\n";
	cppCode +="\tlocalSeq[seqLength] = '\\0';\n\t*seq = localSeq;\n\treturn seqLength;\n}\n\n";

	//defining ReadInput
	string sig(STR(int ReadInput(int argc, char** argv)));
	cppCode += sig +"\n{\n";
	
	cppCode +="\tif(strcmp(argv[1],\"-h\")==0) \n\t{\n\t\tprintf(\"Usage: ./<exec> <input_string_i> <input_string_j> <input_length_i> <input_length_j>\\n\");\n\t\texit(0);\n\t}\n";
	cppCode +="\tFILE* fpi=fopen(argv[1],\"r\"), *fpj= fopen(argv[2],\"r\");\n\tif(!fpi || !fpj)\n\t{\n\t\tprintf(\"ERROR: Unable to open input files.\\n\");\n\t\texit(0);\n\t}\n";
	cppCode += "\tint inputLength_I=MAX_STR_LEN,inputLength_J=MAX_STR_LEN;\n";
	cppCode += "\tinputLength_I=atoi(argv[3]); inputLength_J=atoi(argv[4]);\n";
	cppCode +="\tint ret1=ReadFASTAFile(fpi, inputLength_I, &input_i);\n\tint ret2=ReadFASTAFile(fpj, inputLength_J, &input_j);\n\tfclose(fpi); fclose(fpj);\n";
	cppCode +="\tif((ret1<0) || (ret2<0))\n\t{\n\t\tprintf(\"ERROR: Unable to read input files correctly. file1 len:%d file2 len:%d\\n\",ret1,ret2);\n\t\texit(0);\n\t}\n";
	cppCode +="\telse\n\t{\n\t\tinputSize[0]=ret1; inputSize[1]=ret2;\n\t}\n";
	cppCode += "\tif(inputSize[1] > inputSize[0])\n\t{\n\t\tstd::swap(inputSize[0],inputSize[1]);\n\t\tstd::swap(input_i,input_j);\n\t}\n\t//printf(\"string length (x):%d (y):%d\\n\",inputSize[0],inputSize[1]);\n\treturn inputSize[0]+1;\n}\n\n";

	return std::make_pair(hdrDeclarations,cppCode);
}


pair<string,string> SmithWaterman::InitializeDPTable()
{
	string hdrDecl("");
	string sig ="void InitializeDPTable(Box* b, CELL_TYPE* data)"; 
	hdrDecl += sig+";\n";
	string cppCode = sig+"\n{\n";
	cppCode +="\tint numCells = b->GetBoxSize();\n";
	cppCode +="\tmemset(data,0,sizeof(CELL_TYPE)*(numCells));\n\treturn;\n";
	cppCode +="}\n\n";
	return std::make_pair(hdrDecl,cppCode);
}

string SmithWaterman::StopCondition(vector<string>& args)
{
	string stopCond="\tif(("+args[0]+"->coords[0]=="+args[0]+"->coords[2]) && ("+args[0]+"->coords[1]=="+args[0]+"->coords[3]))\n\t{\n";
	string yArg=args[2];
	if(args.size()<5)
		yArg=args[0];
	stopCond += "\t\tint i="+args[0]+"->coords[1], j="+args[0]+"->coords[0], k="+yArg+"->coords[1], l="+yArg+"->coords[0];\n";
	stopCond +="\t\tif((i==0)||(j==0))\n\t\t\treturn;\n\t\tCELL_TYPE* cost_kl, *cost_ij=GetDPTableCell(i,j,"+args[0]+"Data), costkl = 0;\n";
	//stopCond +="\t\tif((k>="+yArg+"Data[1])&& (k<="+yArg+"Data[1]+"+yArg+"Data[3]-1) && (l>="+yArg+"Data[0]) && (l<="+yArg+"Data[0]+"+yArg+"Data[2]-1))\n";
	stopCond +="\t\t\tcost_kl=((k==0)||(l==0))?&costkl:GetDPTableCell(k,l,"+yArg+"Data);\n";
	//stopCond +="\t\telse\n\t\t\tcost_kl = GetDPTableCell(k,l,"+args[0]+"Data);\n";
	stopCond +="\t\tCELL_TYPE reward_penalty=gap;\n\t\tif((k==i-1) && (l==j-1))\n\t\t\treward_penalty = (input_j[i-1] == input_i[j-1])?match:nomatch;\n";
	stopCond +="\t\tCELL_TYPE newCost = *cost_kl + reward_penalty;\n\t\tif(newCost > *cost_ij)\n\t\t\t*cost_ij =  newCost;\n";
	stopCond +="\t\treturn;\n\t}\n";
	return stopCond;
}

string SmithWaterman::PrintResults()
{
	return string("\tPrintGlobalMax();\n");	
}
