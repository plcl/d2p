#pragma once
#include<utility>
#include <string>
#include <cstring>
#include <vector>
#include <map>
#include <set>
#include<sstream>
using namespace std;
class RecursionBase
{
	public:
	virtual pair<string,string> ReadInput()=0;
	virtual pair<string,string> InitializeDPTable()=0;
	virtual string StopCondition(vector<string>& paramNames)=0;
	virtual string PrintResults()=0;
};

enum ComputeGrid{
COMPUTE_FULL,
COMPUTE_UTM,
COMPUTE_SPARSE
};


string GenerateStopCondForUnfolding(const struct tnode* node, const vector<string>& fnArgs, int dim);
string GenerateSerialCodeInFunctionBody(string varDecls, string fnBody);
int ConvertStringToBinary(const char* str);
string GenerateCall(const struct tnode* parent, const struct tnode* child, map<char, set<string> >& varsUsed);
string GenerateCallForUnroll(const struct tnode* parent, const struct tnode* child, map<char, set<string> >& varsUsed, int d);
