/*************************************** AUTOGEN ALGORITHM ************************************************************/
/* Program    : Autogen algorithm                                                                                     */
/* Input      : A loop-based / iterative dynamic programming implementation                                           */
/* Output     : A cache-efficient cache-oblivious parallel recursive divide-and-conquer dynamic programming algorithm */
/* Debug Info : autogen.txt                                                                                           */
/**********************************************************************************************************************/

#include <iostream>
#include <fstream>
#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <queue>
#include <iterator>
#include <cstdlib>
#include <exception>
#include <iomanip>
#include <time.h>
#include<cassert>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include"autogen_orig.h"
#include "MatrixChain.h"
#include "Parenthesis.h"
#include "SmithWaterman.h"
#include "NeedlemanWunsch.h"
#include "FloydWarshall.h"
#include "Nussinov4D.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define MAX_PROBLEMS 20          /* max number of problems */
#define MAX_TNODECHILD 255        /* max number of children for a tnode */
#define MAX_D 4                  /* max dimensions */
#define MAX_SIZE 5               /* max celltuple size */
#define MAX_CELLTUPLES 2300000   // max celltuple size : 128^3 <= 2.3 million
#define MAX_IDS 9                /* max levels in the tree */
#define print_error(error) { fprintf(stderr, "\n\nError: %s: file: %s: line: %d\n", error, __FILE__, __LINE__); exit(1); }


int computeGrid=COMPUTE_FULL;
int levels, dim;
RecursionBase* recBase=NULL;
map<char,struct tnode*> recFunctions;

/*************************************** GLOBAL VARIABLES AND STRUCTURES ********************************************/

void printline(string s)
{ cout << std::setiosflags(std::ios::left) << std::setw(40) << std::setfill(' ') << s; }
void printend()
{ cout << "[complete]" << endl; }

struct cell
{
    int x[MAX_D]; /* coordinates */
};

struct celltuple
{
    cell c[MAX_SIZE];
    unsigned long long IDs[MAX_IDS];
    int childIDs[MAX_IDS];
    int prevChildIDs[MAX_IDS];
    unsigned long long prevIDs[MAX_IDS];
}cellset[MAX_CELLTUPLES];

struct tnode
{
    int l, r;
    int level;
    int level_num;
    struct tnode *child[MAX_TNODECHILD];
    int children, childID, prevChildID;
    unsigned long long ID, prevID;
    vector<int> fingerprint;
    char functionname;
    vector<struct tnode*> dagEdge;
    int write;
    vector<int> read;
    bool isFlexible;
    int regionNums[MAX_SIZE][MAX_D];
    char regionChars[MAX_SIZE];
    int regionIDs[MAX_SIZE];
    int max_level_num;
    vector<string> varsUsedInFunctionCall;
    vector<char> functionArgs;
    vector<int> uniqParamIndx;
};

vector<unsigned long long> findCombinedNodesByID(int l, int r, int level);

typedef struct Parameter
{
	int portID;	
	void* data;	
}Parameter;

typedef struct FunctionCall
{
	char functionName;
	vector<Parameter> params;
}FunctionCall;

vector<FunctionCall*> fnCalls;

/*************************************** AUTO-GENERATOR CLASS ********************************************/

class AutoGenerator
{
    public:
    enum Problems { PROBLEMPARENTHESIS,            // 0
                    PROBLEMSMITHWATERMAN,                    // 1
                    PROBLEMGAP,                    // 2
                    PROBLEMFLOYDWARSHALL3D,        // 3
                    PROBLEMFUNCTIONAPPROXIMATION,  // 4
                    PROBLEMBITONICTSP,             // 5
                    PROBLEM_4D_NUSSINOV,         // 6
                    PROBLEMNEEDLEMANWUNSCH,      //7
		    PROBLEMMCM};			//8
    string problemnames[MAX_PROBLEMS];
    int problem, n, count, s, size, d;
    bool debug;
    bool debug_cellset;
    bool debug_tree;
    bool debug_dag;
    bool debug_output;
    cell c;
    celltuple ctuple;
    ofstream out;
    string filename;
    tnode* root;
    queue<struct tnode*> q;
    struct tnode* head;
    int functionlabel;
    int flag[MAX_IDS];
    map < vector < int >, int > functions;//for mapping function numbers to nodes
    vector< vector < int > > dagFunctions;
    vector< vector < int > > outFunctions;
    set < int > fnc;
    int uniqueChildIDs[MAX_TNODECHILD];
    int hash_input_fingerprint;
    int copy_functions;
    int unFoldingLevel;
    int numProcesses;
    bool depInfoRequested;
    ofstream outdep;	

    void PrintUsage(char *program);
    void Initialize(int argc, char* argv[]);
        int FindLog2(int);
    void Algorithm();
    void InputReader(char*);
    void CellSetGeneration();
        void FindandSortIDs();
        int canbecombined(int[], int);
        int FindUniqueValue(int, int[], int);
        unsigned long long FindUniqueLongValue(int, int[], int);
        void PrintCellSet();
    void AlgorithmTreeConstruction();
        struct tnode* newtnode(int, int, int, unsigned long long, int);
        void ConstructAlgorithmTree(queue<struct tnode*>);
            void ComputeInputandOutputfingerprint(struct tnode*);
                vector<int> findCombinedNodes(int l, int r, int level);
    void RemoveCopyFunctions();
        void DeleteCopyFunctions(queue<struct tnode*> q);
    void AlgorithmTreeLabeling();
        void initializequeue();
        void LabelAlgorithmTree(queue<struct tnode*>);
    void DisplayFunctions();
        void PrintFunctions(queue<struct tnode*>, int);
        void printnode(struct tnode*);
        void printRegions(struct tnode *root);
    void DAGConstruction();
        void ConstructDAG(queue<struct tnode*>);
        void StoreReadAndWrite(struct tnode*);
    void CreateOutput();
        void OutputCreation(queue<struct tnode*>);
        void SetRegionLabels(struct tnode*);
        void printRegionNode(struct tnode*);
        void printRegionNode(struct tnode*,struct tnode*);

	void GetLeaves(struct tnode* node);
	void PrintReadAndWriteRegionsOfLeaves();
	void GetFunctionCallInfo(vector<FunctionCall*>& fnCalls, struct tnode* node);
	int DetermineUnfoldingLevel(int numProcesses, struct tnode* root);
	void GenerateFunctionDefs();
	RecursionBase* GetRecursionBaseInstance(int problem);
	pair<string,string> CreateFunctionCallHierarchySummary(struct tnode* root);
	string GenerateDeferredCallTemplate() const;
	pair<string,string> GenerateFunctionBody(const struct tnode* node, RecursionBase* base);
	pair<string,string> GenerateFunctionBodyForUnroll(const struct tnode* node, RecursionBase* base);
	vector<char> GenerateFunctionArgs(const struct tnode* node);
	string GenerateVarDefinitions(map<char, set<string> >& varsPendingDefinition);
	string GenerateParamListMacro(const struct tnode* node);
	pair<string, string> GenerateTopLevelCalls();
	string GenerateTopLevelFunctionBody(const struct tnode* node);
	pair<string, string> GenerateDelayedFunctionExecution();
	pair<string,string> GenerateResultsCode(RecursionBase* base);
	string FetchCellFnCallCode();
};

/*************************************** AUTO-GENERATOR FUNCTIONS ********************************************/

void AutoGenerator::PrintUsage(char *program)
{
    cout << "Usage: " << program << " [options]" << endl << endl;
    cout << "Options:" << endl;
    cout << std::setiosflags(std::ios::left) << std::setw(15) << std::setfill(' ') << "-p problem";
    cout << ": name of the problem" << endl;
    cout << std::setiosflags(std::ios::left) << std::setw(15) << std::setfill(' ') << "-n problemsize";
    cout << ": problem size" << endl;
    cout << std::setiosflags(std::ios::left) << std::setw(15) << std::setfill(' ') << "-h";
    cout << ": print this help screen" << endl << endl;
    cout << "Please refer README.txt for more details" << endl;
}
void AutoGenerator::Initialize(int argc, char* argv[])
{
    /* default values */
    count = 0;
    d = 2; size = 3;
    problem = PROBLEMPARENTHESIS;
    n = 8;
    debug = false;
    debug_cellset = false;
    debug_tree = false;
    debug_dag = true;
    debug_output = true;
    copy_functions = 1;
    numProcesses = 1;
    depInfoRequested = false;

    problemnames[PROBLEMPARENTHESIS]            = "parenthesis_problem";
    problemnames[PROBLEMFUNCTIONAPPROXIMATION]  = "function_approximation";
    problemnames[PROBLEMBITONICTSP]             = "bitonic_TSP";
    problemnames[PROBLEMSMITHWATERMAN]                    = "Smith_Waterman";
    problemnames[PROBLEMGAP]                    = "gap_problem";
    problemnames[PROBLEMFLOYDWARSHALL3D]        = "Floyd_Warshall_APSP_3D";
    problemnames[PROBLEM_4D_NUSSINOV]           = "4D_Nussinov";
    problemnames[PROBLEMNEEDLEMANWUNSCH]           = "Needleman_Wunsch";
    problemnames[PROBLEMMCM]           = "MCM";

    /* user-defined values */
    for (int i = 1; i < argc;)
    {
        //int j = i;

        if (!strcmp(argv[i], "-p"))
        {
            if ((i + 1 >= argc) || ( argv[i + 1][0]=='-'))
            {
               cout << "Error: Missing problem name (specify -p problem)!\n\n";
               PrintUsage(argv[0]); exit(1);
            }
            problem = atoi(argv[i + 1]); i += 2;
            if (i >= argc) break;
        }
        if (!strcmp(argv[i], "-n"))
        {
            if ((i + 1 >= argc) || ( argv[i + 1][0]=='-'))
            {
               cout << "Error: Missing problem size (specify -n problemsize)!\n\n";
               PrintUsage(argv[0]); exit(1);
            }
            n = atoi(argv[i + 1]); i += 2;
            if (i >= argc) break;
        }
        if (!strcmp(argv[i], "-c"))
        {
            if ((i + 1 >= argc) || ( argv[i + 1][0]=='-'))
            {
               cout << "Error: Missing problem size (specify -c 0/1)!\n\n";
               PrintUsage(argv[0]); exit(1);
            }
            copy_functions = atoi(argv[i + 1]); i += 2;
            if (i >= argc) break;
        }
        if (!strcmp(argv[i], "-d"))
        {
            debug = true; i++;
            if (i >= argc) break;
        }
        if (!strcasecmp(argv[i], "-h") || !strcasecmp(argv[i], "-help") || !strcasecmp(argv[i], "--help"))
        { PrintUsage(argv[0]); exit(0); }
	
	if (!strcmp(argv[i], "-P"))
        {
            if ((i + 1 >= argc) || ( argv[i + 1][0]=='-'))
            {
               cout << "Error: Missing number of processes (specify -P num_processes)!\n\n";
               PrintUsage(argv[0]); exit(1);
            }
            numProcesses = atoi(argv[i + 1]); i += 2;
            if (i >= argc) break;
        }
	if (!strcmp(argv[i], "-D"))
        {
            depInfoRequested = true; i += 1;
	    outdep.open("DataDeps.txt");
            if (i >= argc) break;
        }
    }

    levels = FindLog2(n) + 1;
    filename = "autogen.txt";
    out.open(filename.c_str());
    functionlabel = 0;
    flag[0] = 1;
    for (int i = 1; i < levels; i++)
        flag[i] = 0;

    out << "AUTOGEN ALGORITHM" << endl << endl;

    out << "INPUT:" << endl;
    out << "Problem = " << problemnames[problem] << endl;
    out << "Problem size = n = " << n << endl << endl;
    //out << "Program debug = ";
    //if (debug) { out << "yes\n\n"; } else { out << "no\n\n"; }

    out << "OUTPUT:" << endl;
}
int AutoGenerator::FindLog2(int num)
{
    static const int MultiplyDeBruijnBitPosition[32] = {
        0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
        8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
    };
    num |= num >> 1;
    num |= num >> 2;
    num |= num >> 4;
    num |= num >> 8;
    num |= num >> 16;
    return MultiplyDeBruijnBitPosition[(unsigned int)(num * 0x07C4ACDDU) >> 27];
}

/*************************************** CELL-SET ********************************************/

void AutoGenerator::CellSetGeneration()
{
    switch (problem)
    {
        case PROBLEMMCM:
        case PROBLEMPARENTHESIS:
            d = 2; size = 3;
            for (int t = 2 ; t < n; t++)
                for (int i = 0 ; i < n-t; i++)
                {
                    int j = t + i;
                    for (int k = i; k <= j; k++)
                    {
                        c.x[0] = i; c.x[1] = j;     ctuple.c[0] = c;
                        c.x[0] = i; c.x[1] = k;     ctuple.c[1] = c;
                        c.x[0] = k; c.x[1] = j;     ctuple.c[2] = c;
                        cellset[count] = ctuple; count++;
                    }
                }
            break;

        case PROBLEMFLOYDWARSHALL3D:
            d = 3; size = 4;
            //d = 3; size = 3;
            for (int k = 1; k < n; k++)
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        //if (i != j && i != k && j != k)
                        //if (false)
                        {

                            c.x[0] = i;     c.x[1] = j;     c.x[2] = k;         ctuple.c[0] = c;
                            c.x[0] = i;     c.x[1] = j;     c.x[2] = k - 1;     ctuple.c[1] = c;
                            c.x[0] = i;     c.x[1] = k;     c.x[2] = k - 1;     ctuple.c[2] = c;
                            c.x[0] = k;     c.x[1] = j;     c.x[2] = k - 1;     ctuple.c[3] = c;
                            cellset[count] = ctuple; count++;
                        }
            break;

        case PROBLEMFUNCTIONAPPROXIMATION:
            d = 2; size = 2;
            for (int i = 1; i < n; i++)
                for (int j = i + 1; j < n; j++)
                    for (int k = 1; k < i; k++)
                    {
                        c.x[0] = i; c.x[1] = j;         ctuple.c[0] = c;
                        c.x[0] = k; c.x[1] = j - 1;     ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;
                    }
            break;

        case PROBLEMBITONICTSP:
            d = 2; size = 2;
            for (int i = 0; i < n; i++)
                for (int j = i; j < n; j++)
                {
                    if (i == 0 && j == 1)
                        continue;

                    if (i < (j - 1))
                    {
                        c.x[0] = i; c.x[1] = j;         ctuple.c[0] = c;
                        c.x[0] = i; c.x[1] = j - 1;     ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;
                    }
                    else if (i == (j - 1))
                    {
                        for (int k = 0; k < i; k++)
                        {
                            c.x[0] = i; c.x[1] = j;     ctuple.c[0] = c;
                            c.x[0] = k; c.x[1] = i;     ctuple.c[1] = c;
                            cellset[count] = ctuple; count++;
                        }
                    }
                }
            break;
	case PROBLEMNEEDLEMANWUNSCH:
        case PROBLEMSMITHWATERMAN:
            d = 2; size = 2;
            for (int i = 1; i < n; i++)
                for (int j = 1; j < n; j++)
                {
                        c.x[0] = i;     c.x[1] = j;     ctuple.c[0] = c;
                        c.x[0] = i - 1; c.x[1] = j;     ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;

                        c.x[0] = i;     c.x[1] = j;     ctuple.c[0] = c;
                        c.x[0] = i;     c.x[1] = j - 1; ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;

                        c.x[0] = i;     c.x[1] = j;     ctuple.c[0] = c;
                        c.x[0] = i - 1; c.x[1] = j - 1; ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;
                }
            break;
            /*d = 2; size = 4;
            for (int i = 1; i < n; i++)
                for (int j = 1; j < n; j++)
                {
                    c.x[0] = i;     c.x[1] = j;         ctuple.c[0] = c;
                    c.x[0] = i - 1; c.x[1] = j - 1;     ctuple.c[1] = c;
                    c.x[0] = i;     c.x[1] = j - 1;     ctuple.c[2] = c;
                    c.x[0] = i - 1; c.x[1] = j;         ctuple.c[3] = c;
                    cellset[count] = ctuple; count++;
                }
            break;*/
            

        case PROBLEMGAP:
            d = 2; size = 2;
            for (int i = 1; i < n; i++)
                for (int j = 1; j < n; j++)
                {
                    c.x[0] = i;     c.x[1] = j;         ctuple.c[0] = c;
                    c.x[0] = i - 1; c.x[1] = j - 1;     ctuple.c[1] = c;
                    cellset[count] = ctuple; count++;

                    for (int q = 0; q < j; q++)
                    {
                        c.x[0] = i; c.x[1] = j;     ctuple.c[0] = c;
                        c.x[0] = i; c.x[1] = q;     ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;
                    }

                    for (int p = 0; p < i; p++)
                    {
                        c.x[0] = i; c.x[1] = j;     ctuple.c[0] = c;
                        c.x[0] = p; c.x[1] = j;     ctuple.c[1] = c;
                        cellset[count] = ctuple; count++;
                    }
                }
            break;
        case PROBLEM_4D_NUSSINOV:
            d = 4; size = 3;
            for (int t = 1; t < n; t++)
                for (int tt = 1; tt < n; tt++)
                    for (int i = 0; i < n-t; i++)
                    {
                        int j = t + i;
                        for (int ii = 0; ii < n-tt; ii++)
                        {
                            int jj = tt + ii;
                            for (int k =i; k < j; k++)
                                for (int kk = ii; kk < jj; kk++)
                                {
                                    // int t = s;
                                    c.x[0] = i; c.x[1] = j; c.x[2] = ii; c.x[3] = jj;  ctuple.c[0] = c;
                                    c.x[0] = i; c.x[1] = k; c.x[2] = ii; c.x[3] = kk;  ctuple.c[1] = c;
                                    c.x[0] = k+1; c.x[1] = j; c.x[2] = kk+1; c.x[3] = jj;  ctuple.c[2] = c;
                                    cellset[count] = ctuple; count++;
				    printf("i j ii jj=:%d %d %d %d -- i k ii kk:%d %d %d %d k+1 j kk+1 jj:%d %d %d %d\n",i,j,ii,jj,i,k,ii,kk,k+1,j,kk+1,jj);
                                }
                        }
                    }
            break;
    }

    s = size - 1;
    FindandSortIDs();
    if (debug) PrintCellSet();
}

/* Compare two cell-tuples. Each cell-tuple is of size (1+s). Sort the cell-tuple starting from the first value to the last. */
struct comparebyID
{
    bool operator()(celltuple const &t, celltuple const &u)
    {
        return comparator(0, t, u);
    }
    bool comparator(int i, celltuple const &t, celltuple const &u)
    {
        if (i == (levels - 1))
            return t.childIDs[i] < u.childIDs[i];

        if (t.childIDs[i] == u.childIDs[i])
            return comparator(i + 1, t, u);
        else
            return t.childIDs[i] < u.childIDs[i];
    }
};

struct comparebyPrevID
{
    bool operator()(celltuple const &t, celltuple const &u)
    {
        return comparator(0, t, u);
    }
    bool comparator(int i, celltuple const &t, celltuple const &u)
    {
        if (i == (levels - 1))
            return t.prevChildIDs[i] < u.prevChildIDs[i];

        if (t.prevChildIDs[i] == u.prevChildIDs[i])
            return comparator(i + 1, t, u);
        else
            return t.prevChildIDs[i] < u.prevChildIDs[i];
    }
};

struct comparebywritecell
{
    bool operator()(celltuple const &t, celltuple const &u)
    {
        return comparator(0, t, u);
    }
    bool comparator(int i, celltuple const &t, celltuple const &u)
    {
        if (i == (dim - 1))
            return t.c[0].x[i] < u.c[0].x[i];

        if (t.c[0].x[i] == u.c[0].x[i])
            return comparator(i + 1, t, u);
        else
            return t.c[0].x[i] < u.c[0].x[i];
    }
};


/* Sort the cell-set first on the first cell, then on the second cell, so on till the last cell (i.e. cell number 1+s). */
/* MOST IMPORTANT CODE FRAGMENT in cell-set generation */
void AutoGenerator::FindandSortIDs()
{
    int val[MAX_SIZE], cval[MAX_SIZE], tcval[MAX_SIZE], tval[MAX_SIZE];
    cell cc;

    dim = d;
    sort(cellset, cellset + count, comparebywritecell());

    int comb_flag = 0;

    for (int i = 1; i < count; i++)
      {
        int j;
        for ( j = 0; j < d; j++ )
           if ( cellset[i].c[0].x[j] != cellset[i - 1].c[0].x[j] ) break;

        if ( j == d )
         {
           comb_flag = 1;
           break;
         }
      }

    /* For every cell-tuple in the cell-set */
    for (int i = 0; i < count; i++)
    {
        ctuple = cellset[i];
        if (debug_cellset)
        {
            out << "Cell-tuple " << i << " = ";
            out << "<";
            for (int j = 0; j < size - 1; j++)
            {
                c = ctuple.c[j];
                out << "(";
                for (int k = 0; k < d - 1; k++)
                    out << c.x[k] << ",";
                out << c.x[d - 1] << "),";
            }
            c = ctuple.c[size - 1];
            out << "(";
            for (int k = 0; k < d - 1; k++)
                out << c.x[k] << ",";
            out << c.x[d - 1] << ")> ";
            out << endl;
        }

        for (int l = 0; l < levels; l++)
        {
            for (int j = 0; j < size; j++)
            {
                c = ctuple.c[j];
                for (int k = 0; k < d; k++)
                   {
                     c.x[k]  = ( c.x[k] / ( n >> l ) );
                     cc.x[k] = ( c.x[k] & 1 );
                   }
                val[j] = FindUniqueValue( ( 1 << ( l ) ), c.x, d );
                cval[j] = FindUniqueValue( 2, cc.x, d );
            }

            for (int j = 0; j < size; j++){
                    tval[j] = val[j];
                    tcval[j] = cval[j];
                }

            if (comb_flag && canbecombined(val, size)){
                for (int j = 1; j < size; j++){
                    val[j] = val[0];//sets every element in val as the first element val
                    cval[j] = cval[0];
                }
            }

            ctuple.prevIDs[l] = FindUniqueLongValue((1 << ((l)*d)), tval, size);
            ctuple.prevChildIDs[l]=FindUniqueValue((1 << d), tcval, size);
            ctuple.IDs[l] = FindUniqueLongValue((1 << ((l)*d)), val, size);
            ctuple.childIDs[l] = FindUniqueValue((1 << d), cval, size);

            if (debug_cellset)
            {
                out << "  Level " << l << " : Region-tuple = <";
                for (int j = 0; j < size - 1; j++)
                    out << val[j] << ",";
                out << val[size - 1] << "> = " << ctuple.IDs[l] << " : ";
                out << "Child Region-tuple = <";
                for (int j = 0; j < size - 1; j++)
                    out << cval[j] << ",";
                out << cval[size - 1] << "> = " << ctuple.childIDs[l] << endl;
            }
        }

      cellset[i] = ctuple;
    }

    if (debug_cellset) { out << endl; }
    sort(cellset, cellset + count, comparebyPrevID());
    sort(cellset, cellset + count, comparebyID());

}
/* The cell-tuples (or region-tuples) are combined based on Rule 1 */
int AutoGenerator::canbecombined(int a[], int length)
{
    //int c=0;
    /* if the first array element a[0] repeats in any of the remaining terms from a[1..n-1] it means the cell-tuple writes and reads from the same cell */
    for (int i = 1; i < length; i++)
        if (a[0] == a[i])
/*            c++;
    if (c>0 && c<size-1)*/
        return 1;
    return 0;
}
/* Convert a tuple to a number */
int AutoGenerator::FindUniqueValue(int max, int arr[], int length)
{
    int sum = 0;
    for (int i = 0; i < length; i++)
        sum = sum * max + arr[i];
    return sum;
}
unsigned long long AutoGenerator::FindUniqueLongValue(int max, int arr[], int length)
{
    unsigned long long sum = 0;
    for (int i = 0; i < length; i++)
        sum = sum * max + arr[i];
    return sum;
}
void AutoGenerator::PrintCellSet()
{
    for (int i = 0; i < count; i++)
    {
        /* print the cell-tuples */
        ctuple = cellset[i];
        out << "Cell-tuple " << i << " = <";
        for (int j = 0; j < size - 1; j++)
        {
            c = ctuple.c[j];
            out << "(";
            for (int k = 0; k < d - 1; k++)
                out << c.x[k] << ",";
            out << c.x[d - 1] << "),";
        }
        c = ctuple.c[size - 1];
        out << "(";
        for (int k = 0; k < d - 1; k++)
            out << c.x[k] << ",";
        out << c.x[d - 1] << ")> ";

        /* region-tuple unique IDs for different sizes of regions */
        out << ": Region-tuple IDs = [";
        for (int l = 0; l < levels - 1; l++)
            out << ctuple.IDs[l] << ",";
        out << ctuple.IDs[levels - 1] << "] ";

        /* region-tuple child IDs for different sizes of regions */
        out << ": Child region-tuple IDs = [";
        for (int l = 0; l < levels - 1; l++)
            out << ctuple.childIDs[l] << ",";
        out << ctuple.childIDs[levels - 1] << "] ";
        out << endl;
    }
    out << endl;
}


/*************************************** ALGORITHM-TREE ********************************************/

struct tnode* AutoGenerator::newtnode(int l, int r, int level, unsigned long long ID, int childID)
{
  struct tnode *temp = new struct tnode;
  temp->l = l; temp->r = r;
  temp->level = level;
  temp->ID = ID; temp->childID = childID;
  return temp;
}

void AutoGenerator::ConstructAlgorithmTree(queue<struct tnode*> q)
{
    if (q.empty())
        return;
    queue<struct tnode*> tempQ;
    int tempChildIDs=0;
    int prev, cur = 0, k = 0;
    struct tnode *root = q.front();
    q.pop();
    int level = root->level;
    if (level >= (levels - 1))
        return;
    int l = root->l, r = root->r;
    prev = l;
    for (int i = l; i < r; i++)
        if (cellset[i].childIDs[level] != cellset[i + 1].childIDs[level])
        {
            cur = i;

            root->child[k] = newtnode(prev, cur, level + 1, cellset[i].IDs[level], cellset[i].childIDs[level]);
            if (root->child[k] == NULL) print_error("Memory exhausted");

            root->child[k]->prevChildID=cellset[i].prevChildIDs[level];
            root->child[k]->prevID=cellset[i].prevIDs[level];

            tempChildIDs+=cellset[i].childIDs[level];
            tempQ.push(root->child[k]);

            prev = i + 1;
            k++;

        }
    root->child[k] = newtnode(prev, r, level + 1, cellset[r].IDs[level], cellset[r].childIDs[level]);
    if (root->child[k] == NULL) print_error("Memory exhausted");
    root->child[k]->prevChildID=cellset[r].prevChildIDs[level];
    root->child[k]->prevID=cellset[r].prevIDs[level];
    tempChildIDs+=cellset[r].childIDs[level];
    tempQ.push(root->child[k]);
    k++;
    root->children = k;

    //tempChildIDs += hash_input_fingerprint;

/*
    bool isUnique = true;
    int placeHolder=-1;
    if (uniqueChildIDs[0]==0){
        uniqueChildIDs[0]=tempChildIDs;
    }
    else{
        for (int j=0;j<MAX_TNODECHILD;j++){
            if (uniqueChildIDs[j]==tempChildIDs || tempChildIDs==0)
                isUnique=false;
            if (placeHolder==-1 && uniqueChildIDs[j]==0)
                placeHolder=j;
        }
    }

    if (isUnique)
    {
        uniqueChildIDs[placeHolder]=tempChildIDs;
        while(!tempQ.empty()){
            struct tnode *troot = tempQ.front();
            tempQ.pop();
            q.push(troot);
        }
    }
*/

    while(!tempQ.empty()){
        struct tnode *troot = tempQ.front();
        tempQ.pop();
        q.push(troot);
    }

    //q.push(root->child[k]);

    ComputeInputandOutputfingerprint(root);

    if (debug)
    {
        //out << "Level -- " << level << printRegionNode(root)
        out << "Level " << level << " : Node ID = " << root->ID << "("; printRegions(root); out << ") : Range = " << l << "-" << r << " : Fingerprint = [";
        for (int i = 0; i < (int)(root->fingerprint.size() - 1); i++)
            out << root->fingerprint[i] << " ";
        if(root->fingerprint.size() > 0)
		out << root->fingerprint[root->fingerprint.size()-1];
	out<<"]"<<endl;
    }
    


    ConstructAlgorithmTree(q);
}

void AutoGenerator::ComputeInputandOutputfingerprint(struct tnode* root)
{
    //int max = (1 << d), inputsign[MAX_SIZE], input_fingerprint[MAX_SIZE], ID;
    int ID = root->childID;
    int max = (1 << d);
    int absolute_region_IDs[MAX_SIZE];
    int regiontuple[MAX_SIZE][MAX_D];
    int store;
    int flag = 0;
    int level = root->level;

    // add output fingerprint to fingerprint
    root->fingerprint.clear();
    for (int i = 0; i < root->children; i++)
        root->fingerprint.push_back(root->child[i]->childID);
    sort(root->fingerprint.begin(), root->fingerprint.end());

    unsigned long long number = cellset[root->r].IDs[level - 1];
    unsigned long long maxnumber = 1 << ((level-1)*d); // is this level or level-1 ?

    // find input fingerprint
    int input_fingerprint[MAX_SIZE];
    input_fingerprint[0] = 0;
    int max_fvalue = input_fingerprint[0];

    for (int i = (size - 1); i >= 0; i--)
    {
        absolute_region_IDs[i] = number % maxnumber;
        number = number / maxnumber;
    }

    //vector<int> nodes = findCombinedNodes(root->l, root->r, level);
    //cout << cellset[root->l].IDs[level] << " " << cellset[root->l].prevIDs[level] << " " << cellset[root->l].childIDs[level] << " " << cellset[root->l].prevChildIDs[level] << " " << nodes.size() << endl;
    if (cellset[root->r].IDs[level - 1] != cellset[root->l].prevIDs[level - 1])             // if there are combined nodes
    {
        for (int i = 0; i < size; i++)
            input_fingerprint[i] = 0;
    }
    else                                                                            // if there are no combined nodes
    {
        for (int i = 1; i < size; i++)
        {
            int flag = 0;
            for (int j = 0; j < i; j++)
                if (absolute_region_IDs[i] == absolute_region_IDs[j])
                {
                    flag = 1;
                    input_fingerprint[i] = input_fingerprint[j];
//                    if (input_fingerprint[i] > max_fvalue)
//                        max_fvalue = input_fingerprint[i];
                    break;
                }
            if (flag == 0)
            {
                max_fvalue = max_fvalue + 1;
                input_fingerprint[i] = max_fvalue;
            }
        }
    }

    // add input fingerprint to fingerprint
    for (int i = 0; i < size; i++)
        root->fingerprint.push_back(input_fingerprint[i]);

    hash_input_fingerprint = FindUniqueLongValue(size, input_fingerprint, size);
}
void AutoGenerator::AlgorithmTreeConstruction()
{
    struct tnode* root = newtnode(0, count - 1, 1, 0, 0); // DEBUG
    q.push(root);
    head = root;

    if (debug)
        { out << "Levels = " << levels << endl; }
    ConstructAlgorithmTree(q);
    if (debug) { out << endl; }
}

int AutoGenerator::DetermineUnfoldingLevel(int numProcesses, struct tnode* root)
{
	int expTasksMax = numProcesses * numProcesses;
	int totalNodesAtLevel = 1;
	vector<struct tnode*> tmpQ;
	int prevLevel = 0;
	tmpQ.push_back(root);
	while(!tmpQ.empty())
	{
		struct tnode* node = tmpQ.front();
		if(node->level != prevLevel)
		{
			if(totalNodesAtLevel >= expTasksMax)
				break;
			totalNodesAtLevel = 0;
			prevLevel  = node->level;
		}
		tmpQ.erase(tmpQ.begin());
		totalNodesAtLevel += node->children;
		for(int i=0;i<node->children;i++)
		{
			tmpQ.push_back(node->child[i]);
		}
	}
	//TODO: remove the upper bound later.
	if(prevLevel > levels-2)
	{
		prevLevel = levels-2;
	}
	//to handle no unfolding
	if(prevLevel == 0)
		prevLevel = 1;
	//printf("Unfolding to level:%d Num Nodes:%d\n",prevLevel, totalNodesAtLevel);
	outdep<<prevLevel<<endl;
	return prevLevel;
}

void AutoGenerator::PrintReadAndWriteRegionsOfLeaves()
{
	unFoldingLevel = DetermineUnfoldingLevel(numProcesses, head);

	while (!q.empty())
		q.pop();

	GetLeaves(head);
	assert(q.size() >= numProcesses); 
	while(!q.empty())
	{
		struct tnode* leafNode = q.front();
		q.pop();
		GetFunctionCallInfo(fnCalls,leafNode);
	}
	
	vector<vector<FunctionCall*> > tasks;
	for(int i=0;i<fnCalls.size();i++)
	{
		int writeRegionID = ((fnCalls[i])->params)[0].portID;
		bool doMerge = false;
		for(int j=0;j<tasks.size();j++)
		{
			vector<FunctionCall*>& tmpFnCalls = tasks[j];
			for(int k=0;k<tmpFnCalls.size();k++)
			{
				int tmpWriteRegionID = ((tmpFnCalls[k])->params)[0].portID;
				if(tmpWriteRegionID == writeRegionID)
				{
					doMerge = true;
					break;
				}
			}
			if(doMerge)
			{
				tmpFnCalls.push_back(fnCalls[i]);
				break;
			}
		}
		if(!doMerge)
		{
			vector<FunctionCall*> m;
			m.push_back(fnCalls[i]);
			tasks.push_back(m);
		}	
	}

	outdep<<tasks.size()<<endl;
	
	vector<vector<vector<int> > > inDeps;
	for(int i=0;i<tasks.size();i++)
	{
		vector<FunctionCall*>& tmpFnCalls = tasks[i];
		vector<vector<int> > inDepFnCall;
		for(int j=0;j<tmpFnCalls.size();j++)
		{
			vector<int> inDepArgs;
			FunctionCall* fnCall = tmpFnCalls[j];
			for(int k=1;k<fnCall->params.size();k++)
			{
				int readRegion  = (fnCall->params[k].portID);
					
				/*char t[256];
				readRegion->PrintStr(t);
				printf("read region: %s ",t);*/
				bool writeTaskFound = false;
				for(int portSearchIndex=0;portSearchIndex<tasks.size();portSearchIndex++)
				{
					vector<FunctionCall*>& tmpFnCalls2 = tasks[portSearchIndex];
					for(int fnCallSearchIndex=0;fnCallSearchIndex<tmpFnCalls2.size();fnCallSearchIndex++)
					{
						FunctionCall* fnCall2 = tmpFnCalls2[fnCallSearchIndex];
						int writeRegion = (fnCall2->params[0].portID);
						if((writeRegion == readRegion) && (i != portSearchIndex))
						{
							writeTaskFound = true;
							//printf("port: %d \n",portSearchIndex+1);
							inDepArgs.push_back(portSearchIndex+1);
							break;
						}
					}
					if(writeTaskFound)
						break;
				}
				if(!writeTaskFound)
				{
					inDepArgs.push_back(-1);
					//printf("port: {}\n");
				}
			}
			inDepFnCall.push_back(inDepArgs);
		}
		inDeps.push_back(inDepFnCall);
	}
	
	/*printf("in Dependencies\n");
	for(int i=0;i<inDeps.size();i++)
	{
		vector<vector<int> >& inDepFnCall = inDeps[i];
		for(int j=0;j<inDepFnCall.size();j++)
		{
			vector<int>& inDepArg = inDepFnCall[j];
			for(int k=0;k<inDepArg.size();k++)
			{
				printf("%d ",inDepArg[k]);
			}
			printf(";");
		}
		printf("\n");
	}*/
	
	vector<vector<int> > outDeps;
	for(int i=0;i<tasks.size();i++)
	{
		vector<int> deps;
		bool readTaskFound = false;
		for(int j=0;j<inDeps.size();j++)
		{
			vector<vector<int> >& taskInDeps = inDeps[j];
			for(int k=0;k<taskInDeps.size();k++)
			{
				vector<int>& fnInDeps = taskInDeps[k];
				if(find(fnInDeps.begin(),fnInDeps.end(),i+1)!=fnInDeps.end())
				{
					readTaskFound = true;
					deps.push_back(j+1);
					break;
				}
			}
		}
		
		if(!readTaskFound)
			deps.push_back(-1);

		outDeps.push_back(deps);
	}
		
	/*printf("out Dependencies\n");
	for(int i=0;i<outDeps.size();i++)
	{
		const vector<int>& outDepTask = outDeps[i];
		for(int j=0;j<outDepTask.size();j++)
		{
			printf("%d ",outDepTask[j]);
		}
		printf("\n");
	}*/
	
	for(int i=0;i<tasks.size();i++)
	{
		vector<vector<int> >& inDepsTask = inDeps[i];
		vector<FunctionCall*>& task = tasks[i];
		for(int j=0;j<task.size();j++)
		{
			vector<int>& inDepsFunction = inDepsTask[j];
			FunctionCall* fnCall = task[j];
			//printf("%c(",fnCall->functionName);
			outdep<<fnCall->functionName<<"(";
			for(int k=0;k<fnCall->params.size();k++)
			{
				char t[1024];
				int arg = (fnCall->params[k].portID);
				sprintf(t,"%d",arg);
				string str("{"); str+=string(t); str+=string("}=");
				//printf("%s",str.c_str());
				outdep<<str;
				if(k==0)
				{
					vector<int>& outDepsTask = outDeps[i];
					//printf("{");
					outdep<<"{";
					for(int l=0;l<outDepsTask.size()-1;l++)
					{
						if(outDepsTask[l] !=-1)
							//printf("%d,",outDepsTask[l]);
							outdep<<outDepsTask[l]<<",";
					}
					if(outDepsTask[outDepsTask.size()-1] !=-1)
							//printf("%d",outDepsTask[outDepsTask.size()-1]);
							outdep<<outDepsTask[outDepsTask.size()-1];
					//printf("}");
					outdep<<"}";
				}
				else
				{
					if(inDepsFunction[k-1] != -1)
						//printf("{%d}",inDepsFunction[k-1]);
						outdep<<"{"<<inDepsFunction[k-1]<<"}";
					else
						//printf("{}");
						outdep<<"{}";
				}
				if((k+1)<fnCall->params.size())
					//printf(" ");
					outdep<<" ";
			}	
			//printf(");");
			outdep<<");";
		}
		//printf("\n"); 
		outdep<<endl;
	}
	
}

void AutoGenerator::GetLeaves(struct tnode* node)
{
	if(node->level >= unFoldingLevel) 
	{
		q.push(node);
		return;
	}

	for(int i=0;i<node->children;i++)
	{
		GetLeaves(node->child[i]);
	}
}

void AutoGenerator::GetFunctionCallInfo(vector<FunctionCall*>& fnCalls, struct tnode* node)
{
	FunctionCall* fnCall = new FunctionCall();
	unsigned long long maxnumber = 1 << ((node->level-1)*d); // is this level or level-1 ?
	fnCall->functionName=functions.find(node->fingerprint)->second+65;
	/*int absolute_region_IDs[MAX_SIZE];
	unsigned long long prevID = node->prevID;
	for (int i = (size - 1); i >= 0; i--)
	{
		absolute_region_IDs[i] = prevID % maxnumber;
		prevID = prevID / maxnumber;
	}
	out<<"Leaf ID:"<<node->ID<<"(";
	for (int i = 0; i <size-1; i++)
	{
		out<<absolute_region_IDs[i]<<",";
	}
	out<<absolute_region_IDs[size-1]<<")\t";*/

	vector<unsigned long long> combinedNodes = findCombinedNodesByID(node->l,node->r,node->level);
	set<int> uniqRegionsRead;
	int tempRead[MAX_SIZE];
	for(int k=0;k<combinedNodes.size();k++)
	{
        	unsigned long long prevID =combinedNodes[k];
        	for (int i = (size - 1); i >=0; i--)
        	{
		    tempRead[i] = prevID % maxnumber;
		    prevID = (prevID - tempRead[i]) / maxnumber;
        	}
		uniqRegionsRead.insert(tempRead+1,tempRead+size);
		if(uniqRegionsRead.size() == (size-1))
		{
			break;
		}
	}
	
	set<int>::iterator iter = uniqRegionsRead.find(tempRead[0]);
	if(iter != uniqRegionsRead.end())
		uniqRegionsRead.erase(iter);
	

	vector<int> portIDs;
	portIDs.push_back(tempRead[0]);
	if(uniqRegionsRead.size()  != (size-1))
	{
		int numAppendsRequired = size-1- uniqRegionsRead.size();
		portIDs.insert(portIDs.end(), uniqRegionsRead.begin(),uniqRegionsRead.end());
		vector<int>::iterator iter = find(portIDs.begin()+1,portIDs.end(),tempRead[0]);
		if(iter == portIDs.end())
			iter = portIDs.begin();
		while(numAppendsRequired)
		{
			vector<int>::iterator newIter = portIDs.insert(iter,tempRead[0]);
			iter = newIter;
			numAppendsRequired--;
		}
	}
	else
		portIDs.insert(portIDs.end(), uniqRegionsRead.begin(),uniqRegionsRead.end());

	assert(portIDs.size() == size);
	Parameter p;
	for(int i=0;i<portIDs.size();i++)
	{
		p.portID = portIDs[i];
		fnCall->params.push_back(p);
	}

	fnCalls.push_back(fnCall);
	/*out << functionName<<"(<"<<tempRead[0]<<",";
	iter = uniqRegionsRead.begin();
	int i;
	for(i=1;i<size-1;i++)
	{
		out<<*iter<<",";
		if(*iter == tempRead[0])
		{
			if(appendWriteRegion)
				appendWriteRegion =false;
			else
				iter++;
		}
		else
			iter++;
		if(iter == uniqRegionsRead.end())
			break;
	}
	out<<*iter<<">)"<<endl;*/
    	
	/*out << "(";
    	for(int k=0;k<combinedNodes.size()-1;k++)
	{
		int temp[MAX_SIZE];
        	unsigned long long prevID =combinedNodes[k];
        	for (int i = (size - 1); i >= 0; i--)
        	{
		    temp[i] = prevID % maxnumber;
		    prevID = (prevID - temp[i]) / maxnumber;
        	}
		out<<"<";
        	for (int i = 0; i <size-1; i++)
			out<<temp[i]<<",";
		out<<temp[size-1]<<">, ";
    	}
	{
		int temp[MAX_SIZE];
        	unsigned long long prevID =combinedNodes[combinedNodes.size()-1];
        	for (int i = (size - 1); i >= 0; i--)
        	{
		    temp[i] = prevID % maxnumber;
		    prevID = (prevID - temp[i]) / maxnumber;
        	}
		out<<"<";
        	for (int i = 0; i <size-1; i++)
			out<<temp[i]<<",";
		out<<temp[size-1]<<">";
    	}
	out<<")"<<endl;*/
}

void AutoGenerator::RemoveCopyFunctions()
{
    while (!q.empty())
        q.pop();
    q.push(head);
    DeleteCopyFunctions(q);
}
void AutoGenerator::DeleteCopyFunctions(queue<struct tnode*> q)
{
    if (q.empty())
        return;
    struct tnode *root = q.front();
    q.pop();
    if (root->level >= (levels - 1))
        return;

    // scan all child nodes and retain only those functions that are not copy functions in childnodes
    queue<struct tnode*> childnodes;
    int k = 0;
    for (int i = 0; i < root->children; i++)
    {
        struct tnode *child = root->child[i];
        if (copy_functions == 0 && child->children < (1 << (d)))
            continue;
        childnodes.push(child);
        k++;
    }
    // copy the child nodes to root
    for (int i = 0; i < root->children; i++)
    {
        if (i < k)
        {
            struct tnode *child = childnodes.front();
            childnodes.pop();
            root->child[i] = child;
            q.push(child);
        }
        else
        {
            root->child[i] = NULL;
        }
    }
    root->children = k;

    ComputeInputandOutputfingerprint(root);
    DeleteCopyFunctions(q);
}


/*************************************** LABELING ********************************************/

void AutoGenerator::AlgorithmTreeLabeling()
{
    initializequeue();
    LabelAlgorithmTree(q);
}
void AutoGenerator::initializequeue()
{
    while (!q.empty())
        q.pop();
    q.push(head);
}
void AutoGenerator::LabelAlgorithmTree(queue<struct tnode*> q)
{
    if (q.empty())
        return;
    struct tnode *root = q.front();
    q.pop();
    int level = root->level;
    if (flag[level - 1] == 0)
        return;

    if (level >= (levels - 1))
        return;

/*    for (int i = 0; i < root->children; i++)
        q.push(root->child[i]); */

    if (root->fingerprint.empty() == false)
    {
        if (functions.find(root->fingerprint) == functions.end())
        {
            flag[level] = 1;
            functions.insert(make_pair(root->fingerprint, functionlabel));
            root->functionname = functionlabel+65;
            functionlabel++;
	    recFunctions.insert(make_pair(root->functionname,root));

            for (int i = 0; i < root->children; i++)
               q.push(root->child[i]);
        }
        else
            root->functionname = functions.find(root->fingerprint)->second+65;
    }

    LabelAlgorithmTree(q);
    //usleep(10000);
}


/*************************************** DISPLAY FUNCTIONS ********************************************/
vector<int> AutoGenerator::findCombinedNodes(int l, int r, int level){
    vector<int> nodes;
    for (int i=l; i<=r;i++){
        int pcID=(int)cellset[i].prevChildIDs[level-1];
        if (find(nodes.begin(),nodes.end(),pcID)==nodes.end())
            nodes.insert(nodes.end(),pcID);
    }
    return nodes;

}


void AutoGenerator::printnode(struct tnode* root){
    int max = (1 << d);
    int temp[MAX_SIZE];
    int regiontuple[MAX_SIZE][MAX_D];
    int store;
    vector<int> nodes;
    //out << root->l << "," << root->r << endl;

    nodes = findCombinedNodes(root->l,root->r,root->level);
    int nodesCombined = nodes.size();
    out << "(";
    for(int k=0;k<nodesCombined;k++){
        int ID =nodes[k];

        for (int i = (size - 1); i >= 0; i--)
        {

            temp[i] = ID % max;
            store = temp[i];
            for (int j = (d - 1); j >= 0; j--)
            {
                regiontuple[i][j] = store % 2;
                store = (store - regiontuple[i][j]) / 2;

            }
            ID = (ID - temp[i]) / max;
        }
        out << "<";
        for (int i = 0; i < size; i++)
        {
            out << temp[i] << ":";
            for (int j = 0; j < d; j++)
                out << regiontuple[i][j];
            if(i != size-1)
            out << ",";
            //out << temp[i] << ",";
        }
        //out << temp[size-1];
        out << ">";
    }
    out << ")";
}

void AutoGenerator::printRegions(struct tnode *root)
{
    int absolute_region_IDs[MAX_SIZE];
    unsigned long long number = cellset[root->r].IDs[root->level - 1];
    //unsigned long long number = root->ID;                  // check this.
    unsigned long long maxnumber = 1 << ((root->level -1 )*d); // is this level or level-1 ?
    for (int i = (size - 1); i >= 0; i--)
    {
        absolute_region_IDs[i] = number % maxnumber;
        number = number / maxnumber;
    }
    for (int i = 0; i < size - 1; i++)
        out << absolute_region_IDs[i] << ",";
    out << absolute_region_IDs[size - 1];
}

void AutoGenerator::PrintFunctions(queue<struct tnode*> q, int height)
{
    if (q.empty())
        return;

    struct tnode *root = q.front();
    q.pop();
    int level = root->level;

    if (level > height)
        return;
    if (flag[level - 1] == 0)
        return;
    int sortSize;
    if (functions.find(root->fingerprint) != functions.end())
    {
        out << root->functionname << "[" << root->ID << ":"; printRegions(root); out << "]";
        printnode(root);
        out << " -> ";
        for (int i = 0; i < root->children; i++)
        {
            q.push(root->child[i]);
            out << root->child[i]->functionname << "[" << root->child[i]->ID << ":"; printRegions(root->child[i]); out << "]";
            printnode(root->child[i]);
            out <<" ";
        }
        out << endl;
        out << "  Fingerprint of the function = ";
        for (int i = 0; i < root->fingerprint.size(); i++)
            out << root->fingerprint[i] << " ";
        out << endl << endl;
        functions.erase(root->fingerprint);
    }

    PrintFunctions(q, height);
}

void AutoGenerator::DisplayFunctions()
{
    initializequeue();
    int height = 1;
    for (int i = 0; i < levels; i++)
        if (flag[i] == 1)
            height = i;

    PrintFunctions(q, height);
    //print dag
    out << endl << endl;
}

/*************************************** DAG CONSTRUCTION ********************************************/

void AutoGenerator::ConstructDAG(queue<struct tnode*> q){
    if (q.empty())
        return;
    struct tnode *root = q.front();
    q.pop();

    if (find(dagFunctions.begin(),dagFunctions.end(),root->fingerprint) == dagFunctions.end())
    {
        dagFunctions.insert(dagFunctions.end(),root->fingerprint);


            for (int i = 0; i < root->children; i++)
            {
                //out << root->child[i]->functionname;
                //out << "<" << root->child[i]->prevChildID << ">";
                StoreReadAndWrite(root->child[i]);
                q.push(root->child[i]);
            }
        if (debug_dag) { out << endl; }

        //create all dagEdges
        for (int i = 0; i < root->children; i++){
            for (int j = 0; j < root->children; j++){
                //don't want to add itself to its dagEdges
                if (i == j) continue;

                tnode* t = root->child[i];
                tnode* u = root->child[j];

                //don't want to add a dagEdge more than once
                if (find(t->dagEdge.begin(), t->dagEdge.end(),u) != t->dagEdge.end()) continue;

                //if t is a dagedge of u, u can't be a dagedge of t
                if(find(u->dagEdge.begin(),u->dagEdge.end(),t)!=u->dagEdge.end()) continue;

                //rule #2
                else if (t->write != u->write and find(u->read.begin(),u->read.end(),t->write)!=u->read.end())
                    t->dagEdge.insert(t->dagEdge.end(),u);

                //rule #3
                else if (t->write == u->write and t->isFlexible and !u->isFlexible)
                    t->dagEdge.insert(t->dagEdge.end(),u);

                //rule #4
                else if (t->write == u->write and t->isFlexible and u->isFlexible)
                    t->dagEdge.insert(t->dagEdge.end(),u);

            }
        }
        //rule #8
        for (int i = 0; i < root->children; i++){
            if(root->child[i]->dagEdge.size()<=1) continue;

            for(int j=0;j<root->child[i]->dagEdge.size();j++){
                for(int k=0;k<root->child[i]->dagEdge.size();k++){
                    struct tnode* t= root->child[i]->dagEdge[j];
                    struct tnode* u= root->child[i]->dagEdge[k];
                    if (find(u->dagEdge.begin(),u->dagEdge.end(),t)!=u->dagEdge.end()){
                        root->child[i]->dagEdge.erase(root->child[i]->dagEdge.begin()+j);
                        j--;
                        break;
                        }
                }
            }
        }

        //find the starting tuple for the DAG
        vector<struct tnode*> firstDAGs;
        //firstDAGs.insert(firstDAGs.end(),root);
        for (int i = 0; i < root->children; i++){
            int check=0;
            for (int j=0;j<root->children;j++){
                if(i==j)
                    continue;
                tnode* t= root->child[i];
                tnode* u=root->child[j];
                if(find(u->dagEdge.begin(),u->dagEdge.end(),t)==u->dagEdge.end())
                    check++;
            }
            if (check==root->children-1)
                firstDAGs.insert(firstDAGs.end(),root->child[i]);
        }

        //printing out DAG
        deque<struct tnode*> dagQ;
        if (firstDAGs.size()>0){
            if (debug_dag) { out << "[Function "<<root->functionname<< "]-> {"; }
            for (int i=0;i<firstDAGs.size()-1;i++)
            {
                if (debug_dag) {
                out << firstDAGs[i]->functionname;
                printnode(firstDAGs[i]);
                out << ", "; }
                dagQ.push_back(firstDAGs[i]);
            }
            if (debug_dag) {
            out << firstDAGs[firstDAGs.size()-1]->functionname;
            printnode(firstDAGs[firstDAGs.size()-1]);
            out << "}";
            }
            dagQ.push_back(firstDAGs[firstDAGs.size()-1]);
            if (debug_dag) { out <<endl; }
        }
        while(!dagQ.empty()){
            struct tnode *dagRoot = dagQ.front();
            dagQ.pop_front();
            if(dagRoot->dagEdge.size()==0)
                continue;
            if (debug_dag) {
            out << dagRoot->functionname;
            printnode(dagRoot);
            out << "-> {";
            }
            for (int i=0;i<dagRoot->dagEdge.size()-1;i++){
                if (debug_dag) {
                out << dagRoot->dagEdge[i]->functionname;
                printnode(dagRoot->dagEdge[i]);
                out << ", ";
                }
                if (find(dagQ.begin(),dagQ.end(),dagRoot->dagEdge[i])==dagQ.end())
                    dagQ.push_back(dagRoot->dagEdge[i]);
            }
            if (debug_dag) {
            out << dagRoot->dagEdge[dagRoot->dagEdge.size()-1]->functionname;
            printnode(dagRoot->dagEdge[dagRoot->dagEdge.size()-1]);
            out << "}";
            }
            if (find(dagQ.begin(),dagQ.end(),dagRoot->dagEdge[dagRoot->dagEdge.size()-1])==dagQ.end())
                dagQ.push_back(dagRoot->dagEdge[dagRoot->dagEdge.size()-1]);
            if (debug_dag) {
            out <<endl;
            }

        }
        if (debug_dag) {
        out << endl;
        }

    }
    ConstructDAG(q);

}

vector<unsigned long long> findCombinedNodesByID(int l, int r, int level){
    vector<unsigned long long> nodes;
    for (int i=l; i<=r;i++){
        unsigned long long pID=cellset[i].prevIDs[level-1];
        if (find(nodes.begin(),nodes.end(),pID)==nodes.end())
            nodes.insert(nodes.end(),pID);
    }
    return nodes;
}

void AutoGenerator::StoreReadAndWrite(struct tnode* root){
    int max = 1 << ((root->level-1)*d);
    unsigned long long temp[MAX_SIZE];
    /*unsigned long long regiontuple[MAX_SIZE][MAX_D];
    unsigned long long store;*/
    vector<unsigned long long> nodes;
    nodes = findCombinedNodesByID(root->l,root->r,root->level);
    int nodesCombined = nodes.size();
    //out << "(" << root->childID;
    for(int k=0;k<nodesCombined;k++){
        unsigned long long ID =nodes[k];
        for (int i = (size - 1); i >= 0; i--)
        {
            temp[i] = ID % max;
            /*store = temp[i];
            for (int j = (d - 1); j >= 0; j--)
            {
                regiontuple[i][j] = store % 2;
                store = (store - regiontuple[i][j]) / 2;
            }*/
            ID = (ID - temp[i]) / max;
        }
        //out << "<";
        for (int i = 0; i < size -1; i++)
        {
    //        for (int j = 0; j < d; j++)
    //            out << regiontuple[i][j];
    //        out << ",";
            //out << temp[i] << ",";
            if (i==0)
                root->write=temp[i];
            else
                root->read.insert(root->read.end(),temp[i]);
        }
    //    for (int j = 0; j < d; j++)
    //        out << regiontuple[size - 1][j];
        //out << temp[size-1];
        root->read.insert(root->read.end(),temp[size-1]);
        if(find(root->read.begin(),root->read.end(),root->write)!=root->read.end())
            root->isFlexible=false;
        else
            root->isFlexible=true;
        //out << ">";
    }
    //out << ")  ";

}

void AutoGenerator::DAGConstruction(){
    initializequeue();
    ConstructDAG(q);
    if (debug_dag) { out << endl; }
}

/*************************************** Output Construction ********************************************/

void AutoGenerator::printRegionNode(struct tnode* parent,struct tnode* root){
    int max = 1 << ((root->level-1)*d);
    int cmax = 1 << d;
    int temp[MAX_SIZE];
    int ctemp[MAX_SIZE];
    vector<unsigned long long> nodes;
    vector<int> cnodes;
    nodes = findCombinedNodesByID(root->l,root->r,root->level);
    cnodes = findCombinedNodes(root->l,root->r,root->level);
    int nodesCombined = cnodes.size();
    out << "(" ;
    int mod = 1 << (root->level - 1);
    int n=88;
    int j=0;

    set<string> varsUsed;
    for(int k=0;k<nodesCombined;k++)
    {
        unsigned long long ID =nodes[k];
        int cID = cnodes[k];
/*        for (int i = (size - 1); i >= 0; i--)
        {
            temp[i] = (int) ID % max;
            int n = cID % cmax;
            ID = (ID - temp[i]) / max;
            cID = (cID - n) / cmax;
            ctemp[i] = 11 + ((int)(n/2))*10 + n%2;
        }
 */
        int regtup[ MAX_SIZE ][ MAX_D ];
        int cregtup[ MAX_SIZE ][ MAX_D ];

        for (int i = (size - 1); i >= 0; i--)
        {
            temp[i] = ID % max; //change to cID?
            ctemp[i] = cID % cmax;

            int store = temp[i];
            for (int j = (d - 1); j >= 0; j--)
            {
                regtup[i][j] = store % 2;
                store = (store - regtup[i][j]) / 2;
            }
            ID = (ID - temp[i]) / max;

            int cstore = ctemp[i];
            for (int j = (d - 1); j >= 0; j--)
            {
                cregtup[i][j] = cstore % 2;
                cstore = (cstore - cregtup[i][j]) / 2;
            }
            cID = (cID - ctemp[i]) / cmax;
        }


        out << "<";
        for (int i = 0; i < size; i++)
        {
            int foo[MAX_D];
            int tempi = temp[ i ];

            for ( int j = d - 1; j >= 0; j-- )
               {
                 foo[ j ] = ( tempi % mod ) / 2;
                 tempi = tempi / mod;
               }

            for (int j=0;j<size;j++)
            {
                int k;

                for ( k = 0; k < d; k++ )
                  if ( foo[ k ] != parent->regionNums[ j ][ k ] ) break;

                if (k == d){
                    out << parent->regionChars[j] << "_" ;
		    ostringstream orthant;
                    for ( int k = 0; k < d; k++ )
                    {
			  out << cregtup[ i ][ k ];
			  orthant<<cregtup[i][k];
		    }
		 
				bool unique = true;
				string var(string(1,parent->regionChars[j])+orthant.str());
				//if(nodesCombined > 1)
				{
					pair<set<string>::iterator,bool> status = varsUsed.insert(var);
					unique = status.second;
				}				

				//if(unique)
					root->varsUsedInFunctionCall.push_back(var);

                    if(i != size-1)
				out << ",";
		    else
                    	out << ">";
                    break;
                }
            }
            //out << "(" << temp[i]/mod << "," << temp[i]%mod << ")," ;
        }
    }
    
    out << ")";

    map<char,struct tnode*>::iterator it = recFunctions.find(root->functionname);
    assert(it != recFunctions.end());	
    struct tnode* recFunction = it->second;
    if(root->varsUsedInFunctionCall.size() != recFunction->uniqParamIndx.size())
    {
	assert(root->varsUsedInFunctionCall.size() == recFunction->functionArgs.size());
	vector<string> tmpVarsUsed;
	for(unsigned int i=0;i<recFunction->uniqParamIndx.size();i++)
	{
		tmpVarsUsed.push_back(root->varsUsedInFunctionCall[recFunction->uniqParamIndx[i]]);
	}
	root->varsUsedInFunctionCall.clear();
	root->varsUsedInFunctionCall.insert(root->varsUsedInFunctionCall.begin(),tmpVarsUsed.begin(),tmpVarsUsed.end());
    }	
	
}

void AutoGenerator::printRegionNode(struct tnode* root)
{
    int max = 1 << ((root->level-1)*d);
    int temp[MAX_SIZE];
    vector<unsigned long long> nodes;
    nodes = findCombinedNodesByID(root->l,root->r,root->level);
    int nodesCombined = nodes.size();
    out << "(" ;

    // for all combined nodes in this node, find the absolute region IDs and store them in temp[]
    for(int k=0;k<nodesCombined;k++)
    {
        unsigned long long ID =nodes[k];

        // temp[] stores the region IDs
        for (int i = (size - 1); i >= 0; i--)
        {
            temp[i] = (int) ID % max;
            ID = (ID - temp[i]) / max;
        }
        out << "<";

        // check whether the region IDs match?
        for (int i = 0; i < size; i++)
        {
            int foo = temp[i];
            for (int j=0;j<size;j++){
                if (foo==root->regionIDs[j])
                {
                    out << root->regionChars[j];
		    if(i !=  size-1)
			out << ",";
                    break;
                }
            }
            //out << "(" << temp[i]/mod << "," << temp[i]%mod << ")," ;
        }
        out << ">";
    }
    out << ")";
}

void AutoGenerator::OutputCreation(queue<struct tnode*> q){
    if (q.empty())
        return;
    struct tnode *root = q.front();
    q.pop();

    if (find(outFunctions.begin(),outFunctions.end(),root->fingerprint) == outFunctions.end())
    {
        vector<struct tnode*> firstDAGs;
        for (int i = 0; i < root->children; i++)
        {
            int check=0;
            for (int j=0;j<root->children;j++){
                if(i==j)
                    continue;
                tnode* t= root->child[i];
                tnode* u=root->child[j];
                if(find(u->dagEdge.begin(),u->dagEdge.end(),t)==u->dagEdge.end())
                    check++;
            }
            if (check==root->children-1)
               {
                firstDAGs.insert(firstDAGs.end(),root->child[i]);
                root->child[i]->level_num = 0;
               }
        }

        if (firstDAGs.size()==0)
            return;
        outFunctions.insert(outFunctions.end(),root->fingerprint);

        //SetRegionLabels(root);

        int printThis = 0;

        if ( copy_functions || ( root == head ) || ( root->children >= ( 1 << d ) ) )
          {
            out << root->functionname;

            if (debug_output)
            {
            printRegionNode(root);
            out << endl;
            out << "If X is a small matrix then " << root->functionname << "loop";
            printRegionNode(root);
            out << endl << "else" << endl;
            }

            printThis = 1;
        }

        for(int i=0;i<root->children;i++){
            SetRegionLabels(root->child[i]);
            q.push(root->child[i]);
        }

        deque<vector<struct tnode*> > dagLevels;
        dagLevels.push_back(firstDAGs);

        int max_level_num = 0;

        while(!dagLevels.empty())
        {
            vector<struct tnode*> currentLevel = dagLevels.front();
            vector<struct tnode*> nextLevel;
            dagLevels.pop_front();

            for (int i=0;i<currentLevel.size();i++)
               {
                    for (int j=0;j<currentLevel.at(i)->dagEdge.size();j++)
                      {
                        if (find(nextLevel.begin(),nextLevel.end(),currentLevel.at(i)->dagEdge.at(j))==nextLevel.end())
                          {
                            nextLevel.insert(nextLevel.end(),currentLevel.at(i)->dagEdge.at(j));
                            currentLevel.at(i)->dagEdge.at(j)->level_num = currentLevel.at(i)->level_num + 1;
                         }
                     }
               }

           if (nextLevel.size()>0)
              {
               dagLevels.push_back(nextLevel);
               max_level_num++;
              }
           else
               break;
        }

	root->max_level_num = max_level_num;
       for ( int l = 0; l <= max_level_num; l++ )
         {
           int count = 0;

           for (int i = 0; i < root->children; i++)
               if ( root->child[i]->level_num == l )
                 {
                   if ( copy_functions || ( root->child[i]->level_num == 0 ) || ( root->child[i]->children >= ( 1 << d ) ) )
                      count++;
                 }

           if ( ( count > 1 ) && printThis )
             {
                if (debug_output) { out << "\t" << "parallel: "; }
             }

           int count2 = 0;

           for (int i = 0; i < root->children; i++)
               if ( root->child[i]->level_num == l )
                  {
                    if ( copy_functions || ( root->child[i]->level_num == 0 ) || ( root->child[i]->children >= ( 1 << d ) ) )
                      {
                        count2++;
                        if (debug_output && printThis) {
                           out << root->child[i]->functionname;
                           printRegionNode(root,root->child[i]);
                           if ( count2 < count ) out << ", ";
                           else out << endl;
                        }
                      }
                  }
         }


/*
        while(!dagLevels.empty())
        {
            vector<struct tnode*> currentLevel = dagLevels.front();
            vector<struct tnode*> nextLevel;
            dagLevels.pop_front();
            if(currentLevel.size()>1)
            {

                if (debug_output) { out << "\t" << "parallel: "; }

                for(int i=0;i<currentLevel.size() - 1;i++)
                {
                    if (debug_output) {
                    out << currentLevel.at(i)->functionname;
                    printRegionNode(root,currentLevel.at(i));
                    out << ", ";
                    }
                    for(int j=0;j<currentLevel.at(i)->dagEdge.size();j++){
                        if (find(nextLevel.begin(),nextLevel.end(),currentLevel.at(i)->dagEdge.at(j))==nextLevel.end()){
                        //nextLevel.insert(nextLevel.end(),currentLevel.at(i)->dagEdge.at(j));
                            if (nextLevel.empty())
                                nextLevel.insert(nextLevel.end(),currentLevel.at(i)->dagEdge.at(j));
                            else{
                                bool check =true;
                                for(int k=0;k<nextLevel.size();k++){
                                    if (find(nextLevel[k]->dagEdge.begin(),nextLevel[k]->dagEdge.end(),currentLevel.at(i)->dagEdge.at(j))!=nextLevel[k]->dagEdge.end())
                                        check=false;
                                }
                                if(check)
                                    nextLevel.insert(nextLevel.end(),currentLevel.at(i)->dagEdge.at(j));
                            }
                        }
                    }
                }

                if (debug_output)
                {
                out << currentLevel.at(currentLevel.size()-1)->functionname;
                printRegionNode(root,currentLevel.at(currentLevel.size()-1));
                }

                for(int j=0;j<currentLevel.at(currentLevel.size()-1)->dagEdge.size();j++)
                {
                    if (find(nextLevel.begin(),nextLevel.end(),currentLevel.at(currentLevel.size()-1)->dagEdge.at(j))==nextLevel.end()){
                        //nextLevel.insert(nextLevel.end(),currentLevel.at((currentLevel.size()-1))->dagEdge.at(j));
                        if (nextLevel.empty())
                            nextLevel.insert(nextLevel.end(),currentLevel.at((currentLevel.size()-1))->dagEdge.at(j));
                        else{
                            bool check =true;
                            for(int k=0;k<nextLevel.size();k++){
                                if (find(nextLevel[k]->dagEdge.begin(),nextLevel[k]->dagEdge.end(),currentLevel.at((currentLevel.size()-1))->dagEdge.at(j))!=nextLevel[k]->dagEdge.end())
                                    check=false;
                            }
                            if (check)
                                nextLevel.insert(nextLevel.end(),currentLevel.at((currentLevel.size()-1))->dagEdge.at(j));
                        }
                    }
                }
            }
            else
            {
                    if (debug_output) {
                out << "\t" << currentLevel[0]->functionname;
                printRegionNode(root,currentLevel[0]);
                    }
                for(int j=0;j<currentLevel.at(0)->dagEdge.size();j++){
                    if (find(nextLevel.begin(),nextLevel.end(),currentLevel.at(0)->dagEdge.at(j))==nextLevel.end()){
                        //nextLevel.insert(nextLevel.end(),currentLevel.at(0)->dagEdge.at(j));
                        if (nextLevel.empty())
                            nextLevel.insert(nextLevel.end(),currentLevel.at(0)->dagEdge.at(j));
                        else{
                            bool check=true;
                            for(int k=0;k<nextLevel.size();k++){
                                if (find(nextLevel[k]->dagEdge.begin(),nextLevel[k]->dagEdge.end(),currentLevel.at(0)->dagEdge.at(j))!=nextLevel[k]->dagEdge.end())
                                    check=false;
                            }
                            if(check)
                                nextLevel.insert(nextLevel.end(),currentLevel.at(0)->dagEdge.at(j));
                        }
                    }
                }
            }

        if (nextLevel.size()>0)
            //out << "\t" << nextLevel[0]->functionname << "par";
            dagLevels.push_back(nextLevel);
        else
            break;
        if (debug_output) { out << endl; }
        }
*/
        if (debug_output) { out << endl << endl; }
    }
    OutputCreation(q);
}

void AutoGenerator::SetRegionLabels(struct tnode* root){

    int max = 1 << ((root->level-1) * d);
    int temp[MAX_SIZE];
    vector<unsigned long long> nodes;
    nodes = findCombinedNodesByID(root->l,root->r,root->level);
    int nodesCombined = nodes.size();
    //out << "(" ;
    int mod = 1 << (root->level-1);
    int n='x';
    int j=0;

    // for all combined nodes
    for(int k=0;k<nodesCombined;k++)
    {
        unsigned long long ID =nodes[k];
        for (int i = (size - 1); i >= 0; i--)
        {
            temp[i] = (int) ID % max;
            ID = (ID - temp[i]) / max;
        }

        //out << "<";


        for (int i = 0; i < size; i++)
        {
            int foo[MAX_D];
            int tempi = temp[ i ];

            for ( int l = d - 1; l >= 0; l-- )
               {
                 foo[ l ] = tempi % mod;
                 tempi = tempi / mod;
               }

            bool newFoo=true;
            for (int k=0;k<j;k++){
                if (root->regionIDs[k]==temp[i]){
                    newFoo=false;
                }
            }
            if (newFoo==true){
                root->regionIDs[j]=temp[i];

                for ( int k = 0; k < d; k++ )
                   root->regionNums[ j ][ k ] = foo[ k ];

		root->regionChars[j]=(char) n;
		if((char)n=='z')
			n='a';
		else
			n++;
                /*if(j == 0)
			root->regionChars[j]= 'X';
		else
		{
			root->regionChars[j]=(char) n;
                	n++;
		}
                if(n==88)
                    n++;
		else if(n==91)
		    n=65;*/
                j++;
            }
	/*else
	{
                root->regionIDs[j]=temp[i];
                for ( int k = 0; k < d; k++ )
                   root->regionNums[ j ][ k ] = foo[ k ];
                root->regionChars[j]=(char) (88);
		j++;
	}*/
/*
        for (int i = 0; i < size; i++)
        {
            int foo[2] = {temp[i]/mod,temp[i]%mod};
            bool newFoo=true;
            for (int k=0;k<j;k++){
                if (root->regionIDs[k]==temp[i]){
                    newFoo=false;
                }
            }
            if (newFoo==true){
                root->regionIDs[j]=temp[i];
                root->regionNums[j][0]=foo[0];
                root->regionNums[j][1]=foo[1];
                root->regionChars[j]=(char) n;
                n--;
                if(n==87)
                    n--;
                j++;
            }
*/

            //out << "(" << temp[i]/mod << "," << temp[i]%mod << ")," ;
        }
        //out << "(" << temp[size-1]/mod << "," << temp[size-1]%mod << ")";
        //out << ">";
        for (int i = 0; i < size; i++)
        {
            int foo = temp[i];
            for (int j=0;j<size;j++){
                if (foo==root->regionIDs[j])
                {
			root->functionArgs.push_back(root->regionChars[j]);
                    break;
                }
            }
            //out << "(" << temp[i]/mod << "," << temp[i]%mod << ")," ;
        }

    }
	
    	set<char> uniqParams;
    	vector<char> ret;int i=0;
	for(vector<char>::iterator it=root->functionArgs.begin();it!=root->functionArgs.end();it++,i++)
	{
		pair<set<char>::iterator,bool> status = uniqParams.insert(*it);
		if(status.second)
		{
			ret.push_back(*it);
			root->uniqParamIndx.push_back(i);
		}
	}

    /*char start = 'x';
    root->functionArgs.push_back(start);
    set<char> uniqChars;
    uniqChars.insert(start);
	for(int i=0;i<j;i++)
		if(root->regionChars[i] != 'x')
		{
			pair<set<char>::iterator,bool> status = uniqChars.insert(root->regionChars[i]);
			if(status.second)
				root->functionArgs.push_back(root->regionChars[i]);
		}*/

	/*if(root->functionArgs.size() != size)
	{
		int numPendingArgs = size-root->functionArgs.size();
		char start = 'x';
		while(numPendingArgs)
		{
			if(start == 'z')
				start = 'a';
			else
				start++; 
			root->functionArgs.push_back(start);	
			numPendingArgs--;
		}
	}*/

	
    //out << ")  ";

}

void AutoGenerator::CreateOutput(){
    initializequeue();
    SetRegionLabels(head);
    OutputCreation(q);
    out << endl;
}

void AutoGenerator::Algorithm()
{
    printline("Step 1: Generating Cell-Set... ");
    if (debug_cellset) { out << "Step 1. Cell-set generation" << endl; }
    CellSetGeneration();
    printend();
    printline("Step 2: Constructing Algorithm-Tree... ");
    if (debug_tree) { out << "Step 2. Algorithm-tree construction" << endl; }
    AlgorithmTreeConstruction();
    printend();
//    RemoveCopyFunctions();
    printline("Step 3: Labeling Algorithm-Tree... ");
    if (debug_tree) { out << "Step 3. Algorithm-tree labeling" << endl; }
    AlgorithmTreeLabeling();
    
    if(depInfoRequested)
	PrintReadAndWriteRegionsOfLeaves();
    if (debug_tree) { DisplayFunctions(); }
    printend();
    printline("Step 4: Constructing DAG... ");
    if (debug_dag) { out << "Step 4. DAG Construction" << endl; }
    DAGConstruction();
    printend();
    printline("Step 5: Creating Output... ");
    if (debug_dag) { out << "Step 5. Output Creation" << endl << endl; }
    CreateOutput();
    GenerateFunctionDefs();
    printend();
}

/*************************************** MAIN PROGRAM ********************************************/

int main(int argc, char* argv[])
{
    clock_t start_time, end_time;
    float algorithm_time = 0;

    cout << "AUTOGEN ALGORITHM" << endl << endl;
    AutoGenerator autogenerator;
    autogenerator.Initialize(argc, argv);

    start_time = clock();
    autogenerator.Algorithm();
    end_time = clock();
    algorithm_time = ((float)end_time - (float)start_time) / CLOCKS_PER_SEC;
    cout << "Algorithm time = " << algorithm_time << endl;

    return 0;
}

void AutoGenerator::GenerateFunctionDefs()
{

	RecursionBase* base = GetRecursionBaseInstance(problem);
	if(!base)
	{
		printf("ERROR: Base case and initialization code of Problem instance %d not defined. exiting..\n",problem);
		exit(0); 
	}
	/*struct stat existStatus = {0};
	string directory("../DMCode");
	if (stat(directory.c_str(), &existStatus) == -1) 
	{
		printf("ERROR: Incorrect program install path\n");
		exit(0);
	}
    	else
	{
		directory += string("/")+string(problemnames[problem]);
		//directory += string("/Parenthesis");
		if (stat(directory.c_str(), &existStatus) == -1) 
		{
			printf("Directory %s does not exist. creating..\n",directory.c_str());
			mkdir(directory.c_str(), 0777);
		}
	}*/
	string directory(".");
	filebuf fbcpp;
	string cppFile(directory+string("/RecursiveFunctions.cpp"));
	fbcpp.open(cppFile.c_str(),ios::out);
	
	ostream oscpp(&fbcpp);

	pair<string,string> fnCallHierarchySummary = CreateFunctionCallHierarchySummary(head);
	
	string fnDeclarations("\n");
	
	oscpp<<"#include \"HelperFunctions.h\"\n";
	oscpp<<fnCallHierarchySummary.second;
	oscpp<<"long int inputSize[DIMENSION];\n";
	oscpp<<"map<int, vector<FunctionCall*> > fnCalls;\n";
	oscpp<<"extern int recursionDepth;\n";
	oscpp<<"int fnCounter=0;\n";
	oscpp<<"extern map<int,int> tileUpdateLog;\n";
	/*string deferredCall = GenerateDeferredCallTemplate();
	oscpp<<deferredCall;*/
	
	//User defined input method. User also defines the type of DP table cell returned as the header declaration.
	pair<string,string> inputMethods = base->ReadInput();
	filebuf globalTypesHeaderBuf;
	//string globalTypesFileName("../DMCode/common/GlobalTypes.h");
	string globalTypesFileName("GlobalTypes.h");
	globalTypesHeaderBuf.open(globalTypesFileName.c_str(),ios::out);
	ostream globalTypesHeader(&globalTypesHeaderBuf);
	globalTypesHeader<<"#pragma once\n";
	globalTypesHeader<<"#define DIMENSION "<<d<<"\n";
	globalTypesHeader<<"#define METADATASPACE (2*DIMENSION+1)\n";
	//Write the user defined DP Table Cell type into GlobalTypes.h. Default:int
	if(inputMethods.first.empty())
		globalTypesHeader<<"typedef int CELL_TYPE\n";
	else
		globalTypesHeader<<inputMethods.first;
	globalTypesHeader<<fnCallHierarchySummary.first;
	globalTypesHeader<<FetchCellFnCallCode();
	
	globalTypesHeaderBuf.close();

	pair<string,string> initDPTable = base->InitializeDPTable();
	fnDeclarations +=initDPTable.first;
	pair<string,string> resultsCode = GenerateResultsCode(base);
	fnDeclarations +=resultsCode.first;

	string macroDefns("\n");
	string recFunctionDefns("");
	string recFunctionDecls("");
	map<char, struct tnode*>::iterator it = recFunctions.begin();
	//for(unsigned int i=0;i<recFunctions.size();i++)
	for(;it!=recFunctions.end();it++)
	{
		pair<string,string> functionCode = GenerateFunctionBody(it->second, base);
		pair<string,string> functionCodeUnroll = GenerateFunctionBodyForUnroll(it->second, base);
		recFunctionDecls +=functionCodeUnroll.first;
		recFunctionDecls +=functionCode.first;
		macroDefns += GenerateParamListMacro(it->second);
		recFunctionDefns +=functionCodeUnroll.second;
		recFunctionDefns +=functionCode.second;
	}
	
	pair<string,string> topLevel = GenerateTopLevelCalls();
	fnDeclarations += topLevel.first;
	
	if(computeGrid == COMPUTE_UTM)
		oscpp<<"int computeGrid = COMPUTE_UTM;\n";
	else if(computeGrid == COMPUTE_FULL)
		oscpp<<"int computeGrid = COMPUTE_FULL;\n";
	else
		oscpp<<"int computeGrid = COMPUTE_SPARSE;\n";
	oscpp<<macroDefns;
	oscpp<<recFunctionDecls;
	oscpp<<inputMethods.second;
	oscpp<<initDPTable.second;
	oscpp<<resultsCode.second;
	oscpp<<recFunctionDefns;
	oscpp<<topLevel.second;

	fbcpp.close();
}

RecursionBase* AutoGenerator::GetRecursionBaseInstance(int problem)
{
	if(recBase)
		return recBase;
	RecursionBase* ret = NULL;
	switch(problem)
	{
		case PROBLEMMCM:
					{
						ret  = new Matrixchain();
					}
					break;
		case PROBLEMPARENTHESIS:
					{
						ret  = new Parenthesis();
					}
					break;
		case PROBLEM_4D_NUSSINOV:
					{
						ret = new Nussinov4D();
					}
					break;
		case PROBLEMSMITHWATERMAN:
				{
					ret = new SmithWaterman();
				}
				break;
		case PROBLEMNEEDLEMANWUNSCH:
				{
					ret = new NeedlemanWunsch();
				}
				break;
		case PROBLEMFLOYDWARSHALL3D:
				{
					ret = new FloydWarshall();
				}
				break;
		default:
			break;
	}
	recBase = ret;
	return ret;
}

pair<string,string> AutoGenerator::CreateFunctionCallHierarchySummary(struct tnode* root)
{
	string hdrDecl(""), cppDefn("");
	map<char, map<char, int> > fnCallHierarchySummary;
	
	vector<struct tnode*> tmpQ;
	tmpQ.push_back(root);
	while(!tmpQ.empty())
	{
		struct tnode* node = tmpQ.front();
		tmpQ.erase(tmpQ.begin());
		char fnName = node->functionname;
		if(fnCallHierarchySummary.find(fnName) != fnCallHierarchySummary.end())
			continue;
		map<char, int> child;
		for(int i=0;i<node->children;i++)
		{
			pair<map<char,int>::iterator,bool> status = child.insert(make_pair(node->child[i]->functionname,1));
			if(!status.second)
				((status.first)->second)++;
			else
				tmpQ.push_back(node->child[i]);
		}
		pair<map<char, map<char,int> >::iterator,bool> callTypeStatus = fnCallHierarchySummary.insert(make_pair(fnName,child));
		assert(callTypeStatus.second);
	}

	ostringstream ostrHdr;
	ostrHdr<<"extern int fnCallHierarchySummary["<<fnCallHierarchySummary.size()<<"]["<<fnCallHierarchySummary.size()<<"];\n";
	hdrDecl += ostrHdr.str();

	ostringstream ostr;
	ostr<<"int fnCallHierarchySummary["<<fnCallHierarchySummary.size()<<"]["<<fnCallHierarchySummary.size()<<"]={";
	map<char,map<char,int> >::iterator callTypeIter = fnCallHierarchySummary.begin();
	for(int i=0;callTypeIter!=fnCallHierarchySummary.end();callTypeIter++,i++)
	{
		ostr<<"{";
		map<char,int>::iterator childrenTypeIter = (callTypeIter->second).begin();
		map<char, map<char, int> >::iterator callTypeIter2 = fnCallHierarchySummary.begin();
		unsigned int j=0;
		for(j=0;callTypeIter2!=fnCallHierarchySummary.end();callTypeIter2++,j++)
		{
			if(childrenTypeIter->first == callTypeIter2->first)
			{
				ostr<<childrenTypeIter->second;
				childrenTypeIter++;
			}
			else
				ostr<<"0";
			if(j!=(fnCallHierarchySummary.size()-1))
				ostr<<",";
		}
		ostr<<"}";
		if(i != (fnCallHierarchySummary.size()-1))
			ostr<<",";	
	}
	ostr<<"};\n";
	cppDefn += ostr.str();
	return make_pair(hdrDecl,cppDefn);
}

string AutoGenerator::GenerateDeferredCallTemplate() const
{
	string defCall("");
	defCall +="template<int ...> struct Sequence{};\n";
	defCall +="template<int N, int ...S> struct Generate_Seq : Generate_Seq<N-1, N-1, S...> {};\n";
	defCall +="template<int ...S> struct Generate_Seq<0, S...>{ typedef Sequence<S...> type; }; //base case of recursive class template definition\n";
	defCall +="template <typename ...Args> struct DeferredCall {\n";
	defCall +="\tstd::tuple<Args...> params;\n";
  	defCall +="\tvoid (*fptr)(Args...);\n";
	defCall +="\tvoid Run()\n\t{\n";
    	defCall +="\t\t_Run(typename Generate_Seq<sizeof...(Args)>::type());\n";
	defCall +="\t\treturn;\n\t}\n\n";
	
	defCall +="\ttemplate<int ...S>\n";
	defCall +="\tvoid _Run(Sequence<S...>)\n";
  	defCall +="\t{\n\t\tfptr(std::get<S>(params)...);\n";
    	defCall +="\t\treturn;\n\t}\n};\n";

	return defCall;
}

pair<string,string> AutoGenerator::GenerateFunctionBodyForUnroll(const struct tnode* node, RecursionBase* base)
{
	string hdrDecl("");
	//function signature
	ostringstream sig;
	sig<<"void "<<node->functionname<<"_unroll(";
	vector<char> args = GenerateFunctionArgs(node);
	vector<string> argList;
	for(unsigned int j=0;j<args.size();j++)	
	{
		sig<<"Box* "<<args[j]<<", unsigned int parentTileID"<<args[j]<<", ";
		argList.push_back(string(1,args[j]));
		argList.push_back("parentTileID"+string(1,args[j]));
	}
	sig<<"int callStackDepth)";
	argList.push_back("callStackDepth");
	string signature = sig.str();
	hdrDecl += sig.str()+";\n";
	string stopCond("");
	string unfoldingStopCond = "\t"+GenerateStopCondForUnfolding(node,argList,d);
	stopCond += unfoldingStopCond;



	//function body
	map<char, set<string> > varsPendingDefinition;
	string ret("\n");

	for ( int l = 0; l <= node->max_level_num; l++)
	{
		for (int i = 0; i < node->children; i++)
		{
			if ( node->child[i]->level_num == l )
			{
				string fnCall = GenerateCallForUnroll(node,node->child[i], varsPendingDefinition, d);
				ret += fnCall;
				ret +="\n";
			}
		}
	}

	//variable declarations corresponding to the variables used in function body.
	string declarations = GenerateVarDefinitions(varsPendingDefinition);
	return make_pair(hdrDecl,signature+"\n{\n"+stopCond+"\n"+declarations+ret+"}\n\n");
}



pair<string,string> AutoGenerator::GenerateFunctionBody(const struct tnode* node, RecursionBase* base)
{
	string hdrDecl("");
	//function signature
	ostringstream sig;
	sig<<"void "<<node->functionname<<"(";
	vector<char> args = GenerateFunctionArgs(node);
	vector<string> argList;
	for(unsigned int j=0;j<args.size();j++)	
	{
		sig<<"Box* "<<args[j]<<", CELL_TYPE* "<<args[j]<<"Data, ";
		argList.push_back(string(1,args[j]));
		argList.push_back(string(1,args[j])+"Data");
	}
	sig<<"int callStackDepth)";
	argList.push_back("callStackDepth");
	string signature = sig.str();
	hdrDecl += sig.str()+";\n";
	string stopCond = base->StopCondition(argList);
	//string unfoldingStopCond = "\telse "+GenerateStopCondForUnfolding(node,argList);
	//stopCond += unfoldingStopCond;



	//function body
	map<char, set<string> > varsPendingDefinition;
	string ret("\n");

	vector<int> numParallelInvocations(node->max_level_num+1,0);
	for ( int l = 0; l <= node->max_level_num; l++)
		for (int i = 0; i < node->children; i++)
			if ( node->child[i]->level_num == l )
				(numParallelInvocations[l])++;

	for ( int l = 0; l <= node->max_level_num; l++)
		if(numParallelInvocations[l] == 1)
			numParallelInvocations[l]=0;
	

	for ( int l = 0; l <= node->max_level_num; l++)
	{
		for (int i = 0; i < node->children; i++)
		{
			if ( node->child[i]->level_num == l )
			{
				string fnCall = GenerateCall(node,node->child[i], varsPendingDefinition);
				(numParallelInvocations[l])--;
				if(numParallelInvocations[l] > 0)
					ret += "#ifdef PARALLEL\n\tcilk_spawn\n#endif\n"+ fnCall;
				else
				{
					ret += fnCall;
					if(numParallelInvocations[l] == 0)
						ret +="\n#ifdef PARALLEL\n\tcilk_sync;\n#endif\n";
				}
				ret +="\n";
			}
		}
	}
	if(node->functionname == 'A')
	{
		
		if(((varsPendingDefinition['X'].size() == (1<<d)-1)||(varsPendingDefinition['x'].size() == (1<<d)-1)))
			computeGrid = COMPUTE_UTM;
		else if((varsPendingDefinition['X'].size() == (1<<d))||(varsPendingDefinition['x'].size() == (1<<d)))
			computeGrid = COMPUTE_FULL;
		else
			computeGrid = COMPUTE_SPARSE;
	}
	//parallel execution up to a certain depth
	//variable declarations corresponding to the variables used in function body.
	string declarations = GenerateVarDefinitions(varsPendingDefinition);
	string serialCode = GenerateSerialCodeInFunctionBody(declarations,ret);
	stopCond += serialCode;
	//printf("%s\n",(signature+stopCond+declarations+ret).c_str());
	return make_pair(hdrDecl,signature+"\n{\n"+stopCond+"\n"+declarations+ret+"\treturn;\n}\n\n");
}

vector<char> AutoGenerator::GenerateFunctionArgs(const struct tnode* node)
{
#if 0
	set<char> args;
	for(unsigned int i=0;i<node->numRegionChars;i++)
		args.insert(node->regionChars[i]);
	
	if(args.size() != size)
	{
		char start = 'X';
		while(args.size() == size)
		{
			if(start == 'Z')
				start = 'A';
			else
				start++; 
			args.insert(start);	
		}
	}
	vector<char> ret(args.begin(),args.end());
	return ret;	
#endif
	
	vector<char> ret;
	for(unsigned int i=0;i<node->uniqParamIndx.size();i++)
		ret.push_back(node->functionArgs[node->uniqParamIndx[i]]);
	return ret;
	//return node->functionArgs;
}

string GenerateStopCondForUnfolding(const struct tnode* node, const vector<string>& fnArgs, const int dim)
{
	string functionName = string(1,node->functionname);
	string stopCond("");
	stopCond += "if("+fnArgs[fnArgs.size()-1]+" == recursionDepth)\n";
	stopCond+="\t{\n";

	ostringstream tmpStr;
	if(dim ==2)
	{
		if(computeGrid != COMPUTE_SPARSE)
			tmpStr<<"\t\tint writeTileID = GetTileID(parentTileID"<<fnArgs[0]<<");\n";
		else
			tmpStr<<"\t\tint writeTileID = GetTileID("<<fnArgs[0]<<");\n";
	}
	else
	{
		if(computeGrid != COMPUTE_SPARSE)
			tmpStr<<"\t\tint writeTileID = GetTileID"<<dim<<"D(parentTileID"<<fnArgs[0]<<");\n";
		else
			tmpStr<<"\t\tint writeTileID = GetTileID("<<fnArgs[0]<<");\n";
	}
	stopCond+=tmpStr.str();
	stopCond+="\t\tbool localUpdate = false;\n";
	stopCond+="\t\tFunctionCall* fnCall= nullptr;\n";
	stopCond+="\t\ttileUpdateLog[writeTileID]=fnCounter;\n";
	if(computeGrid != COMPUTE_SPARSE)
		stopCond+="\t\tif(IsLocalOwner(writeTileID))\n\t\t{\n";
	else
		stopCond+="\t\tif(true)\n\t\t{\n";
	stopCond+="\t\t\tCELL_TYPE* nullData=nullptr;\n";
	stopCond+="\t\t\tfnCall=new FunctionCall();\n";
	stopCond+="\t\t\tfnCall->functionName = '"+functionName+"';\n";
	stopCond+="\t\t\tParameter p;\n";

	string argList("");
	for(unsigned int j=0;j<fnArgs.size()-1;j+=2)	
	{
		ostringstream stopCondStr;
		stopCondStr<<"\t\t\tBox* b"<<j<<"=new Box(*"<<fnArgs[j]<<");\n"; 
		stopCondStr<<"\t\t\tp.data = b"<<j<<";\n";
		ostringstream tmpStr;
		tmpStr<<"b"<<j;
		argList += (tmpStr.str() +", nullData, ");
		stopCond += stopCondStr.str();
		stopCond +="\t\t\tfnCall->params.push_back(p);\n";
	}
	argList += fnArgs[fnArgs.size()-1]+"+1";

	stopCond +="\t\t\tstd::tuple<_paramListFunction"+functionName+"> t = std::make_tuple("+argList+");\n";
  	stopCond +="\t\t\tDeferredCall<_paramListFunction"+functionName+">* defdCall = new DeferredCall<_paramListFunction"+functionName+">();\n";
	stopCond +="\t\t\tdefdCall->params=t;\n\t\t\tdefdCall->fptr="+functionName+";\n";
	stopCond +="\t\t\tfnCall->fnCall = defdCall;\n";
	stopCond +="\t\t\tfnCall->ID = fnCounter;\n";
	stopCond +="\t\t\tif(fnCalls[writeTileID].size() > 0)\n\t\t\t{\n";
	stopCond +="\t\t\t\tfnCall->numInPorts +=1;\n";
	stopCond +="\t\t\t\tfnCall->wawSource = (fnCalls[writeTileID].back())->ID;\n"; 
	if(computeGrid != COMPUTE_SPARSE)
	{
		stopCond +="\t\t\t\tFunctionCall* lastFunctionToUpdate = fnCalls[writeTileID].back();\n";
		stopCond +="\t\t\t\tlastFunctionToUpdate->outPortOwners.insert(procRank);\n";
	}
	stopCond +="\t\t\t}\n\t\t\tfnCalls[writeTileID].push_back(fnCall);\n";
	stopCond +="\t\t\tlocalUpdate = true;\n\t\t}\n";
	stopCond +="\t\tfnCounter++;\n";
	
	if((fnArgs.size()-1)/2 > 1)
	{	
		ostringstream tmpStr;
		tmpStr <<"\t\tint readTileIDs["<<((fnArgs.size()-1)/2) -1<<"];\n";
		for(int i=1;i<(fnArgs.size()-1)/2;i++)
			tmpStr <<"\t\treadTileIDs["<<i-1<<"]=GetTileID(parentTileID"<<fnArgs[2*i]<<");\n";	
		tmpStr<<"\t\tfor(int i=0;i<"<<((fnArgs.size()-1)/2)-1<<";i++)\n\t\t{\n";
		tmpStr<<"\t\t\tint readTileID=readTileIDs[i];\n";
		tmpStr<<"\t\t\tif(readTileID != writeTileID)\n\t\t\t{\n";
		tmpStr<<"#ifdef TASK_AGGREGATION\n";
		tmpStr<<"\t\t\t\tif(fnCalls[readTileID].size() > 0)\n";	
		tmpStr<<"\t\t\t\t\t(fnCalls[readTileID].back())->isReadBeforeNextWrite = true;\n";
		tmpStr<<"#endif\n";			
		tmpStr<<"\t\t\t\tif(localUpdate)\n\t\t\t\t{\n";
		tmpStr<<"\t\t\t\t\tfnCall->numInPorts +=1;\n";
		tmpStr<<"\t\t\t\t\tfnCall->params[i+1].portID = tileUpdateLog[readTileID];\n\t\t\t\t}\n";
		if(computeGrid != COMPUTE_SPARSE)
		{
			tmpStr<<"\t\t\t\tif(fnCalls[readTileID].size() > 0)\n\t\t\t\t{\n";
			tmpStr<<"\t\t\t\t\tFunctionCall* lastFunctionToUpdate = fnCalls[readTileID].back();\n";
			tmpStr<<"\t\t\t\t\tlastFunctionToUpdate->outPortOwners.insert(GetOwner(writeTileID));\n\t\t\t\t}\n";
		}
		tmpStr<<"\t\t\t}\n";
		tmpStr<<"\t\t}\n";
		stopCond += tmpStr.str();
	}
	
	stopCond +="\t\treturn;\n\t}\n";


	return stopCond;

}

/* varsPendingDeginition contains variables that are used (read/written) in a recursive function body.
 * A variable name typically has a letter followed by some binary number. for.e.g. X00, X11, etc. 
 * They key of varsPendingDefinition contains the prefix char and the value contains binary string.*/
string AutoGenerator::GenerateVarDefinitions(map<char, set<string> >& varsPendingDefinition)
{
	int dim = d;
	string declarations("");
	map<char,set<string> >::iterator itArg = varsPendingDefinition.begin();
	for(;itArg!=varsPendingDefinition.end();itArg++)
	{
		set<string>::iterator itOrth = itArg->second.begin();
		for(;itOrth!=itArg->second.end();itOrth++)
		{
			string var = itArg->first + *itOrth;
			ostringstream box;
			box<<"Box "<<var<<"(";
			int orthant = ConvertStringToBinary(itOrth->c_str());//stoi(itOrth->c_str(),NULL,2); 
			vector<string> offsets(d);
			//preparing offsets for topleft coordinates
			for(dim=0;dim<d;dim++)
			{
				ostringstream side;
				side<<"("<<itArg->first<<"->coords["<<dim+d<<"]-"<<itArg->first<<"->coords["<<dim<<"]+1)/2";
				offsets[dim] = (orthant & 1)?side.str():""; 
				orthant >>= 1;
			}
			//setting topleft coordinates
			ostringstream topleft;
			for(dim=0;dim<d;dim++)
			{
				topleft<<itArg->first<<"->coords["<<dim<<"]";
				if(offsets[dim].compare(""))
					topleft<<"+"<<offsets[dim];
				topleft<<",";
			}
			//preparing offsets for bottomright coordinates
			for(dim=0;dim<d;dim++)
			{
				ostringstream side;
				side<<"("<<itArg->first<<"->coords["<<dim+d<<"]-"<<itArg->first<<"->coords["<<dim<<"]+1)/2";
				if(!(offsets[dim].compare("")))
					offsets[dim]="-"+side.str();
				else
					offsets[dim]="";
			}
			//setting bottomright coordinates
			ostringstream bottomright;
			for(dim=0;dim<d;dim++)
			{
				bottomright<<itArg->first<<"->coords["<<d+dim<<"]";
				if(offsets[dim].compare(""))
					bottomright<<offsets[dim];
				if(dim != d-1)
					bottomright<<",";
			}
				
			box<<topleft.str()<<bottomright.str()<<");";

			declarations += "\t"+box.str();
			declarations +="\n";
		}
		
	}
	

	return declarations;
}


string GenerateSerialCodeInFunctionBody(string varDecls, string fnBody)
{
	string serialCode("");
	std::string queryStr1("#ifdef PARALLEL\n\tcilk_spawn\n#endif\n");
	std::string queryStr2("#ifdef PARALLEL\n\tcilk_sync;\n#endif\n");
	int queryStr1Len = queryStr1.size();
	int queryStr2Len = queryStr2.size();
	bool parallelTasksFound=false;
	while(true)
	{
		size_t res1 = fnBody.find(queryStr1,0);
		if(res1==std::string::npos)
			break;
		else
			fnBody.erase(res1,queryStr1Len);
		parallelTasksFound = true;
	}
	
	while(true)
	{
		size_t res2 = fnBody.find(queryStr2,0);
		if(res2==std::string::npos)
			break;
		else
			fnBody.erase(res2,queryStr2Len);
	}

	if(parallelTasksFound)
	{
		serialCode = string("\telse if(callStackDepth > recursionDepth+4)\n\t{\n");
		serialCode += varDecls+fnBody;
		serialCode += string("\n\treturn;\n\t}\n");
	}
	return serialCode;
}


int ConvertStringToBinary(const char* str)
{
	int len=strlen(str);
	int i=len-1, num=0, bit_position=0;
	do
	{
		num += ((1<<bit_position) * (str[i]-'0'));
		bit_position++;
		i--;
	}while(i>=0);
	return num;
}

string AutoGenerator::GenerateParamListMacro(const struct tnode* node)
{
	string paramList("");
	paramList +="#define _paramListFunction"+string(1,node->functionname)+" ";
	vector<char> args = GenerateFunctionArgs(node);
	for(unsigned int i=0;i<args.size();i++)	
		paramList+="Box*, CELL_TYPE*, ";
	paramList+="int\n";
	return paramList;
}

string GenerateCallForUnroll(const struct tnode* parent, const struct tnode* child, map<char, set<string> >& varsUsed, int d)
{
	int K = 1<<d; //K is the number of children/orthants created on a single-level decomposition of the DP table. e.g. K=4 for 2D table. K=8 for 3D etc.
	string ret("");
	char tempCall[3]={0,0,0};
	sprintf(tempCall,"%c_unroll(",child->functionname);
	ret += "\t"+ string(tempCall);

	vector<string>::const_iterator it = child->varsUsedInFunctionCall.begin();
	for(;it!=child->varsUsedInFunctionCall.end();it++)
	{
		char prefix = it->at(0);
		string suffix(it->substr(1));
		ostringstream  childZMortonCode;
		childZMortonCode<<"parentTileID"<<prefix<<"*"<<K<<"+"<<ConvertStringToBinary(it->substr(1).c_str());
		ret += "&"+ *it +", " + childZMortonCode.str()+", ";
		map<char, set<string> >::iterator prefixExists = varsUsed.find(prefix);
		if(prefixExists == varsUsed.end())
		{
			set<string> tmpVars;
			tmpVars.insert(suffix);
			varsUsed.insert(make_pair(prefix,tmpVars));
		}
		else
			prefixExists->second.insert(suffix);
	}
	ret +="callStackDepth+1);";
	return ret;
}

string GenerateCall(const struct tnode* parent, const struct tnode* child, map<char, set<string> >& varsUsed)
{
	string ret("");
	char tempCall[3]={0,0,0};
	sprintf(tempCall,"%c(",child->functionname);
	ret += "\t"+ string(tempCall);

	vector<string>::const_iterator it = child->varsUsedInFunctionCall.begin();
	for(;it!=child->varsUsedInFunctionCall.end();it++)
	{
		char prefix = it->at(0);
		string suffix(it->substr(1));
		ret += "&"+ *it +", " + string(1,prefix) + "Data, ";
		map<char, set<string> >::iterator prefixExists = varsUsed.find(prefix);
		if(prefixExists == varsUsed.end())
		{
			set<string> tmpVars;
			tmpVars.insert(suffix);
			varsUsed.insert(make_pair(prefix,tmpVars));
		}
		else
			prefixExists->second.insert(suffix);
	}
	ret +="callStackDepth+1);";
	return ret;
}

pair<string, string> AutoGenerator::GenerateTopLevelCalls()
{
	string hdrDecl("void Unroll();\n");
	string topLevelCallWrapper("void Unroll()\n{\n");
	map<char, struct tnode*>::iterator it = recFunctions.find('A');
	assert(it!=recFunctions.end());
	string topLevelFunctionCall = GenerateTopLevelFunctionBody(it->second);
	topLevelCallWrapper += topLevelFunctionCall;
	topLevelCallWrapper += "}\n\n";

	pair<string,string> defdFnExecCode = GenerateDelayedFunctionExecution();
	topLevelCallWrapper += defdFnExecCode.second;
	hdrDecl += defdFnExecCode.first;
	return make_pair(hdrDecl,topLevelCallWrapper);
}

string AutoGenerator::GenerateTopLevelFunctionBody(const struct tnode* node)
{
	
	ostringstream topLevelCallArg;
	topLevelCallArg<<"\tunsigned int parentTileIDx=0;\n\tBox *x = new Box(";
	for(unsigned int i=0;i<d;i++)
			topLevelCallArg<<0<<",";
	for(unsigned int i=0;i<d;i++)
	{
		topLevelCallArg<<"inputSize["<<i<<"]";
		if(i!=d-1)
			topLevelCallArg<<",";
		else
			topLevelCallArg<<");\n";
	}
	string topLevelCall = topLevelCallArg.str();

	vector<char> args = GenerateFunctionArgs(node);
	vector<string> argList;
	for(unsigned int i=0;i<args.size();i++)	
	{
		argList.push_back("x");
		argList.push_back("nullData");
	}
	argList.push_back("0");


	topLevelCall += "\t"+GenerateStopCondForUnfolding(node,argList,d);
	topLevelCall += "\t"+string(1, node->functionname)+"_unroll(";
	for(unsigned int i=0;i<argList.size()-1;i+=2)
	{
		topLevelCall += argList[i];
		topLevelCall += ", 0, ";
	}
	topLevelCall +="0);\n\tdelete x;\n";
	return topLevelCall;
}

pair<string, string> AutoGenerator::GenerateDelayedFunctionExecution()
{
	string hdr(""),cpp("");
	hdr += "void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions);\n";
	
	cpp += "void ExecuteFunction(FunctionCall* fn, vector<CELL_TYPE*> dataRegions)\n{\n";
	cpp += "\tswitch(fn->functionName)\n\t{\n";
	//for(unsigned int i=0;i<recFunctions.size();i++)
	map<char, struct tnode*>::iterator it = recFunctions.begin();
	for(;it!=recFunctions.end();it++)
	{
		//string functionName(1,recFunctions[i]->functionname);
		string functionName(1,it->first);
		cpp += "\t\tcase '"+functionName+"':\n";
		cpp += "\t\t\t{\n";
		ostringstream cppStr;
		//vector<char> args = GenerateFunctionArgs(recFunctions[i]);
		vector<char> args = GenerateFunctionArgs(it->second);
		cppStr <<"\t\t\t\tassert(dataRegions.size() == "<<args.size()<<");\n"; 
		cppStr <<"\t\t\t\tDeferredCall<_paramListFunction"<<functionName<<">* df = (reinterpret_cast<DeferredCall<_paramListFunction"<<functionName<<">* >(fn->fnCall));\n";
		for(unsigned int j=0;j<args.size();j++)
			cppStr<<"\t\t\t\tstd::get<"<<(j*2+1)<<">(df->params) = dataRegions["<<j<<"];\n";
		cppStr<<"\t\t\t\t(reinterpret_cast<DeferredCall<_paramListFunction"<<functionName<<">* >(fn->fnCall))->Run();\n";
		cpp += cppStr.str();
		cpp += "\t\t\t}\n\t\t\tbreak;\n";
	}
	cpp +="\t\tdefault: break;\n";	
	cpp += "\t}\n}\n\n";

	return make_pair(hdr,cpp);
}

pair<string, string> AutoGenerator::GenerateResultsCode(RecursionBase* base)
{
	string hdrDecl(""), cppCode("");
	string sig("void PrintResults()");
	hdrDecl +=sig+";\n";
	cppCode += sig+"\n{\n";
	cppCode += base->PrintResults();
	cppCode += "}\n\n";
	return make_pair(hdrDecl,cppCode);	
}

string AutoGenerator::FetchCellFnCallCode()
{
	ostringstream ret("");
	vector<string> argList;
	char startVar = 'i';
	for (int i=0;i<d;i++)
	{
		argList.push_back(string("int ")+string(1,startVar)+string(", "));
		startVar++;
	}
	argList.push_back(string("CELL_TYPE* data"));
	
	string sig("inline CELL_TYPE* GetDPTableCell(");
	for(int i=0;i<argList.size();i++)
		sig+=argList[i];
	sig +=string(")");
	
	ret <<sig<<"\n";
	ret << "{\n";
	ret << "\tCELL_TYPE* cell = data+METADATASPACE;\n";
	ret << "\tint side = data[DIMENSION];\n";

	startVar='i';
	string lastVarName;
	for(int i=0;i<d;i++)
	{
		string varName;
		varName = string(1,startVar)+string("Offset");
		lastVarName = varName;
		ret << "\tint "<<varName <<"="<< startVar <<" - data["<<d-1-i<<"];\n";
		if(i!=d-1)
		{
			ret <<"\tcell += ("<<varName<<"*"<<d-1-i<<"* side);\n"; 
		}
		startVar++;
	}
	ret <<"\tcell += "<<lastVarName<<";\n";
	ret <<"\treturn cell;\n}\n\n"; 
	return ret.str();
}


